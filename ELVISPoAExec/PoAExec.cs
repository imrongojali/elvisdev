﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace ELVISPoAExec
{
    class PoAExec
    {
        public const string USERNAME = "POA_BATCH";
        public const string WS_MESG_SUCCESS = "SUCCESS";
        public const string WS_MESG_ERROR = "ERROR";
        public const string logname = "PoAExec.log";

        public static string _path = GetPath();
        public static string _file = Path.Combine(GetPath(), logname);
        public delegate void Say(string s, params object[] x);
        public static Say say = Talker(0);

        public static void Main(string [] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    int sm = 0;
                    if (int.TryParse(args[0], out sm))
                    {
                        say = Talker(sm);
                    }
                }
                say("Calling Web Service ELVISServicePoA.ProcessAllUnprocessPoA...");

                ServiceReferencePoA.ELVISServicePoASoapClient ws = new ServiceReferencePoA.ELVISServicePoASoapClient();
                
                string mesg = ws.ProcessAllUnprocessPoA(USERNAME);

                say(mesg);
                say("Calling Web Service Ok");
            } catch(Exception ex) {
                say("Calling Web Service Failed...");
                say(ex.Message);
            }
        }

        public static Say Talker(int x)
        {
            switch (x)
            {
                case 0:
                    return cono;

                case 1:
                    return debo;

                case 2:
                    return logo;
                    
                default:
                    return logo;
            }
        }

        public static string GetPath()
        {
            DateTime n = DateTime.Now;
            return Path.Combine(Environment.GetEnvironmentVariable("ALLUSERSPROFILE")
                , ConfigurationManager.AppSettings["LdapDomain"], ConfigurationManager.AppSettings["ApplicationID"], "log"
                , n.Year.ToString()
                , n.Month.ToString()
                , n.Day.ToString());
        }

        public static bool ForceDirectories(string d)
        {
            bool r = false;
            try
            {
                if (!Directory.Exists(d))
                {
                    Directory.CreateDirectory(d);
                }
                r = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return r;
        }

        public static string tick(string s)
        {
            return DateTime.Now.ToString("HH:mm:ss") + "\t" + s;
        }


        public static void logo(string s, params object[] x)
        {
            File.AppendAllText(_file, "\r\n" + tick(string.Format(s, x)));
        }

        public static void debo(string s, params object[] x)
        {
            System.Diagnostics.Debug.WriteLine(string.Format(s, x));
        }

        public static void cono(string s, params object[] x)
        {
            Console.WriteLine(string.Format(s, x));
        }
        
        public static string ExceptionMessageTrace(Exception x)
        {
            return x.Message +
                ((x.InnerException != null) ? "\r\n\t" + x.InnerException.Message : "")
                + "\r\n\t" + x.StackTrace;
        }

    }
}
