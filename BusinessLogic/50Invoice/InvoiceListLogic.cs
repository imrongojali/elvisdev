﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using System.Data;
using System.IO;
using System.Text;

using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;

using Common;
using Common.Data;
using Common.Data._50Invoice;
using Common.Data._20MasterData;
using Common.Function;

using DataLayer.Model;

namespace BusinessLogic._50Invoice
{

    public class InvoiceSearchCriteria
    {
        public string invoiceFrom;
        public string invoiceTo;
        public string No;
        public string vendorCd;
        public List<int> status;
        public string uploadFrom;
        public string uploadTo;
        public string submitFrom;
        public string submitTo;
        public string receivedFrom;
        public string receivedTo;
        public string convertFrom;
        public string convertTo;
        
        private List<string> divs;
        public List<string> Divisions
        {
            get
            {
                return divs;
            }
        }


        public InvoiceSearchCriteria(
            string iDf, string iDt,
            string iNo, string iV,
            List<int> s,
            string divisions,
            string uDf, string uDt,
            string sDf, string sDt,
            string rDf, string rDt,
            string cDf, string cDt)
        {

            string aStatuses = "";
            for (int i = 0; i < s.Count; i++)
            {
                if (i > 0) aStatuses += ",";
                aStatuses += (s[i]).ToString();
            }

            invoiceFrom = iDf;
            invoiceTo = iDt;
            No = iNo;
            vendorCd = iV;
            status = s;
            if (!string.IsNullOrEmpty(divisions))
            {
                divs = divisions.Split(';').ToList();
            }
            else
            {
                divs = new List<string>();
            }
            uploadFrom = uDf;
            uploadTo = uDt;
            submitFrom = sDf;
            submitTo = sDt;
            receivedFrom = rDf;
            receivedTo = rDt;
            convertFrom = cDf;
            convertTo = cDt;
        }
    }


    public enum InvoiceStatus
    {
        IS_NEW = 0,
        IS_UPLOADED = 1,
        IS_SUBMITTED = 2,
        IS_RECEIVED = 3,
        IS_CONVERTED = 4
    }

    public class InvoiceListLogic : LogicBase
    {
        GlobalResourceData _globalResource = new GlobalResourceData();
        List<string> _errs = new List<string>();

        public const string INQ_DATE = "dd/MM/yyyy";
        public const string XLS_DATE = "dd-MM-yyyy";
        public const int MAX_INVOICE_ITEM = 250;
        private LogicFactory logic = LogicFactory.Get();

        public string[] statusName = "New,Upload,Submit,Receive,Convert"
            .Split(',').ToArray();
        public string[] statusDone = "done,uploaded,submitted,received,converted"
            .Split(',').ToArray();

        public List<string> Errors
        {
            get
            {
                return _errs;
            }
        }

        #region Search Inquiry
        public IQueryable<vw_Invoice> Search(InvoiceSearchCriteria c)
        {
            try
            {
                //default ignore all searching criteria
                #region initialization ignore variable

                bool ignoreInvoiceNo = true;
                bool ignoreInvoiceNoFirst = true;
                bool ignoreInvoiceNoLast = true;
                bool ignoreInvoiceNoMiddle = true;
                bool ignoreInvoiceNoFirstLast = true;
                bool ignoreVendorCd = true;
                bool ignoreStatus = true;

                #endregion

             

                #region build non-mandatory searching criteria
                string invoiceNoFirst = "";
                string invoiceNoLast = "";
                if (!string.IsNullOrEmpty(c.No))
                {
                    if (c.No.Contains('*'))
                    {
                        if (c.No.StartsWith("*") && c.No.EndsWith("*"))
                        {
                            c.No = c.No.Replace('*', ' ');
                            ignoreInvoiceNoFirstLast = false;
                        }
                        else
                        {
                            if (c.No.StartsWith("*"))
                            {
                                c.No = c.No.Replace('*', ' ');
                                ignoreInvoiceNoLast = false;
                            }
                            else if (c.No.EndsWith("*"))
                            {
                                c.No = c.No.Replace('*', ' ');
                                ignoreInvoiceNoFirst = false;
                            }
                            else
                            {
                                int idx = c.No.IndexOf('*');
                                c.No = c.No.Trim();
                                invoiceNoFirst = c.No.Substring(0, idx - 1);
                                invoiceNoLast = c.No.Substring(idx + 1);
                                ignoreInvoiceNoMiddle = false;
                            }
                        }
                    }
                    else
                    {
                        ignoreInvoiceNo = false;
                    }
                }

                List<string> vendorCdList = new List<string>();
                if (!string.IsNullOrEmpty(c.vendorCd))
                {
                    ignoreVendorCd = false;
                    c.vendorCd = c.vendorCd.Trim();
                    if (c.vendorCd.EndsWith(";"))
                        c.vendorCd = c.vendorCd.Substring(0, c.vendorCd.Length - 1);
                    string[] vendorCds = c.vendorCd.Replace(" ", "").Split(';');
                    foreach (string vendorCd in vendorCds)
                    {
                        vendorCdList.Add(vendorCd.Trim());
                    }
                }

                ignoreStatus = c.status.Count == 0;

                #endregion

                #region Query

                var q = (from p in db.vw_Invoice
                         // where ((p.INVOICE_DATE >= invoiceDateFrom) && (p.INVOICE_DATE <= invoiceDateTo))

                         //INVOICE NO AREA
                         where (ignoreInvoiceNoFirstLast || p.INVOICE_NO.Contains(c.No.Trim()))
                         where (ignoreInvoiceNoFirst || p.INVOICE_NO.StartsWith(c.No.Trim()))
                         where (ignoreInvoiceNoLast || p.INVOICE_NO.EndsWith(c.No.Trim()))
                         where (ignoreInvoiceNoMiddle || (p.INVOICE_NO.StartsWith(invoiceNoFirst) && p.INVOICE_NO.EndsWith(invoiceNoLast)))
                         where (ignoreInvoiceNo || p.INVOICE_NO.Equals(c.No.Trim()))
                         //END OF INVOICE NO AREA

                         where (ignoreVendorCd || vendorCdList.Contains(p.VENDOR_CD))
                         where (ignoreStatus || c.status.Contains(p.STATUS_CD.Value))

                         select p);

                if (!string.IsNullOrEmpty(c.invoiceFrom) && !string.IsNullOrEmpty(c.invoiceTo))
                {
                    DateTime
                        iFrom = c.invoiceFrom.DateFrom(),
                        iTo = c.invoiceTo.DateTo();

                    q = q.Where(x => x.INVOICE_DATE >= iFrom
                        && x.INVOICE_DATE <= iTo);
                }
                if (!ignoreVendorCd)
                {
                    q = q.Where(p => vendorCdList.Contains(p.VENDOR_CD));
                }
                if (!ignoreStatus)
                {
                    q = q.Where(p => c.status.Contains(p.STATUS_CD.Value));
                }

                if (!string.IsNullOrEmpty(c.uploadFrom) && !string.IsNullOrEmpty(c.uploadTo))
                {
                    DateTime
                        uFrom = c.uploadFrom.DateFrom()
                        , uTo = c.uploadTo.DateTo();

                    q = q.Where(x => x.UPLOAD_DATE.HasValue
                        && x.UPLOAD_DATE >= uFrom
                        && x.UPLOAD_DATE <= uTo);
                }

                if (!string.IsNullOrEmpty(c.submitFrom) && !string.IsNullOrEmpty(c.submitTo))
                {
                    DateTime
                        sFrom = c.submitFrom.DateFrom(),
                        sTo = c.submitTo.DateTo();

                    q = q.Where(x => x.SUBMIT_DATE.HasValue
                        && x.SUBMIT_DATE >= sFrom
                        && x.SUBMIT_DATE <= sTo);
                }

                if (!string.IsNullOrEmpty(c.receivedFrom) && !string.IsNullOrEmpty(c.receivedTo))
                {
                    DateTime
                        rFrom = c.receivedFrom.DateFrom(),
                        rTo = c.receivedTo.DateTo();

                    q = q.Where(x => x.RECEIVE_DATE.HasValue
                        && x.RECEIVE_DATE >= rFrom
                        && x.RECEIVE_DATE <= rTo);
                }

                if (!string.IsNullOrEmpty(c.convertFrom) && !string.IsNullOrEmpty(c.convertTo))
                {
                    DateTime
                        cFrom = c.convertFrom.DateFrom(),
                        cTo = c.convertTo.DateTo();

                    q = q.Where(x => x.CONVERT_DATE.HasValue
                        && x.CONVERT_DATE >= cFrom
                        && x.CONVERT_DATE <= cTo);
                }

                if (c.Divisions.Count > 0)
                {
                    q = q.Where(x => c.Divisions.Contains(x.DIVISION_ID));
                }

                return q;
                #endregion
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return null;
        }


        public List<CodeConstant> getInvoiceStatus()
        {
            return (from s in db.TB_M_SYSTEM
                    where s.SYSTEM_TYPE == "INVOICE_STATUS"
                    select new CodeConstant()
                    {
                        Code = s.SYSTEM_CD,
                        Description = s.SYSTEM_VALUE_TXT
                    }).ToList();
        }

        #endregion


        public string writeReceipt(string imagePath, string outputPath,
                              UserData _UserData,
                              string contentTemp)
        {
            Document doc = new Document(PageSize.A4, 40, 40, 40, 40);
            String _FileName = "Invoice_Receipt_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
            System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
            username = myTI.ToTitleCase(username.ToLower());
            HTMLWorker hw = new HTMLWorker(doc);
            MemoryStream ms = new MemoryStream();
            PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);
            PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

            Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(AppSetting.CompanyLogo)));
            img.ScaleToFit(85, 85);
            StyleSheet sh = new StyleSheet();

            doc.Open();
            PdfContentByte cb = PDFWriter.DirectContent;

            string[] contentPages = contentTemp.Split(new string[] { "<new/>" }, StringSplitOptions.None);
            int pageNum = 0;

            foreach (string content in contentPages)
            {
                if (string.IsNullOrEmpty(content)) continue;
                if (pageNum > 0)
                    doc.NewPage();

                img.SetAbsolutePosition(40, 750);
                doc.Add(img);

                List<IElement> elements = HTMLWorker.ParseToList(new StringReader(content), sh);
                Int16 ind = 0;

                PdfPTable table = elements[ind++] as PdfPTable;


                table.SetWidths(new float[] { 3f, 17f });
                doc.Add(table);

                Paragraph par = elements[ind++] as Paragraph;
                par.SpacingBefore = 5f;
                par.SpacingAfter = 10f;
                doc.Add(par);

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 2f, 4f, 8f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    foreach (var cell in cells)
                    {
                        if (cell != null)
                        {
                            cell.PaddingTop = -3;
                        }
                    }
                }
                doc.Add(table);

                doc.Add(new Paragraph("\n"));

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 3f, 2f, 3f, 2f });

                doc.Add(table);

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 3f, 4f, 3f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    foreach (var c in cells)
                    {
                        if (c != null)
                        {
                            c.PaddingTop = 5f;
                            c.PaddingBottom = 25f;
                        }
                    }
                }
                doc.Add(table);
                pageNum++;
            }
            doc.Close();

            string outfile = Path.Combine(outputPath, _FileName);
            File.WriteAllBytes(outfile, ms.ToArray());
            return _FileName;
        }


        #region Get String Builder

        private string trim0(string a)
        {
            string r = "";
            Int64 d = 0;
            if (Int64.TryParse(a, out d))
            {
                r = Convert.ToString(d);
            }
            return r;
        }

        public string GetReceipt(string _username, string _imagePath, string template, List<InvoiceData> _list)
        {
            string vCode, vName;
            if (_list.Count < 1) return "";

            string html = File.ReadAllText(template);
            _list = _list.OrderBy(x => x.VENDOR_CD)
                .ThenBy(z => z.INVOICE_NO)
                .ToList();

            StringBuilder o = new StringBuilder("");
            int i = 0, j = _list.Count;

            string dn = (from l in _list select l.DIVISION_NAME).FirstOrDefault();

            while (i < j)
            {
                vCode = trim0(_list[i].VENDOR_CD);

                vName = _list[i].VENDOR_NAME;
                Templet t = new Templet(html);
                if (i > 0)
                    o.Append("\r\n<new/>\r\n");
                Filler fh = t.Mark("VENDOR_CODE,VENDOR_NAME,RECEIVE_DATE,FULL_NAME");
                fh.Set(vCode, vName, DateTime.Now.ToString(XLS_DATE), dn + " Staff");
                Filler fd = t.Mark("rows", "INVOICE_NO,CURRENCY_CD,AMOUNT,STATUS");
                while (i < j && vCode.Equals(trim0(_list[i].VENDOR_CD)))
                {
                    var dx = _list[i];
                    fd.Add(
                        dx.INVOICE_NO,
                        dx.CURRENCY_CD,
                        CommonFunction.Eval_Curr(dx.CURRENCY_CD, dx.AMOUNT),
                        " ");
                    i++;
                }
                // newPage 
                o.Append(t.Get());
            }
            // save to file 
            return o.ToString();
        }

        public byte[] Get(string _username, string _imagePath, InvoiceSearchCriteria c)
        {
            List<vw_Invoice> _list = Search(c).ToList();
            ExcelWriter x = new ExcelWriter();

            int rownum = 0;
            int counter = 1;
            int excelLimit = ExcelWriter.xlRowLimit - 10;
            // the first ten rows is used as header
            excelLimit = excelLimit - 10;
            int page = 0;
            bool newPage = true;
            foreach (vw_Invoice dx in _list)
            {
                if (counter >= excelLimit) newPage = true;

                if (newPage)
                {

                    newPage = false;
                    ++page;
                    counter = 1;
                    if (page > 1)
                        x.PutTail();

                    x.PutPage("Invoice" + page);
                    x.PutHeader("Invoice List",
                        "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                        "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                        "Total Record Download : " + _list.Count,

                        "No|Invoice No|Last Status|Vendor Code|Vendor Name|Invoice Date|" +
                        "Total Amount{IDR|USD|JPY|Other}|" +
                        "Date{Upload|Submit|Receive|Convert}|" +
                        "Transaction Type|Description|PV{No|Year}|Assigned Div|" +
                        "Tax {No|Date}|TMMIN Invoice|Remark|Cont Qty{20|40}|Created{By|Date}|Changed{By|Date}",
                        _imagePath);


                }
                x.Put(
                    ++rownum,
                    dx.INVOICE_NO, dx.INVOICE_STATUS, dx.VENDOR_CD, dx.VENDOR_NAME, dx.INVOICE_DATE.DATUM(),
                    dx.IDR.fmt(), dx.USD.fmt(2), dx.JPY.fmt(), dx.Others,
                    dx.UPLOAD_DATE.MINUTE(), dx.SUBMIT_DATE.MINUTE(), dx.RECEIVE_DATE.MINUTE(), dx.CONVERT_DATE.MINUTE(),
                    dx.TRANSACTION_CD + " - " + dx.TRANSACTION_NAME, dx.DESCRIPTION, dx.PV_NO, dx.PV_YEAR, dx.DIVISION_NAME,
                    dx.TAX_NO, dx.INVOICE_TAX_DATE.MINUTE(), dx.TMMIN_INVOICE, dx.REMARK, dx.CONT_QTY_20, dx.CONT_QTY_40,
                    dx.CREATED_BY, dx.CREATED_DT.MINUTE(), dx.CHANGED_BY, dx.CHANGED_DT.MINUTE());
                counter++;
            }
            x.PutTail();
            return x.GetBytes();
        }


        #endregion


        #region Delete
        public bool Delete(List<InvoiceData> listOfInvoice)
        {
            bool _result = false;

            DbTransaction T1 = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var DB = co.db;
                    T1 = DB.Connection.BeginTransaction();

                    foreach (var Invoice in listOfInvoice)
                    {
                        var p = (from j in DB.TB_R_INVOICE_D
                                 where j.INVOICE_NO == Invoice.INVOICE_NO
                                    && j.VENDOR_CD == Invoice.VENDOR_CD
                                    && j.INVOICE_DATE == Invoice.INVOICE_DATE
                                 select j).ToList();
                        var r = (from k in DB.TB_R_INVOICE_AMOUNT
                                 where k.INVOICE_NO == Invoice.INVOICE_NO
                                    && k.VENDOR_CD == Invoice.VENDOR_CD
                                    && k.INVOICE_DATE == Invoice.INVOICE_DATE
                                 select k).ToList();
                        var q = (from i in DB.TB_R_INVOICE_H
                                 where i.INVOICE_NO == Invoice.INVOICE_NO
                                    && i.VENDOR_CD == Invoice.VENDOR_CD
                                    && i.INVOICE_DATE == Invoice.INVOICE_DATE
                                 select i).FirstOrDefault();

                        if (q.STATUS_CD < (int)InvoiceStatus.IS_CONVERTED && q.CONVERT_FLAG == 0)
                        {
                            int idx = 0;

                            if (r.Any())
                            {
                                foreach (var amount in r)
                                {
                                    DB.DeleteObject(r.ElementAtOrDefault(idx));
                                    idx++;
                                }
                            }

                            if (p.Any())
                            {
                                idx = 0;
                                foreach (var detail in p)
                                {
                                    DB.DeleteObject(p.ElementAtOrDefault(idx));
                                    idx++;
                                }
                            }

                            DB.DeleteObject(q);
                            DB.SaveChanges();

                        }

                    }
                    _result = true;
                    T1.Commit();
                }
                catch (Exception Ex)
                {
                    T1.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public bool PreCheckSetStatus(InvoiceStatus status, List<InvoiceData> l)
        {
            bool _result = false;
            _errs.Clear();
            if (l == null)
                return _result;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    List<string>[] ae = new List<string>[5];
                    List<string>[] eno = new List<string>[5];

                    for (int i = 0; i < ae.Length; i++) { ae[i] = new List<string>(); };
                    for (int i = 0; i < eno.Length; i++) { eno[i] = new List<string>(); };
                    LoggingLogic.say("PreCheck", "");

                    var DB = CO.db;
                    foreach (var _i in l)
                    {
                        LoggingLogic.say("PreCheck", "{0} {1} {2}", _i.INVOICE_NO, _i.VENDOR_CD, _i.INVOICE_DATE);
                        var q = (from i in DB.TB_R_INVOICE_H
                                 where i.INVOICE_NO == _i.INVOICE_NO
                                        && i.VENDOR_CD == _i.VENDOR_CD
                                        && i.INVOICE_DATE == _i.INVOICE_DATE
                                 select i);
                        TB_R_INVOICE_H v = null;
                        if (q.Any())
                            v = q.FirstOrDefault();

                        int statusCD = -1;

                        if (v != null)
                        {
                            statusCD = v.STATUS_CD ?? 0;
                            if (statusCD >= (int)status)
                            {
                                // string sname = (statusCD >= 0 && statusCD <= 4) ? statusName[statusCD] : "?";
                                if (statusCD >= 0 && statusCD <= 4)
                                    ae[statusCD].Add(v.INVOICE_NO);
                            }
                            else if (((int)status > 1) && (statusCD < ((int)status - 1)))
                            {
                                eno[(int)status - 1].Add(v.INVOICE_NO);
                            }

                        }
                    }
                    StringBuilder esb = new StringBuilder("");

                    for (int i = 1; i <= 4; i++)
                    {
                        if (ae[i].Count > 0)
                        {
                            esb.AppendFormat("Invoice {0} already {1}. ", CommonFunction.CommaJoin(ae[i]), statusDone[i]);
                        }
                    }

                    for (int i = 2; i <= 4; i++)
                    {
                        if (eno[i].Count > 0)
                        {
                            esb.AppendFormat("Invoice {0} has not been {1}. ", CommonFunction.CommaJoin(eno[i]), statusDone[i]);
                        }
                    }
                    if (esb.Length > 0)
                        _errs.Add(esb.ToString());

                    _result = (_errs.Count < 1);
                }
                catch (Exception Ex)
                {
                    LoggingLogic.err(Ex);
                }
            }
            return _result;
        }

        public bool SetStatus(InvoiceStatus status, List<InvoiceData> l)
        {
            bool _result = false;

            _errs.Clear();
            DbTransaction T1 = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;

                    T1 = DB.Connection.BeginTransaction();

                    foreach (var Invoice in l)
                    {
                        var q = (from i in DB.TB_R_INVOICE_H
                                 where i.INVOICE_NO == Invoice.INVOICE_NO
                                    && i.VENDOR_CD == Invoice.VENDOR_CD
                                    && i.INVOICE_DATE == Invoice.INVOICE_DATE
                                 select i).FirstOrDefault();

                        if (q != null && q.CONVERT_FLAG == 0)
                        {
                            switch (status)
                            {
                                case InvoiceStatus.IS_UPLOADED:
                                    q.UPLOAD_DATE = DateTime.Now;

                                    break;
                                case InvoiceStatus.IS_RECEIVED:
                                    q.RECEIVE_DATE = DateTime.Now;
                                    break;
                                case InvoiceStatus.IS_SUBMITTED:
                                    q.SUBMIT_DATE = DateTime.Now;
                                    break;
                                case InvoiceStatus.IS_CONVERTED:
                                    q.CONVERT_FLAG = 1;
                                    q.CONVERT_DATE = DateTime.Now;
                                    break;
                            }
                            q.STATUS_CD = (int)status;
                            DB.SaveChanges();
                        }
                    }

                    T1.Commit();
                    _result = true;
                }
                catch (Exception Ex)
                {
                    T1.Rollback();
                    LoggingLogic.err(Ex);
                }

            }
            return _result;
        }

        public bool DeleteAttachment(string reffNumber, decimal refSeqNo)
        {
            bool _result = false;

            DbTransaction T1 = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var DB = co.db;
                    T1 = DB.Connection.BeginTransaction();
                    var q = (from i in DB.TB_R_ATTACHMENT
                             where i.REFERENCE_NO == reffNumber && i.REF_SEQ_NO == refSeqNo
                             select i).FirstOrDefault();
                    if (q != null)
                    {
                        DB.DeleteObject(q);
                        DB.SaveChanges();
                        _result = true;
                        T1.Commit();
                    }
                    else
                    {
                        _result = false;
                        T1.Rollback();
                    }
                }
                catch (Exception Ex)
                {
                    T1.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }

            }
            return _result;
        }
        #endregion

        #region Edit

        #endregion

        #region convert to PV
        public List<String> ConvertPV(UserData userData, bool convertPv, List<InvoiceData> _invoiceDataList)
        {
            List<String> resultList = new List<String>();
            //List<String> invList = new List<String>();
            List<ExchangeRate> rates = logic.Look.getExchangeRates();
            DbTransaction TX = null;
            InvoiceData _data = new InvoiceData();


            //var PvNo = _invoiceDataList[0].PV_NO ?? 0;

            int PvNo, PvYear;

            var invoiceYear = DateTime.Now.Year;
            var tempVendorCd = "";
            // int seqNo = this.getDocumentNo(); // reset from 1 for new Converted Invoice PV 

            int seqNo = 1;

            string result = "0";
            string tempOkInv = "";
            string tempConvInv = "";
            string convInv = "";
            string okInv = "";
            int ctr = 0;
            PvYear = DateTime.Now.Year;

            if (convertPv)
            {
                PvNo = 0;
            }
            else
            {
                PvNo = _invoiceDataList[0].PV_NO ?? 0;
            }


            Dictionary<string, decimal> d = new Dictionary<string, decimal>();
            PVFormData pv = null;

            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var DB = co.db;

                    TX = DB.Connection.BeginTransaction();

                    var act = from a in _invoiceDataList
                              where a.INVOICE_DATE != null
                              orderby a.INVOICE_DATE
                              select a.INVOICE_DATE;
                    DateTime aFrom = act.First();
                    DateTime aTo = act.Last();

                    string defaultPaymentMethod = "T";
                    string x = logic.Sys.GetText("INVOICE_CONVERT", "PAYMENT_METHOD");

                    if (x.isEmpty()) defaultPaymentMethod = x.ToString().Trim();
                    PVFormLogic lo = logic.PVForm;
                    foreach (var _invoiceData in _invoiceDataList)
                    {
                        #region get invoice header data

                        var tempInvNo = _invoiceDataList[ctr].INVOICE_NO;
                        tempVendorCd = _invoiceDataList[ctr].VENDOR_CD;
                        var tempInvoiceDate = _invoiceDataList[ctr].INVOICE_DATE;

                        var gridRes = (from p in db.TB_R_INVOICE_H
                                       where p.INVOICE_NO == tempInvNo
                                       where p.VENDOR_CD == tempVendorCd
                                       where p.INVOICE_DATE == tempInvoiceDate
                                       select new
                                       {
                                           p.INVOICE_NO,
                                           p.INVOICE_DATE,
                                           p.VENDOR_CD,
                                           p.DESCRIPTION,
                                           p.PV_NO,
                                           p.PV_YEAR,
                                           p.DIVISION_ID,
                                           p.STATUS_CD,
                                           p.CONVERT_FLAG,
                                           p.TAX_NO,
                                           p.TRANSACTION_CD,
                                           FT_CD =
                                           (from pp in db.TB_M_TRANSACTION_TYPE
                                            where pp.TRANSACTION_CD == p.TRANSACTION_CD
                                            select pp.FT_CD).FirstOrDefault()
                                       }).FirstOrDefault();

                        var data = gridRes;
                        _data.INVOICE_NO = data.INVOICE_NO;
                        _data.INVOICE_DATE = data.INVOICE_DATE;
                        _data.VENDOR_CD = data.VENDOR_CD;
                        _data.DIVISION_ID = data.DIVISION_ID;
                        _data.TRANSACTION_CD = Convert.ToInt32(data.TRANSACTION_CD);
                        _data.TAX_NO = data.TAX_NO;
                        _data.FT_CD = (data.FT_CD != null) ? data.FT_CD.Value : 0;
                        //_data.TAX_CD = data.TAX_CD;
                        _data.CONVERT_FLAG = data.CONVERT_FLAG;
                        #endregion get invoice data
                        if (_data.CONVERT_FLAG == 1)
                        {
                            result = "1";

                            if (!tempConvInv.Equals(_data.INVOICE_NO))
                            {
                                convInv = convInv + _data.INVOICE_NO.ToString() + ", ";
                            }
                            tempConvInv = _data.INVOICE_NO;
                        }
                        else
                        {
                            if (!result.Equals("1"))
                            {
                                #region insert new PV
                                //PVFormHeaderData pvFormHeader = new PVFormHeaderData();
                                //PVFormDetailData pvFormDetail = new PVFormDetailData();

                                string headerWbsNumber = null;
                                bool checkWbs = true;
                                if (convertPv)
                                {
                                    /** create new PV Header data **/
                                    var vendorGroup = (from i in db.vw_Vendor
                                                       where i.VENDOR_CD == _data.VENDOR_CD
                                                       select i.VENDOR_GROUP_CD).FirstOrDefault();
                                    if (pv == null)
                                        pv = new PVFormData()
                                        {
                                            PVNumber = (PvNo <= 0) ? null : (int?)PvNo,
                                            PVYear = invoiceYear,
                                            PVDate = DateTime.Now,
                                            VendorCode = _data.VENDOR_CD,
                                            VendorGroupCode = vendorGroup,
                                            DivisionID = _data.DIVISION_ID,
                                            TransactionCode = _data.TRANSACTION_CD,
                                            UserName = userData.USERNAME,
                                            ActivityDate = aFrom,
                                            ActivityDateTo = aTo,
                                            PaymentMethodCode = defaultPaymentMethod,
                                            StatusCode = 0
                                        };

                                    /** Get Invoice Detail **/
                                    var detRes = (from p in DB.TB_R_INVOICE_D
                                                  where p.INVOICE_NO == tempInvNo
                                                  where p.VENDOR_CD == tempVendorCd
                                                  where p.INVOICE_DATE == tempInvoiceDate
                                                  orderby p.SEQ_NO ascending
                                                  select p).ToList();

                                    if (detRes.Any())
                                    {
                                        foreach (var detData in detRes)
                                        {
                                            string desc = "";
                                            if (_data.TRANSACTION_CD == 1) //if FT_CD == 1
                                            {
                                                var itemTransactionDesc =
                                                    (from i in db.TB_M_TRANSACTION_TYPE
                                                     where i.TRANSACTION_CD == detData.ITEM_TRANSACTION_CD
                                                     select i.TRANSACTION_NAME)
                                                    .FirstOrDefault();
                                                desc = itemTransactionDesc;
                                            }
                                            else
                                            {
                                                desc = detData.ITEM_DESCRIPTION;
                                            }

                                            PVFormDetail de = new PVFormDetail()
                                            {
                                                SequenceNumber = seqNo,
                                                InvoiceNumber = detData.INVOICE_NO,
                                                ItemTransaction = detData.ITEM_TRANSACTION_CD,
                                                Description = desc,
                                                CurrencyCode = detData.CURRENCY_CD,
                                                Amount = detData.AMOUNT,
                                                CostCenterCode = detData.COST_CENTER_CD,
                                                WbsNumber = detData.WBS_NO,
                                                TaxCode = detData.TAX_CD,
                                                InvoiceDate = detData.INVOICE_DATE
                                            };
                                            pv.Details.Add(de);

                                    
                                            if (!string.IsNullOrEmpty(detData.WBS_NO) && string.IsNullOrEmpty(headerWbsNumber))
                                            {
                                                if (headerWbsNumber == null)
                                                {
                                                    headerWbsNumber = detData.WBS_NO;
                                                }
                                                else
                                                {
                                                    if (checkWbs)
                                                    {
                                                        if (!headerWbsNumber.Equals(detData.WBS_NO))
                                                        {
                                                            checkWbs = false;
                                                        }
                                                        headerWbsNumber = detData.WBS_NO;
                                                    }
                                                }
                                            }
                                    
                                            seqNo++;
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(headerWbsNumber))
                                    {
                                    
                                        pv.BudgetNumber = headerWbsNumber;
                                        DB.SaveChanges();
                                    }

                                }
                                #endregion


                            }

                        }
                        ctr++;

                        if (!result.Equals("1") && !String.IsNullOrEmpty(tempOkInv))
                        {
                            /** Get Invoice Amount **/
                            var invAmtRes = (from p in DB.TB_R_INVOICE_AMOUNT
                                             //where invList.Contains(p.INVOICE_NO)
                                             where p.INVOICE_NO == tempOkInv
                                             && p.VENDOR_CD == tempVendorCd
                                             && p.INVOICE_DATE == tempInvoiceDate
                                             group p.TOTAL_AMOUNT by p.CURRENCY_CD into g
                                             select new
                                             {
                                                 CURRENCY_CD = g.Key,
                                                 TOTAL_AMOUNT = g.Sum()
                                             });

                            foreach (var iax in invAmtRes)
                            {
                                string key = iax.CURRENCY_CD ?? "";
                                decimal amt = iax.TOTAL_AMOUNT ?? 0;
                                key = key.Trim();
                                if (key != "")
                                {
                                    if (d.ContainsKey(key))
                                    {
                                        d[key] = d[key] + amt;
                                    }
                                    else
                                    {
                                        d[key] = amt;
                                    }
                                }
                            }
                        }

                    }



                    if (convertPv)
                    {
                        lo.save(pv, userData, false);
                        PvNo = pv.PVNumber ?? 0;
                        foreach (var X in _invoiceDataList)
                        {
                            X.PV_NO = pv.PVNumber;
                            X.PV_YEAR = pv.PVYear;
                        }
                    }
                    foreach (var ix in _invoiceDataList)
                    {
                        if (!result.Equals("1"))
                        {
                            #region update invoice header
                            var _header = (from i in DB.TB_R_INVOICE_H
                                           where i.INVOICE_NO == ix.INVOICE_NO
                                           where i.VENDOR_CD == ix.VENDOR_CD
                                           where i.INVOICE_DATE == ix.INVOICE_DATE
                                           select i).FirstOrDefault();

                            if (convertPv)
                            {
                                _header.PV_NO = PvNo;
                                _header.PV_YEAR = invoiceYear;
                            }
                            _header.CONVERT_FLAG = 1;
                            _header.CONVERT_DATE = DateTime.Now;
                            _header.STATUS_CD = (int)InvoiceStatus.IS_CONVERTED;
                            _header.CHANGED_BY = userData.USERNAME;
                            _header.CHANGED_DT = DateTime.Now;

                            DB.SaveChanges();
                            #endregion
                            result = "2";
                            if (!tempOkInv.Equals(_data.INVOICE_NO))
                            {
                                okInv = okInv + _data.INVOICE_NO.ToString() + ", ";
                                //invList.Add(tempInvNo);
                            }
                            tempOkInv = _data.INVOICE_NO;
                        }

                    }
                    if (result.Equals("2"))
                    {
                        if (convertPv)
                        {

                            #region Invoice Attachment to PV Attachment
                            string referenceNo = "";
                            string newReferenceNo = "";
                            int localCtr = 1;
                            int globalCtr = 1;
                            foreach (var _invoiceData in _invoiceDataList)
                            {
                                referenceNo = _invoiceData.INVOICE_NO + _invoiceData.VENDOR_CD + _invoiceData.INVOICE_DATE.ToString("yyyyMMdd");

                                var attach = (from p in DB.TB_R_ATTACHMENT
                                              where p.REFERENCE_NO == referenceNo
                                              select p).ToList();
                                if (attach.Any())
                                {
                                    int ctrFile = 1;

                                    newReferenceNo = Convert.ToString(PvNo) + Convert.ToString(invoiceYear);
                                    foreach (var invAtt in attach)
                                    {
                                        FtpLogic ftp = new FtpLogic();
                                        string newFileName = String.Concat(localCtr.ToString(), "-", ctrFile.ToString(), "-", invAtt.FILE_NAME);
                                        if (ftp.Rename(invAtt.DIRECTORY, invAtt.FILE_NAME, newFileName))
                                        {
                                            var _tmpAttach = (from q in DB.TB_R_ATTACHMENT
                                                              where q.REFERENCE_NO == invAtt.REFERENCE_NO
                                                              where q.REF_SEQ_NO == invAtt.REF_SEQ_NO
                                                              select q).FirstOrDefault();
                                            _tmpAttach.FILE_NAME = newFileName;
                                            _tmpAttach.CHANGED_BY = userData.USERNAME;
                                            _tmpAttach.CHANGED_DT = DateTime.Now;
                                            DB.SaveChanges();

                                            TB_R_ATTACHMENT newAtt = new TB_R_ATTACHMENT()
                                            {
                                                REFERENCE_NO = newReferenceNo,
                                                REF_SEQ_NO = globalCtr,
                                                FILE_NAME = newFileName,
                                                ATTACH_CD = invAtt.ATTACH_CD,
                                                DESCRIPTION = invAtt.DESCRIPTION,
                                                DIRECTORY = invAtt.DIRECTORY,
                                                CREATED_BY = userData.USERNAME,
                                                CREATED_DT = DateTime.Now
                                            };
                                            DB.AddToTB_R_ATTACHMENT(newAtt);
                                            DB.SaveChanges();
                                            globalCtr++;
                                        }
                                        ctrFile++;
                                    }
                                }
                                localCtr++;
                            }
                            #endregion
                        }

                        //_PVFormListLogic.incrementDocumentNo();
                        TX.Commit();
                    }
                }
                catch (Exception e)
                {
                    LoggingLogic.err(e);

                    if (TX != null)
                        TX.Rollback();

                    result = "0";
                }
            }

            resultList.Add(result);
            resultList.Add(convInv);
            resultList.Add(okInv);
            return resultList;
        }
        #endregion


        public List<InvoiceData> Search(List<InvoiceData> l)
        {
            List<InvoiceData> r = new List<InvoiceData>();
            int i = 0;
            foreach (InvoiceData idata in l)
            {
                var data = (from p in db.TB_R_INVOICE_H
                            where p.INVOICE_DATE == idata.INVOICE_DATE
                            && p.VENDOR_CD == idata.VENDOR_CD
                            && p.INVOICE_NO == idata.INVOICE_NO
                            select p).FirstOrDefault();
                if (data != null)
                {
                    var v = (from inv in db.vw_Invoice
                             where inv.INVOICE_DATE == idata.INVOICE_DATE
                             && inv.VENDOR_CD == idata.VENDOR_CD
                             && inv.INVOICE_NO == idata.INVOICE_NO
                             select inv).FirstOrDefault();

                    string invoiceStatus = (data.STATUS_CD >= 0 && data.STATUS_CD <= 4) ? statusName[data.STATUS_CD ?? 0] : "";

                    r.Add(new InvoiceData()
                            {
                                SEQNO = i++,
                                INVOICE_NO = data.INVOICE_NO,
                                INVOICE_DATE = data.INVOICE_DATE,
                                DESCRIPTION = data.DESCRIPTION,
                                STATUS_CD = data.STATUS_CD,
                                INVOICE_STATUS = invoiceStatus,
                                VENDOR_CD = data.VENDOR_CD,
                                VENDOR_NAME = v.VENDOR_NAME,
                                IDR = v.IDR,
                                JPY = v.JPY,
                                USD = v.USD,
                                SGD = v.SGD,
                                EURO = v.EURO,
                                OTHERS = v.Others,
                                DIVISION_ID = data.DIVISION_ID,
                                DIVISION_NAME = v.DIVISION_NAME,
                                PV_NO = data.PV_NO,
                                PV_YEAR = data.PV_YEAR,
                                TAX_NO = data.TAX_NO,
                                TRANSACTION_CD = data.TRANSACTION_CD,
                                TRANSACTION_NAME = v.TRANSACTION_NAME,

                                INVOICE_TAX_DATE = data.INVOICE_TAX_DATE,
                                CONT_QTY_20 = data.CONT_QTY_20 ?? 0,
                                CONT_QTY_40 = data.CONT_QTY_40 ?? 0,
                                REMARK = data.REMARK,
                                TMMIN_INVOICE = data.TMMIN_INVOICE,

                                UPLOAD_DATE = data.UPLOAD_DATE,
                                SUBMIT_DATE = data.SUBMIT_DATE,
                                RECEIVE_DATE = data.RECEIVE_DATE,
                                CONVERT_DATE = data.CONVERT_DATE,
                                PLANNING_PAYMENT_DATE = v.PLANNING_PAYMENT_DATE,

                                CREATED_BY = data.CREATED_BY,
                                CREATED_DT = data.CREATED_DT,
                                CHANGED_BY = data.CHANGED_BY,
                                CHANGED_DT = data.CHANGED_DT,

                                CONVERT_FLAG = data.CONVERT_FLAG
                            });
                }

            }
            return r;
        }

        //added by Akhmad Nuryanto
        #region Search Inquiry after upload
        public List<InvoiceData> SearchInquryAfterUpload(List<string> _InvoiceList)
        {
            List<InvoiceData> result = new List<InvoiceData>();
            int ctr = 1;
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    #region Query
                    foreach (string k in _InvoiceList)
                    {
                        string[] v = k.Split(':');
                        if (v.Length < 3) continue;
                        DateTime invDate = DateTime.MinValue;

                        invDate = v[2].Date(XLS_DATE);
                        string vendorCode = v[1];
                        string InvoiceNo = v[0];
                        var q = (from p in db.vw_Invoice
                                 where p.INVOICE_DATE == invDate
                                 && p.VENDOR_CD == vendorCode
                                 && p.INVOICE_NO == InvoiceNo
                                 orderby p.CREATED_DT descending
                                 select p);

                        foreach (var data in q)
                            result.Add(
                                new InvoiceData()
                                {
                                    SEQNO = ctr++,
                                    INVOICE_NO = data.INVOICE_NO,
                                    INVOICE_DATE = data.INVOICE_DATE,
                                    DESCRIPTION = data.DESCRIPTION,
                                    STATUS_CD = data.STATUS_CD,
                                    INVOICE_STATUS = data.INVOICE_STATUS,
                                    VENDOR_CD = data.VENDOR_CD,
                                    VENDOR_NAME = data.VENDOR_NAME,
                                    IDR = data.IDR,
                                    JPY = data.JPY,
                                    USD = data.USD,
                                    SGD = data.SGD,
                                    EURO = data.EURO,
                                    OTHERS = data.Others,
                                    DIVISION_ID = data.DIVISION_ID,
                                    DIVISION_NAME = data.DIVISION_NAME,
                                    PV_NO = data.PV_NO,
                                    PV_YEAR = data.PV_YEAR,
                                    TAX_NO = data.TAX_NO,
                                    TRANSACTION_CD = data.TRANSACTION_CD,
                                    TRANSACTION_NAME = data.TRANSACTION_NAME,

                                    INVOICE_TAX_DATE = data.INVOICE_TAX_DATE,
                                    CONT_QTY_20 = data.CONT_QTY_20 ?? 0,
                                    CONT_QTY_40 = data.CONT_QTY_40 ?? 0,
                                    REMARK = data.REMARK,
                                    TMMIN_INVOICE = data.TMMIN_INVOICE,

                                    UPLOAD_DATE = data.UPLOAD_DATE,
                                    SUBMIT_DATE = data.SUBMIT_DATE,
                                    RECEIVE_DATE = data.RECEIVE_DATE,
                                    CONVERT_DATE = data.CONVERT_DATE,

                                    PLANNING_PAYMENT_DATE = data.PLANNING_PAYMENT_DATE,

                                    CREATED_BY = data.CREATED_BY,
                                    CREATED_DT = data.CREATED_DT,
                                    CHANGED_BY = data.CHANGED_BY,
                                    CHANGED_DT = data.CHANGED_DT,

                                    CONVERT_FLAG = data.CONVERT_FLAG
                                });

                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }

            result = (from i in result orderby i.CREATED_DT descending select i).ToList();
            return result;
        }
        #endregion

        public List<InvoiceData> SearchInvoicesByListed(List<InvoiceData> li)
        {
            List<InvoiceData> i_d = new List<InvoiceData>();
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                foreach (var lid in li)
                {
                    var qi = (from vi in db.TB_R_INVOICE_H
                              where vi.VENDOR_CD == lid.VENDOR_CD
                              && vi.INVOICE_DATE == lid.INVOICE_DATE
                              && vi.INVOICE_NO == lid.INVOICE_NO
                              && vi.CONVERT_FLAG != 1
                              select vi
                                  ).FirstOrDefault();
                    if (qi != null)
                    {
                        string divName = (from a in db.vw_Division
                                          where a.DIVISION_ID == qi.DIVISION_ID
                                          select a.DIVISION_NAME).FirstOrDefault();

                        var idet = from d in db.TB_R_INVOICE_AMOUNT
                                   where d.INVOICE_NO == lid.INVOICE_NO
                                   && d.INVOICE_DATE == lid.INVOICE_DATE
                                   && d.VENDOR_CD == lid.VENDOR_CD
                                   select d;

                        string vName = (from vn in db.vw_Vendor
                                        where vn.VENDOR_CD == qi.VENDOR_CD
                                        select vn.VENDOR_NAME).FirstOrDefault();


                        foreach (var id in idet)
                        {
                            string invoiceStatus = (qi.STATUS_CD >= 0 && qi.STATUS_CD <= 4) ?
                                statusName[qi.STATUS_CD ?? 0] : "";


                            i_d.Add(
                                new InvoiceData()
                                {
                                    VENDOR_CD = qi.VENDOR_CD,
                                    VENDOR_NAME = vName,
                                    INVOICE_NO = qi.INVOICE_NO,
                                    INVOICE_DATE = qi.INVOICE_DATE,
                                    DIVISION_ID = qi.DIVISION_ID,
                                    DIVISION_NAME = divName,
                                    TAX_NO = qi.TAX_NO,
                                    INVOICE_TAX_DATE = qi.INVOICE_TAX_DATE,
                                    TMMIN_INVOICE = qi.TMMIN_INVOICE,
                                    REMARK = qi.REMARK,
                                    CONT_QTY_20 = qi.CONT_QTY_20 ?? 0,
                                    CONT_QTY_40 = qi.CONT_QTY_40 ?? 0,

                                    // fill from tb_r_invoice_amount
                                    CURRENCY_CD = id.CURRENCY_CD,

                                    AMOUNT = id.TOTAL_AMOUNT ?? 0,
                                    STATUS_CD = qi.STATUS_CD,
                                    INVOICE_STATUS = invoiceStatus
                                });
                        }
                    }
                }
            }

            return i_d;
        }

        public List<InvoiceData> SearchInvoiceDataByVendor(string vendorCd)
        {
            List<InvoiceData> i_d = new List<InvoiceData>();
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var ih = (from invh in db.vw_Invoice
                          where invh.VENDOR_CD == vendorCd
                          && (invh.STATUS_CD >= (int)InvoiceStatus.IS_UPLOADED)
                          && (invh.CONVERT_FLAG < 1)
                          select invh);
                foreach (var i in ih)
                {
                    var idet = from d in db.TB_R_INVOICE_AMOUNT
                               where d.INVOICE_NO == i.INVOICE_NO
                               && d.INVOICE_DATE == i.INVOICE_DATE
                               && d.VENDOR_CD == i.VENDOR_CD
                               select d;

                    foreach (var id in idet)
                    {

                        i_d.Add(
                            new InvoiceData()
                            {
                                VENDOR_CD = i.VENDOR_CD,
                                VENDOR_NAME = i.VENDOR_NAME,
                                INVOICE_NO = i.INVOICE_NO,
                                INVOICE_DATE = i.INVOICE_DATE,
                                DIVISION_ID = i.DIVISION_ID,
                                TAX_NO = i.TAX_NO,
                                INVOICE_TAX_DATE = i.INVOICE_TAX_DATE,
                                TMMIN_INVOICE = i.TMMIN_INVOICE,
                                REMARK = i.REMARK,
                                CONT_QTY_20 = i.CONT_QTY_20 ?? 0,
                                CONT_QTY_40 = i.CONT_QTY_40 ?? 0,

                                // fill from tb_r_invoice_amount
                                CURRENCY_CD = id.CURRENCY_CD,

                                AMOUNT = id.TOTAL_AMOUNT ?? 0,

                                INVOICE_STATUS = i.INVOICE_STATUS,
                            });

                    }
                }
            }
            return i_d;
        }


        //added by Akhmad Nuryanto
        #region Search Inquiry after upload
        public List<FormAttachment> SearchAttachment(String reffNumber, string downloadHttp)
        {
            List<FormAttachment> result = new List<FormAttachment>();
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    var query = (from t in db.TB_R_ATTACHMENT
                                 join c in db.TB_M_ATTACH_CATEGORY on t.ATTACH_CD equals c.ATTACH_CD
                                 where (t.REFERENCE_NO == reffNumber)
                                 select new
                                 {
                                     ATTACH_CD = t.ATTACH_CD,
                                     DESCRIPTION = t.DESCRIPTION,
                                     FILE_NAME = t.FILE_NAME,
                                     DIRECTORY = t.DIRECTORY,
                                     REFERENCE_NO = t.REFERENCE_NO,
                                     REF_SEQ_NO = t.REF_SEQ_NO,
                                     ATTACH_NAME = c.ATTACH_NAME
                                 }).ToList();

                    if (query.Any())
                    {
                        FormAttachment attachment;
                        foreach (var atc in query)
                        {
                            attachment = new FormAttachment()
                            {
                                CategoryCode = atc.ATTACH_CD,
                                Description = atc.DESCRIPTION,
                                FileName = atc.FILE_NAME,
                                PATH = atc.DIRECTORY,
                                ReferenceNumber = atc.REFERENCE_NO,
                                SequenceNumber = atc.REF_SEQ_NO,
                                CategoryName = atc.ATTACH_NAME,
                                Url = downloadHttp + atc.DIRECTORY + "/" + atc.FILE_NAME
                            };

                            result.Add(attachment);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }
            return result;
        }
        #endregion



        #region Send Email Related
        public List<UserData> GetAdminUserByDivision(string DivCd)
        {
            List<UserData> r = new List<UserData>();
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = from u in db.vw_User
                        where u.DIVISION_ID == DivCd
                        && u.ROLE_ID != null && u.ROLE_ID == "ELVIS_USER_ADMIN"
                        select u;
                
                foreach (var u in q)
                {
                    r.Add(new UserData()
                    {
                        USERNAME = u.USERNAME,
                        REG_NO = u.REG_NO.Dec(0),
                        EMAIL = u.EMAIL,
                        DIV_CD = u.DIVISION_ID.Dec(0),
                        FIRST_NAME = u.FIRST_NAME,
                        LAST_NAME = u.LAST_NAME
                    });
                }
            }
            return r;
        }
        #endregion


        #region Upload
        public bool InvoiceExists(string VendorCd, string InvNo, DateTime InvDate)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var _header = (from i in db.TB_R_INVOICE_H
                               where i.INVOICE_NO == InvNo
                               && i.VENDOR_CD == VendorCd
                               && i.INVOICE_DATE == InvDate
                               select i).FirstOrDefault();
                return (_header != null && _header.CONVERT_FLAG == 1 && _header.STATUS_CD >= (int)InvoiceStatus.IS_RECEIVED);
            }
        }

        public bool Upload(string xlsFilename, string metaFile,
            List<InvoiceData> dd,
            ref string[][] derr,
            UserData ux, string VendorCd, int pid)
        {
            bool Ok = false;
            string me = "InvoiceListLogic.Upload";
            BaseBusinessLogic b = logic.Base;
            VendorCd = VendorCd.Trim().PadLeft(10, '0');

            string VendorNm = (from v in db.vw_Vendor
                               where v.VENDOR_CD == VendorCd
                               select v.VENDOR_NAME).FirstOrDefault();
            _errs.Clear();
            if (!b.isVendorCode(VendorCd))
            {
                _errs.Add(logic.Msg.WriteMessage(pid, "MSTD00051ERR", me, ux.USERNAME
                    , VendorCd
                    , 0
                    , "Vendor"
                    , "VENDOR_CD"));
                return false;
            }

            if (!b.AllowedVendorGroup(VendorCd))
            {
                _errs.Add(logic.Msg.WriteMessage(pid, "MSTD00095ERR", me, ux.USERNAME
                    , VendorCd
                    , 0
                    , "Vendor Group"
                    , "VENDOR_CD"));
                return false;
            }
            CommonExcelUpload ceu;
            try
            {
                if (!System.IO.File.Exists(xlsFilename))
                {
                    _errs.Add("Uploaded File not found");
                }

                string _meta = System.IO.File.ReadAllText(metaFile);
                if (string.IsNullOrEmpty(_meta))
                {
                    _errs.Add("Metadata not found");
                }
                if (_errs.Count > 0) return false;

                ceu = new CommonExcelUpload();
                ceu.LogWriter = logic.Msg.WriteMessage;
                Dictionary<string, object> _data;
                _data = ceu.Read(_meta, xlsFilename);

                if (_data.Keys.Count < 1)
                {
                    _errs.Add("Empty file");
                    return false;
                }

                // validate mandatory & field length 
                Ok = ceu.Check();
                // validate Detail 
                DataTable x = ceu.Rows;
                int ri = 0;

                if (!Ok)
                {
                    foreach (string s in ceu.ReadError)
                    {
                        _errs.Add(s);
                    }

                    if (x.Rows.Count > 0)
                    {
                        derr = new string[x.Rows.Count][];

                        foreach (DataRow r in x.Rows)
                        {
                            derr[ri] = new string[16];
                            for (int i = 0; i < 16; i++)
                            {
                                derr[ri][i] = Convert.ToString(r[i]);
                            }
                            ri++;
                        }
                    }
                    else
                    {
                        derr = new string[1][];
                        derr[0] = new string[16];
                        for (int i = 0; i < 15; i++)
                        {
                            derr[0][i] = "";
                        }
                        derr[0][15] = Common.Function.CommonFunction.CommaJoin(ceu.ReadError, "\r\n");
                    }
                    return false;
                }

                // put 
                if (x != null)
                {
                    List<int> TransactionTypes = b.GetTransactionCodes();
                    List<ItemTransactionData> ItemTransactions = b.GetTransactionItems();
                    List<string> Currencies = b.GetCurrencies();
                    List<string> Divisions = b.GetDivisionIds();
                    List<string> TaxCodes = b.GetTaxCodes();
                    // List<string> RoundCurrency = new List<string>();

                    string[] RoundCurrency = logic.Sys.RoundCurrencies();
                    string roundCurrencies = string.Join(" and ", RoundCurrency, 0, RoundCurrency.Length);

                    derr = new string[x.Rows.Count][];
                    ri = 0;
                    InvoiceData pre = new InvoiceData()
                    {
                        INVOICE_NO = ""
                    };

                    for (int n = 0; n < x.Rows.Count; n++)
                    {
                        DataRow r = x.Rows[n];
                        int errCount = _errs.Count;
                        List<string> lineErr = new List<string>();
                        string sErr = "";

                        DateTime dt = DateTime.Now;

                        DateTime? InvoiceTaxDate = null;

                        if (!Convert.ToString(r[1]).DateOk(XLS_DATE, ref dt))
                        {
                            dt = DateTime.Now;
                        }
                        else
                        {
                            if (dt.Date.CompareTo(DateTime.Now.Date) > 0)
                            {
                                sErr = logic.Msg.WriteMessage(pid, "MSTD00211ERR", me, ux.USERNAME
                                    , "INVOICE_DATE");
                                _errs.Add(sErr);
                                lineErr.Add(sErr);
                            }

                        }

                        DateTime itaxDate = DateTime.Now;
                        string dInvoiceTax = r[5].str();
                        if (!dInvoiceTax.isEmpty() && dInvoiceTax.DateOk(XLS_DATE, ref itaxDate))
                        {

                            int MonthsBetween =
                                ((DateTime.Now.Year * 12) + DateTime.Now.Month)
                                - ((itaxDate.Year * 12) + itaxDate.Month);

                            if (MonthsBetween >= 4)
                            {
                                sErr = logic.Msg.WriteMessage(pid, "MSTD00210ERR", me, ux.USERNAME
                                    , "Invoice Tax Expired"
                                    , " 4 months");
                                _errs.Add(sErr);
                                lineErr.Add(sErr);
                            }
                            if (itaxDate.Date.CompareTo(DateTime.Now.Date) > 0)
                            {
                                sErr = logic.Msg.WriteMessage(pid, "MSTD00211ERR", me, ux.USERNAME
                                    , "INVOICE_TAX_DATE");
                                _errs.Add(sErr);
                                lineErr.Add(sErr);
                            }
                            InvoiceTaxDate = itaxDate;
                        }
                        else
                        {
                            InvoiceTaxDate = null;
                        }

                        InvoiceData d = new InvoiceData()
                        {
                            VENDOR_CD = VendorCd,
                            VENDOR_NAME = VendorNm,
                            INVOICE_NO = r[0].str(),
                            INVOICE_DATE = dt,
                            DIVISION_ID = r[2].str(),
                            DIVISION_NAME = r[3].str(),
                            TAX_NO = r[4].str(),
                            INVOICE_TAX_DATE = InvoiceTaxDate,
                            TMMIN_INVOICE = r[6].str(),
                            REMARK = r[7].str(),
                            CONT_QTY_20 = r[8].Int(),
                            CONT_QTY_40 = r[9].Int(),

                            ITEM_TRANSACTION_CD = r[10].Int(-1),
                            ITEM_DESCRIPTION = r[11].str(),

                            CURRENCY_CD = r[12].str(),
                            AMOUNT = StrTo.str(r[13]).Dec(),
                            TAX_CD = r[14].str(),

                            CREATED_BY = ux.USERNAME,
                            CREATED_DT = DateTime.Now,
                            CHANGED_BY = ux.USERNAME,
                            CHANGED_DT = DateTime.Now
                        };

                        if (!Currencies.Contains(d.CURRENCY_CD))
                        {
                            sErr = logic.Msg.WriteMessage(pid, "MSTD00053ERR", me, ux.USERNAME
                                , "CURRENCY_CODE"
                                , d.CURRENCY_CD
                                , n);
                            _errs.Add(sErr);
                            lineErr.Add(sErr);
                        }

                        if (!TaxCodes.Contains(d.TAX_CD))
                        {
                            sErr = logic.Msg.WriteMessage(pid, "MSTD00053ERR", me, ux.USERNAME
                                , "TAX_CD"
                                , d.TAX_CD
                                , n);
                            _errs.Add(sErr);
                            lineErr.Add(sErr);
                        }

                        if (!string.IsNullOrEmpty(d.TAX_CD) && d.TAX_CD.Equals("V1") && string.IsNullOrEmpty(d.TAX_NO))
                        {
                            sErr = logic.Msg.WriteMessage(pid, "MSTD00096ERR", me, ux.USERNAME);
                            _errs.Add(sErr);
                            lineErr.Add(sErr);
                        }

                        if (RoundCurrency.Contains(d.CURRENCY_CD))
                        {
                            if ((d.AMOUNT - Math.Truncate(d.AMOUNT)) != 0)
                            {
                                sErr = logic.Msg.WriteMessage(pid, "MSTD00102ERR", me, ux.USERNAME, roundCurrencies);
                                _errs.Add(sErr);
                                lineErr.Add(sErr);
                            }
                        }

                        if (InvoiceExists(VendorCd, d.INVOICE_NO, d.INVOICE_DATE))
                        {
                            sErr = logic.Msg.WriteMessage(pid, "MSTD00058ERR", me, ux.USERNAME
                                    , "Invoice No"
                                    , d.INVOICE_NO
                                    , "Vendor Code"
                                    , VendorCd
                                    , "Invoice Date"
                                    , d.INVOICE_DATE);
                            _errs.Add(sErr);
                            lineErr.Add(sErr);
                        }

                        if (pre.INVOICE_NO.Equals(d.INVOICE_NO))
                        {
                            bool similar =
                              pre.INVOICE_DATE.Equals(d.INVOICE_DATE)
                              && pre.INVOICE_TAX_DATE.Equals(d.INVOICE_TAX_DATE)
                              && pre.DIVISION_ID.Equals(d.DIVISION_ID ?? "")
                              && pre.TAX_NO.Equals(d.TAX_NO ?? "")
                              && pre.TMMIN_INVOICE.Equals(d.TMMIN_INVOICE ?? "");
                            if (!similar)
                            {
                                sErr = logic.Msg.WriteMessage(pid, "MSTD00212ERR", me, ux.USERNAME);
                                _errs.Add(sErr);
                                lineErr.Add(sErr);
                            }
                        }
                        else
                        {
                            pre.INVOICE_NO = d.INVOICE_NO;
                            pre.INVOICE_DATE = d.INVOICE_DATE;
                            pre.INVOICE_TAX_DATE = d.INVOICE_TAX_DATE;
                            pre.DIVISION_ID = d.DIVISION_ID ?? "";
                            pre.TAX_NO = d.TAX_NO ?? "";
                            pre.TMMIN_INVOICE = d.TMMIN_INVOICE ?? "";
                        }

                        derr[ri] = new string[16];
                        for (int i = 0; i < 16; i++)
                        {
                            derr[ri][i] = Convert.ToString(r[i]);
                        }

                        if (errCount < _errs.Count)
                        {
                            derr[ri][15] = Common.Function.CommonFunction.CommaJoin(lineErr);
                        }
                        else
                            dd.Add(d);
                        ri++;
                    }
                }
                Ok = (_errs.Count < 1);
            }
            catch (Exception ex)
            {
                Logic.Log.Log("MSTD00019ERR", ex.Message, "UploadInvoice");
                LoggingLogic.err(ex);                
            }
            return Ok;
        }

        public bool SaveUpload(
            int processId, string UserId, decimal userDivision,
            List<InvoiceData> _data, ref List<string> _UploadedInvoiceList)
        {
            /** Modified by FID.Taufika 09/06/2012 **/
            DbTransaction Transaction = null;
            bool Ok = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    List<InvoiceData> tmpList = new List<InvoiceData>();
                    var DB = co.db;


                    var _userDivision = Convert.ToString(userDivision);
                    int ctr = 0;

                    string invoiceNo = "";
                    string vendorCd = "";
                    Transaction = DB.Connection.BeginTransaction();

                    #region Insert to database
                    if (_data.Any())
                    {
                        int seqNo = 1;
                        foreach (var inv in _data)
                        {
                            string description = inv.DESCRIPTION;
                            DateTime invoiceDate = inv.INVOICE_DATE;
                            int itemTransactionCd = inv.ITEM_TRANSACTION_CD;
                            string itemDescription = inv.ITEM_DESCRIPTION;
                            string currencyCode = inv.CURRENCY_CD;
                            decimal amount = inv.AMOUNT;
                            string taxNo = inv.TAX_NO;
                            //string taxCd = _InvoiceData.TAX_CD;
                            int transactionCd = inv.TRANSACTION_CD ?? -1;


                            /** reset counter **/
                            if (!invoiceNo.Equals(inv.INVOICE_NO) || !vendorCd.Equals(inv.VENDOR_CD))
                            {
                                ctr = 0;
                            }

                            invoiceNo = inv.INVOICE_NO;
                            vendorCd = inv.VENDOR_CD;

                            #region checking existance of current invoice data
                            var _header = DB.TB_R_INVOICE_H
                                .Where(i => i.INVOICE_NO == invoiceNo
                                    && i.VENDOR_CD == vendorCd
                                    && i.INVOICE_DATE == invoiceDate)
                                    .FirstOrDefault();
                            bool firstHeader = true;

                            foreach (var _tmp in tmpList)
                            {
                                if (_tmp.INVOICE_NO.Equals(inv.INVOICE_NO) &&
                                    _tmp.VENDOR_CD.Equals(inv.VENDOR_CD) &&
                                    _tmp.INVOICE_DATE.Equals(inv.INVOICE_DATE))
                                {
                                    firstHeader = false;
                                    break;
                                }
                            }
                            if (firstHeader)
                            {
                                var _detail = DB.TB_R_INVOICE_D
                                    .Where(i => i.INVOICE_NO == invoiceNo
                                        && i.VENDOR_CD == vendorCd
                                        && i.INVOICE_DATE == invoiceDate)
                                            .ToList();
                                var _detAmount = DB.TB_R_INVOICE_AMOUNT
                                    .Where(k => k.INVOICE_NO == invoiceNo
                                        && k.VENDOR_CD == vendorCd
                                        && k.INVOICE_DATE == invoiceDate)
                                            .ToList();

                                /** if current invoice data exist **/
                                if (_detail.Any() && ctr == 0)
                                {
                                    /** delete invoice detail and detail amount **/
                                    for (int idx = 0; idx < _detail.Count(); idx++)
                                    {
                                        DB.DeleteObject(_detail.ElementAtOrDefault(idx));
                                        DB.SaveChanges();
                                    }

                                    for (int idx = 0; idx < _detAmount.Count(); idx++)
                                    {
                                        DB.DeleteObject(_detAmount.ElementAtOrDefault(idx));
                                        DB.SaveChanges();
                                    }
                                }

                                seqNo = 1;
                            }
                            #endregion

                            #region insert/update invoice header
                            if (_header != null)
                            {
                                if (ctr == 0)
                                {
                                    #region update
                                    _header.DESCRIPTION = description;
                                    _header.CHANGED_DT = DateTime.Now;
                                    _header.CHANGED_BY = UserId;
                                    _header.DIVISION_ID = inv.DIVISION_ID;
                                    if ((_header.STATUS_CD ?? 0) < (int)InvoiceStatus.IS_UPLOADED) // don't change status when not necessary 
                                    {
                                        _header.STATUS_CD = (int)InvoiceStatus.IS_UPLOADED;
                                    }

                                    _header.TAX_NO = taxNo;
                                    _header.INVOICE_TAX_DATE = inv.INVOICE_TAX_DATE;
                                    _header.TMMIN_INVOICE = inv.TMMIN_INVOICE;
                                    _header.CONT_QTY_20 = inv.CONT_QTY_20;
                                    _header.CONT_QTY_40 = inv.CONT_QTY_40;
                                    _header.REMARK = inv.REMARK;
                                    // _header.TRANSACTION_CD = transactionCd;
                                    _header.UPLOAD_DATE = DateTime.Now;
                                    DB.SaveChanges();
                                    #endregion
                                }
                            }
                            else
                            {
                                #region add new
                                TB_R_INVOICE_H _TB_R_INVOICE_H = new TB_R_INVOICE_H()
                                {
                                    INVOICE_NO = invoiceNo,
                                    INVOICE_DATE = invoiceDate,
                                    VENDOR_CD = vendorCd,
                                    STATUS_CD = (int)InvoiceStatus.IS_UPLOADED,
                                    CREATED_BY = UserId,
                                    DESCRIPTION = description,
                                    CREATED_DT = DateTime.Now,
                                    DIVISION_ID = inv.DIVISION_ID,
                                    TAX_NO = taxNo,
                                    INVOICE_TAX_DATE = inv.INVOICE_TAX_DATE,
                                    TMMIN_INVOICE = inv.TMMIN_INVOICE,
                                    CONT_QTY_20 = inv.CONT_QTY_20,
                                    CONT_QTY_40 = inv.CONT_QTY_40,
                                    REMARK = inv.REMARK,
                                    // TRANSACTION_CD = transactionCd,
                                    UPLOAD_DATE = DateTime.Now,

                                };



                                DB.AddToTB_R_INVOICE_H(_TB_R_INVOICE_H);
                                DB.SaveChanges();
                                #endregion
                            }
                            #endregion

                            tmpList.Add(inv);

                            #region Insert invoice detail
                            if (!amount.Equals(null) && !amount.Equals(0))
                            {
                                TB_R_INVOICE_D _TB_R_INVOICE_D = new TB_R_INVOICE_D()
                                {
                                    SEQ_NO = seqNo,
                                    INVOICE_NO = invoiceNo,
                                    VENDOR_CD = vendorCd,
                                    ITEM_DESCRIPTION = itemDescription,
                                    ITEM_TRANSACTION_CD = itemTransactionCd,
                                    CURRENCY_CD = currencyCode,
                                    AMOUNT = Convert.ToDecimal(amount),
                                    INVOICE_DATE = invoiceDate,
                                    // WBS_NO = wbsNo
                                    CREATED_BY = UserId,
                                    CREATED_DT = DateTime.Now
                                };
                                string taxCd = inv.TAX_CD;
                                if (string.IsNullOrEmpty(taxCd))
                                {
                                    _TB_R_INVOICE_D.TAX_CD = "V0";
                                }
                                else
                                {
                                    _TB_R_INVOICE_D.TAX_CD = taxCd;
                                }

                                DB.AddToTB_R_INVOICE_D(_TB_R_INVOICE_D);
                                DB.SaveChanges();
                                seqNo = seqNo + 1;
                            }
                            #endregion

                            #region tax calculation
                            var newItemTransaction = (from o in DB.TB_M_GL_ACCOUNT_MAPPING
                                                      where (o.TRANSACTION_CD == transactionCd && o.ITEM_TRANSACTION_CD == itemTransactionCd)
                                                      select o).FirstOrDefault();
                            if (newItemTransaction != null)
                            {
                                decimal newAmount = (newItemTransaction.NEW_FORMULA ?? 0) * amount;

                                #region add new record with amount calculated value
                                if (newItemTransaction.NEW_ITEM_TRANSACTION_CD != null && newAmount > 0)
                                {
                                    TB_R_INVOICE_D _TB_R_INVOICE_D = new TB_R_INVOICE_D()
                                    {
                                        SEQ_NO = seqNo,
                                        INVOICE_NO = invoiceNo,
                                        VENDOR_CD = vendorCd,
                                        ITEM_DESCRIPTION = itemDescription,
                                        ITEM_TRANSACTION_CD = (newItemTransaction.NEW_ITEM_TRANSACTION_CD ?? 0),
                                        // WBS_NO = wbsNo,
                                        INVOICE_DATE = invoiceDate
                                    };
                                    string taxCd = inv.TAX_CD;
                                    if (string.IsNullOrEmpty(taxCd))
                                    {
                                        _TB_R_INVOICE_D.TAX_CD = "V0";
                                    }
                                    else
                                    {
                                        _TB_R_INVOICE_D.TAX_CD = taxCd;
                                    }

                                    _TB_R_INVOICE_D.CURRENCY_CD = currencyCode;
                                    _TB_R_INVOICE_D.AMOUNT = newAmount;
                                    _TB_R_INVOICE_D.CREATED_BY = UserId;
                                    _TB_R_INVOICE_D.CREATED_DT = DateTime.Now;
                                    DB.AddToTB_R_INVOICE_D(_TB_R_INVOICE_D);
                                    DB.SaveChanges();
                                    seqNo = seqNo + 1;
                                }
                            }

                                #endregion
                            #endregion
                            ctr++;

                        }
                    #endregion
                    }

                    #region insert detail amount data
                    foreach (var data in _data)
                    {
                        string UploadedInvoiceList = data.INVOICE_NO + ":" + data.VENDOR_CD + ":" + data.INVOICE_DATE.ToString(XLS_DATE);

                        if (!_UploadedInvoiceList.Contains(UploadedInvoiceList))
                            _UploadedInvoiceList.Add(UploadedInvoiceList);

                        DateTime invoiceDate = data.INVOICE_DATE;

                        var _totalAmount = (from t in DB.TB_R_INVOICE_D
                                            group t by
                                                new
                                                {
                                                    t.INVOICE_NO,
                                                    t.VENDOR_CD,
                                                    t.INVOICE_DATE,
                                                    t.CURRENCY_CD
                                                }
                                                into g
                                                where g.Key.INVOICE_NO == data.INVOICE_NO
                                                   && g.Key.VENDOR_CD == data.VENDOR_CD
                                                   && g.Key.INVOICE_DATE == invoiceDate
                                                select new
                                                {
                                                    invoiceNumber = g.Key.INVOICE_NO,
                                                    currencyCode = g.Key.CURRENCY_CD,
                                                    vendorCode = g.Key.VENDOR_CD,
                                                    invoiceDate = g.Key.INVOICE_DATE,
                                                    totalAmount = g.Sum(o => o.AMOUNT)
                                                }
                                            ).ToList();


                        if (_totalAmount != null)
                        {
                            foreach (var j in _totalAmount)
                            {
                                if (!_totalAmount.Equals(0))
                                {
                                    var _amount = DB.TB_R_INVOICE_AMOUNT
                                        .Where(i => i.INVOICE_NO == j.invoiceNumber
                                            && i.VENDOR_CD == j.vendorCode
                                            && i.INVOICE_DATE == j.invoiceDate
                                            && i.CURRENCY_CD == j.currencyCode)
                                            .FirstOrDefault();
                                    if (_amount == null)
                                    {
                                        #region add new
                                        TB_R_INVOICE_AMOUNT _TB_R_INVOICE_AMOUNT = new TB_R_INVOICE_AMOUNT()
                                        {
                                            INVOICE_NO = j.invoiceNumber,
                                            CURRENCY_CD = j.currencyCode,
                                            VENDOR_CD = j.vendorCode,
                                            TOTAL_AMOUNT = j.totalAmount,
                                            INVOICE_DATE = j.invoiceDate,
                                            CREATED_BY = UserId,
                                            CREATED_DATE = DateTime.Now
                                        };
                                        DB.AddToTB_R_INVOICE_AMOUNT(_TB_R_INVOICE_AMOUNT);
                                        DB.SaveChanges();
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    Transaction.Commit();

                    Ok = true;

                }

                catch (Exception ex)
                {
                    Transaction.Rollback();
                    _errs.Add(logic.Msg.WriteMessage(processId, "MSTD00042ERR",
                        "InvoiceListLogic.SaveUpload", "",
                        LoggingLogic.Trace(ex)));
                }
            }
            return Ok;

        }
        #endregion
    }

}
