﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Data;
using DataLayer;

namespace BusinessLogic
{
    public class WorkFlowLogic
    {
        private LogicFactory logic = LogicFactory.Get();

        #region SendCommand --> WithCheckWList
        public bool K2SendCommandWithoutCheckWorklist(string _ReffNo, UserData _UserData, string _Command, Dictionary<string, string> _DataField)
        {
            bool kode = false;
            string SN = "";
            string _ProcID = "";
            try
            {
                logic.k2.Open();
                _ProcID = logic.k2.StartProccessWithID(_Command, _ReffNo, _DataField);
                kode = true;
            }
            catch (Exception Ex)
            {
                LoggingLogic.err(Ex);
                throw;
            }
            finally
            {
                logic.k2.Close();
            }
            return kode;
        }
        #endregion

        #region SendCommand --> WithoutCheckWList
        public bool K2SendCommandWithCheckWorklist(string _ReffNo, UserData _UserData, string _Command, Dictionary<string, string> _DataField)
        {
            bool kode = false;
            string SN = "";
            string _ProcID = "";
            try
            {
                logic.k2.Open();

                var lstAllWorklist = new List<WorklistHelper>();
                var lstOrigWorklist = logic.k2.GetWorklist(_UserData.USERNAME);
                var lstGrantor = logic.PoA.GetGrantorByAttorney(_UserData.USERNAME, false);

                foreach (var kvp in lstOrigWorklist)
                {
                    WorklistHelper wh = new WorklistHelper();

                    wh.Folio = kvp.Key;
                    wh.SN = kvp.Value;
                    lstAllWorklist.Add(wh);
                }
                string folio = _ReffNo;

                if (lstOrigWorklist.Where(i => i.Key == folio)
                        .ToList()
                        .Count != null
                    && lstOrigWorklist.Where(i => i.Key == folio)
                        .ToList()
                        .Count > 0)
                {
                    #region Original work list
                    SN = lstOrigWorklist.Where(i => i.Key == folio).Select(i => i.Value).FirstOrDefault();
                    List<string> ActionList = logic.k2.GetAction(SN, _UserData.USERNAME);
                    if (SN != "" && SN != null)
                    {
                        int ProcID = logic.k2.GetProcID(SN);
                        logic.k2.Revert();
                        logic.k2.EditDatafield(_DataField, ProcID);
                        logic.k2.Impersonate(_UserData.USERNAME);

                        logic.k2.Action(_Command, SN);
                        kode = true;
                    }
                    #endregion
                }
                else
                {
                    #region Worklist dari POA
                    if (lstGrantor != null)
                    {
                        foreach (var item in lstGrantor)
                        {
                            var grantorWorklist = logic.k2.GetWorklist(item.GRANTOR);
                            foreach (var kvp in grantorWorklist)
                            {
                                WorklistHelper wh = new WorklistHelper();

                                wh.Folio = kvp.Key;
                                wh.SN = kvp.Value;
                                wh.UserID = item.GRANTOR;
                                lstAllWorklist.Add(wh);
                            }
                        }
                    }

                    if (lstAllWorklist.Count > 0)
                    {
                        string Grantor = lstAllWorklist.Where(i => i.Folio == folio).Select(i => i.UserID).FirstOrDefault();
                        SN = lstAllWorklist.Where(i => i.Folio == folio).Select(i => i.SN).FirstOrDefault();
                        if (SN != "" && SN != null)
                        {
                            int ProcID = logic.k2.GetProcID(SN);
                            logic.k2.Revert();
                            _DataField["WindowsID"] = Grantor;
                            logic.k2.EditDatafield(_DataField, ProcID);
                            logic.k2.Impersonate(Grantor);

                            logic.k2.Action(_Command, SN);
                            kode = true;
                        }
                    }
                    #endregion
                }
                Heap.Set<List<WorklistData>>("Worklist" + _UserData.USERNAME, null);
            }
            catch (Exception Ex)
            {
                LoggingLogic.err(Ex);
                throw;
            }
            finally
            {
                logic.k2.Close();
            }
            return kode;

        }
        #endregion

        public bool K2SendRedirectCommand(string _ReffNo, UserData _UserData, String byPassUserName, Dictionary<string, string> _DataField)
        {
            bool kode = false;
            string SN = "";
            try
            {
                logic.k2.Open();

                var lstAllWorklist = new List<WorklistHelper>();
                var lstOrigWorklist = logic.k2.GetWorklistBy("user=" + // Common.AppSetting.K2_LABEL + ":" + _UserData.USERNAME 
                        byPassUserName 
                        + ";folio=" + _ReffNo);
                foreach (var kvp in lstOrigWorklist)
                {
                    WorklistHelper wh = new WorklistHelper();

                    wh.Folio = kvp.Folio;
                    wh.SN = kvp.SN;
                    lstAllWorklist.Add(wh);
                }
                string folio = _ReffNo;

                if (lstAllWorklist.Count > 0)
                {
                    SN = lstAllWorklist.Where(i => i.Folio == folio).Select(i => i.SN).FirstOrDefault();
                    if (SN != "" && SN != null)
                    {
                        int ProcID = logic.k2.GetProcID(SN);
                        logic.k2.RedirectItem(Common.AppSetting.K2_LABEL + ":" + _UserData.USERNAME, SN);
                        kode = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                LoggingLogic.err(Ex);
                throw;
            }
            finally
            {
                logic.k2.Close();
            }
            return kode;
        }

        // fid.Hadid 20180611 
        // Common to start Worklist in K2
        public bool K2StartWorklist(K2DataField k2Data, UserData userData)
        {
            ITick its = Ticker.Tick;
            its.In("K2StartWorklist");
            bool postResult = false;
            try
            {
                Dictionary<string, string> submitData = new Dictionary<string, string>();
                submitData.Add("ApplicationID", k2Data.ApplicationID);
                submitData.Add("ApprovalLevel", k2Data.ApprovalLevel);
                submitData.Add("ApproverClass", k2Data.ApproverClass);
                submitData.Add("ApproverCount", k2Data.ApproverCount);
                submitData.Add("CurrentDivCD", k2Data.CurrentDivCD);
                submitData.Add("CurrentPIC", k2Data.CurrentPIC);
                submitData.Add("EmailAddress", k2Data.EmailAddress);
                submitData.Add("Entertain", k2Data.Entertain);
                submitData.Add("K2Host", k2Data.K2Host);
                submitData.Add("LimitClass", k2Data.LimitClass);
                submitData.Add("ModuleCD", k2Data.ModuleCD);
                submitData.Add("NextClass", k2Data.NextClass);
                submitData.Add("PIC", k2Data.PIC);
                submitData.Add("Reff_No", k2Data.Reff_No);
                submitData.Add("RegisterDt", k2Data.RegisterDt);
                submitData.Add("RejectedFinance", k2Data.RejectedFinance);
                submitData.Add("StatusCD", k2Data.StatusCD);
                submitData.Add("StepCD", k2Data.StepCD);
                submitData.Add("VendorCD", k2Data.VendorCD);
                submitData.Add("TotalAmount", k2Data.TotalAmount);

                if (k2Data.isResubmitting)
                {
                    logic.k2.deleteprocess(k2Data.folio);
                }

                its.In("K2SendCommandWithCheckWorklist");

                postResult = logic.WorkFlow.K2SendCommandWithoutCheckWorklist(k2Data.folio, userData, k2Data.Command, submitData);
                
                its.Out();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
            finally
            {
                its.Out();
            }

            return postResult;
        }

        public bool K2DeleteProcess(string folio)
        {
            ITick its = Ticker.Tick;
            bool success = false;
            its.In("K2DeleteProcess");
            try
            {
                logic.k2.deleteprocess(folio);
                success = true;
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
            finally
            {
                its.Out();
            }

            return success;
        }
    }
}