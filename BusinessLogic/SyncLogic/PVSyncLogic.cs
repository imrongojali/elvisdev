﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using BusinessLogic.VoucherForm;

namespace BusinessLogic.SyncLogic
{
    public class PVSyncLogic
    {
        public const int SYNC_PV_DRAFT = 1;
        public const int SYNC_CANCEL_PV = 2;
        public const int SYNC_SUBMIT_PV = 3;

        public LogicFactory logic = LogicFactory.Get();

        public List<PVSyncH> GetUnsyncPVH(int syncAction)
        {
            List<PVSyncH> listPVH = new List<PVSyncH>();

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var hd = db.vw_UnsyncPVH
                        .Where(x => x.SYNC_ACTION == syncAction);

                if (hd.Any())
                {
                    foreach(var data in hd)
                    {
                        var pvH = new PVSyncH();

                        pvH.SyncId = data.SYNC_ID;
                        pvH.PVTowass = data.PV_TOWASS;
                        pvH.PVYear = data.PV_YEAR ?? -1;
                        pvH.PayMethodCode = data.PAY_METHOD_CD;
                        pvH.VendorCode = data.VENDOR_CD;
                        pvH.PVType = data.PV_TYPE_CD;
                        pvH.TransCode = data.TRANSACTION_CD;
                        pvH.BudgetNo = data.BUDGET_NO;
                        pvH.DivCd = data.DIVISION_ID;
                        pvH.PVDate = data.PV_DATE;
                        pvH.TotalAmount = data.TOTAL_AMOUNT;
                        pvH.CreatedBy = data.CREATED_BY;

                        if (syncAction == SYNC_PV_DRAFT)
                        {
                            pvH.details = GetUnsyncPVD(data.SYNC_ID);
                            pvH.attachments = GetUnsyncAtt(data.SYNC_ID); 
                        }

                        listPVH.Add(pvH);
                    }
                }
            }

            return listPVH;
        }

        public List<PVSyncD> GetUnsyncPVD(int syncId)
        {
            List<PVSyncD> listPVD = new List<PVSyncD>();

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var det = db.vw_UnsyncPVD
                        .Where(x => x.SYNC_ID == syncId);

                if (det.Any())
                {
                    foreach (var data in det)
                    {
                        var pvD = new PVSyncD();

                        pvD.SyncId = data.SYNC_ID;
                        pvD.PVTowass = data.PV_TOWASS;
                        pvD.PVYear = data.PV_YEAR ?? -1;
                        pvD.PVNo = data.PV_NO;
                        pvD.SeqNo = data.SEQ_NO;
                        pvD.CostCenter = data.COST_CENTER_CD;
                        pvD.CurrCode = data.CURRENCY_CD;
                        pvD.Description = data.DESCRIPTION;
                        pvD.Amount = data.AMOUNT;
                        pvD.PPNAmount = data.PPN_AMOUNT;
                        pvD.DPPAmount = data.DPP_AMOUNT;
                        pvD.InvoiceNo = data.INVOICE_NO;
                        pvD.InvoiceDate = data.INVOICE_DATE;
                        pvD.TaxNo = data.TAX_NO;
                        pvD.TaxCd = data.TAX_CD;
                        pvD.TaxAssignment = data.TAX_ASSIGNMENT;
                    }
                }
            }

            return listPVD;
        }

        public List<PVSyncAtt> GetUnsyncAtt(int syncId)
        {
            List<PVSyncAtt> listAtt = new List<PVSyncAtt>();

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var det = db.vw_UnsyncAtt
                        .Where(x => x.SYNC_ID == syncId);

                if (det.Any())
                {
                    foreach (var data in det)
                    {
                        var att = new PVSyncAtt();

                        att.SyncId = data.SYNC_ID;
                        att.PVTowass = data.PV_TOWASS;
                        att.PVYear = data.PV_YEAR ?? -1;
                        att.PVNo = data.PV_NO;
                        att.SeqNo = data.SEQ_NO;
                        att.FileName = data.FILE_NAME;
                    }
                }
            }

            return listAtt;
        }

        public List<PVFormData> GetUnsycPVFormData(int syncAction, int pid)
        {
            FormPersistence fp = new FormPersistence();
            List<PVFormData> listPV = new List<PVFormData>();

            var unsyncPV = GetUnsyncPVH(syncAction);

            foreach (var data in unsyncPV)
            {
                var pv = new PVFormData();

                pv.BookingNo = data.PVTowass;
                pv.PVNumber = 0;
                pv.PVYear = data.PVYear;
                pv.PaymentMethodCode = data.PayMethodCode;
                pv.VendorCode = data.VendorCode;
                pv.PVTypeCode = data.PVType;
                pv.TransactionCode = data.TransCode;
                pv.DivisionID = data.DivCd;
                pv.PVDate = data.PVDate;
                pv.UserName = data.CreatedBy;

                pv._TransactionType = fp.GetTransactionType(pv.TransactionCode ?? 0);
                if (pv.Budgeted)
                {
                    pv.BudgetNumber = data.BudgetNo;
                }

                pv.isSubmit = false;
                pv.FinanceAccessEnabled = false;
                pv.StatusCode = 0;

                if (syncAction == SYNC_PV_DRAFT && data.details.Count > 0)
                {
                    List<PVFormDetail> listDet = new List<PVFormDetail>();

                    foreach (var det in data.details)
                    {
                        var pvDet = new PVFormDetail();

                        pvDet.SequenceNumber = det.SeqNo;
                        pvDet.DisplaySequenceNumber = det.SeqNo + 1;
                        pvDet.CostCenterCode = det.CostCenter;
                        pvDet.CurrencyCode = det.CurrCode;
                        pvDet.Description = det.Description;
                        pvDet.Amount = det.Amount ?? 0;
                        pvDet.DppAmount = det.DPPAmount ?? 0;
                        pvDet.InvoiceNumber = det.InvoiceNo;
                        pvDet.InvoiceDate = det.InvoiceDate;
                        pvDet.TaxNumber = det.TaxNo;
                        pvDet.TaxCode = det.TaxCd;
                        pvDet.TaxAssignment = det.TaxAssignment;

                        listDet.Add(pvDet);
                    }

                    pv.Details = listDet;
                }

                if (syncAction == SYNC_PV_DRAFT && data.attachments.Count > 0)
                {
                    List<FormAttachment> lstAttachment = pv.AttachmentTable.Attachments;
                    FormAttachment attachment;

                    foreach (var att in data.attachments)
                    {
                        string reffNo = "T" + pid.ToString();
                        string dir = att.PVYear + "/PV/" + reffNo;

                        attachment = new FormAttachment()
                        {
                            Blank = false,
                            CategoryCode = "4",                 // GENERAL ATTACHMENT
                            Description = "",
                            FileName = att.FileName,
                            PATH = dir,
                            ReferenceNumber = reffNo,
                            SequenceNumber = att.SeqNo,
                        };

                        lstAttachment.Add(attachment);
                    }
                }

                pv.UpdateGlAccount(logic.PVForm);

                listPV.Add(pv);
            }

            return listPV;
        }
    }
}
