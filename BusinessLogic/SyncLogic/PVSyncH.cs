﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.SyncLogic
{
    [Serializable]
    public class PVSyncH
    {
        public int SyncId { get; set; }
        public string PVTowass { get; set; }
        public int PVYear { get; set; }
        public int? PVNo { get; set; }
        public string PayMethodCode { get; set; }
        public string VendorCode { get; set; }
        public string VendorNm { get; set; }
        public int? PVType { get; set; }
        public int? TransCode { get; set; }
        public string BudgetNo { get; set; }
        public string DivCd { get; set; }
        public DateTime? PVDate { get; set; }
        public Decimal? TotalAmount { get; set; }
        public int? NeedCompare { get; set; }
        public int SyncStatus { get; set; }
        public int? SyncAction { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDt { get; set; }
        public List<PVSyncD> details { get; set; }
        public List<PVSyncAtt> attachments { get; set; }
        public string PVCover { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public int? ResultSts { get; set; }
        public string Message { get; set; }
    }
}
