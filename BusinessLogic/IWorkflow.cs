﻿using System;
using System.Collections.Generic;
using Common.Data;
namespace K2.Helper
{
    public interface IWorkflow
    {
        void Open();
        void Close();
        void Impersonate(String User);
        void Revert();
        int GetProcID(String SN);
        Dictionary<String, String> GetDatafield(List<String> Keys, int procid); // 
        void EditDatafield(Dictionary<String, String> datafield, int procid);
        void EditDatafield(Dictionary<String, object> datafield, int procid); // 
        void deleteprocess(string folio);
        void deleteprocess(List<string> folioLIst);
        void deleteprocess(int ProcID); //
        string StartProccessWithID(String Process, String folio, Dictionary<String, String> datafield);
        string StartProccessWithID(String Process, String folio, Dictionary<String, object> datafield); // 
        Dictionary<string, string> GetManagedWorklist(string user); // 
        List<WorklistHelper> GetWorklistComplete(string user);
        List<WorklistHelper> GetAllWorklistComplete(string user);
        List<WorklistHelper> GetWorklistFiltered(string user, string key, string value);
        List<WorklistHelper> GetWorklistBy(string aCriteria);
        List<WorklistHelper> GetWorklistCompleteByProcess(string user, string processFullName);
        Dictionary<string, string> GetWorklist(string user);
        Dictionary<string, string> GetWorklistByCriteria(string user, string property, string propertyvalue); //

        void Action(String action, String sn);
        void Action(String action, String sn, String manageduser, String shareduser); // 
        List<String> GetAction(String sn); // 
        List<String> GetAction(String sn, string UserID);
        void RedirectItem(String User, String sn);
        void SleepItem(Boolean sleep, String sn, int time);
        void FinishItem(String sn);
        void SetUserstatus(String status); // 
        String GetUserstatus(String User);
        List<string> Redirect(List<string> works, string fromUser, string toUser);
        void ShareWorklist(DateTime Start, String DestinationUser, int userStatus);

    }
}
