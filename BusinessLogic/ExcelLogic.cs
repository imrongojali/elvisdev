﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Common;

namespace BusinessLogic
{
    public class ExcelLogic
    {
        #region initializing
        DataTable _dt = null;
        OleDbConnection _oleDBConnection = null;
        OleDbCommand _oleDBCommand = null;
        OleDbDataAdapter _oleDBDataAdapter = null;
        #endregion

        public DataTable RetriveData(string SheetName, string ConnString, int ProcessId)
        {
            try
            {              
  
                if (_oleDBConnection.State == ConnectionState.Closed)
                {
                    _oleDBConnection.Open();
                }
                string _query = "SELECT * FROM [" + SheetName + "$]";
                _oleDBCommand = new OleDbCommand(_query, _oleDBConnection);
                _oleDBDataAdapter = new OleDbDataAdapter(_oleDBCommand);
                _dt = new DataTable();
                _oleDBDataAdapter.Fill(_dt);
                _oleDBConnection.Close();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw ;
            }
            return _dt;
        }

        #region Get Query Table 
        public DataTable GetTable(string strTableName, string _strSheetName, string _strSheetRange)
        {
            try
            {
                //Open and query
                if (_oleDBConnection.State == ConnectionState.Closed)
                {
                    _oleDBConnection.Open();
                }
                if (SetSheetQuerySelect(_strSheetName,_strSheetRange) == false) return null;

                //Fill table
                _dt = new DataTable();
                OleDbDataAdapter oleAdapter = new OleDbDataAdapter();
                oleAdapter.SelectCommand = _oleDBCommand;
                DataTable dt = new DataTable(strTableName);
                oleAdapter.FillSchema(dt, SchemaType.Source);
                oleAdapter.Fill(dt);
                
                if (_strSheetRange.IndexOf(":") > 0)
                {
                    string FirstCol = _strSheetRange.Substring(0, _strSheetRange.IndexOf(":") - 1);
                    int intCol = ColNumber(FirstCol);
                    for (int intI = 0; intI < dt.Columns.Count; intI++)
                    {
                        dt.Columns[intI].Caption = ColName(intCol + intI);
                    }
                }
                
                //SetPrimaryKey(dt);
                //Cannot delete rows in Excel workbook
                dt.DefaultView.AllowDelete = false;

                //Clean up
                _oleDBCommand.Dispose();
                _oleDBCommand = null;
                oleAdapter.Dispose();
                oleAdapter = null;
                if (_oleDBConnection.State == ConnectionState.Open)
                {
                    _oleDBConnection.Close();
                }
                return dt;

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw ;
            }
        }

        private bool SetSheetQuerySelect(string _strSheetName, string _strSheetRange)
        {
            try
            {
                if (_oleDBConnection == null)
                {
                    throw new Exception("Connection is unassigned or closed.");
                }

                if (_strSheetName.Length == 0)
                    throw new Exception("Sheetname was not assigned.");

                _oleDBCommand = new OleDbCommand(
                    @"SELECT * FROM ["
                    + _strSheetName
                    + "$" + _strSheetRange
                    + "]", _oleDBConnection);

                return true;
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw ;
            }


        }

        public String[] GetExcelSheetNames()
        {

            System.Data.DataTable dt = null;

            try
            {
                if (_oleDBConnection.State == ConnectionState.Closed)
                {
                    _oleDBConnection.Open();
                }
                // Get the data table containing the schema
                dt = _oleDBConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if (dt == null)
                {
                    return null;
                }

                String[] excelSheets = new String[dt.Rows.Count];
                int i = 0;

                // Add the sheet name to the string array.
                foreach (DataRow row in dt.Rows)
                {
                    string strSheetTableName = row["TABLE_NAME"].ToString();
                    excelSheets[i] = strSheetTableName.Substring(0, strSheetTableName.Length - 1);
                    i++;
                }


                return excelSheets;
            }
            catch (Exception Ex)
            {
                LoggingLogic.err(Ex);
                throw ;
            }
            finally
            {
                // Clean up.
                if (_oleDBConnection.State == ConnectionState.Open)
                {
                    _oleDBConnection.Close();
                }
                if (dt != null)
                {
                    dt.Dispose();
                    dt = null;
                }
            }
        }

        public int ColNumber(string strCol)
        {
            strCol = strCol.ToUpper();
            int intColNumber = 0;
            if (strCol.Length > 1)
            {
                intColNumber = Convert.ToInt16(Convert.ToByte(strCol[1]) - 65);
                intColNumber += Convert.ToInt16(Convert.ToByte(strCol[1]) - 64) * 26;
            }
            else
                intColNumber = Convert.ToInt16(Convert.ToByte(strCol[0]) - 65);
            return intColNumber;
        }

        public string ColName(int intCol)
        {
            string sColName = "";
            if (intCol < 26)
                sColName = Convert.ToString(Convert.ToChar((Convert.ToByte((char)'A') + intCol)));
            else
            {
                int intFirst = ((int)intCol / 26);
                int intSecond = ((int)intCol % 26);
                sColName = Convert.ToString(Convert.ToByte((char)'A') + intFirst);
                sColName += Convert.ToString(Convert.ToByte((char)'A') + intSecond);
            }
            return sColName;
        }


        #endregion

        #region is excel format
        public bool IsExcelFormat(string strFileType)
        {
            if (strFileType.Trim() == ".xls")
            {
                return true;
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region get connection string
        public string GetConnectionString(string strFileType, string strNewPath)
        {
            string _connString = null;
            if (strFileType.Trim() == ".xls")
            {
                _connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strNewPath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                _connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strNewPath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }
            _oleDBConnection = new OleDbConnection(_connString);
            return _connString;
        }
        #endregion
    }
}
