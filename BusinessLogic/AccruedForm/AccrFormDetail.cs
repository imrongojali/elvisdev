﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Common.Data;

namespace BusinessLogic.AccruedForm
{
    [Serializable]
    public class AccrFormDetail
    {   
        public int SequenceNumber { get; set; }
        public string BookingNo { get; set; }
        public string WbsNumber { get; set; }
        public string WbsDesc { get; set; }
        public string SuspenseNo { get; set; }
        public int SuspenseNoOld { get; set; }
        public int? SuspenseNoOldNull { get; set; }
        public string SuspenseNoYear { get; set; }
        public string PVType { get; set; }
        public string CostCenterCode { get; set; } 
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountIdr { get; set; }
        public decimal? SapAmtAvailable { get; set; }
        public decimal? SapAmtRemaining { get; set; }
        public string AccruedNo { set; get; }
        public string ActivityDescription { set; get; }
        public int? PVTypeCd { get; set; }
        public string DivisionId { set; get; }
        public string DivisionName { set; get; }
        public string DivisionFulName { set; get; }
        public string ChangedBy { set; get; }
        public DateTime? ChangedDt { set; get; }
        public string CreatedBy { set; get; }
        public DateTime? CreatedDt { set; get; }
        public int? BudgetYear { set; get; }

        public string CostCenterName { get; set; }
        public string Description { get; set; }
        public string StandardDescriptionWording { get; set; }
        public string TaxCode { get; set; }
        public string TaxNumber { get; set; }
        public int? ItemTransaction { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; } 
        public string GlAccount { get; set; }
        public string FromCurr { get; set; }
        public int? FromSeq { get; set; }
        public int? ItemNo { get; set; }
        public decimal DppAmount { get; set; }
        public string TaxAssignment { get; set; }
        public string AccInfAssignment { get; set; }
        public DateTime? TaxDate { get; set; }

        public void Copy(AccrFormDetail d)
        {
            SequenceNumber = d.SequenceNumber;
            FromCurr = d.FromCurr;
            CostCenterCode = d.CostCenterCode;
            CostCenterName = d.CostCenterName;
            Description = d.Description;
            StandardDescriptionWording = d.StandardDescriptionWording;
            CurrencyCode = d.CurrencyCode;
            Amount = d.Amount;
            TaxCode = d.TaxCode;
            TaxNumber = d.TaxNumber;
            ItemTransaction = d.ItemTransaction;
            WbsNumber = d.WbsNumber;
            InvoiceDate = d.InvoiceDate;
            InvoiceNumber = d.InvoiceNumber;
            GlAccount = d.GlAccount;
            FromSeq = d.FromSeq;
            ItemNo = d.ItemNo;
            DppAmount = d.DppAmount;
            TaxAssignment = d.TaxAssignment;
            AccInfAssignment = d.AccInfAssignment;
            TaxDate = d.TaxDate;

            Persisted = d.Persisted;
            ExrateSuspense = d.ExrateSuspense;
            DeletionControl = d.DeletionControl;
            DisplaySequenceNumber = d.DisplaySequenceNumber;
        }


        // additional property for inner control 
        public bool Persisted { get; set; }
        public string DeletionControl { get; set; }
        public decimal? ExrateSuspense { get; set; }

        ///  additional property for display
        
        public int DisplaySequenceNumber { get; set; }
        public string AmountDisplay
        {
            get
            {
                return Common.Function.CommonFunction.Eval_Curr(CurrencyCode, Amount);
            }
        }
        public string InvoiceNumberUrl { get; set; }
        public static int RuleOrder(AccrFormDetail x, AccrFormDetail y)
        {
            //ordered by WBS No, PV Type, Activity

            string xWbsNo = (x.WbsNumber ?? "");
            string yWbsNo = (y.WbsNumber ?? "");
            if (!xWbsNo.ToLower().Equals(yWbsNo.ToLower()))
                return xWbsNo.ToLower().CompareTo(yWbsNo.ToLower());

            int xPVType = x.PVTypeCd ?? 0;
            int yPVType = y.PVTypeCd ?? 0;
            if (xPVType != yPVType)
                return xPVType.CompareTo(yPVType);

            string xAct = (x.ActivityDescription ?? "");
            string yAct = (y.ActivityDescription ?? "");
            return xAct.ToLower().CompareTo(yAct.ToLower());
        }
    }
}