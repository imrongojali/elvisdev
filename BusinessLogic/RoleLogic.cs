﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common;
using DataLayer.Model;
using System.Data;
using System.Data.EntityClient;
using Dapper; 

namespace BusinessLogic
{
    public class RoleLogic: LogicBase
    {
        private List<RoleAcessData> _ObjectList;
        private List<string> _objects;

        public RoleLogic()
        {
            _ObjectList = new List<RoleAcessData>();
            _objects = new List<string>();
        }
        public RoleLogic(string username, string screenID)
        {
            _ObjectList = new List<RoleAcessData>();
            _objects = new List<string>();
            screenID = screenID.Trim().ToUpper();
            _ObjectList = hQu<RoleAcessData>("GetRoleAccessDataByUserNameScreen", username, screenID).ToList();
            _objects = _ObjectList.Select(a => a.OBJECT_ID.ToUpper()).ToList();
        }

        public bool isAllowedAccess(string _ObjectID)
        {
            string id = _ObjectID.ToUpper();
            bool allowed =_objects.Contains(id);
            Ticker.Spin("isAllowedAccess {0} : {1}", _ObjectID, allowed);
            return allowed;
        }

        public UserRoleHeader getUserRoleHeader(string username)
        {
            return hQu<UserRoleHeader>("GetUserRoleHeader", username).FirstOrDefault();
        }

        public List<UserRoleHeader> getUsersByRole(string roleid)
        {
            return hQu<UserRoleHeader>("GetUsersByRole", roleid).ToList();
        }

        public List<UserRoleHeader> getUserWorklist(string reffno)
        {
            return Qu<UserRoleHeader>("GetUserWorklist", reffno).ToList();
        }

        public List<RoleAcessData> getRoleAcessDataPerScreen(string userID, string screenID, ref ErrorData Err)
        {
            return hQu<RoleAcessData>("GetRoleAccessDataByUserNameScreen", userID, screenID).ToList();
        }
       
        public bool isAllowedAccess(string objectID, ref ErrorData Err)
        {
            bool kode = false;
            try
            {
                if (objectID != null) objectID = objectID.ToUpper();
                bool allowed = _objects.Contains(objectID);
                Ticker.Spin("isAllowedAccess {0} : {1}", objectID, allowed);
                return allowed;
            }
            catch (Exception ex)
            {
                Err = new ErrorData();
                Err.ErrMsg = ex.Message;
                Err.ErrID = 2;
            }
            return kode;
        }

        public List<RoleData> getUserRole(string UserID, ref ErrorData Err)
        {
            return hQu<RoleData>("GetUserRoleData", UserID).ToList();
        }
    }
    public class MenuLogic: LogicBase
    {
        private Stack<string> sx = new Stack<string>();

        public List<ScreenData> GetMenu(string userId)
        {
            List<ScreenData> m = new List<ScreenData>();
            
            var q = hQu<ScreenData>("Menu", AppSetting.ApplicationID.Trim().ToUpper()
                    , userId).ToList();
            
            if (q != null && q.Count > 0)
            {
                List<string> authorizedPVFormCreator; // = new List<string>();

                LogicFactory logic = LogicFactory.Get();
                authorizedPVFormCreator = logic.Sys.GetArray("PvForm", "AuthorizedCreatorRole").ToList();
                if (authorizedPVFormCreator == null || authorizedPVFormCreator.Count < 1)
                {
                    authorizedPVFormCreator = new List<string>();
                    authorizedPVFormCreator.Add("ELVIS_ADMIN");
                    authorizedPVFormCreator.Add("ELVIS_USER_ADMIN");
                    authorizedPVFormCreator.Add("ELVIS_PV_MAKER_ONLY");
                }

                foreach (ScreenData d in q)
                {
                    if (d.SCREEN_ID.ToLower().Contains("pvform")
                        || d.SCREEN_ID.ToLower().Contains("rvform"))
                    {
                        if (!authorizedPVFormCreator.Contains(d.ROLE_ID))
                        {
                            continue;
                        }
                    }

                    if (!m.Where(x => x.MENU_ID == d.MENU_ID).Any())
                    {
                        m.Add(d);
                    }
                }
            }
            sx.Clear();
            List<ScreenData> a = new List<ScreenData>();
            // for (int i = 0; i < m.Count ; i++)
            foreach(var me in m) 
            {
                if (me.PARRENT_ID.Equals("0000"))
                {
                    a.Add(me);
                }
                else
                {
                    ScreenData p = GetByMenuId(a, me.PARRENT_ID);
                    if (p != null)
                    {
                        if (p.ChildList == null)
                        {
                            p.ChildList = new List<ScreenData>();
                        }
                        p.ChildList.Add(me);
                    }
                }
            }
            return a;
        }

        private ScreenData GetByMenuId(List<ScreenData> s, string id)
        {
            ScreenData p = s.Where(x => x.MENU_ID == id).FirstOrDefault();
            if (p == null)
            {
                
                for (int i = 0; i < s.Count; i++)
                {
                    if (s[i].ChildList != null && s[i].ChildList.Count > 0)
                    {
                        if (sx.Count > 0 && sx.Contains(s[i].MENU_ID)) /// Circular menu detected skip this sub
                            continue;

                        sx.Push(s[i].MENU_ID);
                        
                        p = GetByMenuId(s[i].ChildList, id);

                        sx.Pop();
                    }

                    if (p != null)
                        break;
                }
            }
            return p;
        }
    }
}
