﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class DataFieldHeader
    {
        public string CATEGORY_CODE { get; set; }
        public Int32 TRANSACTION_CD { get; set; }
        public string REFERENCE_NO { get; set; }
        public Int32 THN_PAJAK { get; set; }
        public string TMP_PENANDATANGAN { get; set; }
        public DateTime TGL_PENANDATANGAN { get; set; }
        public string NAMA_PENANDATANGAN { get; set; }
        public string JABATAN_PENANDATANGAN { get; set; }

    }
}
