﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    public class RVFormData
    {
        private List<RVFormDetail> lstDetail;
        private int rvNumber;
        private int rvYear;
        private int statusCode;
        private string payMethodCode;
        private int vendorGroupCode;
        private string vendorCode;
        private int rvTypeCode;
        private int transactionCode;
        private int suspenseNumber;
        private string budgetNumber;
        private DateTime activityDate;
        private string divisionId;
        private DateTime rvDate;
        private string taxCode;
        private string taxInvoiceNumber;
        private DateTime postingDate;
        //private DateTime planningPaymentDate;
        //private int bankType;
        private bool taxCalculated;

        public RVFormData()
        {
            lstDetail = new List<RVFormDetail>();
            this.rvNumber = int.MinValue;
            this.rvYear = 2012;
            this.statusCode = int.MinValue;
            this.payMethodCode = "A";
            this.vendorGroupCode = int.MinValue;
            this.vendorCode = "AAA";
            this.rvTypeCode = int.MinValue;
            this.transactionCode = int.MinValue;
            this.suspenseNumber = int.MinValue;
            this.budgetNumber = "AAA";
            this.activityDate = DateTime.Now;
            this.divisionId = "AAA";
            this.rvDate = DateTime.Now;
            this.taxCode = null;
            this.taxInvoiceNumber = null;
            this.taxCalculated = false;
        }


        public bool IsTaxCalculated
        {
            get
            {
                return taxCalculated;
            }
            set
            {
                taxCalculated = value;
            }
        }


        public DateTime PostingDate
        {
            get
            {
                return postingDate;
            }

            set
            {
                postingDate = value;
            }
        }

        //public void setCalculateTax(bool calculate)
        //{
        //    this.taxCalculated = calculate;
        //}

        //public bool isTaxCalculated()
        //{
        //    return taxCalculated;
        //}

        /*public void setBankType(int type)
        {
            this.bankType = type;
        }

        public int getBankType()
        {
            return bankType;
        }

        public void setPlanningPaymentDate(DateTime date)
        {
            this.planningPaymentDate = date;
        }

        public DateTime getPlanningPaymentDate()
        {
            return planningPaymentDate;
        }*/


        //public void setPostingDate(DateTime date)
        //{
        //    this.postingDate = date;
        //}

        //public DateTime getPostingDate()
        //{
        //    return postingDate;
        //}


        public string TaxInvoiceNumber
        {
            get
            {
                return taxInvoiceNumber;
            }
            set
            {
                taxInvoiceNumber = value;
            }
        }

        //public void setTaxInvoiceNumber(string taxInvoiceNumber)
        //{
        //    this.taxInvoiceNumber = taxInvoiceNumber;
        //}

        //public string getTaxtInvoiceNumber()
        //{
        //    return taxInvoiceNumber;
        //}


        public string TaxCode
        {
            get
            {
                return taxCode;
            }
            set
            {
                taxCode = value;
            }
        }

        //public void setTaxCode(string taxCode)
        //{
        //    this.taxCode = taxCode;
        //}

        //public string getTaxCode()
        //{
        //    return taxCode;
        //}

        public DateTime RVDate
        {
            get
            {
                return rvDate;
            }
            set
            {
                rvDate = value;
            }
        }


        //public void setRVDate(DateTime rvDate)
        //{
        //    this.rvDate = rvDate;
        //}

        //public DateTime getRVDate()
        //{
        //    return rvDate;
        //}

        public string DivisionId
        {
            get
            {
                return divisionId;
            }
            set
            {
                divisionId = value;
            }
        }


        //public void setDivisionId(string divisionId)
        //{
        //    this.divisionId = divisionId;
        //}

        //public string getDivisionId()
        //{
        //    return divisionId;
        //}


        public DateTime ActivityDate
        {
            get
            {
                return activityDate;
            }

            set
            {
                activityDate = value;
            }
        }
        //public void setActivityDate(DateTime activityDate)
        //{
        //    this.activityDate = activityDate;
        //}

        //public DateTime getActivityDate()
        //{
        //    return activityDate;
        //}

        public string BudgetNumber
        {
            get
            {
                return budgetNumber;
            }
            set
            {
                budgetNumber = value;
            }
        }


        //public void setBudgetNumber(string budgetNumber)
        //{
        //    this.budgetNumber = budgetNumber;
        //}

        //public string getBudgetNumber()
        //{
        //    return budgetNumber;
        //}


        public int SuspenseNumber
        {
            get
            {
                return suspenseNumber;
            }

            set
            {
                suspenseNumber = value;
            }
        }

        //public void setSuspenseNumber(int suspenseNumber)
        //{
        //    this.suspenseNumber = suspenseNumber;
        //}

        //public int getSuspenseNumber()
        //{
        //    return suspenseNumber;
        //}


        public int TransactionCode
        {
            get
            {
                return transactionCode;
            }
            set
            {
                transactionCode = value;
            }
        }

        //public void setTransactionCode(int transactionCode)
        //{
        //    this.transactionCode = transactionCode;
        //}

        //public int getTransactionCode()
        //{
        //    return transactionCode;
        //}


        public int RVTypeCode
        {
            get
            {
                return rvTypeCode;
            }

            set
            {
                rvTypeCode = value;                    
            }
        }

        //public void setRVTypeCode(int rvTypeCode)
        //{
        //    this.rvTypeCode = rvTypeCode;
        //}

        //public int getRVTypeCode()
        //{
        //    return rvTypeCode;
        //}

        public string VendorCode
        {
            get
            {
                return vendorCode;
            }

            set
            {
                vendorCode = value;
            }
        }

        //public void setVendorCode(string vendorCode)
        //{
        //    this.vendorCode = vendorCode;
        //}

        //public string getVendorCode()
        //{
        //    return vendorCode;
        //}


        public int VendorGroupCode
        {
            get
            {
                return vendorGroupCode;
            }

            set
            {
                vendorGroupCode = value;
            }
        }

        //public void setVendorGroupCode(int vendorGroupCode)
        //{
        //    this.vendorGroupCode = vendorGroupCode;
        //}

        //public int getVendorGroupCode()
        //{
        //    return vendorGroupCode;
        //}


        public string PayMethodCode
        {
            get
            {
                return payMethodCode;
            }
            set
            {
                payMethodCode = value;
            }
        }

        //public void setPayMethodCode(string payMethodCode)
        //{
        //    this.payMethodCode = payMethodCode;
        //}

        //public string getPayMethodCode()
        //{
        //    return payMethodCode;
        //}


        public int StatusCode
        {
            get
            {
                return statusCode;
            }
            set
            {
                statusCode = value;
            }
        }

        //public void setStatusCode(int statusCode)
        //{
        //    this.statusCode = statusCode;
        //}

        //public int getStatusCode()
        //{
        //    return statusCode;
        //}

        //public void setRVNumber(int number)
        //{
        //    rvNumber = number;
        //}

        //public int getRVNumber()
        //{
        //    return rvNumber;
        //}

        public int RVNumber
        {
            get
            {
                return rvNumber;
            }
            set
            {
                rvNumber = value;
            }
        }


        public int RVYear
        {
            get
            {
                return rvYear;
            }
            set
            {
                rvYear = value;
            }
        }
        //public void setRVYear(int year)
        //{
        //    rvYear = year;
        //}

        //public int getRVYear()
        //{
        //    return rvYear;
        //}


        public List<RVFormDetail> Details
        {
            get
            {
                return lstDetail;
            }

            set
            {
                lstDetail = value;
            }
        }

        //public List<RVFormDetail> getDetails()
        //{
        //    return lstDetail;
        //}

        //public void setDetails(List<RVFormDetail> details)
        //{
        //    lstDetail = details;
        //}

        public void addDetail(RVFormDetail detail)
        {
            Boolean exists = false;
            foreach (RVFormDetail d in lstDetail)
            {
                if (d.SequenceNumber == detail.SequenceNumber)
                {
                    exists = true;
                    break;
                }
            }

            if (!exists)
            {
                lstDetail.Add(detail);
            }
        }

        public void removeDetail(int sequenceNumber)
        {
            foreach (RVFormDetail d in lstDetail)
            {
                if (d.SequenceNumber == sequenceNumber)
                {
                    lstDetail.Remove(d);
                }
            }
        }
    }
}
