﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer.Model;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data.SAPData;
using Common.Function;
using Common.Data._20MasterData;
using Common.Data._30PVFormList;
using System.Web;
using Dapper;

namespace BusinessLogic.VoucherForm
{
    public class VoucherListLogic : LogicBase
    {
        protected LogicFactory logic = LogicFactory.Get();

        protected MessageLogic m = new MessageLogic();
        protected List<string> errs = new List<string>();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";

        private Dictionary<string, object> o = new Dictionary<string, object>();
        protected bool checkOneTime = false;
        public bool CheckOneTimeVendor
        {
            get
            {
                return checkOneTime;
            }
            set
            {
                checkOneTime = value;
            }
        }
        public object this[string Index]
        {
            get
            {
                if (o.Keys.Contains(Index))
                    return o[Index];
                else
                    return null;
            }

            set
            {
                if (o.Keys.Contains(Index))
                {
                    o[Index] = value;
                }
                else
                {
                    o.Add(Index, value);
                }
            }
        }

        public virtual List<CodeConstant> getDocumentStatus(string module)
        {
            return hQx<CodeConstant>("getDocumentStatus", new { module = module }).ToList();
            
        }

        public virtual bool UpdateNextApprover(int _no, int _yy, string by)
        {
            return true;
        }

        public string CommaJoinList(string key)
        {
            List<string> nr = this[key] as List<string>;
            if (nr == null) return "";
            StringBuilder aList = new StringBuilder("");
            for (int i = 0; i < nr.Count; i++)
            {
                string[] a = nr[i].Split(':');
                if (i > 0) aList.Append(", ");
                aList.Append(a[0]);
            }
            return aList.ToString();
        }

        public virtual bool PostToSap(List<string> keys, int _ProcessId, ref string _Message, UserData u, int? transType = null)
        {
            /// insert posting redirect code here 
            if (u.canPost)
            {
                List<string> xNR = new List<string>();
                List<string> xRF = new List<string>();

                List<string> nr = new List<string>();
                for (int i = 0; i < keys.Count; i++)
                {
                    string[] x = keys[i].Split(':');
                    string pre = "";
                    bool go = CanPost(x[0], x[1], u, ref pre);
                    if (go)
                    {
                        go = (u.USERNAME.Equals(pre)) 
                             || logic.PVRVWorkflow.Redirect(x[0] + x[1], u, pre);
                        if (!go)
                        {
                            xRF.Add(keys[i]);
                        }
                    }
                    else
                    {
                        xNR.Add(keys[i]);
                    }
                }
                if (xNR.Count > 0)
                    this["NR"] = xNR;
                if (xRF.Count > 0)
                    this["RF"] = null;
                return (xNR.Count < 1 && xRF.Count < 1);
            }
            return true;
        }

        public List<string> LastErrors
        {
            get { return errs; }
        }

        public DateTime? GetSuspenseDate(int sNo, int sYear)
        {
            return Qu<DateTime?>("GetSuspenseDate", sNo, sYear).FirstOrDefault();            
        }

        public object GetDirectToMeReasons(string uName)
        {
            var q = hQu<DirectToMeReason>("DirectToMeReason", uName); 
                    
            return q.ToList();
        }

        public List<CodeConstant> GetUserDivisions(UserData u)
        {
            string[] divs = (u != null) ? u.GetDivisions() : null;
            List<CodeConstant> r = null;
            
                if (logic.User.IsAuthorizedFinance || (u != null && u.Roles.Contains("ELVIS_ADMIN")))
                    divs = null;
                r = logic.PVList.GetDivisons(divs);
                
            
            return r;
        }

        public List<CodeConstant> GetDivisons(string[] divs)
        {
            return hQu<CodeConstant>("GetUserDivisions", (((divs == null) || (divs.Length < 1)) ? "": string.Join(",", divs))).ToList();
        }

        public bool CanPost(string _no, string _yy, UserData u, ref string byU)
        {
            bool r = true;
            r = u.isFinanceDivision && u.canPost;
            if (!r) return false;
           
            string reff = _no + _yy;
            Decimal reff_no = Convert.ToDecimal(_no + _yy);
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from d in db.TB_R_DISTRIBUTION_STATUS
                         orderby d.STATUS_CD
                         where d.REFF_NO == reff_no
                         && (d.STATUS_CD <= 27 || d.STATUS_CD <= 68)
                         && d.ACTUAL_DT == null
                         select d).FirstOrDefault();
                r = false;
                if (q != null && (q.STATUS_CD == 27 || q.STATUS_CD == 68))
                {
                    string by = q.PROCCED_BY;

                    r = !by.isEmpty(); // && !by.Equals(u.USERNAME);

                    byU = by;
                    if (r)
                    {
                        var qc = u.Positions.Where(p => p.DIVISION_ID == q.PIC).FirstOrDefault();
                        byte aClass = (qc != null) ? (qc.CLASS ?? 0) : (byte)0;

                        DistributionStatusData ds = DistributionData(q);
                        ds.PROCCED_BY = u.USERNAME;
                        ds.CLASS = aClass;
                        
                        db.DeleteObject(q);

                        var qq = new TB_R_DISTRIBUTION_STATUS()
                        {
                            REFF_NO = ds.REFF_NO,
                            STATUS_CD = ds.STATUS_CD,
                            PIC = ds.PIC,
                            PLAN_DT = ds.PLAN_DT,
                            //ACTUAL_DT = ds.ACTUAL_DT,
                            //ACTUAL_LT = ds.ACTUAL_LT,
                            //ACTUAL_UOM = ds.ACTUAL_UOM,
                            SLA_UNIT = ds.SLA_UNIT,
                            SLA_UOM = ds.SLA_UOM,
                            PROCCED_BY = ds.PROCCED_BY,
                            CLASS = ds.CLASS
                            //SKIP_FLAG = ds.SKIP_FLAG
                        };
                        db.AddToTB_R_DISTRIBUTION_STATUS(qq);
                        db.SaveChanges();

                        int docNo = _no.Int();
                        int docYear = _yy.Int();

                        SetDocNextApprover(docNo, docYear, ds.STATUS_CD, ds.PROCCED_BY);
                    }
                }
            }
            return r;
        }

        public DistributionStatusData DistributionData(TB_R_DISTRIBUTION_STATUS q)
        {
            return new DistributionStatusData()
            {
                REFF_NO = q.REFF_NO,
                STATUS_CD = q.STATUS_CD,
                PIC = q.PIC,
                PLAN_DT = q.PLAN_DT,
                ACTUAL_DT = q.ACTUAL_DT,
                ACTUAL_LT = q.ACTUAL_LT,
                ACTUAL_UOM = q.ACTUAL_UOM,
                SLA_UNIT = q.SLA_UNIT,
                SLA_UOM = q.SLA_UOM,
                PROCCED_BY = q.PROCCED_BY,
                CLASS = q.CLASS,
                SKIP_FLAG = q.SKIP_FLAG
            };
        }

        public void SetDocNextApprover(int docNo, int docYear, int StatusCd, string NextApprover)
        {
            if (StatusCd < 50)
            {
                var doc = db.TB_R_PV_H.Where(v => v.PV_NO == docNo && v.PV_YEAR == docYear).FirstOrDefault();
                if (doc != null)
                {
                    doc.NEXT_APPROVER = NextApprover;
                    db.SaveChanges();
                }
            }
            else
            {
                var doc = db.TB_R_RV_H.Where(v => v.RV_NO == docNo && v.RV_YEAR == docYear).FirstOrDefault();
                if (doc != null)
                {
                    doc.NEXT_APPROVER = NextApprover;
                    db.SaveChanges();
                }
            }
        }

        public DateTime NextPlanDate(DateTime a, int unit, string uom)
        {
            ObjectParameter o = new ObjectParameter("NextPlanDT", typeof(DateTime));
            db.GetNextPlanDate(a, unit, uom, o);

            DateTime r = DateTime.Now;
            if (o != null)
                r = Convert.ToDateTime(o.Value);
            return r;
        }

        public bool IsDirectorOf(string DivCd, UserData u)
        {
            var x = u.Positions.Where(p => p.CLASS >= 8 && p.DIVISION_ID == DivCd); /// user is director in selected DivCd
            return x.Any();
        }

        public bool Take(string _no, string _yy, string DivCd, UserData u, ref string by)
        {
            bool r = false;
            string reff = _no + _yy;
            Decimal reff_no = Convert.ToDecimal(_no + _yy);

            bool isDirector = IsDirectorOf(DivCd, u);
            if (!isDirector) return false;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from d in db.TB_R_DISTRIBUTION_STATUS
                         orderby d.STATUS_CD
                         where d.REFF_NO == reff_no
                         && d.ACTUAL_DT == null
                         select d);
                var xx = q.FirstOrDefault();
                int status = 99;
                if (xx != null)
                {
                    status = xx.STATUS_CD;
                    by = xx.PROCCED_BY;
                }
                var z = q.Where(a => a.STATUS_CD == 15);
                if (xx != null && status < 15 && !z.Any())
                {
                    var uom = (from m in db.TB_M_STATUS
                               where m.STATUS_CD == 15
                               select m).FirstOrDefault();
                    string uoms = "hours";
                    int uom_unit = 3;
                    if (uom != null)
                    {
                        uoms = uom.SLA_UOM.ToUpper();
                        uom_unit = (int)uom.SLA_UNIT;
                    }
                    DateTime plan = NextPlanDate(DateTime.Now, uom_unit, uoms);

                    var n = new TB_R_DISTRIBUTION_STATUS()
                    {
                        REFF_NO = reff_no,
                        PIC = DivCd,
                        PLAN_DT = plan,
                        STATUS_CD = 15,
                        SLA_UNIT = uom_unit,
                        SLA_UOM = uoms,
                        PROCCED_BY = u.USERNAME,
                        CLASS = 8
                    };
                    db.AddToTB_R_DISTRIBUTION_STATUS(n);
                    db.SaveChanges();
                    r = true;
                }
            }
            return r;
        }


        public virtual bool SetNextApprover(int _no, int _yy, string by)
        {
            return true;
        }

        public int? GetSuspenseMaxItemNo(int _no, int _yy)
        {
            var sus = (from s in db.TB_R_SAP_DOC_NO
                       where s.DOC_NO == _no
                       && s.DOC_YEAR == _yy
                       select s.ITEM_NO);
            int? r = null;
            if (sus.Any())
            {
                r = sus.Max();
            }

            log.Log(_INF, string.Format(" max suspense {0} {1} item no = {2}", _no, _yy, r), "GetSuspenseMaxItemNo");
            return r;

        }

        public void UpdateSapDocNo(ELVIS_DBEntities db, List<PostItemData> l)
        {

            foreach (PostItemData p in l)
            {
                var q = from s in db.TB_R_SAP_DOC_NO
                        where s.DOC_NO == p.DOC_NO &&
                        s.DOC_YEAR == p.DOC_YEAR &&
                        s.ITEM_NO == p.ITEM_NO
                        select s;
                bool added = false;
                TB_R_SAP_DOC_NO sado = null;
                if (q.Any())
                {
                    sado = q.FirstOrDefault();
                }
                else
                {
                    sado = new TB_R_SAP_DOC_NO()
                    {
                        DOC_NO = p.DOC_NO,
                        DOC_YEAR = p.DOC_YEAR,
                        ITEM_NO = p.ITEM_NO,
                    };
                    added = true;
                }
                sado.CURRENCY_CD = p.CURRENCY_CD;
                sado.INVOICE_NO = p.INVOICE_NO;
                if (p.SAP_DOC_NO != null)
                {
                    sado.SAP_DOC_NO = p.SAP_DOC_NO.str();
                }
                if (p.SAP_DOC_YEAR != null)
                {
                    sado.SAP_DOC_YEAR = p.SAP_DOC_YEAR;
                }
                if (added)
                {
                    db.AddToTB_R_SAP_DOC_NO(sado);
                }
                db.SaveChanges();
            }
        }

        protected static string Textuality(string word, string desc)
        {
            string r = desc;
            if (!r.isEmpty() && r.Length > 34)
            {
                r = r.Substring(0, 34);
            }
            if (!word.isEmpty() && word.Length > 15)
            {
                word = word.Substring(0, 15);
            }
            r = word + " " + r;
            return r;
        }

        public IQueryable<VendorData> GetVendorCodes()
        {
            return db.vw_Vendor.Select(t =>
                    new VendorData()
                    {
                        VENDOR_CD = t.VENDOR_CD,
                        VENDOR_NAME = t.VENDOR_NAME,
                        VENDOR_GROUP_CD = t.VENDOR_GROUP_CD,
                        SEARCH_TERMS = t.SEARCH_TERMS,
                        DIVISION_ID = t.DIVISION_ID
                    });
        }


        public void RevertApproverInList(List<string> l)
        {
            if (l == null || l.Count < 1) return;

            for (int i = 0; i < l.Count; i++)
            {
                string[] id = l[i].Split(':');

                int _no = (id != null && id.Length > 0) ? id[0].Int() : 0;
                int _yy = (id != null && id.Length > 1) ? id[1].Int() : 0;
                logic.PVList.RevertApprover(_no, _yy);
            }
        }

        public bool RevertApprover(int _no, int _yy, int StatusCd = 27, int ModuleCd = 1)
        {
            string reff_no = _no.str() + _yy.str();
            decimal reff = reff_no.Dec();
            int TransactionType = 0;

            if (ModuleCd == 1)
            {
                var qpv = db.TB_R_PV_H
                    .Where(v=>v.PV_NO == _no && v.PV_YEAR == _yy)
                    .Select(a=>a.TRANSACTION_CD).FirstOrDefault();
                if (qpv!=null)  
                {
                    TransactionType = qpv??0;
                }
            }
            else if (ModuleCd == 2)
            {
                var qrv = db.TB_R_RV_H
                    .Where(v=>v.RV_NO == _no && v.RV_YEAR == _yy)
                    .Select(a=>a.TRANSACTION_CD).FirstOrDefault();
                if (qrv!=null)  
                {
                    TransactionType = qrv??0;
                }
            }

            var qd = db.TB_R_DISTRIBUTION_STATUS
                .Where(d => d.REFF_NO == reff && d.STATUS_CD == StatusCd && d.ACTUAL_DT == null)
                .FirstOrDefault();
            if (qd != null)
            {
                string currentApprover = qd.PROCCED_BY;
                string sql = logic.SQL["OriginalApprover"];

                sql = sql.Replace("@moduleCode", ModuleCd.str())
                        .Replace("@statusCd", StatusCd.str())
                        .Replace("@tt", TransactionType.str());

                var q = Db.Query<string>(sql).ToList(); 

                string ori = null;
                if (q != null && q.Count > 0)
                    ori = q[0];
                
                if (!ori.isEmpty() && !currentApprover.Equals(ori))
                {
                    DistributionStatusData ds = DistributionData(qd);
                    
                    byte uCLASS = db.vw_User
                        .Where(v => v.USERNAME == ori)
                        .Select(a => a.CLASS)
                        .FirstOrDefault() ?? 0;
                    ds.CLASS = uCLASS;
                    ds.PROCCED_BY = ori;

                    List<string> toReturn = new List<string>();
                    toReturn.Add(reff_no);
                    logic.PVRVWorkflow.Redirect(reff_no, new UserData() { USERNAME = ori }, currentApprover);

                    db.DeleteObject(qd);
                    db.AddToTB_R_DISTRIBUTION_STATUS(new TB_R_DISTRIBUTION_STATUS()
                    {
                        REFF_NO = ds.REFF_NO,
                        STATUS_CD = ds.STATUS_CD,
                        PIC = ds.PIC,
                        PLAN_DT = ds.PLAN_DT,
                        ACTUAL_DT = ds.ACTUAL_DT,
                        ACTUAL_LT = ds.ACTUAL_LT,
                        ACTUAL_UOM = ds.ACTUAL_UOM,
                        SLA_UNIT = ds.SLA_UNIT,
                        SLA_UOM = ds.SLA_UOM,
                        PROCCED_BY = ds.PROCCED_BY,
                        CLASS = ds.CLASS
                    });
                    db.SaveChanges();
                    SetDocNextApprover(_no, _yy, ds.STATUS_CD, ori);
                                       
                    return true;
                }

            }
            return false;
        }

        public List<WorklistHelper> GetWorklist(string UserName)
        {
            List<WorklistHelper> lstWorklist = null;
            const string KEY = "UserWorklist";
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                lstWorklist = HttpContext.Current.Session[KEY] as List<WorklistHelper>;

                if (lstWorklist == null)
                {
                    Ticker.In("GetWorklistCompleteByProcess " + UserName);
                    logic.k2.Open();
                    lstWorklist = logic.k2.GetWorklistCompleteByProcess(UserName, "");
                    logic.k2.Close();
                    Ticker.Out("GetWorklistCompleteByProcess " + UserName);

                    HttpContext.Current.Session[KEY] = lstWorklist;
                }
            }
            return lstWorklist;
        }

        protected int GetDaysBetweenToToday(DateTime a)
        {
            double LT = Qx<double>("GetDaysBetweenToToday", new { today = a.ToString("yyyy-MM-dd hh:mm:ss") }).FirstOrDefault();

            return ((int)Math.Round(LT));
        }

        
    }

    
}
