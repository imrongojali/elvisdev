﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class Promotion
    {
           public Int64  SEQ_NO { get; set; }
           public string  CATEGORY_CODE { get; set; }
           public Int32  TRANSACTION_CD { get; set; }
           public string  REFERENCE_NO { get; set; }
           public string  ENOM_NO { get; set; }
			public Int32 PV_NO { get; set; }
           public Int32 NO { get; set; }
           public string  NAMA { get; set; }
           public string  NPWP { get; set; }
           public string NPWPFORMAT { get; set; }
        
           public string  ALAMAT { get; set; }
          
            public DateTime  CREATED_DT { get; set; }
           public string  CREATED_BY { get; set; }
    }
}
