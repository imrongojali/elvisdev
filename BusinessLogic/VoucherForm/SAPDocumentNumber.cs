﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    public class SAPDocumentNumber
    {
        public string InvoiceNumber { set; get; }
        public string SAPDocNumber { set; get; }
        public string ClearingDocNumber { set; get; }
        public string CurrencyCode { set; get; }
        public string PayPropDocNo { set; get; }
        public string PayPropId { set; get; } 
    }
}
