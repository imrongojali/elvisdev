﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class AttachmentFakturPajakTable
    {
        public AttachmentFakturPajakTable()
        {
            attachmentsFakturPajak = new List<FormAttachmentFakturPajak>();
        }

        public bool Editing { set; get; }

        private List<FormAttachmentFakturPajak> attachmentsFakturPajak;
        public List<FormAttachmentFakturPajak> AttachmentsFakturPajak
        {
            get
            {
                return attachmentsFakturPajak;
            }
        }

        public FormAttachmentFakturPajak getBlankAttachmentFakturPajak()
        {
            foreach (FormAttachmentFakturPajak att in attachmentsFakturPajak)
            {
                if (att.Blank)
                {
                    return att;
                }
            }

            FormAttachmentFakturPajak attachmentFakturPajak = new FormAttachmentFakturPajak();
            attachmentsFakturPajak.Add(attachmentFakturPajak);
            return attachmentFakturPajak;
        }

        public FormAttachmentFakturPajak getAttachmentFakturPajak(int sequenceNumber)
        {
            foreach (FormAttachmentFakturPajak at in attachmentsFakturPajak)
            {
                if (at.SequenceNumber == sequenceNumber)
                {
                    return at;
                }
            }

            return null;
        }

        public void closeEditingAttachmentFakturPajak()
        {
            int cntAttachment = attachmentsFakturPajak.Count;
            FormAttachmentFakturPajak attachmentFakturPajak;
            for (int i = 0; i < cntAttachment; i++)
            {
                attachmentFakturPajak = attachmentsFakturPajak[i];
                if (attachmentFakturPajak.Blank)
                {
                    attachmentsFakturPajak.RemoveAt(i);
                    break;
                }
            }

            Editing = false;
        }

        public void openEditingAttachmentFakturPajak()
        {
            addBlankRowAttachmentFakturPajak();
            Editing = true;
        }

        public void addBlankRowAttachmentFakturPajak()
        {
            if (attachmentsFakturPajak.Count > 0 && attachmentsFakturPajak[attachmentsFakturPajak.Count - 1].Blank) return;

            attachmentsFakturPajak.Add(new FormAttachmentFakturPajak()
            {
                Blank = true,
                SequenceNumber = attachmentsFakturPajak.Count + 1
            });
        }

        public void resetAttachmentFakturPajak()
        {
            attachmentsFakturPajak.Clear();
        }
    }
}
