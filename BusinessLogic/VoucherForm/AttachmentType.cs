﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class AttachmentType
    {
        public Int32 RowNumber { get; set; }
        public string AttachmentId { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
    }
}
