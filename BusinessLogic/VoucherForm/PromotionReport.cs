﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class PromotionReport
    {

        public string CompanyName { get; set; }
        public string CompanyNPWP { get; set; }
        public string CompanyAddress { get; set; }
        public string TMP_PENANDATANGAN { get; set; }
        public DateTime TGL_PENANDATANGAN { get; set; }
        public string NAMA_PENANDATANGAN { get; set; }
        public string JABATAN_PENANDATANGAN { get; set; }
        public Int32 THN_PAJAK { get; set; }
        public string REFERENCE_NO { get; set; }
        public string ENOM_NO { get; set; }
        public Int32 PV_NO { get; set; }
        public string Number { get; set; }
        public string NAMA { get; set; }
        public string NPWP { get; set; }
        public string ALAMAT { get; set; }
        public string GroupNumber { get; set; }
        public DateTime TANGGAL { get; set; }
        public string BENTUK_DAN_JENIS_BIAYA { get; set; }
        public decimal JUMLAH { get; set; }
        public decimal JUMLAH_GROSS_UP { get; set; }
        public string KETERANGAN { get; set; }
        public decimal JUMLAH_PPH { get; set; }
        public string JENIS_PPH { get; set; }
        public decimal JUMLAH_NET { get; set; }
        public string NOMOR_REKENING { get; set; }
        public string NAMA_REKENING_PENERIMA { get; set; }
        public string NAMA_BANK { get; set; }
        public string NO_KTP { get; set; }
        public string NOMOR_BUKTI_POTONG { get; set; }
    }
}
