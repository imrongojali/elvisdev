﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class PromotionDetail
    {
        public Int64 SEQ_NO { get; set; }
        public Int64 SEQ_NO_DETAIL { get; set; }
        public Int32 NO { get; set; }

        public DateTime TANGGAL { get; set; }
        public string BENTUK_DAN_JENIS_BIAYA { get; set; }
        public decimal JUMLAH { get; set; }
        public decimal JUMLAH_GROSS_UP { get; set; }
        public string KETERANGAN { get; set; }
        public decimal JUMLAH_PPH { get; set; }
        public string JENIS_PPH { get; set; }
        public decimal JUMLAH_NET { get; set; }
        public string NOMOR_REKENING { get; set; }
        public string NAMA_REKENING_PENERIMA { get; set; }
        public string NAMA_BANK { get; set; }
        public string NO_KTP { get; set; }
        public string NOMOR_BUKTI_POTONG { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
    }
}
