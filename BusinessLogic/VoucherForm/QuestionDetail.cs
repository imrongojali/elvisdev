﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class QuestionDetail
    {
        public Int64 QuestionId { get; set; }
        public bool IsYes { get; set; }
    }
}
