﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using DataLayer.Model;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using BusinessLogic.CommonLogic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data;
using System.IO;

namespace BusinessLogic._20MasterData
{
    public class UserRoleLogic : LogicBase
    {
        // ELVIS_DBEntities db = new ELVIS_DBEntities();

        public List<UserRoleData> SearchInqury(string _userName, string _regNo, string _positonId,
                string _sectionId, string _departmentId, string _divisionId, string _roleId)
        {
            List<UserRoleData> l = new List<UserRoleData>();

            try
            {
                string sfUSERNAME = "", slUSERNAME = "";
                _userName = _userName.Trim();
                _regNo = _regNo.Trim();
                int iUSERNAME = LikeLogic.ignoreWhat(_userName, ref sfUSERNAME, ref slUSERNAME);
                string sfROLE_ID = "", slROLE_ID = "";
                _roleId = _roleId.Trim();
                int iROLE_ID = LikeLogic.ignoreWhat(_roleId, ref sfROLE_ID, ref slROLE_ID);

                var v = db.vw_User.AsQueryable();
                switch (iUSERNAME)
                {
                    case LikeLogic.igNONE:
                        v = v.Where(x => x.USERNAME.Equals(_userName));
                        break;
                    case LikeLogic.igFIRST:
                        v = v.Where(x => x.USERNAME.EndsWith(slUSERNAME));
                        break;
                    case LikeLogic.igLAST:
                        v = v.Where(x => x.USERNAME.StartsWith(sfUSERNAME));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        v = v.Where(x => x.USERNAME.Contains(sfUSERNAME));
                        break;
                    case LikeLogic.igMIDDLE:
                        v = v.Where(x => x.USERNAME.StartsWith(sfUSERNAME)
                                      && x.USERNAME.EndsWith(slUSERNAME));
                        break;
                    default: break;
                }
                if (!String.IsNullOrEmpty(_regNo))
                    v = v.Where(x => x.REG_NO.Equals(_regNo));
                if (!String.IsNullOrEmpty(_divisionId))
                    v = v.Where(x => x.DIVISION_ID.Equals(_divisionId));
                if (!String.IsNullOrEmpty(_departmentId))
                    v = v.Where(x => x.DEPARTMENT_ID.Equals(_departmentId));
                if (!String.IsNullOrEmpty(_sectionId))
                    v = v.Where(x => x.SECTION_ID.Equals(_sectionId));
                if (!String.IsNullOrEmpty(_positonId))
                    v = v.Where(x => x.POSITION_ID.Equals(_positonId));

                switch (iROLE_ID)
                {
                    case LikeLogic.igNONE:
                        v = v.Where(x => x.ROLE_ID.Equals(_userName));
                        break;
                    case LikeLogic.igFIRST:
                        v = v.Where(x => x.ROLE_ID.EndsWith(slROLE_ID));
                        break;
                    case LikeLogic.igLAST:
                        v = v.Where(x => x.ROLE_ID.StartsWith(sfROLE_ID));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        v = v.Where(x => x.ROLE_ID.Contains(sfROLE_ID));
                        break;
                    case LikeLogic.igMIDDLE:
                        v = v.Where(x => x.ROLE_ID.StartsWith(sfROLE_ID) 
                                      && x.ROLE_ID.EndsWith(slROLE_ID));
                        break;
                    default: break;
                }
                var q = (from p in v
                         orderby p.USERNAME
                         select new UserRoleData()
                         {
                             REG_NO = p.REG_NO,
                             USERNAME = p.USERNAME,
                             ROLE_ID = p.ROLE_ID,
                             TITLE = p.TITLE,
                             FIRST_NAME = p.FIRST_NAME,
                             LAST_NAME = p.LAST_NAME,
                             EMAIL = p.EMAIL,
                             POSITION_ID = p.POSITION_ID,
                             POSITION_NAME = p.POSITION_NAME,
                             DIVISION_ID = p.DIVISION_ID,
                             DIVISION_NAME = p.DIVISION_NAME,
                             DEPARTMENT_ID = p.DEPARTMENT_ID,
                             DEPARTMENT_NAME = p.DEPARTMENT_NAME,
                             SECTION_ID = p.SECTION_ID,
                             SECTION_NAME = p.SECTION_NAME,
                             GROUP_CD = p.GROUP_CD,
                             GROUP_NAME = p.GROUP_NAME,
                             CLASS = p.CLASS
                         });

                l = q.ToList<UserRoleData>();
               
            }
            catch (Exception ex)
            {
                l = null;
                Handle(ex);
            }

            return l;
        }

        public UserRoleData FindUser(string username, ref int check)
        {
            var q = (from p in db.vw_User
                     where p.USERNAME == username
                     select new UserRoleData
                     {
                         REG_NO = p.REG_NO,
                         USERNAME = p.USERNAME,
                         ROLE_ID = p.ROLE_ID,
                         TITLE = p.TITLE,
                         FIRST_NAME = p.FIRST_NAME,
                         LAST_NAME = p.LAST_NAME,
                         EMAIL = p.EMAIL,
                         POSITION_ID = p.POSITION_ID,
                         POSITION_NAME = p.POSITION_NAME,
                         DIVISION_ID = p.DIVISION_ID,
                         DIVISION_NAME = p.DIVISION_NAME,
                         DEPARTMENT_ID = p.DEPARTMENT_ID,
                         DEPARTMENT_NAME = p.DEPARTMENT_NAME,
                         SECTION_ID = p.SECTION_ID,
                         SECTION_NAME = p.SECTION_NAME,
                         GROUP_CD = p.GROUP_CD,
                         GROUP_NAME = p.GROUP_NAME,
                         CLASS = p.CLASS
                     }).FirstOrDefault();
            check = (q != null)? 1 :0;
            return q;
        }

        public int GetUserPositionsCount(string username)
        {
            int pc = (from x in db.vw_User
                     where x.USERNAME == username
                     select new { x.POSITION_ID, x.DEPARTMENT_ID }).Count();
            return pc;
        }
        public List<ComboClassData> RoleList()
        {
            return (from r in db.TB_M_SYSTEM 
                    where r.SYSTEM_TYPE.Equals("ROLE_ID")
                    select new ComboClassData() 
                    { 
                        COMBO_CD = r.SYSTEM_CD,
                        COMBO_NAME =  r.SYSTEM_VALUE_TXT.Trim()
                    }).ToList<ComboClassData>();
        }

        public bool AddRow(UserRoleData u, UserData userData)
        {
            bool aOk = false;
                    
            try
            {
               
                int ok;
                ok = db.PutUserRole(
                        u.USERNAME, u.TITLE, u.FIRST_NAME, u.LAST_NAME, 
                        Convert.ToInt32(u.REG_NO), u.GROUP_CD, null, u.ROLE_ID, 
                        u.EMAIL, userData.USERNAME, 1, 8);
                ok = db.PutUserPosition(
                        u.USERNAME, u.POSITION_ID, u.SECTION_ID, u.DIVISION_ID, 
                        u.DEPARTMENT_ID, u.DIVISION_ID, u.POSITION_ID, 1, 8);

                if (ok ==0)
                    throw new Exception("Role Add error " + ok);
               
                aOk = true;

            }
            catch (Exception ex)
            {
                Handle(ex);
            }

            return aOk;
        }

        public bool EditRow(UserRoleData u, UserRoleData pre, UserData userData)
        {
            bool aOk = false;

            bool userEdited = !(u.REG_NO.Equals(pre.REG_NO) &&
                     u.FIRST_NAME.Equals(pre.FIRST_NAME) &&
                     u.LAST_NAME.Equals(pre.LAST_NAME) &&
                     u.GROUP_CD.Equals(pre.GROUP_CD) &&
                     u.TITLE.Equals(pre.TITLE) &&                     
                     u.EMAIL.Equals(pre.EMAIL));

            bool poEdited = !(u.POSITION_ID.Equals(pre.POSITION_ID) &&
                u.DIVISION_ID.Equals(pre.DIVISION_ID));
            bool poChanged = !(u.SECTION_ID.Equals(pre.SECTION_ID) && u.DEPARTMENT_ID.Equals(pre.DEPARTMENT_ID));
            bool roleEdited = !u.ROLE_ID.Equals(pre.ROLE_ID);

            bool anyEdit = poEdited || poChanged || roleEdited || userEdited;

            try
            {
                int updated = 0;
                if (poChanged|poEdited) updated += 1;
                if (roleEdited) updated += 2;
                if (userEdited) updated += 4;
                int Ok;
                if (poChanged || poEdited)
                {
                    Ok = db.PutUserPosition(u.USERNAME, u.POSITION_ID, u.SECTION_ID,
                        u.DIVISION_ID, u.DEPARTMENT_ID, pre.DIVISION_ID, pre.POSITION_ID, 1, updated);
                }

                if (userEdited || roleEdited)
                {
                    Ok = db.PutUserRole(u.USERNAME, u.TITLE, 
                        u.FIRST_NAME, u.LAST_NAME, Convert.ToDecimal(u.REG_NO),
                        u.GROUP_CD, null, u.ROLE_ID,  u.EMAIL, userData.USERNAME, 1, 
                        updated);
                }

                aOk = true;
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return aOk;
        }

        public bool DeleteRow(List<UserRoleData> listOfCode)
        {
            bool aOk = false;

            try
            {
                foreach (UserRoleData u in listOfCode)
                {
                    if (String.IsNullOrEmpty(u.USERNAME)) continue;
                    int Ok;
                    
                    if (GetUserPositionsCount(u.USERNAME) > 1) 
                    {
                        Ok = db.PutUserPosition(u.USERNAME, u.POSITION_ID, null, u.DIVISION_ID,  null, u.DIVISION_ID, u.POSITION_ID, -1, 0);
                    }
                    else 
                    {
                        Ok = db.PutUserRole(u.USERNAME, null, null, null, null, null, null, null, null, null, -1, 8);
                    }
                    
                }
                aOk = true;

            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return aOk;
        }

        public byte[] Get(string _username, string _imagePath, List<UserRoleData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("User");
            x.PutHeader("User",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Reg No|UserName|Title|Name|Email|Position|"
                + "Division|Department|Section|Group|Role",
                _imagePath);
            int rownum = 0;
            UserRoleData pre = new UserRoleData()
            {
                USERNAME = ""
            };
            UserRoleData dd = new UserRoleData() {
                USERNAME = ""
            };
            foreach (UserRoleData dx in _list)
            {
                if (dx.USERNAME.Equals(pre.USERNAME, StringComparison.OrdinalIgnoreCase))
                {
                    dd.USERNAME = "";
                    dd.TITLE = "";
                    dd.FIRST_NAME = "";
                    dd.LAST_NAME = "";
                    dd.EMAIL = "";
                    dd.REG_NO = "";
                    dd.GROUP_CD = 0;
                    dd.GROUP_NAME = "";
                    dd.ROLE_ID = "";
                }
                else
                {
                    dd.USERNAME = dx.USERNAME;
                    dd.TITLE = dx.TITLE;
                    dd.FIRST_NAME = dx.FIRST_NAME;
                    dd.LAST_NAME = dx.LAST_NAME;
                    dd.EMAIL = dx.EMAIL;
                    dd.REG_NO = dx.REG_NO;
                    dd.GROUP_CD = dx.GROUP_CD;
                    dd.GROUP_NAME = dx.GROUP_NAME;
                    dd.ROLE_ID = dx.ROLE_ID;
                }

                x.Put(++rownum,
                    dd.REG_NO,
                    dd.USERNAME,
                    dd.TITLE,
                    String.Format("{0} {1}", dd.FIRST_NAME, dd.LAST_NAME),
                    dd.EMAIL,
                    dx.POSITION_NAME,
                    dx.DIVISION_NAME,
                    dx.DEPARTMENT_NAME,
                    dx.SECTION_NAME,                   
                    (dd.GROUP_NAME != null) ? dd.GROUP_NAME : dd.GROUP_CD.ToString(),
                    dd.ROLE_ID);

                pre.USERNAME = dx.USERNAME;
            }
            x.PutTail();
            return x.GetBytes();
        }
    }
}
