﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using DataLayer.Model;

namespace BusinessLogic._20MasterData
{
    public class RoleAuthorizationLogic : LogicBase
    {
        public List<RoleMasterData> SearchRoles(string _id)
        {
            using (ContextWrap c = new ContextWrap())
            {
                var _db = c.db;
                var q = (from r in _db.TB_M_SYSTEM
                         where r.SYSTEM_TYPE == "ROLE_ID"
                         select new RoleMasterData()
                         {
                             ROLE_ID = r.SYSTEM_CD,
                             ROLE_NAME = r.SYSTEM_VALUE_TXT.Trim(),
                             CREATED_DT = r.CREATED_DT,
                             CREATED_BY = r.CREATED_BY,
                             CHANGED_DT = r.CHANGED_DT,
                             CHANGED_BY = r.CHANGED_BY
                         });
                if (!string.IsNullOrEmpty(_id))
                {
                    q = q.Where(s => s.ROLE_ID.Contains(_id));
                }

                return q.ToList<RoleMasterData>();
            }
        }


        public List<RoleScreenData> SearchScreen(string _id, string _screen)
        {
            using (ContextWrap c = new ContextWrap())
            {
                var _db = c.db;
                var s = (from q in _db.vw_RoleScreen
                         orderby q.ROLE_ID, q.LevelID
                         select new RoleScreenData()
                         {
                             ROLE_ID = q.ROLE_ID,
                             SCREEN_ID = q.SCREEN_ID,
                             CAPTION = q.CAPTION,
                             object_count = q.object_count,
                             LevelID = q.LevelID,
                             Indent = q.Indent
                         });

                if (!string.IsNullOrEmpty(_id))
                {
                    s = s.Where(x => x.ROLE_ID.Equals(_id));
                }
                if (!string.IsNullOrEmpty(_screen))
                {
                    s = s.Where(x => x.SCREEN_ID.Equals(_screen));
                }
                return s.ToList();
            }
        }

        public List<RoleAuthorizationData> SearchObject(string _id, string _screen, string _o)
        {
            using (ContextWrap c = new ContextWrap())
            {
                var _db = c.db;
                var o = (from q in _db.vw_RoleDetail
                         where (q.OBJECT_ID != null) && (q.OBJECT_ID.Length > 0)
                         select new RoleAuthorizationData()
                         {
                             ROLE_ID = q.ROLE_ID,
                             SCREEN_ID = q.SCREEN_ID,
                             OBJECT_ID = q.OBJECT_ID
                         });

                if (!string.IsNullOrEmpty(_id))
                {
                    o = o.Where(x => x.ROLE_ID.Equals(_id));
                }
                if (!string.IsNullOrEmpty(_screen))
                {
                    o = o.Where(x => x.SCREEN_ID.Equals(_screen));
                }
                if (!string.IsNullOrEmpty(_o))
                {
                    o = o.Where(x => x.OBJECT_ID.Contains(_o));
                }
                return o.Distinct().ToList();
            }
        }

        public object SearchScreenObject(string _screen)
        {
            using (ContextWrap c = new ContextWrap())
            {
                var _db = c.db;
                var o = (from q in _db.vw_ScreenObject
                         where q.SCREEN_ID.Equals(_screen)
                         select new { OBJECT_ID = q.OBJECT_ID ?? "" }).ToList();
                return o;
            }
        }


        public bool PutMenu(ScreenData s, int o)
        {
            using (ContextWrap c = new ContextWrap()) 
            try
            {
                var _db = c.db;
                _db.PutMenu(s.MENU_ID, s.PARRENT_ID, s.SCREEN_ID, s.CAPTION, s.PATH, 0);
            }
            catch (Exception ex)
            {
                Handle(ex);
                return false;
            }
            return true;
        }

        private bool Op(RoleAuthorizationData r, UserData u, int o)
        {
            using (ContextWrap c = new ContextWrap()) 
            try
            {
                var _db = c.db;
                _db.PutRoleDetail(r.ROLE_ID, r.SCREEN_ID, r.OBJECT_ID, o);
            }
            catch (Exception ex)
            {
                Handle(ex);
                return false;
            }
            return true;
        }

        public bool Delete(List<RoleMasterData> l)
        {
            bool r = true;
            using (ContextWrap c = new ContextWrap())
            try
            {
                var _db = c.db;
                foreach (RoleMasterData x in l)
                {
                    var q = (from M in _db.TB_M_SYSTEM
                             where M.SYSTEM_TYPE == "ROLE_ID"
                                && M.SYSTEM_CD == x.ROLE_ID
                             select M);
                    TB_M_SYSTEM s = q.FirstOrDefault();
                    if (s != null)
                    {
                        _db.DeleteObject(s);
                    }
                    else
                    {
                        r = false;
                    }
                    if (!r) break;
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                Handle(ex);
                r = false;
            }

            return r;

        }

        public bool Delete(List<RoleAuthorizationData> l)
        {
            bool r = true;
            foreach (RoleAuthorizationData x in l)
            {
                r = r && Op(x, null, -1);
                if (!r) break;
            }
            return r;
        }

        public bool AddRow(RoleAuthorizationData r, UserData user)
        {
            return Op(r, user, 1);
        }

        public bool EditRow(RoleAuthorizationData r, UserData user)
        {
            return Op(r, user, 0);
        }

        public bool AddRow(RoleMasterData r, UserData u)
        {
            using (ContextWrap c = new ContextWrap()) 
            try
            {
                var _db = c.db;
                var q = (from M in _db.TB_M_SYSTEM
                         where M.SYSTEM_TYPE == "ROLE_ID"
                            && M.SYSTEM_CD == r.ROLE_ID
                         select M);

                TB_M_SYSTEM s = q.FirstOrDefault();

                if (s == null)
                {

                    int seq = (from ii in _db.TB_M_SYSTEM
                               select ii.ID).Max();
                    ++seq;

                    s = new TB_M_SYSTEM()
                    {
                        ID=seq,
                        SYSTEM_TYPE = "ROLE_ID",
                        SYSTEM_CD = r.ROLE_ID,
                        SYSTEM_VALUE_TXT = r.ROLE_NAME,
                        CREATED_BY = u.USERNAME,
                        CREATED_DT = DateTime.Now
                    };
                    _db.AddToTB_M_SYSTEM(s);
                }
                else
                {
                    s.SYSTEM_VALUE_TXT = r.ROLE_NAME;
                    s.CHANGED_BY = u.USERNAME;
                    s.CHANGED_DT = DateTime.Now;
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                Handle(ex);
                return false;
            }
            return true;
        }


        public byte[] Get(string _username, string _imagePath, List<RoleAuthorizationData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("Role_Authorization");
            x.PutHeader("Role Authorization",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Role|Screen|Object",
                _imagePath);
            int i = 0;
            foreach (RoleAuthorizationData dx in _list)
            {
                x.Put(++i, dx.ROLE_ID, dx.SCREEN_ID, dx.OBJECT_ID);
            }
            x.PutTail();
            return x.GetBytes();
        }

    }
}
