﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.Objects;
using Common.Data;
using Common.Data._20MasterData;
using Common.Function;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Diagnostics;
using DataLayer.Model;
using BusinessLogic.CommonLogic;
using BusinessLogic;
using Common.Data.SAPData;
using Dapper;

namespace BusinessLogic._20MasterData
{
    public class VendorLogic : LogicBase
    {
        #region Search Inquiry

        public List<VendorData> Search(string _vendorCd, int _vendorGroupCd, string _searchTerms, string _name, string _divCd, string _sCount, string _sCountTo)
        {
            return Qx<VendorData>("GetVendorDataSearch",
                    new
                    {
                        code = _vendorCd,
                        group = _vendorGroupCd,
                        terms = _searchTerms,
                        name = _name,
                        div = _divCd,
                        from = _sCount,
                        to = _sCountTo
                    }
                    ).ToList();
        }

        #endregion

        #region delete
        public bool Delete(List<string> listOfCode)
        {
            bool _result = false;

            try
            {
                int saveResult = 0;
                DynamicParameters d = new DynamicParameters();
                d.Add("@list", CommonFunction.CommaJoin(listOfCode));
                d.Add("@result", saveResult, DbType.Int32, ParameterDirection.Output);

                Exec("sp_DeleteVendors", d);
                saveResult = d.Get<int>("@result");

                _result = saveResult == 1;
            }
            catch (Exception ex)
            {
                Handle(ex);
            }

            return _result;
        }
        #endregion

        public string InternalGroup()
        {
            return Logic.Sys.GetText("VENDOR_GROUP_CODE", "VENDOR_GROUP_INTERNAL");
        }

        #region Add
        public bool InsertVendor(VendorData v
            , UserData userData)
        {
            bool result = false;
            try
            {
                if (v.DIVISION_NAME.isEmpty())
                {
                    v.DIVISION_NAME = Qx<string>("getDivisionName", new { divisionId = v.DIVISION_ID }).FirstOrDefault();
                }
                VendorPostResult r = Post(v, 0, userData.USERNAME);

                result = (r != null) && r.MESSAGE.Equals("SUCCESS");

                if (result)
                {
                    int saveResult = 0; 

                    DynamicParameters d = new DynamicParameters();
                    d.Add("@VENDOR_CD", r.VENDOR_NO);
                    d.Add("@VENDOR_NAME", v.VENDOR_NAME);
                    d.Add("@SEARCH_TERMS", v.SEARCH_TERMS);
                    d.Add("@CONTACT_PERSON", v.DIVISION_ID);
                    d.Add("@RESULT", saveResult, DbType.Int32, ParameterDirection.Output);

                    Exec("sp_UpdateVendor", d);

                    saveResult = d.Get<int>("@RESULT");
                    result = saveResult == 1;
                }
                else
                {
                    throw new Exception("SAP Vendor Post failed");
                }

            }
            catch (Exception ex)
            {
                result = false;
                Handle(ex);
            }
            return result;
        }
        #endregion

        #region Edit
        public bool EditVendor(
            VendorData v
            , UserData userData)
        {
            bool result = false;
            try
            {
                int saveResult = 0;

                DynamicParameters d = new DynamicParameters();
                d.Add("@VENDOR_CD", v.VENDOR_CD);
                d.Add("@VENDOR_NAME", v.VENDOR_NAME);
                d.Add("@SEARCH_TERMS", v.SEARCH_TERMS);
                d.Add("@CONTACT_PERSON", v.DIVISION_ID);
                d.Add("@RESULT", saveResult, DbType.Int32, ParameterDirection.Output);

                Exec("sp_UpdateVendor", d);

                saveResult = d.Get<int>("@RESULT");

                result = saveResult == 1;
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return result;
        }
        #endregion

        #region Get String Builder

        public byte[] Get(string _username, string _imagePath, List<VendorData> _list, bool hasSuspense = false)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("Vendor");
            x.PutHeader("Vendor",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Vendor Code|Vendor Name|Vendor Group|Search Terms|Division|" +
                ((hasSuspense) ? "Suspense Count|" : "") +
                "Created{Date|By}",
                _imagePath);
            int rownum = 0;

            if (hasSuspense)
            {
                foreach (VendorData dx in _list)
                {
                    x.Put(++rownum,
                        dx.VENDOR_CD,
                        dx.VENDOR_NAME,
                        dx.VENDOR_GROUP_NAME,
                        dx.SEARCH_TERMS,
                        GetDivisionName(dx.CONTACT_PERSON),
                        dx.SUSPENSE_COUNT,
                        dx.CREATED_DT.fmt(StrTo.TO_MINUTE), dx.CREATED_BY
                        );
                }
            }
            else
            {
                foreach (VendorData dx in _list)
                {
                    x.Put(++rownum,
                        dx.VENDOR_CD,
                        dx.VENDOR_NAME,
                        dx.VENDOR_GROUP_NAME,
                        dx.SEARCH_TERMS,
                        GetDivisionName(dx.CONTACT_PERSON),
                        dx.CREATED_DT.fmt(StrTo.TO_MINUTE), dx.CREATED_BY
                        );
                }
            }
            x.PutTail();
            return x.GetBytes();
        }

        #endregion

        public VendorPostResult Post(VendorData vendor, int id, string User)
        {
            VendorInput vi = new VendorInput()
            {
                DIVISION_NAME = vendor.DIVISION_NAME,
                DIVISION = vendor.DIVISION_ID,
                NAME = vendor.VENDOR_NAME,
                SEARCH_TERM = vendor.SEARCH_TERMS
            };

            return Logic.SAP.PostVendor(vi, id, User);
        }

        public string GetDivisionName(string divisionId)
        {
            return Qx<string>("getDivisionName", new { divisionId = divisionId }).FirstOrDefault();
        }
    }
}
