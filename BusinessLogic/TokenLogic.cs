﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;
using Common.Data;
using Common.Function;
using System.Threading;


namespace BusinessLogic
{
    public class TokenLogic
    {
        public bool SaveToken(string SYSTEM_KEY, string TOKEN)
        {
            bool kode = false;
            TB_T_TOKEN data = new TB_T_TOKEN();
            data.SYSTEM_KEY = SYSTEM_KEY;
            data.TOKEN = TOKEN;
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                _db.AddToTB_T_TOKEN(data);
                _db.SaveChanges();
            }
            kode = true;
            return kode;
        }
        public string GetToken(string SYSTEM_KEY)
        {
            bool kode = false;
            RandomStringNumber random = new RandomStringNumber(16);
            string TOKEN = "";
            int iteration = 0;
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                while (kode != true && iteration < 5)
                {
                    TOKEN = random.GetRandomStringNumber();
                    if (_db.TB_T_TOKEN.Where(i => i.TOKEN == TOKEN).ToList().Count == 0)
                    {
                        kode = SaveToken(SYSTEM_KEY, TOKEN);
                    }
                    else
                    {
                        TOKEN = "";
                        Thread.Sleep(500);
                    }
                    iteration = iteration + 1;
                }
            }
            if (kode == false)
            {
                TOKEN = "";
            }
            return TOKEN;
        }
        public bool isValidToken(string SYSTEM_KEY, string TOKEN)
        {
            bool kode = false;
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = _db.TB_T_TOKEN.Where(i => i.TOKEN == TOKEN && i.SYSTEM_KEY == SYSTEM_KEY).FirstOrDefault();
                if (q != null)
                {
                    _db.DeleteObject(q);
                    _db.SaveChanges();
                    kode = true;
                }
            }
            return kode;
        }
    }
}
