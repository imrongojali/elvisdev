﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.CommonLogic
{
    [Serializable]
    public class CodeConstant
    {
        public string Code { set; get; }
        public string Description { set; get; }
    }
}
