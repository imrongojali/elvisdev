﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Data.Common;
using System.Data.EntityClient;
using Common;
using Common.Data;
using Common.Function;
using DataLayer.Model;
using Dapper; 

namespace BusinessLogic
{
    public class MassActLogic: LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        protected string[] AuthorizedRoles = new string[] { };

        public int Do(string refnos, string note, int act, string username, int pid=0) 
        {
            int r = -1;
            if (pid == 0)
                pid = logic.PROCESS_ID;
            try
            {
                r = Qx<int>("SetMassAct", 
                        new
                        {
                            REFF_NO = refnos,
                            ACT_NOTE = note,
                            ACT_TAKEN = act,
                            ACTOR = username,
                            PID = pid
                        }).FirstOrDefault();
            }
            catch (Exception e)
            {
                LoggingLogic.err(e);
            }
            return r;
        }

        public int Done(string reffno, int MassActId, string username)
        {
            int r = -1;
            try
            {
                r = Qx<int>("SetMassActDone", new
                {
                    REFF_NO = reffno,
                    ACT_ID = MassActId,
                    ACTOR = username
                }).FirstOrDefault();
            }
            catch (Exception e)
            {
                LoggingLogic.err(e);                
            }
            return r;
        }
    }
}
