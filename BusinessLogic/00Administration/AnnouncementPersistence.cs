using BusinessLogic;
using Common;
using Common.Data;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;

namespace BusinessLogic._00Administration
{
	public class AnnouncementPersistence
	{
		public const string SEPARATOR_VALUE_STRING = ";";

		public AnnouncementPersistence()
		{
		}

		public List<Division> listDivision()
		{
			List<Division> lstDivision = new List<Division>();
			using (ContextWrap co = new ContextWrap())
			{
				IQueryable<vw_Division> query = 
					from t in co.db.vw_Division
					select t;
				if (query != null)
				{
					List<vw_Division> queryResult = query.ToList<vw_Division>();
					if (queryResult.Any<vw_Division>())
					{
						foreach (vw_Division data in queryResult)
						{
							Division division = new Division()
							{
								DivisionID = data.DIVISION_ID,
								DivisionName = data.DIVISION_NAME
							};
							lstDivision.Add(division);
						}
					}
				}
			}
			return lstDivision;
		}

		public List<UserPosition> listUserPosition()
		{
			List<UserPosition> lstPosition = new List<UserPosition>();
			using (ContextWrap co = new ContextWrap())
			{
				IQueryable<vw_UserPosition> query = 
					from t in co.db.vw_UserPosition
					select t;
				if (query != null)
				{
					List<vw_UserPosition> queryResult = query.ToList<vw_UserPosition>();
					if (queryResult.Any<vw_UserPosition>())
					{
						foreach (vw_UserPosition data in queryResult)
						{
							UserPosition userPosition = new UserPosition()
							{
								Class = data.CLASS,
								PositionID = data.POSITION_ID,
								PositionName = data.POSITION_NAME
							};
							lstPosition.Add(userPosition);
						}
					}
				}
			}
			return lstPosition;
		}

		public void save(AnnouncementMaster form)
		{
			using (ContextWrap co = new ContextWrap())
			{
				ELVIS_DBEntities DB = co.db;
				try
				{
					if (form.AnnouncementCode == 0)
					{
						form.AnnouncementCode = 1;
						IQueryable<int> indexQuery = 
							from t in DB.TB_M_INFORMATION
							select t.INFORMATION_CD;
						if (indexQuery.Any<int>())
						{
							form.AnnouncementCode = indexQuery.Max<int>() + 1;
						}
					}
					List<object> selectedDivision = form.DivisionID;
					List<object> selectedPosition = form.PositionID;
					string strDivision = "";
					foreach (object div in selectedDivision)
					{
						strDivision = string.Concat(strDivision, div, ";");
					}
					string strPosition = "";
					foreach (object pos in selectedPosition)
					{
						strPosition = string.Concat(strPosition, pos, ";");
					}
					IQueryable<TB_M_INFORMATION> query = 
						from t in DB.TB_M_INFORMATION
						where t.INFORMATION_CD == form.AnnouncementCode
						select t;
					if (query != null)
					{
						bool updating = false;
						TB_M_INFORMATION tblInformation = new TB_M_INFORMATION();
						List<TB_M_INFORMATION> queryResult = query.ToList<TB_M_INFORMATION>();
						if (queryResult.Any<TB_M_INFORMATION>())
						{
							updating = true;
							tblInformation = queryResult[0];
						}
						tblInformation.INFORMATION_CD = form.AnnouncementCode;
						tblInformation.INFORMATION_DESC = form.Description;
						tblInformation.INFORMATION_TITLE = form.Title;
						tblInformation.POSITION_LIST = strPosition;
						tblInformation.DIVISION_LIST = strDivision;
						tblInformation.EXPIRE_DATE = form.ExpireDate.Value;
						tblInformation.RELEASE_DATE = form.ReleaseDate.Value;
						UserData user = form.UserData;
						DateTime today = DateTime.Now;
						if (!updating)
						{
							tblInformation.CREATED_BY = user.USERNAME;
							tblInformation.CREATED_DT = today;
						}
						else
						{
							tblInformation.CHANGED_BY = user.USERNAME;
							tblInformation.CHANGED_DT = new DateTime?(today);
						}
						if (!updating)
						{
							DB.TB_M_INFORMATION.AddObject(tblInformation);
						}
					}
					DB.SaveChanges();
				}
				catch (Exception exception)
				{
					LoggingLogic.err(exception);
					throw;
				}
			}
		}

		public List<Announcement> search()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("select ");
			sb.AppendLine("     INFORMATION_CD as Code, ");
			sb.AppendLine("     INFORMATION_TITLE as Title, ");
			sb.AppendLine("     INFORMATION_DESC as Description, ");
			sb.AppendLine("     CREATED_BY as CreatedBy, ");
			sb.AppendLine("     RELEASE_DATE as ReleaseDate, ");
			sb.AppendLine("     EXPIRE_DATE as ExpireDate ");
			sb.AppendLine("from TB_M_INFORMATION ");
			List<Announcement> l = new List<Announcement>();
			using (ContextWrap co = new ContextWrap())
			{
				l = co.db.ExecuteStoreQuery<Announcement>(sb.ToString(), new object[0]).ToList<Announcement>();
			}
			return l;
		}
	}
}