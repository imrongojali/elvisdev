using BusinessLogic;
using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Function;
using Dapper;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace BusinessLogic._00Administration
{
	public class LogLogic : LogicBase
	{
		public List<LogData> List(int id = 0, int start = 1, int limit =-1, string filter = "")
		{
            List<LogData> r = Qx<LogData>("GetLog",
                new { id = id, limit = limit, start = start, filter = filter }).ToList();
			return r;
		}		
	}
}