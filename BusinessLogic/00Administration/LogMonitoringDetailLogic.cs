using BusinessLogic;
using Common.Data;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace BusinessLogic.Admin
{
	public class LogMonitoringDetailLogic
	{
		private object retVal;

		private List<LogDetailData> _LogData;

		public LogMonitoringDetailLogic()
		{
		}

		public string GetCSS()
		{
			return "\r\n            <style type=text/css>\r\n                body \r\n                {\r\n                    font-family: Arial Verdana 'Times New Roman';\r\n                }\r\n            \r\n                .right\r\n                {\r\n                    text-align:right;  \r\n                    mso-font-charset:1;\r\n\t                mso-number-format:'\\@';\r\n \r\n                }\r\n            </style>";
		}

		public string getLogDetail(string startDT, string endDT, string procID, string status, string userID, string funcName)
		{
			string _logDetailBuilder = this.GetCSS();
			string str = _logDetailBuilder;
			string[] strArrays = new string[] { str, "<table>\r\n                    <tr><td></td><td><h4>Log Monitoring Details</h4><br /><br /></td></tr>         \r\n                    <tr> <td></td>\r\n                    <td>\r\n                    <table>\r\n                        <tr>            \r\n                        <td>Start Date </td> <td>: ", startDT, "</td>  \r\n                        </tr>          \r\n                        <tr>\r\n                        <td>End Date </td> <td>: ", endDT, "</td>\r\n                        </tr>\r\n                        <tr>\r\n                        <td>Proccess ID </td> <td>: ", procID, "</td>\r\n                        </tr>    \r\n                        <tr>\r\n                        <td>Status </td> <td>: ", status, "</td>\r\n                        </tr>\r\n                        <tr>\r\n                        <td>User ID </td> <td>: ", userID, "</td>\r\n                        </tr>\r\n                        <tr>\r\n                        <td>Function Name </td> <td>: ", funcName, "</td>\r\n                        </tr>\r\n                    </table>\r\n                    <br />\r\n                    </td>            \r\n                    </tr>  \r\n\r\n                    " };
			_logDetailBuilder = string.Concat(strArrays);
			if (this._LogData == null || this._LogData.Count <= 0)
			{
				_logDetailBuilder = string.Concat(_logDetailBuilder, "\r\n                        <tr> <td>D</td>              \r\n                        <td>\r\n                            <table border='1px'>\r\n                              <tr>                \r\n                                <td style='text-align:center;'>No</td>\r\n                                <td style='text-align:center;'>Category</td>\r\n                                <td style='text-align:center;'>WBS Code</td>\r\n                                <td style='text-align:center;'>WBS Code Description</td>\r\n                                <td style='text-align:center;'>Ifem</td>\r\n                                <td style='text-align:center;'>Expense Category</td>\r\n                                <td style='text-align:center;'>Fix Amount</td>\r\n                                <td style='text-align:center;'>Variable Amount</td>\r\n                              </tr>\r\n                              <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>\r\n                            </table>\r\n                        </td>\r\n                        </tr>");
			}
			else
			{
				_logDetailBuilder = string.Concat(_logDetailBuilder, "\r\n                    <tr> <td></td>              \r\n                        <td>\r\n                            <table border='1px'>\r\n                              <tr>                \r\n                                <td style='text-align:center;'>Seq.No</td>\r\n                                <td style='text-align:center;'>Message Date Time</td>\r\n                                <td style='text-align:center;'>Location</td>\r\n                                <td style='text-align:center;'>Message Type</td>\r\n                                <td style='text-align:center;'>Message ID</td>\r\n                                <td style='text-align:center;'>Message Detail</td>\r\n                              </tr>\r\n                            </table>\r\n                        </td>\r\n                        </tr>");
				for (int x = 0; x < this._LogData.Count; x++)
				{
					LogDetailData _logDetails = this._LogData[x];
					string str1 = _logDetailBuilder;
					string[] strArrays1 = new string[] { str1, "\r\n                        <tr> <td></td>\r\n                            <td>\r\n                               <table border='1px'>\r\n                                <tr>\r\n                                    <td valign='top'>", _logDetails.seq_no.ToString(), "</td>\r\n                                    <td valign='top'>", _logDetails.err_dt.ToString(), "</td>\r\n                                    <td valign='top'>", _logDetails.err_location, "</td>\r\n                                    <td valign='top'>", _logDetails.msg_type, "</td>\r\n                                    <td valign='top'>", _logDetails.msg_id, "</td>\r\n                                    <td valign='top'>", _logDetails.err_message, "</td>\r\n                                </tr>\r\n                               </table>\r\n                            </td>\r\n                        </tr>  " };
					_logDetailBuilder = string.Concat(strArrays1);
				}
			}
			return _logDetailBuilder;
		}

		public List<LogDetailData> newLogDetailList(string processID, ref ErrorData Err)
		{
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities _db = co.db;
					int num = Convert.ToInt32(processID);
					List<TB_R_LOG_D> q = (
						from i in _db.TB_R_LOG_D
						where i.process_id == num
						select i).ToList<TB_R_LOG_D>();
					if (q == null)
					{
						Err = new ErrorData()
						{
							ErrID = 2,
							ErrMsgID = "",
							ErrMsg = ""
						};
					}
					else if (q.Count > 0)
					{
						this._LogData = new List<LogDetailData>();
						for (int i = 0; i < q.Count; i++)
						{
							LogDetailData data = new LogDetailData()
							{
								seq_no = q[i].seq_no,
								err_dt = q[i].err_dt,
								err_location = q[i].err_location,
								msg_type = q[i].msg_type,
								msg_id = q[i].msg_id,
								err_message = q[i].err_message
							};
							this._LogData.Add(data);
						}
					}
				}
				catch (Exception exception)
				{
					Exception ex = exception;
					Err = new ErrorData()
					{
						ErrID = 2,
						ErrMsgID = "",
						ErrMsg = ex.Message
					};
				}
			}
			return this._LogData;
		}

		public object searchLog(string procID)
		{
			object obj;
			int num = Convert.ToInt32(procID);
			using (ContextWrap co = new ContextWrap())
			{
				List<TB_R_LOG_D> q = (
					from i in co.db.TB_R_LOG_D
					where i.process_id == num
					select i).ToList<TB_R_LOG_D>();
				this.retVal = q;
				obj = this.retVal;
			}
			return obj;
		}
	}
}