using Common.Data;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BusinessLogic._00Administration
{
	[Serializable]
	public class AnnouncementMaster
	{
		public int AnnouncementCode
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public List<object> DivisionID
		{
			get;
			set;
		}

		public DateTime? ExpireDate
		{
			get;
			set;
		}

		public List<object> PositionID
		{
			get;
			set;
		}

		public DateTime? ReleaseDate
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public Common.Data.UserData UserData
		{
			get;
			set;
		}

		public AnnouncementMaster()
		{
		}

		public void reset()
		{
			this.AnnouncementCode = 0;
			this.Title = null;
			this.Description = null;
			this.DivisionID = null;
			this.PositionID = null;
			this.ExpireDate = null;
			this.ReleaseDate = null;
		}
	}
}