﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Diagnostics;
using System.Reflection;
using Common;

namespace BusinessLogic
{
    public class InOutMark : ITick
    {
        private LoggingLogic _log = null;
        private LoggingLogic Log
        {
            get
            {
                if (_log == null)
                {
                    _log = new LoggingLogic();
                }
                return _log;
            }
        }

        private int pid = 0;

        private Stack<string> stak = null;
        private Stack<string> St
        {
            get
            {
                if (stak == null)
                {
                    stak = new Stack<string>();
                }
                return stak;
            }
        }

        private int level = 0;
        public int Level
        {
            get
            {
                return level;
            }

            set
            {
                level = value;
            }
        }

        private static string Tab(int n)
        {
            string r = "";
            for (int i = 0; i < n; i++)
            {
                r += "     ";
            }
            return r;
        }

        public void In(string w, params object[] x)
        {
            Op(0, string.Format(w, x));
        }        

        public void Out(string w, params object[] x)
        {
            if (!string.IsNullOrEmpty(w))
            {
                w = string.Format(w, x);
                if (St.Contains(w))
                {
                    string y = null;
                    do
                    {
                        y = Op(1, w);
                    }
                    while (!y.Equals(x) && St.Count > 0);
                }
            }
            else
            {
                Op(1, w);
            }
        }

        public void Spin(string w, params object[] x)
        {
            Op(-1, string.Format(w, x));
        }

        public string Op(int op = 0, string x= null) 
        {
            string z= "";
            if (op == 0)
            {
                St.Push(x);
                z = x;
                
                pid = Log.Log("INF", Tab(level) + x , "In", "io", "", pid);
                level++;
                
            }
            else if (op > 0)
            {
                if (St.Count > 0)
                    z = St.Pop();
                else
                    z = x;
                level--;
                Log.Log("INF", Tab(level) + z, "Out", "io", "", pid);
            }
            else
            {
                Log.Log("INF", Tab(level) + x, "Spin", "io", "", pid);
            }
            return z;
        }


        public void Trace(string w)
        {
            Log.Log("INF", LoggingLogic.Trace(null, ".InOutMark,.ITick,.Ticker") , w, "io", "", pid);
        }
    }
}
