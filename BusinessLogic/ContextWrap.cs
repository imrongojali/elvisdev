﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using DataLayer.Model;

namespace BusinessLogic
{
    public class ContextWrap : IDisposable
    {
        protected ELVIS_DBEntities edb = null;
        protected EntityConnection ece = null;
        protected Dictionary<string, object> dbdict = null;
        protected ITick itick = null;

        public ContextWrap()
        {
            
        }

        public ELVIS_DBEntities db
        {
            get
            {
                return DBElvis();
            }
            set
            {
                edb = value;
                if (edb != null)
                {
                    ece = (EntityConnection)edb.Connection;
                }
            }
        }

        public ELVIS_DBEntities DBElvis(EntityConnection co = null)
        {
            if (edb == null)
            {
                
                if (co == null)
                {
                    ece = new EntityConnection("name=ELVIS_DBEntities");
                }
                else
                {
                    
                    ece = co;
                }
                if (ece.State == ConnectionState.Closed)
                {
                    
                    ece.Open();
                }
                
                edb = new ELVIS_DBEntities(ece);
                
            }
            return edb;
        }

        protected IDbConnection _DB = null;
        public IDbConnection DB
        {
            get
            {

                if (_DB == null)
                {
                    
                    _DB = LogicFactory.Get().DB;
                    Add(_DB);
                }
                
                    
                if ((_DB.State == ConnectionState.Closed) || (_DB.State == ConnectionState.Broken))
                {
                    
                    _DB.Open();
                }
                
                return _DB;
            }
        }

        public IDbConnection NewDB()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString);                        
        }

        private static void Disposal(EntityConnection e, ObjectContext db)
        {
            if (db != null)
            {
                if (db.Connection != null && db.Connection.State == ConnectionState.Open)
                {
                    db.Connection.Close();
                }
                db.Dispose();
                db = null;
                try
                {
                    e.Dispose();
                }
                catch (ObjectDisposedException ode)
                {
                    Debug.WriteLine(ode.Message);
                    e = null;
                }
            }
        }

        private static void Disposal(IDbConnection x)
        {
            if (x != null)
            {
                x.Dispose();
            }
        }

        public void Dispose()
        {
            Disposal(ece, edb);
            if (dbdict != null)
            {
                foreach (var n in dbdict)
                {
                    Disposal(n.Value as IDbConnection);
                }
                dbdict.Clear();
            }
            _DB = null;
            
        }

        private void Add(object o)
        {
            if (dbdict == null)
            {
                dbdict = new Dictionary<string, object>() ;
            }
            int i = LogicFactory.Get().IDX;
            string oKey = o.GetType().Name + i.ToString();
            
            dbdict.Add(oKey, o);            
        }
    }
}
