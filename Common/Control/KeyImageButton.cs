﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Common.Control
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:KeyImageButton runat=\"server\"></{0}:KeyImageButton>")]
    public class KeyImageButton : ImageButton, IPostBackEventHandler
    {
        public KeyImageButton(): base()
        {
        }
        
        public string Key { get; set; }

        public void RaisePostBackEvent(string eventArgument)
        {
            base.RaisePostBackEvent(eventArgument);
        }        

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
    }
}
