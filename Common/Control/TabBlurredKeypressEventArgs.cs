﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Control
{
    public class TabBlurredKeypressEventArgs : EventArgs
    {
        public string KeyCode { set; get; }
        public bool HasControlKey { set; get; }
        public bool HasAlternateKey { set; get; }
    }
}
