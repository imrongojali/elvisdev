﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.ComponentModel;

namespace Common.Data._50Invoice
{
    [Serializable]
    public class InvoiceDetailData
    {
        #region Header 
        
        public string INVOICE_NO { get; set; }
        public DateTime INVOICE_DATE { get; set; }
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public string DIVISION_ID { get; set; }
        public int STATUS_CD { get; set; }
        public string STATUS_NAME { get; set; }
        public string TRANSACTION_TYPE_CD { get; set; }
        public string TRANSACTION_TYPE_NAME { get; set; }
        #endregion

        #region Detail
        public int GRID_NO { get; set; }
        public int SEQ_NO { get; set; }
        public int ITEM_TRANSACTION_CD { get; set; }
        public string ITEM_TRANSACTION { get; set; }
        public string ITEM_DESCRIPTION { get; set; }
        public string CURRENCY_CD { get; set; }
        public string AMOUNT { get; set; }
        public string COST_CENTER_CD { get; set; }
        public string WBS_NO { get; set; }
        public string TAX_CD { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        #endregion

    }

}
