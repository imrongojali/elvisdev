﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class LogHeaderData
    {
        private int _process_id;
        public int process_id
        {
            get { return _process_id; }
            set { _process_id = value; }
        }

        private DateTime _process_dt;
        public DateTime process_dt
        {
            get { return _process_dt; }
            set { _process_dt = value; }
        }

        private string _function_id;
        public string function_id
        {
            get { return _function_id; }
            set { _function_id = value; }
        }

        private string _process_sts;
        public string process_sts
        {
            get { return _process_sts; }
            set { _process_sts = value; }
        }

        private string _user_id;
        public string user_id
        {
            get { return _user_id; }
            set { _user_id = value; }
        }

        private DateTime _end_dt;
        public DateTime end_dt
        {
            get { return _end_dt; }
            set { _end_dt = value; }
        }

        private string _remarks;
        public string remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
    }
}
