﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class DocumentCountSummaryData
    {
        public int id { get; set; } 
        public string DIV_PIC { get; set; }
        public string PIC { get; set; }
        public string POSITION_NAME { get; set; }
        public int? STATUS_CD { get; set; } 
        public string STATUS_NAME { get; set; }
        public int DOCUMENT_COUNT { get; set; }
        public int? PERSON_TOTAL { get; set; }
        public int? TOTAL_DOC_AGE { get; set; }
    }
}
