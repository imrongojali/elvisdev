﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class UserPosition
    {
        public string PositionID { set; get; }
        public string PositionName { set; get; }
        public byte Class { set; get; }
    }
}
