﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class RVCheckResult
    {
        public string RV_NO { get; set; }
        public string RV_YEAR { get; set; }
        public string ITEM_NO { get; set; }
        public string MESSAGE_TEXT { get; set; }
        public int SAP_DOC_NO { get; set; }
        public int SAP_DOC_YEAR { get; set; }
        public string STATUS { get; set; } 
    }
}
