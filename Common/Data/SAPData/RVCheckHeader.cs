﻿using System;

namespace Common.Data.SAPData
{
    [Serializable]
    public class RVCheckHeader
    {
        public string RV_NO { get; set; }
        public string RV_YEAR { get; set; }
        public string ITEM_NO { get; set; }
        public string RV_DATE { get; set; } 
        public string RV_TYPE { get; set; }
        public string TRANS_TYPE { get; set; }
        
        public string VENDOR_NO { get; set; }
        public string POSTING_DT { get; set; }
        public string AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string TEXT { get; set; }
        public string METHODE_FLAG { get; set; }
        
        public string PV_NO_SUSPENSE { get; set; }
        public string PV_YEAR_SUSPENSE { get; set; }
        public string ITEM_NO_SUSPENSE { get; set; }

        public string FROM_CURR { get; set; }
        public string EXRATE_SUSPENSE { get; set; }
        public string EXCHANGE_RATE { get; set; }
        public string DIFF_EXRATE { get; set; }

        public string REFUND_FLAG { get; set; }
        public string TEMP_ACC_AMOUNT { get; set; }
        public string TEMP_ACC_ORI_AMOUNT { get; set; }
        
        public int? COST_CENTER_FLAG { get; set; } 
        
        public string STATUS { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_DOC_YEAR { get; set; }

        public DateTime? PV_DATE_SUSPENSE { get; set; }  // for generating fiscal year
    }
}
