﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class ShiftingBudget
    {
        public string BUDGET_NO { get; set; }
        public decimal SHIFTING_AMT { get; set; }
        public string SHIFTING_TYPE { get; set; }
        public string DOC_CHANGE_NO { get; set; }
        public string SHIFTING_DESC { get; set; }
    }
}
