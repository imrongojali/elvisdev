﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    public class VendorPostResult
    {
        public string STATUS { get; set; }
        public string VENDOR_NO { get; set; }
        public string MESSAGE { get; set;  }
    }
}
