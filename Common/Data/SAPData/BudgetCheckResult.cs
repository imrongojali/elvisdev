﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class BudgetCheckResult
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
        public string ORIGINAL { get; set; }
        public string AVAILABLE { get; set; }
    }
}
