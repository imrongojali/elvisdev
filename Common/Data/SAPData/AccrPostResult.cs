﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class AccrPostResult
    {
        public string TABLE_NAME = "BAPI_RETURN";
        public string TABLE_TYPE = "ZSTFI_RETURN";
        public string COMP_CODE { get; set; }
        public string FISC_YEAR { get; set; }
        public string DOC_NUMSAP { get; set; }
        public string DOC_NUMCORE { get; set; }
        public string MESSAGE { get; set; }
    }
}
