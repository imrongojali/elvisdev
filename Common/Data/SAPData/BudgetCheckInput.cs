﻿using System;
using System.Collections.Generic;

namespace Common.Data.SAPData
{
    public enum BudgetCheckStatus
    {
        Get = 0,
        Book = 1,
        Release = 2,
        Delete = 3, 
        Reverse = 4
    }

    [Serializable]
    public class BudgetCheckInput
    {
        public int DOC_NO { get; set; }//pv number
        public int DOC_YEAR { get; set; }
        public int DOC_STATUS { get; set; }
        public string WBS_NO { get; set; }
        public decimal AMOUNT { get; set; }
        public int Item_No { get; set; }
        public string Description { get; set; }
        public string System { get; set; }
        public string Reference { get; set; }
        public string ReferenceItemNo { get; set; }
        public string Status { get; set; }

    }
    public class BudgetPostInput
    {
        public string TransactionNo { get; set; }//pv number
        public int ItemNo { get; set; }
        public int Qty { get; set; }
        public string Description { get; set; }
        public string WBS { get; set; }
        public string Year { get; set; }
        public decimal Amount { get; set; }
        public string System { get; set; }
        public string Reference { get; set; }
        public string ReferenceItemNo { get; set; }
        public string Status { get; set; }

    }
    public class BudgetInfoTalend
    {
        public transactions transactions { get; set;}
        public string TransactionNo { get; set; }
        public string ItemNo { get; set; }
        public Guid UUID { get; set; }
        public string StatusMessage { get; set; }
        public string WBS { get; set; }
        public int Year { get; set; }
        public double Initial { get; set; }
        public double Commitment { get; set; }
        public double Actual { get; set; }
        public decimal Available { get; set; }
        public string Message { get; set; }

    }
    public partial class transactions
    {
        public string TransactionNo { get; set; }
        public string ItemNo { get; set; }
        public Guid UUID { get; set; }
        public string StatusMessage { get; set; }
        public string WBS { get; set; }
        public int Year { get; set; }
        public double Initial { get; set; }
        public double Commitment { get; set; }
        public double Actual { get; set; }
        public double Available { get; set; }

    }
}
