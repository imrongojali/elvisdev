﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class AccrPostData
    {
        public string USERNAME { get; set; }
        public string HEADER_TXT { get; set; }
        public string COMP_CODE { get; set; }
        public string DOC_DATE { get; set; }
        public string PSTNG_DATE { get; set; }
        public string TRANS_DATE { get; set; }
        public string FISC_YEAR { get; set; }
        public string FIS_PERIOD { get; set; }
        public string REF_DOC_NO { get; set; }
        public string DOC_TYPE { get; set; }
        public string GROUP_DOC { get; set; }
        public string DOC_REQUEST { get; set; }

        private List<AccrPostGL> _accountGl = new List<AccrPostGL>();
        public List<AccrPostGL> ACCOUNT_GL 
        { 
            get 
            {
                return _accountGl;
            }
        }
        private List<AccrPostVendor> _accountVendor = new List<AccrPostVendor>();
        public List<AccrPostVendor> ACCOUNT_VENDOR
        {
            get
            {
                return _accountVendor;
            }
        }
        private List<AccrPostCurr> _currencyAmount = new List<AccrPostCurr>();
        public List<AccrPostCurr> CURRENCYAMOUNT
        {
            get
            {
                return _currencyAmount;
            }
        }
    }

    public class AccrPostGL
    {
        public string TABLE_NAME = "ACCOUNT_GL";
        public string TABLE_TYPE = "ZSTFI_ACCOUNTGL";
        public string ITEMNO_ACC { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string ITEM_TEXT { get; set; }
        public string COMP_CODE { get; set; }
        public string BUS_AREA { get; set; }
        public string FIS_PERIOD { get; set; }
        public string FISC_YEAR { get; set; }
        public string PSTNG_DATE { get; set; }
        public string VALUE_DATE { get; set; }
        public string WBS_ELEMENT { get; set; }
        public string ORDERID { get; set; }
        public string FUNDS_CTR { get; set; }
        public string COSTCENTER { get; set; }
        public string TAX_CODE { get; set; }
    }

    public class AccrPostVendor
    {
        public string TABLE_NAME = "ACCOUNT_VENDOR";
        public string TABLE_TYPE = "ZSTFI_ACCOUNTVENDOR";
        public string ITEMNO_ACC { get; set; }
        public string VENDOR_NO { get; set; }
        public string BUS_AREA { get; set; }
        public string ITEM_TEXT { get; set; }
        public string BLINE_DATE { get; set; }
        public string PYMT_CUR { get; set; }
        public string PYMT_AMT { get; set; }
        public string SP_GL_IND { get; set; }
        public string TAX_CODE { get; set; }
        public string PYMT_METH { get; set; }
        public string PARTNER_BK { get; set; }
        public string PMNTTRMS { get; set; }
        public string REF_KEY_3 { get; set; }
    }

    public class AccrPostCurr
    {
        public string TABLE_NAME = "CURRENCYAMOUNT";
        public string TABLE_TYPE = "ZSTFI_CURRENCYAMOUNT";
        public string ITEMNO_ACC { get; set; }
        public string CURRENCY { get; set; }
        public string AMT_DOCCUR { get; set; }
        public string DEBIT_CREDIT { get; set; }
    }
}
