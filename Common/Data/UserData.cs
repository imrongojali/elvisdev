﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class UserData
    {
        public decimal REG_NO { get; set; }
        public string USERNAME { get; set; } 
        public string TITLE { get; set; } 
        public string FIRST_NAME { get; set; } 
        public string LAST_NAME { get; set; } 
        public string EMAIL { get; set; } 
        public decimal DIV_CD { get; set; } 
        public string DIV_NAME { get; set; }
        public string DEPARTMENT_ID { get; set; } 
        public string DEPARTMENT_NAME { get; set; }
        public string PHONE_EXT{ get; set; }
        public string PHONE_MOBILE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT  { get; set; }
        public string CHANGED_BY  { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public int GROUP_CD { get; set; }

        public List<RoleAcessData> _RoleAcessData { get; set; }

        private List<UserPositionData> _positions = new List<UserPositionData>();
        public List<UserPositionData> Positions 
        { 
            get 
            {
                return _positions;
            }
        }
        public List<string> Divisions
        {
            get
            {
                List<string> divs = new List<string>();
                
                divs = (from d in _positions
                        select d.DIVISION_ID)
                        .Distinct()
                        .ToList();

                return divs;
            }
        }

        public List<string> Roles
        {
            get
            {
                List<string> s = new List<string>();
                if (_RoleAcessData!=null && _RoleAcessData.Count > 0)
                        s = (from r in _RoleAcessData 
                             select r.ROLE_ID)
                            .Distinct()
                            .ToList();
                return s;
            }
        }

        public string[] GetDivisions()
        {
            return Divisions.ToArray();            
        }

        public bool isInDivision(string divisionId) 
        {
            return (!String.IsNullOrEmpty(divisionId))
                && (Convert.ToString(DIV_CD) == divisionId
                ||   ((from d in _positions 
                      where d.DIVISION_ID == divisionId 
                     select d.DIVISION_ID)
                   .Count() > 0));
        }

        public bool isFinanceDivision {
            get { return isInDivision(AppSetting.FINANCE_DIVISION_CODE.ToString()); }
        }

        public bool isCounter
        {
            get
            {
                return Roles.Contains("ELVIS_COUNTER");
            }
        }
        private bool? _isAuthorizedCreator = null;

        public bool isAuthorizedCreator
        {
            get
            {
                return _isAuthorizedCreator ?? false;
            }
            set
            {
                _isAuthorizedCreator = value;
            }
        }

        public bool canPost
        {
            get
            {
                return Roles.Contains("ELVIS_FD_APPROVER") &&
                    (Positions.Where(p => p.CLASS > 1).Count() > 0);

            }
        }
    }

    [Serializable]
    public class UserPositionData
    {
        public byte? CLASS { get; set; }
        public string POSITION_ID { get; set; }
        public string POSITION_NAME { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public string DEPARTMENT_ID { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public string SECTION_ID { get; set; }
        public string SECTION_NAME { get; set; }
    }
}