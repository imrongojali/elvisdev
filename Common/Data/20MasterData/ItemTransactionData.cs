﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.ComponentModel;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class ItemTransactionData
    {
        public int TRANSACTION_CD { get; set; }
        public int ITEM_TRANSACTION_CD { get; set; }
        public string ITEM_TRANSACTION_DESC { get; set; }

        public int? DPP_FLAG { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
