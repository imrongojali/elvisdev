﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class GroupCode
    {
        public int GROUP_CD { get; set; }
        public string GROUP_NAME { get; set; } 
    }
}
