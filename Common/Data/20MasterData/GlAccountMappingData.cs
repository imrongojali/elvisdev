﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class GlAccountMappingData
    {
        public int TRANSACTION_CD { get;set; }
        public string TRANSACTION_NAME { get; set; } 
        public int ITEM_TRANSACTION_CD { get; set; }
        public string ITEM_TRANSACTION_DESC { get; set; }
        public int GL_ACCOUNT { get; set; }
        public int? NEW_ITEM_TRANSACTION_CD { get; set; }
        public string NEW_ITEM_TRANSACTION_DESC { get; set; } 
        public decimal? NEW_FORMULA { get; set; }
        public int SUM_UP_FLAG { get; set; }
        public string SUM_UP_FLAG_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
