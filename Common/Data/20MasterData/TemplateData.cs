﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class TemplateData
    {
        public int TemplateCode { get; set; }
        public string TemplateName { get; set; } 
    }
}
