﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class DocTypeData
    {
        public int DocType { get; set; }
        public string DocName { get; set; }
    }
}
