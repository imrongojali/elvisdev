﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class TransactionType
    {
        public string Code { set; get; }
        public string Name { set; get; }
        public string StandardWording { set; get; }
        public bool CostCenterFlag { set; get; }
        public bool BudgetFlag { set; get; }
        public bool AttachmentFlag { set; get; }
        public int ProductionFlag { get; set; }
        public int TemplateCd { get; set; }
        public string TemplateName { get; set; } 
    }
}