﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class DirectToMeReason
    {
        public int REASON_ID { get; set; }
        public string REASON_DESCRIPTION { get; set; }
        public int? SPECIAL_FLAG { get; set; }
        public byte? CLASS { get; set; } 
    }
}
