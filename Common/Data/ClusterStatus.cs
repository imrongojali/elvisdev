﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class ClusterStatus
    {
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string StatusList { get; set; } 
    }
}
