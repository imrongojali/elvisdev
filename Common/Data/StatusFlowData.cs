﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class StatusFlowData
    {
        public int MODUL_CODE { get; set; }
        public string MODUL_DESC  { get; set; }
        public int STATUS_CD  { get; set; }
        public string STATUS_NAME { get; set; }
    }
}
