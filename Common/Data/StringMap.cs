﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class StringMap
    {
        public Dictionary<string, string> d;

        public StringMap()
        {
            d = new Dictionary<string, string>();
        }

        public void Put(string key, string value)
        {
            d[key] = value;
        }

        public string Get(string key, string value = null)
        {
            if (d.ContainsKey(key)) 
                return d[key]; 
            else 
                return value;
        }

        public bool Has(string key)
        {
            return d.ContainsKey(key);
        }

        public string KV(string separatorString = ", ")
        {
            StringBuilder b = new StringBuilder("");
            string sep = "";
            foreach (string k in d.Keys)
            {
                b.Append(sep);
                b.Append(string.Format("\"{0}\":{1}", k, d[k]));
                sep = separatorString;
            }
            return b.ToString();
        }
    }
}
