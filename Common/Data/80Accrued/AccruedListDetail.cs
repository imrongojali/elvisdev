﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccruedListDetail
    {

        #region Header

        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public DateTime? UPDATED_DT { get; set; }
        public string ACCRUED_NO { get; set; }
        public int EXCHANGE_RATE { get; set; }
        public int USD_RATE { get; set; }
        public int JPY_RATE { get; set; }
        public string PRINTED_BY { get; set; }
        public DateTime PRINTED_DT { get; set; }
        #endregion

        #region Detail
        //fid.pras
        public string EXPIRE_DT_STR { get; set; }

        public string BOOKING_NO { get; set; }
        public int? BOOKING_STS { get; set; }
        public string OPEN_STATUS { get; set; }
        public string WBS_NO_OLD { get; set; }
        public string WBS_NO { get; set; }
        public string WBS_NO_PR { get; set; }
        public string WBS_DESC { get; set; }
        public string WBS_DESC_PR { get; set; }
        public int? SUSPENSE_NO_OLD { get; set; }
        public int? SUSPENSE_NO_PR { get; set; }
        public int? PV_TYPE_CD { get; set; }
        public string PV_TYPE_NAME { get; set; }
        public int? BUDGET_YEAR { get; set; }
        public string ACTIVITY_DES { get; set; }
        public string COST_CENTER { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal? INIT_AMT { get; set; }
        public decimal AMOUNT_IN_RP { get; set; }
        public decimal? SAP_AVAILABLE { get; set; }
        public decimal? SAP_REMAINING { get; set; }
        public decimal? SAP_SPENT { get; set; }
        public decimal? TOTAL_SAP_AVAILABLE { get; set; }
        public decimal? TOTAL_SAP_REMAINING { get; set; }
        public decimal? TOTAL_SAP_SPENT { get; set; }
        public decimal? TOTAL_SHIFTING { get; set; }
        public decimal? REMAIN_SHIFTING { get; set; }
        public decimal? REMAIN_EXTEND { get; set; }
        public decimal? SHIFTED_AMT { get; set; }
        public decimal? SPENT_AMT { get; set; }
        public decimal? AVAILABLE_AMT { get; set; }
        public decimal? OUTSTANDING_AMT { get; set; }
        public decimal? BLOCKED_AMT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public DateTime? EXPIRE_DT { get; set; }
        public int? STATUS_CD_SHIFTED { get; set; }
        public int? STATUS_CD_EXTEND { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_DOC_NO_CLOSE { get; set; }
        public int? SAP_ITEM_NO { get; set; }
        public bool IS_CHECKED { get; set; }
        #endregion


        #region Footer
        public int TOTAL_AMOUNT { get; set; }
        #endregion

        public static int RuleOrder(AccruedListDetail x, AccruedListDetail y)
        {

            if (!x.BOOKING_NO.ToLower().Equals(y.BOOKING_NO.ToLower()))
                return x.BOOKING_NO.ToLower().CompareTo(y.BOOKING_NO.ToLower());

            return x.WBS_NO_OLD.ToLower().CompareTo(y.WBS_NO_OLD.ToLower());
        }
    }

    public class AccruedResultComparer : IComparer
    {
        int IComparer.Compare(object a, object b)
        {
            var x = (AccruedListDetail)a;
            var y = (AccruedListDetail)b;
            if (x.ACCRUED_NO == y.ACCRUED_NO
                && x.BOOKING_NO == y.BOOKING_NO
                && x.WBS_NO_OLD == y.WBS_NO_OLD)
                return 0;
            else
                return 1;
        }
    }
}
