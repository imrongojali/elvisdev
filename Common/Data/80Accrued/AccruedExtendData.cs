﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccruedExtendData
    {
        readonly private string green = "#00ff00";
        readonly private string red = "#ff0000";
        readonly private string yellow = "#fffb00";

        private int? workflow_status;
        public int? WORKFLOW_STATUS
        {
            get
            {
                return workflow_status;
            }
            set
            {
                workflow_status = value;
                if (workflow_status == 0)
                {
                    WORKFLOW_STATUS_COLOR = red;
                }
                else if (workflow_status == 1)
                {
                    WORKFLOW_STATUS_COLOR = green;
                }
            }
        }
        public string WORKFLOW_STATUS_COLOR { get; set; }
        public int? DisplaySequenceNumber { get; set; }
		//public int? SEQ_NO { get; set; }
        public string EXTEND_NO { get; set; }
        public string BOOKING_NO { get; set; }
        public string BOOKING_NO_OLD { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public int STATUS_CD { get; set; }
        public string SUBMISSION_STATUS { get; set; }
        public decimal TOT_AMOUNT { get; set; }
        public string PIC_CURRENT { get; set; }
        public string PIC_NEXT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string MODULE_CD { get; set; }
        public int BUDGET_YEAR { get; set; }
        public string PV_TYPE_NAME { get; set; }
        public int? PV_TYPE_CD { get; set; }

        //add by FID.Arri on 8 May 2018 for AccrExtendReport
        public string WBS_NO_OLD { get; set; }
        public string WBS_NO_PR { get; set; }
        public string WBS_DESC_OLD { get; set; }
        public string WBS_DESC_PR { get; set; }
        public int? SUSPENSE_NO_OLD { get; set; }
        public int? SUSPENSE_NO_PR { get; set; }
        public decimal BLOCKING_AMT { get; set; }
        public decimal AVAILABLE_AMT { get; set; }
        public string EXTEND_TYPE { get; set; }
        public int EXTEND_TYPE_CD { get; set; }
        public decimal? RECLAIMABLE_AMT { get; set; }
        public decimal? EXTENDABLE_AMT { get; set; }
        public decimal? EXTEND_AMT { get; set; }
        public string ACTIVITY_DES { get; set; }
        public DateTime EXTEND_DT { get; set; }
        public string REFF_NO { get; set; }

        //add by FID.Ridwan 04072018
        public string ACTIVITY_DES_VIEW { get; set; }
    }
}
