﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Enum;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccrFormSimulationDetail
    {
        public int SEQ_NO { get; set; }
        public String ACCOUNT { get; set; }
        public String ACCOUNT_SHORT { get; set; }
        public String TX { get; set; }
        public String AMOUNT { get; set; }
        public String TEXT { get; set; }
        public String WBS_NO { get; set; }
        public String COST_CENTER { get; set; }
        public decimal AMOUNT_DEC { get; set; }
        public DataSAPMode dataMode { get; set; }
    }
}