﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class CashJournalMessage
    {
        public string BusinessTransaction { get; set; }
        public decimal? Amount { get; set; }
        public string Text { get; set; }
        public string Vendor { get; set; }
        public string Customer { get; set; }
        public string Reference { get; set; }
        public string InternalDocNo { get; set; }
        public string Message { get; set; } 
    }
}
