﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class OutstandingSuspense
    {
        public string DIVISION_ID { get; set; }
        public string DOC_NO { get; set; }
        public string DOC_YEAR { get; set; }
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public int DAYS_OUTSTANDING { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
    }
}
