﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class PVFormHeaderData
    {
        public int PV_NO { get; set; }
        public int PV_YEAR { get; set; }
        public int STATUS_CD { get; set; }
        public string PAY_METHOD_CD { get; set; }
        public int VENDOR_GROUP_CD{ get; set; }
        public string VENDOR_CD { get; set; }
        public int PV_TYPE_CD { get; set; }
        public int TRANSACTION_CD { get; set; }
        public string DIV_CD { get; set; }
        public DateTime DOC_DATE { get; set; }
        public decimal? SUSPENSE_NO { get; set; }
        public decimal? SETTLEMENT_NO { get; set; }
        public string BUDGET_NO { get; set; }
        public DateTime ACTIVITY_DATE { get; set; }
        public string DIVISION_ID { get; set; }
        public string TAX_CD { get; set; }
        public bool HOLD { get; set; } 
        public DateTime PLANNING_PAYMENT_DATE { get; set; }
        public DateTime POSTING_DATE { get; set; }        
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
