﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class PVApprovalData
    {
        #region Header
        public int PV_NO {get; set; }
        public int PV_YEAR { get; set; }
        public DateTime PV_DATE { get; set; }        
        public String PV_TYPE { get; set; }
        public int PV_TYPE_CD { get; set; } 
        public String STATUS_NAME { get; set; }
        public int STATUS_CD { get; set; }
        public Decimal? TOTAL_AMOUNT { get; set; }
        public String VENDOR_CD { get; set; }
        public String VENDOR_NAME { get; set; }
        public String TRANSACTION_TYPE { get; set; }
        public String PAYMENT_METHOD { get; set; }
        public String ISSUING_DIVISION { get; set; }
        public String DIVISION_ID { get; set; }
        public DateTime? PLANNING_PAYMENT_DATE { get; set; }
        public DateTime? DUE_DATE { get; set; }
        public String STD_WORDING { get; set; }
        public String HOLD_BY { get; set; }
        public int HOLD_FLAG { get; set; }
        public string BUDGET_NO { set; get; }
        //added by Akhmad Nuryanto, 22/09/2012
        public string REMAINING_BUDGET { get; set; }
        //end of addition by Akhmad Nuryanto
        #endregion

        #region Detail
        public String COST_CENTER_CD { get; set; }
        public String COST_CENTER_NAME { get; set; }
        public String DESCRIPTION { get; set; }
        public String CURRENCY { get; set; }
        public String CURRENCY_CD { get; set; }
        public String AMOUNT { get; set; }
        public decimal? AMOUNT_VALUE { get; set; } 
        #endregion

        public int? VENDOR_SUSPENSE_COUNT { set; get; }
        public int? COUNTER_FLAG { get; set; } 
        public int TicketPrinted { get; set; }

    }
}
