﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class CashierPolicyData
    {
        public int Seq { get; set; }
        public string Curr { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public decimal Policy { get; set; }
        public decimal Total { get; set; }
    }    
}
