﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace Common.Function
{
    public class ExcelData
    {
        #region retrive data
        public DataTable RetriveData(string SheetName, string ConnString, int ProcessId)
        {
            try
            {
                DataTable _dt = new DataTable();

                using (OleDbConnection _oleDBConnection = new OleDbConnection(ConnString))
                {
                    _oleDBConnection.Open();

                    string _query = "SELECT * FROM [" + SheetName + "$]";

                    using (OleDbCommand _oleDBCommand = new OleDbCommand(_query, _oleDBConnection))
                    {
                        OleDbDataAdapter _oleDBDataAdapter = new OleDbDataAdapter(_oleDBCommand);
                        _oleDBDataAdapter.Fill(_dt);
                    }

                    _oleDBConnection.Close();
                }

                return _dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region is excel format
        public bool IsExcelFormat(string strFileType)
        {

            if (strFileType.Trim() == ".xls")
            {
                return true;
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region get connection string
        public string GetConnectionString(string strFileType, string strNewPath)
        {
            string _connString = null;
            if (strFileType.Trim() == ".xls")
            {
                _connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strNewPath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                _connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strNewPath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }
            return _connString;
        }
        #endregion
    }
}
