﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace Common.Function
{
    public class CommonFunction
    {
        public static string CommaJoin(List<string> _list, string separator=", ")
        {
            return (_list != null && _list.Count > 0) 
                ? string.Join(separator, _list.ToArray()) 
                : "";
        }

        public delegate string Num2Str(decimal d, string CurrencyCd);

        public static string DecToStr(decimal d, string CurrencyCd)
        {
            return d.ToString("0.00");
        }

        public static string PostCurr(decimal d, string CurrencyCd)
        {
            if (AppSetting.RoundCurrency.Contains(CurrencyCd))
            {
                decimal td = Math.Truncate(d);
                return td.ToString("0");
            }
            else 
            {
                return d.ToString("0.00");
            }
        }

        // Evaluate the number format when displaying on the aspx page - Neir Kate
        // Usage Example: <%# Eval_Curr(Eval("CURRENCY"), Eval("SPENT_AMOUNT")) %>
        public static String Eval_Curr(Object currencyCd, Object decimalValue, int displayCurrency=0)
        {
            decimal decimalResult = 0;
            string preCurr = (displayCurrency == 1) ? currencyCd.str() + " " : "";
            string postCurr = (displayCurrency == 2) ? " " + currencyCd.str() : "";
            bool parseOk = Decimal.TryParse(decimalValue.str(), out decimalResult);

            if (currencyCd != null 
                && !String.IsNullOrWhiteSpace(currencyCd.ToString()) 
                && decimalValue != null && parseOk)
            {
                if ( AppSetting.RoundCurrency.Contains(currencyCd))
                // if (currencyCd.Equals("IDR") || currencyCd.Equals("JPY"))
                {
                    return  preCurr  + decimalResult.ToString("###,###,###,###,###,##0") + postCurr;
                }
                else
                {
                    return preCurr + decimalResult.ToString("###,###,###,###,###,##0.00") + postCurr;
                }
            }

            if (decimalValue != null && parseOk)
            {
                return preCurr + decimalResult.ToString() + postCurr;
            }
            
            
            return null;
        }

        // Evaluate the date format when displaying on the aspx page - Neir Kate
        // Usage Example: <%# Eval_Date(Eval("DATE_FROM")) %>
        public static String Eval_Date(Object dateValue, bool withTime)
        {
            DateTime dateResult = DateTime.Now;
            try
            {
                if (dateValue != null && DateTime.TryParse(dateValue.ToString(), out dateResult))
                {
                    if (withTime)
                        return dateResult.ToString("dd MMM yyyy HH:mm");
                    else
                        return dateResult.ToString("dd MMM yyyy");
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
       
        // Check whether the input is in number format - Neir Kate
        // Usage Example: if (IsDecimal(txtGridSpentAmount.Text))
        public static bool IsDecimal(String strVal)
        {
            decimal result;
            return Decimal.TryParse((strVal ?? "").Trim(), out result);
        }

        public static Decimal TryStrToDecimalDef(object a, decimal value = 0)
        {
            decimal R = 0;
            if (!Decimal.TryParse(Convert.ToString(a), out R)) R = value;
            return R;
        }

        //public static string CurrencyCol(string curr, string amt)
        //{
        //    if (!(string.IsNullOrEmpty(amt)))
        //        return String.Format("<tr><td>{0}</td><td style='font-size: 16pt;' align='right'>{1:N0}</td></tr>", curr, Eval_Curr(curr, Decimal.Parse(amt)));
        //    else
        //        return "";
        //}

        readonly static string DirtyFileNamePattern = "[\\+/\\\\\\#%&*{}/:<>?|\"-]";

        public static string SanitizeFilename(string insane)
        {
            //string pattern = "[\\+/\\\\\\~#%&*{}/:<>?|\"-]";
            string replacement = "_";

            Regex regEx = new Regex(DirtyFileNamePattern);
            return Regex.Replace(regEx.Replace(insane, replacement), @"\s+", " ");
        }

        public static bool isDirty(string s) 
        {
            return Regex.IsMatch(s, DirtyFileNamePattern);
            //char[] dirty = new char[] { '&', '+', '%', '?', '*', '=', '\\', ':', '<', '>', '|'};
            //bool iam = false;
            //if (!s.isEmpty()) 
            //    foreach (char c in dirty) {
            //        if (s.Contains(c)) {
            //            iam = true;
            //            break;
            //        }
            //    }
            //return iam;
        }
        public static string CleanFilename(string s)
        {   
            
            //char[] dirty = new char[] { '&', '+', '%', '?', '*', '=', '\\', ':', '<', '>', '|'};
            string t = SanitizeFilename(s);
            //foreach (char c in dirty)
            //{
            //    t = t.Replace(c, '_');
            //}
            if (t.Length > 75)
            {
                string ext = Path.GetExtension(t);
                t = t.Substring(0, 75 - ext.Length - 1) + ext; 
            }
            return t; 
        }

        public static string CombineUrl(params object [] x)
        {
            StringBuilder b = new StringBuilder("");
            if (x != null && x.Length > 0)
            {
                
                for (int i = 0; i < x.Length; i++) 
                {
                    string d = x[i].str();
                    if (i < x.Length - 1 
                        && !d.isEmpty()
                        && d.Length > 1 && !d.Substring(d.Length - 1, 1).Equals("/"))
                    {
                        d = d + "/";
                    }
                    else
                    {
                        d = System.Web.HttpUtility.UrlPathEncode(d);
                    }
                    if (!d.isEmpty())
                        b.Append(d);
                }

            }
            return b.ToString();
        }
        public static int HoldValueOf(Common.Enum.ApprovalEnum a)
        {
            int r = 0;
            switch (a)
            {
                case Common.Enum.ApprovalEnum.Hold: r = 1; break;
                case Common.Enum.ApprovalEnum.UnHold: r = 0; break;
                default: r = 0; break;
            }
            return r;
        }
    }

}
