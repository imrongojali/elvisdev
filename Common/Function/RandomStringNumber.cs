﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Function
{
    public class RandomStringNumber
    {
        private int _Length;
        public RandomStringNumber(int length)
        {
            _Length = length;
        }

        public string GetRandomStringNumber()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _Length; i++)
            {
                if (random.Next() % 2 == 1)
                {
                    char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
                    sb.Append(ch);
                }
                else
                {
                    char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    sb.Append(ch);
                }
            }
            return sb.ToString();
        }
        public string GetRandomString()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _Length; i++)
            {
                char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                sb.Append(ch);
            }
            return sb.ToString();
        }
        public string GetRandomNumber()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _Length; i++)
            {
                char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
                sb.Append(ch);
            }
            return sb.ToString();
        }
    }
}
