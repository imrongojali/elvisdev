﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace ElvisWorkflowStatusUpdater
{
    public class DocumentUpdater
    {
        private string name;
        //private int lowerBound;
        //private int upperBound;
        private WorkflowUpdater wf = null;
        private Stopwatch stopWatch;
        private TimeSpan elapsed;
        private SqlContext dbdays;
        private SqlCommand getDays;
        private SqlParameter pBegin;
        private SqlParameter pEnd;
        private SqlParameter pDay;
        private int doFlag = 0xFF;
        private IniSQL Q = null;

        public DocumentUpdater(string name,
            //int lowerBound, int upperBound, 
                WorkflowUpdater w, int DoFlag = 0xFF)
        {
            //this.lowerBound = lowerBound;
            //this.upperBound = upperBound;
            this.name = name;
            Q = new IniSQL();
            dbdays = new SqlContext(w.ConnectionString);
            getDays = dbdays.NewProc("dbo.sp_GetWorkDaySpan");
            pBegin = getDays.Parameters.Add("@begins", SqlDbType.DateTime);
            pEnd = getDays.Parameters.Add("@ends", SqlDbType.DateTime);
            pDay = getDays.Parameters.Add("@ws", SqlDbType.Int);
            pDay.Direction = ParameterDirection.Output;

            wf = w;
        }

        public void Start()
        {
            Thread thread = new Thread(new ThreadStart(this.run));
            thread.Start();
            while (!thread.IsAlive) ;
        }

        public int GetWorkingDaysBetween(DateTime from, DateTime upto)
        {
            int r = 0;
            pBegin.Value = from;
            pEnd.Value = upto;
            SqlDataReader x = getDays.ExecuteReader();
            if (pDay.Value != null)
                r = Convert.ToInt32(pDay.Value);
            x.Close();
            return r;
        }

        public int GetCount(string whole)
        {
            SqlContext db = new SqlContext(wf.ConnectionString);
            db.sql = "SELECT COUNT(1) COUNTS FROM (" + whole + ") x";
            int r = 0;
            if (db.Read())
                r = db.IntVal(0);
            return r;
        }

        public void WorkflowUpdate(string doctype, string what, string toUpdate)
        {
            WF.say("\r\nUpdate Workflow Status");
            SqlContext con =null;
            SqlContext conUpdateWorkflow = null;
            int succededProcessCounter = 0;
            try
            {
                con = new SqlContext(wf.ConnectionString);
                
                SqlCommand cmdPartial = con.Command;

                cmdPartial.CommandText = what; 

                SqlContext conDist = new SqlContext(wf.ConnectionString);
                SqlCommand cmdDist = conDist.Command;
                cmdDist.CommandText = Q["DISTRIBUTION_STATUS"]; 
                cmdDist.Parameters.Add(WF.REFF_NUMBER, SqlDbType.VarChar, 20);

                conUpdateWorkflow = new SqlContext(wf.ConnectionString);
                conUpdateWorkflow.Begin();
                SqlCommand cmdUpdateWorkflow = conUpdateWorkflow.Command;
                
                cmdUpdateWorkflow.CommandText = toUpdate; 
                conUpdateWorkflow.AddParam(WF.WORKFLOW_STATUS, SqlDbType.Int);
                conUpdateWorkflow.AddParam(WF.DOC_NO, SqlDbType.Int);
                conUpdateWorkflow.AddParam(WF.DOC_YEAR, SqlDbType.Int);

                SqlDataReader dataReader = cmdPartial.ExecuteReader();

                SqlDataReader distStatusDataReader;
                int num;
                int year;
                DateTime? planDate;
                int? workflowStatus;
                
                DateTime today = DateTime.Now;

                while (dataReader.Read())
                {
                    num = Convert.ToInt32(dataReader[1]);
                    year = Convert.ToInt32(dataReader[2]);
                    //WF.say("#{0} {1}", num, year);
                    cmdDist.Parameters[WF.REFF_NUMBER].Value =
                        Convert.ToString(num) + Convert.ToString(year);
                    distStatusDataReader = cmdDist.ExecuteReader();

                    workflowStatus = 1;
                    if (distStatusDataReader.HasRows)
                    {
                        if (distStatusDataReader.Read())
                        {
                            if (!distStatusDataReader.IsDBNull(0))
                                planDate = distStatusDataReader.GetDateTime(0);
                            else
                                planDate = null;

                            if (planDate != null)
                            {
                                if (DateTime.Compare(today, (DateTime)planDate) > 0)
                                {

                                    workflowStatus = 0;
                                }
                            }
                        }
                    }

                    cmdUpdateWorkflow.Parameters[WF.DOC_NO].Value = num;
                    cmdUpdateWorkflow.Parameters[WF.DOC_YEAR].Value = year;
                    cmdUpdateWorkflow.Parameters[WF.WORKFLOW_STATUS].Value = workflowStatus;
                    int rQ = cmdUpdateWorkflow.ExecuteNonQuery();
                    
                    WF.say("    " + doctype + " WORKFLOW_STATUS={0} @ # {1} {2} : {3}", workflowStatus, num, year, rQ);
                    if (rQ > 0)
                    {
                        succededProcessCounter++;
                    }

                    distStatusDataReader.Close();
                }

                dataReader.Close();
                conDist.Close();
                conUpdateWorkflow.Commit();
                WF.say("\r\n{0}: {1} row updated.", name, succededProcessCounter);
            }
            catch (Exception ex)
            {
                WF.say("Error @WorkflowUpdate({0}, ...,...)", doctype);
                WF.say("Rollback Update");
                WF.err(ex);
                conUpdateWorkflow.RollBack();
            }
            finally
            {
                con.Close();
                conUpdateWorkflow.Close();
            }
            
        }

        


        public void SettlementStatusUpdate()
        {
            WF.say("\r\nUpdate Settlement Status");
            SqlContext db = null;
            SqlContext uSetStat = null;
            SqlContext uDays = null;
            SqlContext dbGetSS = new SqlContext(wf.ConnectionString);
            dbGetSS.sql = Q["SETTLEMENT_STATUS_PV"];
            dbGetSS.AddParam("@no", SqlDbType.Int);
            dbGetSS.AddParam("@year", SqlDbType.Int);

            try
            {
                db = new SqlContext(wf.ConnectionString);
                uSetStat = new SqlContext(wf.ConnectionString);
                uSetStat.Begin();
                uDays = new SqlContext(wf.ConnectionString);
                uDays.sql = Q["SYSTEM_SETTING_DAYS"];
                int WaitDay = 5;
                if (uDays.Read())
                {
                    WaitDay = uDays.IntVal(0, 5);
                }
                uDays.Close();

                uSetStat.sql = Q["UPDATE_PV_SETTLEMENT_STATUS"];
                SqlCommand cUpdateSettlement = uSetStat.Command;
                DateTime ACTIVITY_DATE_TO;
                uSetStat.AddParam("@status", SqlDbType.Int);
                uSetStat.AddParam("@no", SqlDbType.Int);
                uSetStat.AddParam("@year", SqlDbType.Int);

                db.sql = Q["UNSETTLED_SUSPENSE"];
                while (db.Read())
                {
                    int SUSPENSE_NO = db.IntVal(0, 0);
                    int SUSPENSE_YEAR = db.IntVal(1, 0);
                    if (SUSPENSE_NO < 1 || SUSPENSE_YEAR < 1)
                        continue;

                    int STATUS_PV = db.IntVal(4);
                    int? PV_NO = db.NullInt(5);
                    int STATUS_RV = db.IntVal(7);
                    int? RV_NO = db.NullInt(8);
                    object vActivityDate = db.Value(10);
                    if (vActivityDate != null)
                        ACTIVITY_DATE_TO = Convert.ToDateTime(vActivityDate);
                    else
                    {
                        WF.say("    SUSPENSE # {0} {1} ACTIVITY_DATE_TO IS NULL", SUSPENSE_NO, SUSPENSE_YEAR);
                        continue;
                    }

                    int SETTLEMENT_STATUS = 0;
                    int WorkingDays = GetWorkingDaysBetween(ACTIVITY_DATE_TO, DateTime.Now);
                    WF.say("    SUSPENSE # {0} {1} {2:dd-MM-yyyy} : {3} days", SUSPENSE_NO, SUSPENSE_YEAR, ACTIVITY_DATE_TO, WorkingDays);
                    if (WorkingDays >= WaitDay)
                    {
                        bool SettledPV = false, SettledRV = false;
                        bool hasPV = false, hasRV = false;
                        if (PV_NO != null)
                        {
                            hasPV = true;
                            if (STATUS_PV == 99)
                            {
                                SettledPV = true;
                            }
                        }

                        if (RV_NO != null)
                        {
                            hasRV = true;
                            if (STATUS_RV == 99)
                            {
                                SettledRV = true;
                            }
                        }

                        if ((hasPV && SettledPV) && (hasRV && SettledRV))
                            SETTLEMENT_STATUS = 9;
                        else if (hasPV && SettledPV && !hasRV)
                            SETTLEMENT_STATUS = 9;
                        else if (!hasPV && hasRV && SettledRV)
                            SETTLEMENT_STATUS = 9;
                        else
                        {
                            WF.say("    read status PV");
                            dbGetSS.SetParam("@no", SUSPENSE_NO);
                            dbGetSS.SetParam("@year", SUSPENSE_YEAR);
                            if (dbGetSS.Read())
                            {
                                SETTLEMENT_STATUS = dbGetSS.IntVal(0, 0);
                                WF.say("    returns: {0}", SETTLEMENT_STATUS);
                            }
                        }

                        // update settlement status in SUSPENSE 

                        uSetStat.SetParam("@status", SETTLEMENT_STATUS);
                        uSetStat.SetParam("@no", SUSPENSE_NO);
                        uSetStat.SetParam("@year", SUSPENSE_YEAR);
                        uSetStat.Do();
                        WF.say("    PV SETTLEMENT_STATUS = {0} @ PV # {1} {2} : {3}"
                            , SETTLEMENT_STATUS
                            , SUSPENSE_NO
                            , SUSPENSE_YEAR
                            , uSetStat.LastResult);
                    }
                }
                uSetStat.Commit();
            }
            catch (Exception ex)
            {
                WF.say("Error @SettlementStatusUpdate()");
                WF.say("Rollback Update");
                WF.err(ex);
                uSetStat.RollBack();
            }
            finally
            {
                uSetStat.Close();
            }
            
        }

        public void VendorSuspenseCountUpdate()
        {
            WF.say("\r\nUpdate Vendor Suspense Count");
            SqlContext db = null;
            SqlContext uVendor = null;

            try
            {
                db = new SqlContext(wf.ConnectionString);
                db.sql = Q["VENDOR_SETTLEMENTS"];

                uVendor = new SqlContext(wf.ConnectionString);
                uVendor.Begin();
                uVendor.sql = Q["UPDATE_VENDOR_SUSPENSE_COUNT"];

                SqlParameter pSuspenses = uVendor.AddParam("@suspenses", SqlDbType.Int);
                SqlParameter pVendor = uVendor.AddParam("@vendor", SqlDbType.VarChar);

                while (db.Read())
                {
                    string vendor = db.Value(0).str();
                    //int Unknown = db.Value(1).Int();
                    //int Settled = db.Value(2).Int();
                    int Suspenses = db.Value(3).Int();
                    if (!vendor.isEmpty() && vendor.Trim().Length > 0)
                    {
                        pSuspenses.Value = Suspenses;
                        pVendor.Value = vendor;

                        uVendor.Do();

                        WF.say("    VENDOR SUSPENSE_COUNT={0} @ VENDOR={1} : {2}"
                            , Suspenses, vendor, uVendor.LastResult);
                    }
                }
                uVendor.Commit();
            }
            catch (Exception ex)
            {
                WF.say("Error @SettlementStatusUpdate()");
                WF.say("Rollback Update");
                WF.err(ex);
                uVendor.RollBack();
            }

            finally
            {
                uVendor.Close();
            }


        }

        private void Tests()
        {
            // WF.say("Working Days Between 1 - 10 = {0} ",
            // GetWorkingDaysBetween(new DateTime(2012, 12, 01), 
            // new DateTime(2012, 12, 10)));    
        }

        public void run()
        {
            stopWatch = Stopwatch.StartNew();
            if (!stopWatch.IsRunning)
            {
                stopWatch.Start();
            }

            WF.say("{0}: Start", name);

            if ((doFlag & 1) > 0)
                WorkflowUpdate("PV", Q["PARTIAL_PV"], Q["UPDATE_WORKFLOW_STATUS"]);

            if ((doFlag & 2) > 0)
                WorkflowUpdate("RV", Q["PARTIAL_RV"], Q["UPDATE_RV_WORKFLOW_STATUS"]);

            if ((doFlag & 4) > 0)
                WorkflowUpdate("PV", Q["DONE_PV"], Q["UPDATE_WORKFLOW_STATUS"]);

            if ((doFlag & 8) > 0)
                WorkflowUpdate("RV", Q["DONE_RV"], Q["UPDATE_RV_WORKFLOW_STATUS"]);

            if ((doFlag & 16) > 0)
                SettlementStatusUpdate();

            if ((doFlag & 32) > 0)
                VendorSuspenseCountUpdate();

            dbdays.Close();

            stopWatch.Stop();
            elapsed = stopWatch.Elapsed;

            wf.terminated++;
            if ((wf.terminated >= AppSetting.MaxThread) || !wf.usingThreading)
            {
                WF.say("\r\n{0} done, {1} seconds elapsed."
                    , wf.terminated
                    , elapsed.Seconds);
            }
            WF.say("\r\n{0}: Finished", name);
        }
    }
}
