﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace ElvisWorkflowStatusUpdater
{
    public class AppSetting
    {
        private static string _ConnectionString =  ConfigurationManager.ConnectionStrings["ELVIS_DBEntities"].ConnectionString; 
        public static string ConnectionString 
        {
            get
            {
                return _ConnectionString;
            }
        }
        private static int MAX_THREAD = Read("MAX_THREAD","10").Int();
        public static int MaxThread
        {
            get 
            { 
                return MAX_THREAD; 
            }
        }
        private static int THREADING_MIN_DATA = Read("THREADING_MIN_DATA","100").Int();
        public static int ThreadingMinData
        {
            get
            {
                return THREADING_MIN_DATA;
            }
        }

        private static bool USE_THREAD = (Read("USE_THREAD","0").str().Int() != 0);
        public static bool UseThread
        {
            get
            {
                return USE_THREAD;
            }
        }

        private static int OUTPUT = (Read("OUTPUT","0").Int());
        public static int Output
        {
            get
            {
                return OUTPUT;
            }
        }
        public static string ApplicationId = Read("ApplicationID", "ELVIS");
        public static string LdapDomain = Read("LdapDomain", "tam");
        public static string Read(string setting, string value = "")
        {
            string b = ConfigurationManager.AppSettings[setting];
            if (string.IsNullOrEmpty(b))
                return value;
            else
            {
                return b;
            }
        }

    }
}
