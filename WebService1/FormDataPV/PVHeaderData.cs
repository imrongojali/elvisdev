﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELVISServices
{
    public class PVHeaderData
    {
        public int PVNumber { get; set; }
        public int PVYear { get; set; }
        public string PaymentMethodCode { set; get; }        
        public string VendorCode { set; get; }
        public int? PVTypeCode { set; get; }
        public int? TransactionCode { set; get; }        
        public string BudgetNumber { set; get; }
        public DateTime? ActivityDate { set; get; }
        public DateTime? ActivityDateTo { set; get; }
        public string DivisionID { set; get; }                
        public DateTime? PostingDate { set; get; }
        //public DateTime? PlanningPaymentDate { set; get; }
        public int? BankType { set; get; }
        public decimal ReffNo { set; get; }
        public int ApprovalLevel { set; get; }
        public bool SingleWbsUsed { set; get; }
        public bool FinanceHeader { set; get; }
        public bool Submitting { set; get; }
    }
}