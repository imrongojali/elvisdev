﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELVISServices.FormDataPV
{
    public class UserDataPV
    {
        public string UserName { get; set; }
        public string DivisionCode { get; set; }
    }
}