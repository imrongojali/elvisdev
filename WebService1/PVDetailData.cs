﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELVISServices
{
    public class PVDetailData
    {       
        //public int SequenceNumber { set; get; }
        public string CostCenterCode { set; get; }        
        public string Description { set; get; }        
        public string CurrencyCode { set; get; }
        public decimal Amount { set; get; }
        //public string TaxCode { set; get; }
        //public string TaxNumber { set; get; }
        //public int? ItemTransaction { set; get; }
        public string WbsNumber { set; get; }
        public string InvoiceNumber { set; get; }
        //public string InvoiceNumberUrl { set; get; }
    }
}