﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Data._80Accrued;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using Common.Messaging;
using DevExpress.Web.ASPxTabControl;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrSimulBal : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8016");
        private readonly string SESS_BAL_SIMUL = "BAL_GRID_SIMULATION";
        private bool firstLoadSuspense = true;
        private int idxHeader = 0;
        private int susType = 0;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }
        #endregion
        
        #region Getter Setter for Data List

        private string BookingNo
        {
            get
            {
                if (!Session["ACCRUED_BAL_SIMULATION"].ToString().isEmpty())
                {
                    return Session["ACCRUED_BAL_SIMULATION"].ToString();
                }
                return null;
            }
            set
            {
                Session["ACCRUED_BAL_SIMULATION"] = value;
            }
        }

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }

        protected List<AccrFormSimulationData> SimulDataBal
        {
            get { return (List<AccrFormSimulationData>)ViewState[SESS_BAL_SIMUL]; }
            set { ViewState[SESS_BAL_SIMUL] = value; }
        }
       

        
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            if (!IsPostBack && !IsCallback)
            {
                string _bookNo = Request["bookno"];
                if (!_bookNo.isEmpty())
                {
                    BookingNo = _bookNo;
                }

                PrepareLogout();
                SetFirstLoad();

                SimulateAccrued();
            }
        }

        protected void SimulateAccrued()
        {
            logic.Say("btnSimulate_Click", "Simulate {0} : {1}", "CLosing Balance", BookingNo);

            if (Session == null)
            {
                Nag("MSTD00002ERR", "No Session!");
                return;
            }

            #region Retrieve Simulation
            
            if (SimulDataBal == null)
            {
                SimulDataBal = logic.AccrForm.GetBalSimulationData(BookingNo);
            }

            firstLoadSuspense = true;
            susType = 3;
            idxHeader = 0;
            #endregion

            BalGridSimulation.DataSource = SimulDataBal;
            BalGridSimulation.DataBind();
            BalGridSimulation.DetailRows.ExpandAllRows();
            BalGridSimulation.Visible = true;
        }

        protected void gridPopUpSimulation_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView g = sender as ASPxGridView;
            if (g == null) return;

            if(Session == null)
            {
                g.DataSource = null;
                g.Visible = false;
                return;
            }

            if (SimulDataBal == null || SimulDataBal.Count <= 0)
            {
                g.DataSource = null;
                g.Visible = false;
                return;
            }

            String masterKey = g.GetMasterRowKeyValue().ToString();
            AccrFormSimulationData d = null;
            if ((d = SimulDataBal.Where(x => x.KEYS == masterKey).FirstOrDefault()) != null)
            {
                g.DataSource = d.Details;
                g.Visible = true;
            }
            else
            {
                g.DataSource = null;
                g.Visible = false;
            }
        }

        protected void grid_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Value != null && e.Column.FieldName == "AMOUNT")
            {
                e.DisplayText = e.Value.ToString().Replace(" ", "&nbsp;");
            }
        }

        #region Set Button Visible

        private void SetFirstLoad()
        {
            btnClose.Visible = true;
        }

        #endregion


        #endregion

        #region Buttons
        

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        #endregion

        #region Function


        #endregion
    }
}