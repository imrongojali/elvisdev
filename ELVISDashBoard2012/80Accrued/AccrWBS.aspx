﻿<%@ Page Title="Accrued WBS Form" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrWBS.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrWBS" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        function convertConfirmation() {
            var selection = confirm("Convert Settlement into PV and/or RV ?");
            if (selection) {
                $("#hidDialogConfirmation").val("y");
            } else {
                $("#hidDialogConfirmation").val("n");
            }
            loading();
            return selection;
        }

        function Detail_TextChanged(s, e) {
            var g = s.GetGridView();
            var idxFocused = g.GetFocusedRowIndex();
            g.GetRowValues(idxFocused, 'WbsNumber', function (values) {
                gridGeneralInfo.PerformCallback('p:update:' + values);
            });
            g.Refresh();
        }

        function hidePop() {
            var pop = $find("ConfirmationDeletePopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#rvLink").length > 0) {
                $("#rvLink").click(function () {
                    if ($("#pvLink").length < 1) 
                        hidePop();
                });
            }

            if ($("#pvLink").length > 0) {
                $("#pvLink").click(function () {
                    if ($("#rvLink").length < 1)
                        hidePop();
                });
            }
        }

        function search() {
            var btn = $("#btnSearch");
            if (btn.length > 0) {
                btn.click();
            };
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }

        function WBSNoChanged(s, e) {
            console.log(s);
            console.log(e);
            var values = s.lastChangedValue;
            gridGeneralInfo.PerformCallback('p:update:' + values);
        }

        function OnSelectSuspense(s, e) {
            var sus = $("#ddlSuspenseNo"); 
            if (sus != null && sus.length > 0) {
                search();   
            } else {                
            }
        }
    </script>

<style type="text/css">
   .boxDesc
   {
       border: 1px solid #afafaf;
       border-style: inset;
       background-color: #efefef;
       display: block;              
       overflow: auto;
       padding-left: 5px;
       height: 22px;
       float: left;
       vertical-align: middle;
   }

   .redBg{
       background-color : red;
   }
   
   #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 80px;
   }
   #divVendorName
   {
       width: 254px;
   }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />
    <asp:UpdatePanel runat="server" ID="pnupdateLitMessage">
        <ContentTemplate>
            <asp:Literal runat="server" ID="messageControl" Visible="false" EnableViewState="false"
                OnLoad="evt_messageControl_onLoad" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>

        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />

              <div class="contentsection" style="width:987px !important;">
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                <tr>
                    <td valign="middle" style="width: 175px" class="td-layout-item">
                        Issuing Division <span class="right-bold">:</span>
                    </td>
                    <td valign="middle" style="width: 90px" class="td-layout-item">
                        <dx:ASPxLabel runat="server" ID="tboxIssueDiv" Width="100px" CssClass="display-inline-table" />
                    </td>
                    
                </tr>
                <tr>
                    <td valign="middle" style="width: 175px" class="td-layout-item">
                        Accrued No <span class="right-bold">:</span>
                    </td>
                    <td valign="middle" style="width: 90px" class="td-layout-item">
                        <dx:ASPxLabel runat="server" ID="tboxAccruedNo" Width="100px" CssClass="display-inline-table" />
                    </td>
                    
                </tr>
                <tr>
                    <td valign="middle" style="width: 175px" class="td-layout-item">
                        Updated Date <span class="right-bold">:</span>
                    </td>
                    <td valign="middle" style="width: 90px" class="td-layout-item">
                        <dx:ASPxLabel runat="server" ID="tboxUpdateDt" Width="100px" CssClass="display-inline-table" />
                    </td>
                    
                </tr>
                <tr>
                    <td valign="middle" style="width: 175px" class="td-layout-item">
                        PV Type <span class="right-bold">:</span>
                    </td>
                    <td valign="middle" style="width: 90px" class="td-layout-item">
                        <dx:ASPxLabel runat="server" ID="tboxPvType" Width="100px" CssClass="display-inline-table" />
                    </td>
                    
                </tr>
                <tr>
                    <td valign="middle" style="width: 175px" class="td-layout-item">
                        Status <span class="right-bold">:</span>
                    </td>
                    <td valign="middle" style="width: 90px" class="td-layout-item">
                        <dx:ASPxLabel runat="server" ID="tboxStatus" Width="100px" CssClass="display-inline-table" />
                    </td>
                    
                </tr>
                
             </table>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <div style="visibility:hidden; width:0px; height:0px;">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" ClientIDMode="Static"
                                onclick="btnSearch_Click" OnClientClick="loading()"  />
                        </div>
                    </div>
                </div>
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Accrued WBS Form" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" />

    


    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadTemplate" />
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
        <ContentTemplate>
            <div class="rowbtn">
                <div class="btnright">
                </div>
            </div>
            <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:FileUpload ID="fuAccrWBS" runat="server" ClientIDMode="Static" />
                                    &nbsp;<asp:Button runat="server" ID="btnUploadTemplate" Text="Upload" OnClick="btnUploadTemplate_Click"
                                        OnClientClick="return BeforeUpload_Click()" />
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnEditRow" runat="server" Text="Edit"  OnClick="btnEdit_Click" OnClientClick="loading();"
                                         />
                                </td>  
                               
                            </tr>
                        </table>
                    </td>
                </tr>    
                <tr>                       
                    <td colspan="3">
                        <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                            ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated" EnableCallBacks="False" OnHtmlDataCellPrepared="gridStatusAmount_HtmlDataCellPrepared"
                            OnCustomCallback="evt_gridGeneralInfo_CustomCallback"
                            OnSelectionChanged="evt_gridGeneralInfo_onSelectionChanged" 
                            KeyFieldName="BOOKING_NO" Styles-AlternatingRow-CssClass="even">
                            <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="True"
                                AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                            <SettingsEditing Mode="Inline" />
                            <Columns>
                                
                                <dx:GridViewDataTextColumn Caption="Booking No" VisibleIndex="1" Width="120px" 
                                    FieldName="BOOKING_NO">
                                    <CellStyle HorizontalAlign="Left"/>
                                    <EditItemTemplate>
                                        <dx:ASPxTextBox runat="server" ID="txtGridBookingNo" Width="118px"
                                        ReadOnly="true"
                                            ReadOnlyStyle-Border-BorderStyle="None"/>                                
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="Budget" VisibleIndex="2">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="WBS No." VisibleIndex="2"
                                            Width="200px" FieldName="WBS_NO_OLD">
                                            <CellStyle HorizontalAlign="Left"/>
                                            <EditItemTemplate>
                                                <dx:ASPxTextBox runat="server" ID="txtGridWBSNoOld" Width="198px"
                                                ReadOnly="true"
                                                    ReadOnlyStyle-Border-BorderStyle="None"/>                                
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WBS Description" VisibleIndex="3" Width="200px" 
                                            FieldName="WBS_DESC">
                                            <CellStyle HorizontalAlign="Left"/>
                                            <EditItemTemplate>
                                                <dx:ASPxTextBox runat="server" ID="txtGridWBSDesc" Width="198px"
                                                ReadOnly="true"
                                                    ReadOnlyStyle-Border-BorderStyle="None"/>                                
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>

                                <dx:GridViewDataTextColumn Caption="Amount in IDR" VisibleIndex="4" Width="146px" 
                                    FieldName="INIT_AMT">
                                    <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                    <DataItemTemplate>
                                        <asp:Literal runat="server" 
                                            ID="litGridTotalAmountIDR" 
                                            Text='<%# CommonFunction.Eval_Curr("IDR", Eval("INIT_AMT")) %>' />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxTextBox runat="server" ID="txtGridInitAmt" 
                                        ReadOnly="true" HorizontalAlign="Right" 
                                            ReadOnlyStyle-Border-BorderStyle="None"/>                                
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="WBS No. (New)" VisibleIndex="5" Width="200px" 
                                    FieldName="WBS_NO_PR">
                                    <CellStyle HorizontalAlign="Left" />
                                    <EditItemTemplate>

                                        <dx:ASPxTextBox runat="server" ID="txtGridWbsNoNew" Width="190px" ClientSideEvents-TextChanged="WBSNoChanged" 
                                            Text="<%# Bind('WBS_NO_PR') %>"/> 
                                        

<%--                                        <dx:ASPxGridLookup runat="server" ID="ddlGridWBSNoNew" ClientInstanceName="ddlGridWBSNoNew"
                                            ClientIDMode="Static" SelectionMode="Single" KeyFieldName="WbsNumber" TextFormatString="{0}"
                                            AutoPostBack="false" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains"
                                            OnLoad="LookupBudgetNo_Load" OnValueChanged="NewWBSNo_Changed">
                                            <ClientSideEvents TextChanged="Detail_TextChanged" />        
                                            <GridViewProperties EnableCallBacks="false">
                                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                                <SettingsPager PageSize="7" />
                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                <dx:GridViewDataTextColumn Caption="WBS Number" FieldName="WbsNumber" Width="200px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>--%>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="SAP Balance" VisibleIndex="6">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Available" VisibleIndex="6"
                                            Width="200px" FieldName="SAP_AVAILABLE">
                                            <CellStyle HorizontalAlign="Right" Cursor="pointer" VerticalAlign="Middle" />
                                            <EditCellStyle HorizontalAlign="Right" Cursor="pointer" VerticalAlign="Middle" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="valSapAvailable" runat="server" CssClass="alignright"  Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SAP_AVAILABLE")) %>'  />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <dx:ASPxTextBox runat="server" ID="txtGridSapAvailable" Width="198px"
                                                    ReadOnly="true" HorizontalAlign="Right" 
                                                    ReadOnlyStyle-Border-BorderStyle="None"/>                                
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Difference" VisibleIndex="7" Width="200px" 
                                            FieldName="SAP_REMAINING">
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" /> 
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="valSapRemaining" runat="server" CssClass="alignright"  Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SAP_REMAINING")) %>'  />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <dx:ASPxTextBox runat="server" ID="txtGridSapRemaining" Width="198px" 
                                                    ReadOnly="true" HorizontalAlign="Right" 
                                                    ReadOnlyStyle-Border-BorderStyle="None"/>                                
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" />
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                                </Cell>
                            </Styles>
                            <SettingsPager AlwaysShowPager="False" Mode="ShowAllRecords"></SettingsPager>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsPager Visible="false" />
                        </dx:ASPxGridView>


                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
                <tr>
                    <td align="left">
                         <asp:Button ID="btnDownload" runat="server" Text="Download" 
                                        OnClick="btnDownload_Click" OnClientClick="loading();" CssClass="xlongButton" SkinID="xlongButton" />
                    </td>  

                    <td valign="top" style="width: 270px" align="left">
                        <asp:Button runat="server" ID="btnComplete" Text="Complete" OnClick="btnComplete_Click" OnClientClick="loading()" />
                    </td>

                    <td></td>
                    <td></td>

                    <td colspan="2" valign="top" style="width: 270px" align="right">
                        <asp:Button ID="btnSave" runat="server" OnClick="btnSaveDetail_Click" Text="Save" OnClientClick="loading();"
                                         />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancelEdit_Click" OnClientClick="loading();"
                                         />
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%//********************region Complete Confirmation**********************//%>
    <asp:UpdatePanel ID="upnlConfirmationComplete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnComplete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationComplete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationCompletePopUp" runat="server" TargetControlID="hidBtnConfirmationComplete"
                PopupControlID="ConfirmationCompletePanel" CancelControlID="BtnCancelConfirmationComplete" OkControlID="BtnOkConfirmationComplete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationCompletePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationComplete" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="BtnOkConfirmationComplete" Text="OK" runat="server" 
                                OnClientClick="loading();HiddenCompleteOkButton.click();"/>
                                &nbsp; 
                            <asp:Button ID="BtnCancelConfirmationComplete" Text="Cancel" runat="server" 
                                OnClick="btnCancelConfirmationComplete_Click" />
                        </div>
                            <asp:Button ID="HiddenCompleteOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationComplete_Click" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%//*********************end region***********************//%>

 
</asp:Content>
