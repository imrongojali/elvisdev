﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Data._80Accrued;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using Common.Messaging;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrShiftingList : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8013");
        private string susNo, susYear;
        private int suNO, suYY;
        private SelectableDropDownHelper sel = null;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }
        #endregion

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }
       

        #region Getter Setter for Data List
        private List<AccruedShiftingListData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedShiftingListData>();
                }
                else
                {
                    return (List<AccruedShiftingListData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion
        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            sel = new SelectableDropDownHelper(IssuingDivisionDropDown, "IssuingDivisionListBox");
            ASPxListBox divBox = (ASPxListBox)IssuingDivisionDropDown.FindControl("IssuingDivisionListBox");

            if (!IsPostBack && !IsCallback)
            {
                if (divBox != null) sel.SelectAll();

                PrepareLogout();

                Ticker.In("GenerateComboData");
                GenerateComboData(ddlWorkflowStatus, "WORKFLOW_STATUS", true);
                //GenerateComboData(ddlSubSts, "ACCR_SUBM_STATUS", true);
                GenerateComboData(ddlSubSts, "ACCR_SHIFTING_SUBM_STATUS", true);
                Ticker.Out();

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();

                gridGeneralInfo.DataSource = null;

                DoSearch();

                #region Init Startup Script
                AddEnterComponent(IssuingDivisionDropDown);
                AddEnterComponent(txtPVYear);
                AddEnterComponent(txtAccrNo);
                AddEnterComponent(ddlSubSts);
                AddEnterComponent(dtCreatedDateFrom);
                AddEnterComponent(dtCreatedDateTo);
                AddEnterComponent(ddlWorkflowStatus);
                AddEnterComponent(txtTotAmountFrom);
                AddEnterComponent(txtTotAmountTo);
                if (logic.User.IsAuthorizedFinance || logic.User.IsDirectorUp || UserData.Roles.Contains("ELVIS_ADMIN"))
                {
                    AddEnterComponent(IssuingDivisionDropDown);
                }
                else
                {
                    IssuingDivisionDropDown.Enabled = false;
                }
                AddEnterAsSearch(btnSearch, _ScreenID);
                #endregion
            }
        }


        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }

            btnNew.Visible = true;

            if (ValidateInputSearch())
            {
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                gridGeneralInfo.DataBind();

                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.GetRowValues(i, "STATUS_NAME").ToString() == "Draft")
                    {
                        btnNew.Visible = false;
                        break;
                    }
                }
            }

            btnView.Visible = false;
        }


        #region Set Button Visible
        private void SetFirstLoad()
        {
            btnSearch.Visible = true;
            btnClear.Visible = true;
            btnView.Visible = false;
            btnClose.Visible = true;
            btnNew.Visible = RoLo.isAllowedAccess("btnNew");
            btnDelete.Visible = RoLo.isAllowedAccess("btnDelete");
        }

        private void SetSearchLoad(bool noData)
        {
            if (noData)
            {
                SetFirstLoad();
            }
            else
            { 
                btnSearch.Visible = true;
                btnClear.Visible = true;
                btnView.Visible = false;
                btnClose.Visible = true;
                btnNew.Visible = RoLo.isAllowedAccess("btnNew");
                btnDelete.Visible = RoLo.isAllowedAccess("btnDelete");
            }
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.PageCount > 0)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
                    if (txtGridSpentAmount != null)
                    {
                        txtGridSpentAmount.Visible = false;
                        litGridSpentAmount.Visible = true;
                    }

                    HyperLink hypDetail = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["SHIFTING_NO"] as GridViewDataColumn
                        , "hypDetail") as HyperLink;

                    string shiftingNo = e.GetValue("SHIFTING_NO").str();

                    if (hypDetail != null)
                    {
                        hypDetail.NavigateUrl = string.Format("javascript:openWin('/80Accrued/AccrShiftingForm.aspx?shiftingno={0}&mode=view', 'ELVIS_Screen_8014')", shiftingNo);
                        hypDetail.Text = shiftingNo;
                    }
                }
            }
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = logic.AccrShiftingList.Search(SearchCriteria()).ToList();
            SetSearchLoad(!ListInquiry.Any());

            e.Result = ListInquiry;
        }

        protected AccruedShiftingSearchCriteria SearchCriteria()
        {
            return new AccruedShiftingSearchCriteria(
                    sel.SelectedValues(CommonFunction.CommaJoin(UserData.Divisions, ";"), (UserData != null) ? UserData.isFinanceDivision : false),
                    txtAccrNo.Text, ddlSubSts.SelectedValue, ddlWorkflowStatus.SelectedValue, txtPVYear.Text,
                    txtTotAmountFrom.Text, txtTotAmountTo.Text, dtCreatedDateFrom.Text, dtCreatedDateTo.Text
                );
        }

        #endregion
        #endregion

        #region Buttons
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //logic.Say("btnSearch_Click", "Search {0} {1}", ddlSuspenseNo.Text, txtPVYear.Text);
            DoSearch();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (IssuingDivisionDropDown.Enabled)
                sel.Clear();
            txtPVYear.Text = null;
            txtAccrNo.Text = null;
            ddlSubSts.SelectedIndex = 0;
            dtCreatedDateFrom.Text = null;
            dtCreatedDateTo.Text = null;
            ddlWorkflowStatus.SelectedIndex = 0;
            txtTotAmountFrom.Text = null;
            txtTotAmountTo.Text = null;

            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logic.Say("btnDelete_Click", "Delete");
            if (gridGeneralInfo.Selection.Count == 1)
            {
                int idx = 0;
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        idx = i;
                        break;
                    }
                }

                string shiftingNo = gridGeneralInfo.GetRowValues(idx, "SHIFTING_NO").ToString();

                Boolean checkValid = logic.AccrShiftingList.isDraft(shiftingNo);

                if (checkValid)
                {
                    lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete");
                    ConfirmationDeletePopUp.Show();
                }
                else
                {
                    Nag("MSTD00226ERR", "Submission Status is not Draft or Rejected by Budget.");
                }
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                #region Error

                Nag("MSTD00016WRN");
                #endregion
            }
            else
            {
                Nag("MSTD00009WRN");
            }

        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            string shiftingNo = "";
            if (gridGeneralInfo.Selection.Count != 1) return;

            int row = GetFirstSelectedRow();
            shiftingNo = gridGeneralInfo.GetRowValues(row, "SHIFTING_NO").ToString();

            logic.Say("btnOkConfirmationDelete_Click", String.Format("Confirm Delete Shifting no : {0}", shiftingNo));

            if (!String.IsNullOrEmpty(shiftingNo) && logic.AccrShiftingList.isDraft(shiftingNo))
            {
                result = logic.AccrShiftingList.Delete(shiftingNo, UserName);
                if (!result)
                {
                    Nag("MSTD00014ERR");
                    gridGeneralInfo.Selection.UnselectAll();
                }
                else
                {
                    Nag("MSTD00015INF");
                    gridGeneralInfo.Selection.UnselectAll();
                    DoSearch();
                }
            }
            else
            {
                Nag("MSTD00216ERR", "Draft List Accrued");
            }
        }

        protected void BtnCancelConfirmationDelete_Click(object sender, EventArgs e)
        {
            ConfirmationDeletePopUp.Hide();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            var crit = new AccruedSearchCriteria();
            crit.issuingDivision = UserData.DIV_CD.ToString();
            crit.budgetYear = DateTime.Now.Year - 1;
            crit.ignoreIssuingDivision = false;
            
            var qdata = logic.AccrList.SearchByDiv(crit);
            if (qdata.Any())
            {
                bool anyNotPosted = qdata.Any(x => x.STATUS_CD != 403);

                if (anyNotPosted)
                {
                    Nag("MSTD00002ERR", "There is still Document that has not Posted to SAP");
                    return;
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "ELVIS_AccrShiftingForm",
                    "openWin('AccrShiftingForm.aspx?shiftingno=new','ELVIS_AccrShiftingForm');",
                    true);
            
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count == 1)
            {
                int row = GetFirstSelectedRow();
                string shiftingNo = gridGeneralInfo.GetRowValues(row, "SHIFTING_NO").ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "ELVIS_AccrShiftingForm",
                    "openWin('AccrShiftingForm.aspx?shiftingno=" + shiftingNo + "&mode=view','ELVIS_AccrShiftingForm');",
                    true);
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                #region Error

                Nag("MSTD00016WRN");
                #endregion
            }
            else
            {
                Nag("MSTD00009WRN");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonDownload_Click", "Download");
            if (ValidateInputSearch())
            {
                DownloadList();
            }

        }

        protected void DownloadList()
        {

            UserData userdata = (UserData)Session["UserData"];

            try
            {
                Xmit.Send(logic.AccrShiftingList.Get(
                            UserName,
                            Server.MapPath(Common.AppSetting.CompanyLogo),
                            SearchCriteria()
                        ), HttpContext.Current, "AccruedList" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls");
            }


            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                Nag("MSTD00002ERR", "Error Download List : " + ex.Message);
            }
            //this.Response.End();
        }

        private void SplitReffNo(string reffno, out string Num, out string Year)
        {
            if (reffno.Length > 4)
            {
                Num = reffno.Substring(0, reffno.Length - 4);
                Year = reffno.Substring(reffno.Length - 4, 4);
            }
            else
            {
                Num = "";
                Year = "";
            }
        }


        private void SetLiteralToTextDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
                    if (txtGridSpentAmount != null)
                    {
                        txtGridSpentAmount.Visible = true;
                        litGridSpentAmount.Visible = false;
                    }
                }
            }
        }


        private void SetTextToLiteralDetail()
        {
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                    gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                    gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
                if (txtGridSpentAmount != null)
                {
                    txtGridSpentAmount.Visible = false;
                    litGridSpentAmount.Visible = true;
                }
            }
        }
        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            bool valid = ValidateInputSearch();
            if (valid)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
            

                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;

                    if (txtGridSpentAmount != null && String.IsNullOrWhiteSpace(txtGridSpentAmount.Text))
                    {
                        Nag("MSTD00017WRN", "Spent");
                        valid = false;
                    }
                    else
                    {
                        if (!CommonFunction.IsDecimal(txtGridSpentAmount.Text))
                        {
                            litMessage.Text += Nagging("MSTD00018WRN", "Spent");
                            valid = false;
                        }
                        else
                        {
                            Match match = Regex.Match(txtGridSpentAmount.Text, @"^[0-9]{1,14}([\.][0-9]{1,2})?$",
                                RegexOptions.IgnoreCase);
                            if (!match.Success)
                            {
                                litMessage.Text += Nagging("MSTD00032WRN", "Spent", "max (14 digits and 2 decimal)");
                                valid = false;
                            }
                            else
                            {
                                //fid.Hadid 20180409
                                string bookingNo = logic.Look.getBookingNoBySuspense(suNO, suYY);
                                if(!String.IsNullOrEmpty(bookingNo))
                                {
                                    HiddenField hidGridAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                                        gridGeneralInfo.Columns["AMOUNT"] as GridViewDataColumn, "hidGridAmount") as HiddenField;
                                    decimal susAmt = Convert.ToDecimal(hidGridAmount.Value);
                                    decimal spentAmt = Convert.ToDecimal(txtGridSpentAmount.Value);
                                    if (spentAmt > susAmt)
                                    {
                                        litMessage.Text += Nagging("MSTD00083ERR", "Spent Amount of Accrued Suspense", "Suspense Amount");
                                        valid = false;
                                    }
                                }
                                //end fid.Hadid
                            }
                        }
                    }

                    if (!valid)
                        break;
                }
            }
            return valid;
        }

        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw ;
            }
            return true;
        }

        private void Focus_On_Next_Empty_TextBox(int currentIndex)
        {
            bool setFocus = false;
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (txtGridSpentAmount != null && (String.IsNullOrWhiteSpace(txtGridSpentAmount.Text)))
                    {
                        txtGridSpentAmount.Focus();
                        setFocus = true;
                        break;
                    }
                }

                if (!setFocus)
                {
                    ASPxTextBox nxtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(currentIndex + 1,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (nxtGridSpentAmount != null)
                    {
                        nxtGridSpentAmount.Focus();
                    }
                }
            }
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetUserDivisions(UserData);
        }

        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 1)
                    color = red;
                else if (statusInt == 2)
                    color = yellow;
                else if (statusInt > 2)
                    color = green;
            }
            return color;
        }

        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            PageSize = newSize;
            // Session[GridCustomPageSizeName] = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                if (ValidateInputSearch())
                {
                    grid.DataBind();

                }
            }
        }

        protected int GetFirstSelectedRow()
        {
            int row = -1;
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                if (!gridGeneralInfo.Selection.IsRowSelected(i)) continue;

                row = i;
                break;
            }
            return row;
        }
        #endregion
    }
}