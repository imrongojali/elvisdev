﻿<%@ Page Title="List Accrued Result" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrResultList.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrResultList" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        function convertConfirmation() {
            var selection = confirm("Convert Settlement into PV and/or RV ?");
            if (selection) {
                $("#hidDialogConfirmation").val("y");
            } else {
                $("#hidDialogConfirmation").val("n");
            }
            loading();
            return selection;
        }

        function hidePop() {
            var pop = $find("ConfirmationDeletePopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#rvLink").length > 0) {
                $("#rvLink").click(function () {
                    if ($("#pvLink").length < 1) 
                        hidePop();
                });
            }

            if ($("#pvLink").length > 0) {
                $("#pvLink").click(function () {
                    if ($("#rvLink").length < 1)
                        hidePop();
                });
            }
        }

        function search() {
            var btn = $("#btnSearch");
            if (btn.length > 0) {
                btn.click();
            };
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }

        function isNumberKey(evt) {
            var thestring = evt.inputElement.value;
            var thenum = thestring.replace(/\D/g, "");
            evt.inputElement.value = thenum;
            return true;
        }

        function OnSelectSuspense(s, e) {
            var sus = $("#ddlSuspenseNo"); 
            if (sus != null && sus.length > 0) {
                search();   
            } else {                
            }
        }
    </script>

<style type="text/css">
    .boxDesc
    {
        border: 1px solid #afafaf;
        border-style: inset;
        background-color: #efefef;
        display: block;              
        overflow: auto;
        padding-left: 5px;
        height: 22px;
        float: left;
        vertical-align: middle;
    }
   
    #divTransactionType
    {
        width: 350px;
    }
    #divVendorCode 
    {
        width: 80px;
    }
    #divVendorName
    {
        width: 254px;
    }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />

            <div class="contentsection" style="width:987px !important;">
                <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                    <tr>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Issuing Division <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxIssueDiv" Width="100px" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 150px" class="td-layout-item" colspan="2">
                            Exchange Rate
                        </td>
                        <td valign="middle" style="width: 150px" class="td-layout-item" colspan="2">
                            PIC
                        </td>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Total Amount <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 125px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxTotAmt" Width="110px" CssClass="display-inline-table" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Accrued No <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxAccruedNo" Width="100px" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-item">
                            USD to IDR <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxUsdIdr" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 80px" class="td-layout-item">
                            Current <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxPicCur" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Submission Status <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 125px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxSubmSts" Width="110px" CssClass="display-inline-table" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Updated Date <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxUpdateDt" Width="100px" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-item">
                            JPY to IDR <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxJpyIdr" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 80px" class="td-layout-item">
                            Next <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxPicNext" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Workkflow Status <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 125px" class="td-layout-value value-pv">

                            <dx:ASPxLabel runat="server" ID="tboxWFSts" Width="110px" CssClass="display-inline-table" />
                        </td>
                    </tr>
                </table>
                <div class="rowbtn" style="text-align: right">
                </div>
            </div>
            <div class="contentsection" style="width:987px !important; margin-top:10px; margin-bottom:5px">
                <table>
                    <tr><td>
                        
                    <div class="rowbtn" style="text-align: left">
                            <asp:LinkButton ID="linkShowDetail" runat="server">
                                <asp:Label ID="lblText" runat="server" />
                            </asp:LinkButton>
                    </div>
                        </td></tr>
                </table>
                <asp:Panel runat="server" ID="pSearchCrit">
                    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; border-collapse: collapse">
                        <tr>
                            <td colspan="4" class="td-layout-item">&nbsp;</td>
                            <td colspan="2" valign="middle" class="td-layout-item colapset1" >
                                Existing
                            </td>
                            <td colspan="2" valign="middle" class="td-layout-item colapset2">
                                Accrued
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="width: 130px" class="td-layout-item">
                                Booking No. <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" colspan="3" style="width: 105px" class="td-layout-value">
                                <dx:ASPxTextBox runat="server" ID="tboxBookingNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                            <td valign="middle" style="width: 130px" class="td-layout-item colapset1">
                                WBS No. <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" style="width: 105px" class="td-layout-value colapset1">
                                <dx:ASPxTextBox runat="server" ID="tboxOldWbsNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                            <td valign="middle" style="width: 130px" class="td-layout-item colapset2">
                                WBS No. <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" style="width: 105px" class="td-layout-value colapset2">
                                <dx:ASPxTextBox runat="server" ID="tboxNewWbsNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="width: 130px" class="td-layout-item">
                                Doc. Type <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" colspan="3" style="width: 105px" class="td-layout-value">
                                <asp:DropDownList ID="DropDownListPVType" runat="server" Width="103px">
                                </asp:DropDownList>
                            </td>
                            <td valign="middle" style="width: 130px" class="td-layout-item colapset1">
                                WBS Description <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" style="width: 105px" class="td-layout-value colapset1">
                                <dx:ASPxTextBox runat="server" ID="tboxOldWbsDesc" Width="100px" CssClass="display-inline-table" />
                            </td>
                            <td valign="middle" style="width: 130px" class="td-layout-item colapset2">
                                WBS Description <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" style="width: 105px" class="td-layout-value colapset2">
                                <dx:ASPxTextBox runat="server" ID="tboxNewWbsDesc" Width="100px" CssClass="display-inline-table" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="width: 130px" class="td-layout-item">
                                Activity <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" colspan="3" style="width: 105px" class="td-layout-value">
                                <dx:ASPxTextBox runat="server" ID="tboxActivity" Width="100px" CssClass="display-inline-table" />
                            </td>
                            <td valign="middle" style="width: 130px" class="td-layout-item colapset1">
                                Suspense No <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" style="width: 105px" class="td-layout-value colapset1">
                                <dx:ASPxTextBox runat="server" ID="tboxOldSusNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                            <td valign="middle" style="width: 130px" class="td-layout-item colapset2">
                                Suspense No <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" style="width: 105px" class="td-layout-value colapset2">
                                <dx:ASPxTextBox runat="server" ID="tboxNewSusNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                        </tr>
                       <%-- <tr>
                            <td colspan="4" valign="middle" class="td-layout-item">
                                Amount
                            </td>
                        </tr>--%>
                        <tr>
                            <td valign="middle" style="width: 130px" class="td-layout-item">
                              Amount  Accrued <span class="right-bold">:</span>
                            </td>
                            <td colspan="3" valign="middle" class="td-layout-item">
                                <dx:ASPxTextBox runat="server" ID="tboxAmtAccrFrom" Width="100px" CssClass="display-inline-table" HorizontalAlign="Right" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey" />&nbsp;&nbsp;
                                To &nbsp;&nbsp;
                                <dx:ASPxTextBox runat="server" ID="tboxAmtAccrTo" Width="100px" CssClass="display-inline-table" HorizontalAlign="Right" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey" />&nbsp;&nbsp;
                            </td>
                        </tr>
                        <%--<tr>
                            <td valign="middle" style="width: 130px" class="td-layout-item">
                                Spent <span class="right-bold">:</span>
                            </td>
                            <td colspan="3" valign="middle" class="td-layout-item">
                                <dx:ASPxTextBox runat="server" ID="tboxAmtSpentFrom" Width="100px" CssClass="display-inline-table" HorizontalAlign="Right" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey" />&nbsp;&nbsp;
                                To &nbsp;&nbsp;
                                <dx:ASPxTextBox runat="server" ID="tboxAmtSpentTo" Width="100px" CssClass="display-inline-table" HorizontalAlign="Right" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey" />&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" style="width: 130px" class="td-layout-item">
                                Remaining <span class="right-bold">:</span>
                            </td>
                            <td colspan="3" valign="middle" class="td-layout-item">
                                <dx:ASPxTextBox runat="server" ID="tboxAmtRemainFrom" Width="100px" CssClass="display-inline-table"
                                    HorizontalAlign="Right" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey" />&nbsp;&nbsp;
                                To &nbsp;&nbsp;
                                <dx:ASPxTextBox runat="server" ID="tboxAmtRemainTo" Width="100px" CssClass="display-inline-table"
                                    HorizontalAlign="Right" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey" />&nbsp;&nbsp;
                            </td>
                        </tr>--%>
                    </table>
                    <div style="text-align: right">
                        <asp:Button ID="btnDownload" runat="server" OnClick="btnDownload_Click" Text="Download" CssClass="xlongButton" SkinID="xlongButton" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnSearch" runat="server" Text="Search" ClientIDMode="Static" onclick="btnSearch_Click" OnClientClick="loading()"  />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" />
                    </div>
                    </asp:Panel>
                    
                    <cc1:CollapsiblePanelExtender ID="collapsiblePanelExtCriteria" runat="server" TargetControlID="pSearchCrit"
                        CollapseControlID="linkShowDetail" ExpandControlID="linkShowDetail" Collapsed="true"
                        TextLabelID="lblText" CollapsedText="Search Filter" ExpandedText="Collapse"
                        CollapsedSize="0">
                    </cc1:CollapsiblePanelExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="List Accrued Result" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />



    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
        <ContentTemplate>
            <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                <tr>                       
                    <td>
                        <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="980px" 
                            AutoGenerateColumns="False"
                            ClientInstanceName="gridGeneralInfo" 
                            OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                            KeyFieldName="ACCRUED_NO" 
                            OnCustomCallback="grid_CustomCallback" 
                            OnCustomButtonCallback="grid_CustomButtonCallback"
                            EnableCallBacks="false"
                            Styles-AlternatingRow-CssClass="even">
                            <Columns>
                                <%--<dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                    CellStyle-VerticalAlign="Middle" FixedStyle="Left">
                                    <EditButton Visible="false" Text=" ">
                                    </EditButton>
                                    <UpdateButton Visible="false" Text=" ">
                                    </UpdateButton>
                                    <NewButton Visible="false" Text=" ">
                                    </NewButton>
                                    <CancelButton Visible="false" Text=" ">
                                    </CancelButton>
                                    <CellStyle VerticalAlign="Middle">
                                    </CellStyle>
                                    <HeaderTemplate>
                                        <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientInstanceName="SelectAllCheckBox"
                                            ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked(); }"
                                            CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                        </dx:ASPxCheckBox>
                                    </HeaderTemplate>
                                </dx:GridViewCommandColumn>--%>
                                <dx:GridViewDataTextColumn Caption="Booking No" FieldName="BOOKING_NO" Width="150px" CellStyle-HorizontalAlign="Left"
                                    VisibleIndex="1" FixedStyle="Left">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Doc. Type" FieldName="PV_TYPE_NAME" Width="65px"
                                    VisibleIndex="2" CellStyle-HorizontalAlign="Left" >
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Activity" VisibleIndex="3" Width="85px" ButtonType="Button">
                                    <CellStyle VerticalAlign="Middle" />
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnActivity" Text="View">
                                            <%--<Image ToolTip="View Activity" Url="~/App_Themes/BMS_Theme/Images/redbox.png" ></Image>--%>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewBandColumn Caption="Legacy Budget" VisibleIndex="4">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Wbs No." FieldName="WBS_NO_OLD" VisibleIndex="0"
                                            Width="150px" CellStyle-HorizontalAlign="Left">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WBS Description" FieldName="WBS_DESC" VisibleIndex="1"
                                            Width="210px" CellStyle-HorizontalAlign="Left">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Suspense No" FieldName="SUSPENSE_NO_OLD" VisibleIndex="2"
                                            Width="100px" CellStyle-HorizontalAlign="Left">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Accrued Budget" VisibleIndex="5">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Wbs No." FieldName="WBS_NO" VisibleIndex="0"
                                            Width="150px" CellStyle-HorizontalAlign="Left">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WBS Description" FieldName="WBS_DESC_PR" VisibleIndex="1"
                                            Width="210px" CellStyle-HorizontalAlign="Left">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Suspense No" FieldName="SUSPENSE_NO_PR" VisibleIndex="2"
                                            Width="100px" CellStyle-HorizontalAlign="Left">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewDataTextColumn Caption="Accrued Amount" FieldName="INIT_AMT" VisibleIndex="6"
                                    Width="120px" CellStyle-HorizontalAlign="Right" >
                                    <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="SAP Doc. No."  FieldName="SAP_DOC_NO" VisibleIndex="7" Width="165px"
                                     CellStyle-HorizontalAlign="Left" >
                                    <PropertiesTextEdit  EncodeHtml="False" />
                                    <DataItemTemplate>
                                        <asp:LinkButton runat="server" ID="linkSapDocNo" OnClick="LoadGridPopUpSapDocNo"
                                            Text='<%# Eval("SAP_DOC_NO") %>' CssClass="smallFont"></asp:LinkButton>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                               
                            </Columns>
                            <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                            <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" VerticalScrollableHeight="330" />
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <AlternatingRow CssClass="even">
                                </AlternatingRow>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                    <Paddings PaddingTop="1px" PaddingBottom="1px" />
                                </Cell>
                                <Row CssClass="doubleRow" />
                            </Styles>
                            <Settings ShowStatusBar="Visible" />
                            <SettingsPager Visible="false" />
                            <Templates>
                                <StatusBar>
                                    <div style="text-align: left;">
                                        Records per page: <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                            <option value="5" <%# WriteSelectedIndexInfo(5) %>>5</option>
                                            <option value="10" <%# WriteSelectedIndexInfo(10) %>>10</option>
                                            <option value="15" <%# WriteSelectedIndexInfo(15) %>>15</option>
                                            <option value="20" <%# WriteSelectedIndexInfo(20) %>>20</option>
                                            <option value="25" <%# WriteSelectedIndexInfo(25) %>>25</option>
                                            <option value="50" <%# WriteSelectedIndexInfo(50) %>>50</option>
                                            <option value="100" <%# WriteSelectedIndexInfo(100) %>>100</option>
                                            <option value="150" <%# WriteSelectedIndexInfo(150) %>>150</option>
                                            <option value="200" <%# WriteSelectedIndexInfo(200) %>>200</option>
                                            <option value="250" <%# WriteSelectedIndexInfo(250) %>>250</option>
                                            <option value="500" <%# WriteSelectedIndexInfo(500) %>>500</option>
                                        </select>&nbsp; <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">&lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a> &nbsp;
                                        Page <input type="text" onchange="if(!gridGeneralInfo.InCallback()) gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                            onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                            value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                            style="width: 20px" />of <%# gridGeneralInfo.PageCount%>&nbsp; <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%><a
                                            title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp; <a title="Last"
                                                href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; 
                                    </div>
                                </StatusBar>
                            </Templates>
                        </dx:ASPxGridView>
                        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            EntityTypeName=""
                            OnSelecting="LinqDataSource1_Selecting" TableName="TB_R_ACCR_LIST_H" >
                        </asp:LinqDataSource>
                        <asp:LinqDataSource ID="dsDivision" runat="server" OnSelecting="dsDivision_Selecting"/>

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
                <tr>
                    <td colspan="3" valign="top" style="width: 270px" align="right">
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <!-- Pop up SAP Doc No -->
    <asp:Button ID="ButtonHidSapDocNo" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupSapDocNo" runat="server" TargetControlID="ButtonHidSapDocNo"
        PopupControlID="panelSapDocNo" CancelControlID="ButtonCloseSapDocNo" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelSapDocNo" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelSapDocNo">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgSapDocNo"></asp:Literal><asp:Literal runat="server"
                        ID="LitModalSapDocNo"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="600px">
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridSapDocNo" runat="server" Width="800px" Visible="true" KeyFieldName="CURRENCY_CD"
                                        Styles-AlternatingRow-CssClass="even" AutoGenerateColumns="false" OnHtmlRowCreated="gridSapDocNo_HtmlRowCreated">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="50px" Name="NO">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoSapDocNo" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Invoice No" VisibleIndex="0" FieldName="INVOICE_NO" Width="150px"/>
                                            <dx:GridViewDataTextColumn Caption="SAP Doc No" VisibleIndex="1" FieldName="SAP_DOC_NO" Width="150px" />                                            
                                            <dx:GridViewDataTextColumn Caption="Sap Clearing Doc No" VisibleIndex="2" FieldName="SAP_CLEARING_DOC_NO" Width="150px"/>                                                                           <dx:GridViewDataTextColumn Caption="Pay Prop Doc No" VisibleIndex="3" FieldName="PAYPROP_DOC_NO" Width="140px"/>
                                            <dx:GridViewDataTextColumn Caption="Pay Prop ID" VisibleIndex="4" FieldName="PAYPROP_ID" Width="100px"/>
                                        </Columns>
                                        
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False"  ColumnResizeMode="Control"/>
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="false"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                            <Cell HorizontalAlign="Center">
                                            </Cell>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="ButtonCloseSapDocNo" ClientIDMode="Static" Text="Close" OnClick="ButtonClosePopUpSapDocNo_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <asp:Button ID="hidBtnPopup" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popupActivity" runat="server" TargetControlID="hidBtnPopup"
        PopupControlID="panelActivity" CancelControlID="btnCloseActivity" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelActivity" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="upnModal">
                <ContentTemplate>
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="400px">
                        <tr>
                            <td valign="middle" style="width:98px" class="td-layout-item">
                                Booking No. <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" class="td-layout-item">
                                <dx:ASPxLabel runat="server" ID="lblBookingNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <dx:ASPxGridView ID="gridActivity" runat="server" Width="100%" ClientInstanceName="gridActivity"
                                    KeyFieldName="BOOKING_NO" AutoGenerateColumns="False">
                                    <Columns>
                                        <dx:GridViewDataColumn Caption="Activity Description" FieldName="ACTIVITY_DES" VisibleIndex="0">
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; float:right">
                                <br />
                                <asp:Button runat="server" ID="btnCloseActivity" Text="Close" OnClick="btnCloseActivity_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
 
</asp:Content>
