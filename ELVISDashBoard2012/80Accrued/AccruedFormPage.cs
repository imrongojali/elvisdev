﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ELVISDashBoard.presentationHelper;
using DevExpress.Web.ASPxGridLookup;
using System.Web.UI.WebControls;
using System.Text;
using DevExpress.Web.ASPxEditors;
using Common.Messaging;
using Common.Data;
using Common.Function;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using BusinessLogic._80Accrued;
using BusinessLogic.AccruedForm;
using DevExpress.Web.ASPxGridView;
using System.Web.UI;
using AjaxControlToolkit;
using Common.Data._80Accrued;
using Common.Control;
using System.Collections.Specialized;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using Common;
using DevExpress.Web.Data;

namespace ELVISDashBoard._80Accrued
{
    public abstract class AccruedFormPage : BaseAccrFormBehind
    {
        public const string SCREEN_TYPE_ACCR = "Accrued";
        public const int MAX_PAGE_ROW = 5;

        public const int VIEW_MODE = 0;
        public const int ADD_MODE = 1;
        public const int EDIT_MODE = 2;

        protected string _ScreenID;
        protected string ScreenType { set; get; }
        protected readonly FormPersistence formPersistence;
        
        protected readonly AccrListLogic lilo;
        protected int Mode
        {
            get { return Convert.ToInt32(ViewState["MODE_ACCR_FORM"]); }
            set { ViewState["MODE_ACCR_FORM"] = value; }
        }

        protected ASPxGridView HistoryGrid { set; get; }
        protected ASPxGridView DetailGrid { set; get; }
        protected ASPxGridView AttachmentGrid { set; get; }
        protected ASPxComboBox AttachmentCategoryDropDown { set; get; }
        protected ASPxHyperLink DownloadUploadTemplateLink { set; get; }
        protected ASPxLabel IssuingDivisionLabel { set; get; }
        protected ASPxLabel AccruedNoLabel { set; get; }
        protected ASPxLabel USDIDRLabel { set; get; }
        protected ASPxLabel JPYIDRLabel { set; get; }
        protected ASPxLabel PicCurLabel { set; get; }
        protected ASPxLabel PicNextLabel { set; get; }
        protected ASPxLabel TotAmountLabel { set; get; }
        protected ASPxLabel CreatedDtLabel { set; get; }
        protected ASPxLabel SubmStsLabel { set; get; }
        protected ASPxLabel WorkflowStsLabel { set; get; }
        protected ASPxDateEdit PostingDate { get; set; }
        protected Literal MessageControlLiteral { set; get; }
        protected Literal TotalAmountLiteral { set; get; }
        protected Literal PostingDateLiteral { set; get; }
        protected ASPxComboBox NoteRecipientDropDown { set; get; }
        protected TextBox NoteTextBox { set; get; }
        protected Repeater NoteRepeater { set; get; }
        protected HtmlGenericControl NoteCommentContainer { set; get; }
        protected HiddenField HiddenMessageExpandFlag { set; get; }
        protected HiddenField HiddenScreenID { set; get; }
        protected Button EditButton { set; get; }
        protected Button SaveButton { set; get; }
        protected Button CancelButton { set; get; }
        protected ASPxButton CloseButton { set; get; }
        protected ASPxButton ClsButton { set; get; }
        protected Button CheckBudgetButton { set; get; }
        protected Button ExportButton { set; get; }
        protected Button SubmitButton { set; get; }
        protected ASPxButton ApproveButton { set; get; }
        protected ASPxButton RejectButton { set; get; }
        protected Button DataUploadButton { set; get; }
        protected Button PostSAPButton { set; get; }
        protected Button WbsUpdateButton { set; get; }
        protected Button SimulateUserButton { set; get; }
        protected FileUpload DataUpload { set; get; }
        protected FileUpload AttachmentUpload { set; get; }
        protected HtmlControl UploadDiv { set; get; }
        protected ASPxGridLookup BudgetNumberLookup { set; get; }
        protected ModalPopupExtender AttachmentPopup { set; get; }
        protected ModalPopupExtender SAPpop = null;
        protected HtmlGenericControl DownloadEntertainmentTemplateLink { set; get; }
        protected ModalPopupExtender PopUpRevise { set; get; }
        protected Button BtnReviseProceed { set; get; }
        protected Button BtnReviseCancel { set; get; }
        protected TextBox TxtReviseAccruedNo { get; set; }
        protected DropDownList DdlReviseCategory { get; set; }
        protected TextBox TxtReviseComment { get; set; }
        protected int[] CanApproveReject = new int[] { 101, 102, 113 };

        private List<ExchangeRate> _exchRate = null;
        private List<ExchangeRate> ExchRate
        {
            get 
            {
                if (_exchRate == null)
                {
                    _exchRate = logic.Look.getExchangeRates();
                }
                return _exchRate;
            }
        }

        protected AccrFormTable Tabel
        {
            get
            {
                return FormData.FormTable;
            }
        }

        protected readonly string SID_DATA = "AccrFormData";

        protected AccrFormData FormData
        {
            set
            {
                Session[ScreenType + SID_DATA] = value;
            }
            get
            {
                AccrFormData f = Session[ScreenType + SID_DATA] as AccrFormData;
                if (f == null)
                {
                    f = new AccrFormData();

                    int pvNumber = 0;
                    int pvYear = int.Parse(string.Format("{0:yyyy}", DateTime.Now));
                    f.PVNumber = pvNumber;
                    f.PVYear = pvYear;
                    f.StatusCode = null;
                    AccrFormTable Tabel = f.FormTable;

                    Session[ScreenType + SID_DATA] = f;
                }
                return f;
            }
        }

        protected string _accruedNo
        {
            get
            {
                if (FormData.AccruedNo.isEmpty())
                {
                    //FormData.AccruedNo = logic.AccrForm.getAccrNo(UserData.DIV_CD.fmt(0), DateTime.Now.Year - 1); // cuman buat testing tar ilangin -1 nya
                    FormData.AccruedNo = logic.AccrForm.getAccrNo(UserData.DIV_CD.fmt(0), DateTime.Now.Year);
                }
                return FormData.AccruedNo;
            }
        }

        public AccruedFormPage(string screenType)
        {
            //Mode = VIEW_MODE;
            ScreenType = screenType;
            switch (screenType)
            {
                case SCREEN_TYPE_ACCR:
                    formPersistence = logic.AccrForm;
                    lilo = logic.AccrList;
                    break;
                default:
                    formPersistence = logic.AccrForm;
                    lilo = logic.AccrList;
                    break;
            }
        }

        #region Page Load

        protected void Page_Init(object sender, EventArgs e)
        {
            postbackInit();
            base.Page_Init(sender, e);
        }

        public virtual void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            formPersistence.UserName = UserName;

            initFormTable();

            if (!IsPostBack)
            {
                Mode = getMode();
                
                if (UserData == null)
                {

                    Response.Write("<script language='javascript'> { window.close(); }</script>");
                    formPersistence.UserName = UserName;
                    return;
                }
                PrepareLogout();

                HiddenScreenID.Value = _ScreenID;
                init();
            }
            clearScreenMessage();
        }

        protected abstract void postbackInit();
        protected override void BaseElementInit()
        {
            BaseScreenType = SCREEN_TYPE_ACCR;
            BaseScreenID = _ScreenID;
            BaseAttachmentGrid = AttachmentGrid;
            BaseAttachmentUpload = AttachmentUpload;
            BaseAttachmentPopup = AttachmentPopup;
            BaseAttachmentCategoryDropDown = AttachmentCategoryDropDown;
            BaseNoteRecipientDropDown = NoteRecipientDropDown;
            BaseNoteTextBox = NoteTextBox;
            BaseNoteRepeater = NoteRepeater;
            BaseNoteCommentContainer = NoteCommentContainer;
            BaseHiddenMessageExpandFlag = HiddenMessageExpandFlag;
            BaseMessageControlLiteral = MessageControlLiteral;
        }
        protected override void BaseDataInit()
        {
            BaseData = new BaseAccrData();
            BaseData.Notes = FormData.Notes;
            BaseData.AttachmentTable = FormData.AttachmentTable;

            if (FormData.AccruedNo == null) return;

            BaseData.AccruedNo = FormData.AccruedNo;
            BaseData.ReffNo = FormData.ReffNo;
            BaseData.DivisionID = FormData.IssuingDiv;
            BaseData.StatusCode = FormData.StatusCode;
            BaseData.CreatedDt = FormData.CreatedDt;
        }

        protected void init()
        {
            clearSession();
            BaseElementInit();
            PrepareDataHeader();
            BaseDataInit();
            reloadFooter();
            evaluatePageComponentAvailibility();
            if (Mode == ADD_MODE)
                resetAttachment();

            //litle bit of hardcode. Budget user can resize column
            if (UserData.Roles.Contains("ELVIS_BUDGET"))
                DetailGrid.SettingsBehavior.ColumnResizeMode = DevExpress.Web.ASPxClasses.ColumnResizeMode.Control;

            //fid.Taufik performance tuning
            CbDataWBS = formPersistence.getWbsNumbersAccrued(UserData);
            CbDataCurrency = formPersistence.getCurrencyCodeListWOEurSgd();
            CbDataCostCenter = formPersistence.getCostCenterCodes();
            CbDocType = formPersistence.getDocTypes();
        }
        private void PrepareDataHeader()
        {
            if (Mode == ADD_MODE)
            {
                IssuingDivisionLabel.Text = normalizeDivisionName(UserData.DIV_NAME);
                AccruedNoLabel.Text = _accruedNo;

                CreatedDtLabel.Text = DateTime.Now.date();
            }
            else
            {
                if(FormData.AccruedNo.isEmpty())
                    evaluateExternalParameter();
                
                IssuingDivisionLabel.Text = normalizeDivisionName(logic.Vendor.GetDivisionName(FormData.IssuingDiv));
                AccruedNoLabel.Text = FormData.AccruedNo;
                PicNextLabel.Text = FormData.NextApprover;
                PicCurLabel.Text = FormData.CurrApprover;
                CreatedDtLabel.Text = FormData.CreatedDt.ToString("dd MMM yyyy");
                TotAmountLabel.Text = FormData.TotalAmount.fmt();
                SubmStsLabel.Text = FormData.SubmissionSts;
            }

            USDIDRLabel.Text = ExchRate
                .Where(x => x.CurrencyCode == "USD")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);

            JPYIDRLabel.Text = ExchRate
                .Where(x => x.CurrencyCode == "JPY")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
        }

        protected int getMode()
        {
            string mode = Request["mode"];
            int m = VIEW_MODE;

            if (mode.isEmpty())
            {
                m = ADD_MODE;
            }
            else if (mode.ToLower().Equals("view"))
            {
                m = VIEW_MODE;
            }
            else if (mode.ToLower().Equals("edit"))
            {
                m = EDIT_MODE;
            }
            return m;

        }

        private void loadApprovalHistory()
        {
            List<AccrFormHistory> data = logic.AccrForm.getApprovalHistory(FormData.AccruedNo, FormData.StatusCode.Int(), UserData);
            HistoryGrid.DataSource = data;
            HistoryGrid.DataBind();
        }
        protected void gridApprovalHistory_Load(object sender, EventArgs e)
        {
            if (FormData.AccruedNo != null)
                loadApprovalHistory();
        }

        protected void gridApprovalHistory_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                AccrFormHistory data = (AccrFormHistory)HistoryGrid.GetRow(e.VisibleIndex);
                if (data != null)
                {
                    Image imgStatus = HistoryGrid.FindRowCellTemplateControl(e.VisibleIndex, HistoryGrid.Columns["status"] as GridViewDataColumn, "imgStatus") as Image;

                    string img = "wait";
                    int stsCd = data.STATUS;
                    if (stsCd == 1)
                    {
                        img = "check";
                    }
                    else if (stsCd == 2)
                    {
                        img = "cross";
                    }


                    string logoPath = AppSetting.CompanyLogo.Substring(1, AppSetting.CompanyLogo.LastIndexOf("/"));
                    imgStatus.ImageUrl = logoPath + img + ".gif";
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
        }

        protected override void clearSession()
        {
            base.clearSession();
            Session[ScreenType + SID_DATA] = null;
            
            Session[ScreenType + "MsgList"] = null;
            Session["selectedWBS"] = null;
            Session["cbDataSuspense"] = null;
        }

        public void ReloadGrid()
        {
            DetailGrid.DataSource = Tabel.DataList;
            DetailGrid.SettingsPager.PageSize = Tabel.DataList.Count;
            DetailGrid.DataBind();
        }

        protected void ReOrderDetail()
        {
            Tabel.DataList.Sort(AccrFormDetail.RuleOrder);
            Tabel.resetDisplaySequenceNumber();
            Tabel.resetSequenceNumber();
        }

        public void initFirstRow()
        {
            Tabel.DataList.Clear();
            Tabel.addNewRow();
            ReloadGrid();
            DetailGrid.StartEdit(0);
            Tabel.EditIndex = 0;
            DetailGrid.SettingsBehavior.AllowFocusedRow = false;
        }
        protected void gridGeneralInfo_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            int curRow = e.VisibleIndex;

            if (e.DataColumn == null) return;

            if (e.DataColumn.FieldName == "SapAmtRemaining")
            {
                string sapString = e.GetValue("SapAmtRemaining") == null ? "" : e.GetValue("SapAmtRemaining").ToString();
                decimal sapAmtRem = 0;

                if (sapString.isEmpty())
                {
                    e.Cell.Style.Add("background-color", grey);
                }
                else if (Decimal.TryParse(sapString, out sapAmtRem))
                { 
                    if(sapAmtRem < 0)
                    {
                        e.Cell.Style.Add("background-color", red);
                    }
                }
            }
            else if (e.DataColumn.FieldName == "SapAmtAvailable")
            {
                string sapString = e.GetValue("SapAmtAvailable") == null ? "" : e.GetValue("SapAmtAvailable").ToString();
                
                if (sapString.isEmpty())
                {
                    e.Cell.Style.Add("background-color", grey);
                }
            }
        }

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {

            if (e.RowType == GridViewRowType.InlineEdit)
            {

                ASPxGridLookup lkpgridBudgetNo = DetailGrid.FindEditRowCellTemplateControl(
                    DetailGrid.Columns["WbsNumber"] as GridViewDataColumn, "lkpgridBudgetNo")
                    as ASPxGridLookup;
                ASPxGridLookup lkpgridSuspenseNo = DetailGrid.FindEditRowCellTemplateControl(
                    DetailGrid.Columns["SuspenseNo"] as GridViewDataColumn, "lkpgridSuspenseNo")
                    as ASPxGridLookup;

                if (Mode != VIEW_MODE)
                {
                    int _VisibleIndex = Tabel.EditIndex;

                    lkpgridBudgetNo.Value = Tabel.rows[_VisibleIndex].WbsNumber;

                    List<SuspenseStructure> listSuspense = lkpgridSuspenseNo.GridView.DataSource as List<SuspenseStructure>;
                    if (listSuspense != null)
                    {
                        string susNo = Tabel.rows[_VisibleIndex].SuspenseNo;
                        lkpgridSuspenseNo.Value = listSuspense.Where(x => x.PVNo.Equals(susNo))
                                                    .Select(x => x.ReffNo)
                                                    .FirstOrDefault()
                                                    .ToString();
                    }

                    //if (!Tabel.rows[_VisibleIndex].SuspenseNo.isEmpty())
                    //{
                    //    ControlSuspenseImpact(false);
                    //}
                }
            }
        }

        private bool isEnabledSuspense()
        {
            return Tabel.rows[Tabel.EditIndex].SuspenseNo.isEmpty();
        }

        public void ControlSuspenseImpact(bool enabled)
        {
            ASPxGridLookup ddlDocType = DetailGrid.FindEditRowCellTemplateControl(
                DetailGrid.Columns["PVType"] as GridViewDataColumn, "ddlDocType")
                as ASPxGridLookup;

            ASPxTextBox tboxActivity = DetailGrid.FindEditRowCellTemplateControl(
                DetailGrid.Columns["ActivityDescription"] as GridViewDataColumn, "tboxActivity")
                as ASPxTextBox;

            ASPxGridLookup lkpCostCenter = DetailGrid.FindEditRowCellTemplateControl(
                DetailGrid.Columns["CostCenterCode"] as GridViewDataColumn, "lkpgridCostCenterCode")
                as ASPxGridLookup;

            ASPxTextBox tboxAmount = DetailGrid.FindEditRowCellTemplateControl(
                DetailGrid.Columns["Amount"] as GridViewDataColumn, "tboxAmount")
                as ASPxTextBox;

            ASPxComboBox coCurrencyCode = DetailGrid.FindEditRowCellTemplateControl(
                DetailGrid.Columns["CurrencyCode"] as GridViewDataColumn, "coCurrencyCode")
                as ASPxComboBox;

            if (ddlDocType != null) ddlDocType.Enabled = enabled;
            if (tboxActivity != null) tboxActivity.Enabled = enabled;
            if (lkpCostCenter != null) lkpCostCenter.Enabled = enabled;
            if (tboxAmount != null) tboxAmount.Enabled = enabled;
            if (coCurrencyCode != null) coCurrencyCode.Enabled = enabled;
        }

        #endregion

        #region Detail

        public int getSelectedRow()
        {
            ReloadGrid();
            WebDataSelection selection = DetailGrid.Selection;
            int cntData = Tabel.DataList.Count;
            for (int i = 0; i < cntData; i++)
            {
                if (selection.IsRowSelected(i))
                {
                    return i;
                }
            }
            return 0;
        }
        public int OnSelectionChanged(object sender, EventArgs args)
        {
            if (DetailGrid.EditingRowVisibleIndex > -1)
            {
                DetailGrid.UpdateEdit();
                int idxSelected = getSelectedRow();
                DetailGrid.StartEdit(idxSelected);
                return idxSelected;
            }
            else
                return -1;
        }
        public void OnRowUpdating(object sender, ASPxDataUpdatingEventArgs args)
        {
            OrderedDictionary values = args.NewValues;

            if (values.Count > 0)
            {
                int sequenceNumber = (int)values[VouCol.SEQUENCE_NUMBER];
                if (sequenceNumber >= 0)
                {
                    Tabel.EditIndex = sequenceNumber - 1;
                }

                AccrFormDetail data = Tabel.getDataBySequence(sequenceNumber);
                if (data == null)
                {
                    Tabel.DataList.Add(new AccrFormDetail());
                }

                //apply this code for disabled field
                string susNo = Tabel.rows[Tabel.EditIndex].SuspenseNo;
                if (susNo.isEmpty())
                {
                    decimal? amount = values.Val("Amount", data.Amount) as decimal?;
                    string actDes = values.Val("ActivityDescription", data.ActivityDescription) as string;

                    data.Amount = amount ?? 0;
                    data.ActivityDescription = actDes;
                }
                else
                {
                    decimal amount = Convert.ToDecimal(DetailGrid.GetRowValuesByKeyValue(
                                    Convert.ToInt32(args.Keys[DetailGrid.KeyFieldName]), "Amount"));

                    string actDes = DetailGrid.GetRowValuesByKeyValue(Convert.ToInt32(args.Keys[DetailGrid.KeyFieldName]), "ActivityDescription").ToString();

                    data.Amount = amount;
                    data.ActivityDescription = actDes;
                }

                args.Cancel = true;
                DetailGrid.CancelEdit();
            }
        }
        protected void evt_gridAccrDetail_onSelectionChanged(object sender, EventArgs args)
        {
            int i = OnSelectionChanged(sender, args);
            Tabel.EditIndex = i;
            string cc = "", act = "", curr = "", wbs = "", sus = "";
            decimal amt = 0, amtIdr = 0, sapAmtAv = 0, sapAmtRe = 0;
            int seq = 0;
            if (i >= 0 && i < Tabel.DataList.Count)
            {
                seq = Tabel.DataList[i].SequenceNumber;
                wbs = Tabel.DataList[i].WbsNumber;
                sus = Tabel.DataList[i].SuspenseNo;
                act = Tabel.DataList[i].ActivityDescription;
                cc = Tabel.DataList[i].CostCenterCode;
                curr = Tabel.DataList[i].CurrencyCode;
                amt = Tabel.DataList[i].Amount;
                amtIdr = Tabel.DataList[i].AmountIdr;
                sapAmtAv = Tabel.DataList[i].SapAmtAvailable ?? 0;
                sapAmtRe = Tabel.DataList[i].SapAmtRemaining ?? 0;
            }
            logic.Say("evt_gridAccrDetail_onSelectionChanged", "{0} OnSelectionChanged [{1}] @{2} - {3} {4} '{5}' {6} {7} {8} {9} {10} {11}",
                ScreenType, i, seq, wbs, sus, act, cc, curr, amt, amtIdr, sapAmtAv, sapAmtRe);
        }
        
        protected void evt_gridAccrDetail_onCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs arg)
        {
            string fieldName = arg.Column.FieldName;
            string susNo = Tabel.rows[Tabel.EditIndex].SuspenseNo;
            bool enabledField = susNo.isEmpty();

            string[] disabledFields = new string[]
            {
                "PVType",
                "ActivityDescription",
                "CostCenterCode",
                "Amount",
                "CurrencyCode"
            };

            if (disabledFields.Contains(fieldName))
            {
                arg.Editor.Enabled = enabledField;
            }
        }
        protected void evt_gridAccrDetail_onRowUpdating(object sender, ASPxDataUpdatingEventArgs args)
        {
            OnRowUpdating(sender, args);
        }
        protected void evt_gridAccrDetail_onStartRowEditing(object sender, ASPxStartRowEditingEventArgs arg)
        {
            string key = arg.EditingKeyValue.ToString();

            ASPxEdit e = (ASPxEdit)DetailGrid.FindEditRowCellTemplateControl(
                (GridViewDataColumn)DetailGrid.Columns["WbsNumber"], "lkpgridBudgetNo");
            e.Focus();
        }
        protected void evt_gridAccrDetail_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;

            string argument = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";

            if (argument.Equals("CurrencyCode"))
            {
                if (!isNull(paramFracs[3]))
                {
                    int row = DetailGrid.EditingRowVisibleIndex;
                    if (row >= 0 && row < FormData.Details.Count)
                    {
                        updateCurrencyCode(DetailGrid.EditingRowVisibleIndex, paramFracs[3]);
                    }
                }
            }
            else if (argument.Equals("PVType"))
            {
                if (!isNull(paramFracs[3]))
                {
                    int row = DetailGrid.EditingRowVisibleIndex;
                    if (row >= 0 && row < FormData.Details.Count)
                    {
                        updatePVType(DetailGrid.EditingRowVisibleIndex, paramFracs[3]);
                    }
                }
            }
            else if (argument.Equals("WbsNumber"))
            {
                int row = DetailGrid.EditingRowVisibleIndex;
                if (row >= 0 && row < FormData.Details.Count)
                {
                    updateWbsNo(row,paramFracs[3]);
                }
            }
            else if (argument.Equals("SuspenseNo"))
            {
                int row = DetailGrid.EditingRowVisibleIndex;
                if (row >= 0 && row < FormData.Details.Count)
                {
                    updateSuspenseNo(row, paramFracs[3]);
                }
            }
        }

        private void updateSuspenseNo(int row, string susNo)
        {
            int idxEditing = row;
            DetailGrid.CancelEdit();

            if (isNull(susNo))
            {
                Tabel.rows[idxEditing].PVType = null;
                Tabel.rows[idxEditing].PVTypeCd = null;
                Tabel.rows[idxEditing].SuspenseNo = null;
                Tabel.rows[idxEditing].SuspenseNoYear = null;
                Tabel.rows[idxEditing].Amount = 0;
                Tabel.rows[idxEditing].AmountIdr = 0;
                Tabel.rows[idxEditing].CurrencyCode = null;
                Tabel.rows[idxEditing].ActivityDescription = null;
                Tabel.rows[idxEditing].CostCenterCode = null;
                //ControlSuspenseImpact(true);
            }
            else
            {
                SuspenseStructure selectedSuspense = CbDataSuspense.Where(x => x.PVNo.Equals(susNo)).FirstOrDefault();

                Tabel.rows[idxEditing].PVType = formPersistence.getDocType(2);
                Tabel.rows[idxEditing].PVTypeCd = 2;
                Tabel.rows[idxEditing].SuspenseNo = susNo;
                Tabel.rows[idxEditing].SuspenseNoYear = selectedSuspense.ReffNo.Value.ToString();
                Tabel.rows[idxEditing].Amount = selectedSuspense.TotalAmount ?? 0;
                Tabel.rows[idxEditing].AmountIdr = selectedSuspense.TotalAmount ?? 0;
                Tabel.rows[idxEditing].CurrencyCode = "IDR";
                Tabel.rows[idxEditing].ActivityDescription = selectedSuspense.ActivityDesc;
                Tabel.rows[idxEditing].CostCenterCode = selectedSuspense.CostCenter;
                //ControlSuspenseImpact(false);
            }

            ReloadGrid();
            DetailGrid.StartEdit(idxEditing);
        }

        private void updateWbsNo(int row, string wbsNo)
        {
            int idxEditing = row;
            DetailGrid.CancelEdit();

            if (isNull(wbsNo))
            {
                Tabel.rows[idxEditing].WbsNumber = null;
                Tabel.rows[idxEditing].WbsDesc = null;
            }
            else
            {
                Tabel.rows[idxEditing].WbsNumber = wbsNo;
                Tabel.rows[idxEditing].WbsDesc = logic.AccrForm.GetDescWBS(wbsNo);
            }

            Tabel.rows[idxEditing].SuspenseNo = null;
            Tabel.rows[idxEditing].SuspenseNoYear = null;
            //ControlSuspenseImpact(true);

            ReloadGrid();
            DetailGrid.StartEdit(idxEditing);
        }

        private void updateCurrencyCode(int row, string code)
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;

            Tabel.rows[row].CurrencyCode = code.ToUpper();

            var tboxAmount = (ASPxTextBox)DetailGrid.FindEditRowCellTemplateControl(
                (GridViewDataColumn)DetailGrid.Columns["Amount"], "tboxAmount");

            UpdateAmountColumns(row, Convert.ToDecimal(tboxAmount.Value));

            DetailGrid.CancelEdit();
            ReloadGrid();

            DetailGrid.StartEdit(idxEditing);
        }

        private void updatePVType(int row, string code)
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;

            var codeText = code.Split('|');

            Tabel.rows[row].PVTypeCd = Convert.ToInt16(codeText[0]);
            Tabel.rows[row].PVType = codeText[1];

            DetailGrid.CancelEdit();
            ReloadGrid();

            DetailGrid.StartEdit(idxEditing);
        }
        public void deleteAllRow()
        {
            Tabel.DataList.Clear();
            initFirstRow();
        }

        public void deleteRow(int sequence)
        {
            List<AccrFormDetail> dataList = Tabel.DataList;
            if (sequence <= dataList.Count)
            {
                AccrFormDetail matched = null;
                foreach (AccrFormDetail d in dataList)
                {
                    if (d.DisplaySequenceNumber == sequence)
                    {
                        matched = d;
                        break;
                    }
                }

                if (matched != null)
                {
                    dataList.Remove(matched);
                    int seqNumber = 0;
                    foreach (AccrFormDetail d in dataList)
                    {
                        seqNumber++;
                        d.DisplaySequenceNumber = seqNumber;
                    }
                }
            }

        }

        protected void initFormTable()
        {
        }
        public void StartBrowsingTabel()
        {
            DetailGrid.CancelEdit();
            ASPxGridViewPagerSettings pager = DetailGrid.SettingsPager;
            pager.PageSize = MAX_PAGE_ROW;
            pager.Mode = GridViewPagerMode.ShowPager;

            GridViewColumn column = DetailGrid.Columns[VouCol.DELETION_CONTROL];
            column.Visible = false;
            ReloadGrid();
        }
        protected void ShowUpload(bool b = true)
        {
            if (UploadDiv == null) return; //fid.Hadid
            if (b)
            {
                UploadDiv.Style.Clear();
            }
            else
            {
                UploadDiv.Style.Add("display", "none");
            }
        }
        
        protected void evaluateExternalParameter()
        {
            string accrNo = Request["accrno"];
            string mode = Request["mode"];

            if (!String.IsNullOrEmpty(accrNo))
            {
                AccrFormData f = formPersistence.search(accrNo);
                if (f != null)
                {
                    f.copyStatus(FormData);

                    FormData = f;
                    FormData.InWorklist = hasWorklist(FormData.AccruedNo);

                    f.OpenExisting = true;
                    f.OpenEmpty = false;
                    int? status = f.StatusCode;
                    f.Editable = status == 0 || status == 112;

                    initFormTable();
                    loadForm();

                    if (f.Editable && !mode.isEmpty() && mode.ToLower().Equals("edit"))
                    {
                        StartEditing();
                    }

                }
                else
                {
                    // not found script

                }
            }
        }

        protected void evaluatePageComponentAvailibility()
        {
            AccrFormData f = FormData;
            AttachmentTable attachmentTable = f.AttachmentTable;
            if (f.OpenEmpty)
            {
                f.Editing = true;
                initFirstRow();
            }
            string loggedUserRole = UserData.Roles.FirstOrDefault();

            f.CanUploadDetail = UserCanUploadDetail && RoLo.isAllowedAccess("btnUploadTemplate");

            var now = DateTime.Now;
            PostingDate.MaxDate = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));

            evaluateEditingControlVisibility();
        }

        protected void evaluateEditingControlVisibility()
        {
            AccrFormData f = FormData;
            bool hasWl = hasWorklist(f.AccruedNo);

            f.Editable = RoLo.isAllowedAccess("btEdit") 
                        && !f.Editing
                        && (f.StatusCode == 0 || hasWl);
						//&& (f.StatusCode == 0 || (f.StatusCode == 112 && hasWl)  || (f.StatusCode == 113 && f.CANCEL_FLAG == 1));

            EditButton.Visible = f.Editable;

            if (f.OpenEmpty)
            {
                CloseButton.Enabled = true;
                CancelButton.Visible = false;
            }
            else
            {
                CloseButton.Visible = !f.Editing;
                CancelButton.Visible = f.Editing;
            }

            SaveButton.Visible = f.Editing;

            DataUpload.Visible = f.Editing;

            DataUploadButton.Visible = f.Editing && f.CanUploadDetail;
            DownloadUploadTemplateLink.Visible = f.Editing && f.CanUploadDetail;
            ShowUpload(f.CanUploadDetail);

            CheckBudgetButton.Visible = RoLo.isAllowedAccess("btCheckBudget")
                                        && (f.StatusCode != 104 && f.StatusCode != 105);
            ExportButton.Visible = RoLo.isAllowedAccess("btExport") && !f.Editing;

            bool submitButtonVisible = false;
            if (f.OpenEmpty)
            {
                submitButtonVisible = !f.Editing 
                                        && RoLo.isAllowedAccess("btSubmit");
            }
            else
            {
                submitButtonVisible = !f.Editing
                                        && RoLo.isAllowedAccess("btSubmit")
                                        && (f.StatusCode == 0 || hasWl);
										//&& (f.StatusCode == 0 
                                        //    || (f.StatusCode == 112 && hasWl)
                                        //    || (f.StatusCode == 113 && f.CANCEL_FLAG == 1));
            }


            SubmitButton.Visible = submitButtonVisible;

            bool canApprove = RoLo.isAllowedAccess("btnApprove");
            bool canReject = RoLo.isAllowedAccess("btReject");
            bool canPost = RoLo.isAllowedAccess("btPostToSAP")
                            && (f.StatusCode == 103);

            RejectButton.Visible = canReject 
                                    && hasWl
                                    && (CanApproveReject.Contains(f.StatusCode ?? 0))
                                    && !f.Editing;
            ApproveButton.Visible = canApprove
                                    && hasWl
                                    && (CanApproveReject.Contains(f.StatusCode ?? 0))
                                    && !f.Editing;

            PostSAPButton.Visible = canPost;
            PostingDate.Visible = canPost;
            PostingDateLiteral.Visible = canPost;

            WbsUpdateButton.Visible = !f.Editing 
                                        && RoLo.isAllowedAccess("btWBSUpdate")
                                        && (f.StatusCode == 104 || f.StatusCode == 105);

            if (f.StatusCode != null)
            {
                SimulateUserButton.Visible = RoLo.isAllowedAccess("btSimulation");
            }
            else
            {
                SimulateUserButton.Visible = false;
            }

            initFormTable();
            if (f.Editing)
            {
                DetailGrid.CancelEdit();

                StartEditingTabel();
                //f.AttachmentTable.openEditing();
            }
            else
            {
                StartBrowsingTabel();
                //f.AttachmentTable.closeEditing();
            }

            if ((f.StatusCode ?? 0) == 0 || hasWl)
            {
                f.AttachmentTable.openEditing();
            }
            else
            {
                f.AttachmentTable.closeEditing();
            }

            updateAttachmentList();
        }

        #endregion

        #region Form
        protected void loadForm(AccrFormData f = null)
        {
            if (f == null && FormData == null) return;
            if (f == null) f = FormData;

            f.OpenExisting = true;
            
            reloadFooter();
        }
        protected void reloadFooter()
        {
            if (BaseData == null)
                return;

            reloadNote();
            reloadAttachment();
        }

        public void UpdateTabel()
        {
            if (DetailGrid.IsEditing)
            {
                DetailGrid.UpdateEdit();
            }
        }

        protected void CancelEdit()
        {
            Mode = VIEW_MODE;

            loadForm();
            FormData.OpenExisting = true;
            FormData.Editing = false;
            CancelEditingTabel();
            evaluatePageComponentAvailibility();
        }
        public void CancelEditingTabel()
        {
            DetailGrid.CancelEdit();

            Tabel.CancelNonPersistedData();
            Tabel.DataList = Tabel.sanityCheck(FormData.OpenExisting);
        }

        protected void StartEditing()
        {
            clearScreenMessage();
            Mode = EDIT_MODE;
            FormData.Editing = true;
            evaluatePageComponentAvailibility();
            if (UserData.isAuthorizedCreator)
            {
                StartEditingTabel();
            }
            loadForm();
        }

        protected void StartEditingTabel()
        {
            GridViewColumn column = DetailGrid.Columns[VouCol.DELETION_CONTROL];
            column.Visible = true;

            ReloadGrid();

            DetailGrid.CancelEdit();
            int cntData = Tabel.DataList.Count;
            if (cntData > 0)
            {
                Tabel.EditIndex = cntData - 1;
                DetailGrid.StartEdit(Tabel.EditIndex);
            }
            else
            {
                initFirstRow();
            }
        }

        protected void SaveFormData()
        {
            logic.Say("SaveFormData", "Count={0} {1} {2}", Tabel.DataList.Count, ScreenType, FormData.AccruedNo);
            clearScreenMessage();
            bool saved = false;

            try
            {
                FormData.Submitted = false;

                int statusCd = FormData.StatusCode ?? 0;

                Tabel.ReSequence(); /// prevent unable to edit when user added second line then delete first line then add line

                saved = saveGeneral(FormData);

                saved = saved && (!FormData.isSubmit || FormData.FormIsValid);

            }
            catch (Exception ex)
            {
                PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        protected bool saveGeneral(AccrFormData f)
        {
            if (f.FormIsValid)
            {
                initFormTable();
                AttachID a = GetAttachID();
                f.Saved = logic.AccrForm.save(f, UserData);

                ReArrangeAttachments(a.ReffNo, (f.PVNumber ?? 0).str(), a.Year);
            }

            logic.Say("saveGeneral", "{0}# {1} {2}", ScreenType, f.AccruedNo, f.Saved.box("Saved"));
            return true;
        }

        private void reloadFormData()
        {
            AccrFormData _loaded = formPersistence.search(FormData.AccruedNo);

            if (_loaded != null)
            {
                _loaded.copyStatus(FormData);

                FormData = _loaded;
                BaseDataInit();
                FormData.InWorklist = hasWorklist(FormData.AccruedNo);
                initFormTable();

                loadForm();

                FormData.OpenExisting = true;
                FormData.Editable = true;
            }
            ReloadGrid();

            dump();
        }
        #endregion

        #region Editing Control

        protected void evt_btPostToSAP_onClick(object sender, EventArgs arg)
        {
            string loc = "evt_btPostToSAP_onClick";

            logic.Say(loc, "Post Accrued {0}", FormData.AccruedNo);

            if (PostingDate.Text.isEmpty())
            {
                PostNow(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Posting Date");
                return;
            }
            FormData.PostingDate = PostingDate.Date;

            if (FormData.AccruedNo.isEmpty())
            {
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Accrued No");
                return;
            }

            try
            {
                bool haveSuspense = FormData.Details.Any(x => x.PVTypeCd == 2);
                if (!haveSuspense)
                    logic.Say(loc, "no suspense here");

                bool ok = true;
                bool finish = true;
                do
                {
                    // save posting date
                    ok = logic.AccrForm.SavePostingDate(FormData.AccruedNo, FormData.PostingDate.Value);
                    if (!ok)
                    {
                        logic.Say(loc, "failed to save Posting Date");
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Posting");
                        break;
                    }

                    // assign new number for suspense no old. for Migration needs
                    if (haveSuspense)
                    {
                        ok = logic.AccrForm.doSuspenseGetDocNo(FormData.ReffNo, UserData);
                        if (!ok)
                        {
                            logic.Say(loc, "failed to assign new suspense");
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Failed to assign new suspense");
                            break;
                        }
                    }

                    // do Post to SAP here
                    List<Common.Data.SAPData.AccrSAPError> sapErr = new List<Common.Data.SAPData.AccrSAPError>();
                    ok = logic.AccrForm.AccruedPosting(FormData, UserData, ref sapErr);
                    if (!ok)
                    {
                        logic.Say(loc, "failed to post to SAP");
                        if (sapErr.Count > 0)
                        {
                            foreach (var err in sapErr)
                            {
                                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR"
                                    , string.Format("{0} Failed : {1}{2} {3}"
                                                    , err.PROCESS_NAME
                                                    , err.TYPE
                                                    , err.NUMBER
                                                    , err.MESSAGE));
                            }
                        }
                        else
                        {
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "There is at least 1 journal that failed to Post to SAP"); 
                        }
                        break;
                    }

                    // assign new number for suspense no old. for Migration needs
                    if (haveSuspense)
                    {
                        ok = logic.AccrForm.doSuspenseMigration(FormData.ReffNo, UserData);
                        if (!ok)
                        {
                            logic.Say(loc, "failed to migrate suspense");
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Failed to migrate suspense");
                            break;
                        }
                    }

                    // update status on K2 into Posted
                    doApprove(UserData, loc);

                    logic.Say(loc, "finish successfully");
                    PostNow(ScreenMessage.STATUS_INFO, "MSTD00001INF", "Posting finish successfully");

                    reloadFormData();
                    ReloadOpener();
                } while (!finish);
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }

            PrepareDataHeader();
            evaluateEditingControlVisibility();
            postScreenMessage();
        }

        protected void evt_imgDeleteAllRow_clicked(object sender, EventArgs args)
        {
            logic.Say("evt_imgDeleteAllRow_clicked", "Delete All Row");
            deleteAllRow();
            ReloadGrid();
        }
        protected void evt_imgDeleteRow_clicked(object sender, EventArgs args)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string sequenceNumber = image.Key;
            int numSequenceNumber = int.Parse(sequenceNumber.Trim());
            logic.Say("evt_imgDeleteRow_clicked", "seq={0}", numSequenceNumber);
            deleteRow(numSequenceNumber);
            ReloadGrid();
            StartEditingTabel();
        }
        protected void evt_imgAddRow_clicked(object sender, EventArgs arg)
        {
            logic.Say("evt_imgAddRow_clicked", "Add Row {0}", Tabel.DataList.Count + 1);
            OnTabKeypress();
        }

        protected void evt_tboxActivity_TabKeypress(object sender, EventArgs e)
        {
            OnTabKeypress();
        }
        protected void evt_tboxActivity_ValueChanged(object sender, EventArgs e)
        {
            int row = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();

            ASPxTextBox tboxActivity = (ASPxTextBox)sender;

            Tabel.rows[row].ActivityDescription = (tboxActivity.Value ?? "").ToString();

            ReloadGrid();
            DetailGrid.StartEdit(row);
        }

        protected void evt_tboxActivity_Load(object sender, EventArgs arg)
        {
            if (sender == null)
            {
                return;
            }

            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    ASPxTextBox tbox = (ASPxTextBox)sender;

                    //AccrFormDetail detail = Tabel.DataList[idxSelected];
                    tbox.Enabled = isEnabledSuspense();
                    //if (tbox.Enabled)
                    //{
                    //    tbox.Value = detail.ActivityDescription;
                    //    tbox.Enabled = isEnabledSuspense();
                    //}
                    //else
                    //{
                    //    tbox.Enabled = isEnabledSuspense();
                    //    tbox.Value = detail.ActivityDescription;
                    //}
                }
            }
        }
        protected void evt_tboxAmount_Load(object sender, EventArgs arg)
        {
            if (sender == null)
            {
                return;
            }

            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    ASPxTextBox tbox = (ASPxTextBox)sender;

                    //AccrFormDetail detail = Tabel.DataList[idxSelected];
                    tbox.Enabled = isEnabledSuspense();
                }
            }
        }
        protected void evt_tboxAmount_TabKeypress(object sender, EventArgs e)
        {
            OnTabKeypress();
        }
        protected void evt_tboxAmount_ValueChanged(object sender, EventArgs e)
        {
            int row = DetailGrid.EditingRowVisibleIndex;
            ASPxTextBox tboxAmount = (ASPxTextBox)sender;
            if (tboxAmount.Value == null)
            {
                tboxAmount.Value = 0;

            }

            DetailGrid.UpdateEdit();

            
            decimal amount = 0;
            
            if (!decimal.TryParse(tboxAmount.Value.ToString(), out amount))
                amount = 0;

            Tabel.rows[row].Amount = amount;
            UpdateAmountColumns(row, amount);

            ReloadGrid();
            DetailGrid.StartEdit(row);
        }
        protected void evt_gridDetail_ReturnKeypress(object sender, EventArgs arg)
        {
            logic.Say("evt_gridDetail_ReturnKeypress", "Enter");
            OnTabKeypress();
        }

        public void OnTabKeypress()
        {
            DetailGrid.UpdateEdit();
            int idxSelected = Tabel.EditIndex;
            int lastDataIndex = Tabel.DataList.Count - 1;
            if (Tabel.EditIndex == lastDataIndex)
            {
                ReOrderDetail();
                Tabel.addNewRow();
                idxSelected = Tabel.EditIndex + 1;
            }
            else
            {
                idxSelected++;
            }

            Tabel.EditIndex = idxSelected;
            ReloadGrid();
            DetailGrid.StartEdit(idxSelected);
        }

        protected void UpdateAmountColumnsAll()
        {
            ReOrderDetail();
            foreach (var d in FormData.Details)
            {
                UpdateAmountColumns(d.SequenceNumber, d.Amount);
            }
        }

        protected void UpdateAmountColumns(int row, decimal amount)
        {
            //decimal sapAvail = Tabel.rows[row].SapAmtAvailable;
            string curr = Tabel.rows[row].CurrencyCode;

            if (curr.isEmpty())
                return;

            decimal rate = 1;
            if (!curr.Equals("IDR"))
            {
                rate = ExchRate.Where(x => x.CurrencyCode == curr).Select(x => x.Rate).FirstOrDefault();

                if (rate <= 0) rate = 1;
            }

            decimal amountIDR = amount * rate;

            Tabel.rows[row].AmountIdr = amountIDR;
        }

        protected void btSubmit_Click(object sender, EventArgs e)
        {
            logic.Say("btSubmit_Click", "Submit");
            clearScreenMessage();

            try
            {
                bool valid = true;

                UpdateAmountColumnsAll();
                logic.AccrForm.FillSAPAmount(FormData.Details);

                valid = CheckWbsNotExceeded();
                if(!FormData.AttachmentTable.Attachments.Any(x => x.FileName.isNotEmpty()))
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment");
                    valid = false;
                }

                if (valid)
                {
                    logic.Say("btSubmit_Click", "K2StartWorklist K2 {0}", FormData.AccruedNo);

                    K2DataField k2Data = new K2DataField();
                    k2Data.Command = resx.K2ProcID("Form_Registration_Submit_New");
                    k2Data.folio = FormData.AccruedNo;
                    k2Data.Reff_No = FormData.ReffNo;
                    k2Data.ApprovalLevel = "1";
                    k2Data.ApproverClass = "1";
                    k2Data.ApproverCount = "1";
                    k2Data.CurrentDivCD = UserData.DIV_CD.ToString();
                    k2Data.CurrentPIC = UserData.USERNAME;
                    k2Data.EmailAddress = UserData.EMAIL;
                    k2Data.Entertain = "0";
                    k2Data.K2Host = "";
                    k2Data.LimitClass = "0";
                    k2Data.ModuleCD = "3";
                    k2Data.NextClass = "0";
                    k2Data.PIC = UserData.DIV_CD.ToString();
                    k2Data.RegisterDt = string.Format("{0:G}", DateTime.Now);
                    k2Data.RejectedFinance = "0";
                    k2Data.StatusCD = FormData.StatusCode.Value.ToString();
                    k2Data.StepCD = "";
                    k2Data.VendorCD = FormData.VendorCode;
                    k2Data.TotalAmount = FormData.Details.Sum(x => x.AmountIdr).ToString();

                    if ((FormData.StatusCode ?? 0) != 0)
                    {
                        k2Data.isResubmitting = true;
                    }

                    bool result = logic.WorkFlow.K2StartWorklist(k2Data, UserData);

                    //if (result && FormData.StatusCode != 0)
                    if (result)
                    {
                        int newStatus = 0;
                        if (WaitForK2UpdateStatus(out newStatus, 200))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Submit");

                            reloadFormData();
                            resetWorklist();

                            FormData.Submitted = true;
                            Mode = VIEW_MODE;
                        	logic.AccrForm.SetCancelFlag(FormData.AccruedNo, UserData, 0);

                            string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                            if (!Common.AppSetting.NoticeMailUsePort)
                                port = "";
                            string linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?accrno={4}&mode=view",
                                                Request.Url.Scheme,
                                                Request.ServerVariables["SERVER_NAME"],
                                                port,
                                                "/80Accrued/AccrForm.aspx",
                                                FormData.AccruedNo));

                            if (SendEmail(linkEmail))
                            {
                                logic.Say("SendEmail", "SendEmail Accrued {0} finished successfully", FormData.AccruedNo);
                            }
                            else
                            {
                                logic.Say("SendEmail", "SendEmail Accrued {0} finished with failed", FormData.AccruedNo);
                            }
                         }
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Submit");
                    }
                }
                ReloadOpener();
            }
            catch (Exception ex) 
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }

            PrepareDataHeader();
            evaluateEditingControlVisibility();
            postScreenMessage();
        }

        protected void doApprove(UserData u, string loc)
        {
            string command = resx.K2ProcID("Form_Registration_Approval");
            // Call K2
            bool success = logic.PVRVWorkflow.Go(FormData.ReffNo, command, u, "N/A", false, FormData.AccruedNo);
            if (success)
            {
                int newStatus = 0;
                if (WaitForK2UpdateStatus(out newStatus, 200))
                {
                    logic.Say(loc, "success do approval to K2");

                    //FormData.StatusCode = newStatus;
                    //SubmStsLabel.Text = logic.AccrForm.GetStatusName(newStatus);
                    reloadFormData();
                    resetWorklist();

                    string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                    if (!Common.AppSetting.NoticeMailUsePort)
                        port = "";
                    string linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?accrno={4}&mode=view",
                                        Request.Url.Scheme,
                                        Request.ServerVariables["SERVER_NAME"],
                                        port,
                                        "/80Accrued/AccrForm.aspx",
                                        FormData.AccruedNo));

                    if (newStatus == 103)
                    {
                        #region update balance
                        // Insert/Update balance On Approval by Accounting
                        bool submitted = logic.AccrBalance.Submit(FormData, UserData);
                        if (submitted)
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Approve");
                        }
                        else
                        {
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Update balance");
                        }
                        #endregion update balance
                    }
                    else if (newStatus == 104)
                    {
                        //if status Posted and there is no PR/PO type in this accrued no
                        //immediately move to next status
                        bool noPR = !FormData.Details.Any(x => x.PVTypeCd == 4);
                        if (noPR)
                        {
                            // wait K2 finish its next activity (Get Next Approver)
                            System.Threading.Thread.Sleep(1000);

                            var nextAppr = new UserData() { USERNAME = FormData.CurrApprover };
                            doApprove(nextAppr, loc);
                            linkEmail = "";
                        }
                        else
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Approve");
                            linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?accr_no={4}",
                                            Request.Url.Scheme,
                                            Request.ServerVariables["SERVER_NAME"],
                                            port,
                                            "/80Accrued/AccrWBS.aspx",
                                            FormData.AccruedNo));
                        }
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Approve");
                    }

                    if (linkEmail.isNotEmpty())
                    {
                        if (SendEmail(linkEmail))
                        {
                            logic.Say("SendEmail", "SendEmail Accrued {0} finished successfully", FormData.AccruedNo);
                        }
                        else
                        {
                            logic.Say("SendEmail", "SendEmail Accrued {0} finished with failed", FormData.AccruedNo);
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00082WRN");
                }
            }
            else
            {
                logic.Say(loc, "Process approval to K2 failed");
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Approve");
            }
        }

        protected void evt_btApprove_onClick(object sender, EventArgs e)
        {
            string loc = "evt_btApprove_onClick";
            logic.Say(loc, "Approve Accrued {0} by {1} on status {2}", FormData.AccruedNo, UserData.USERNAME, FormData.StatusCode);

            // do validation here
            UpdateAmountColumnsAll();
            logic.AccrForm.FillSAPAmount(FormData.Details);

            if (CheckWbsNotExceeded())
            {
                doApprove(UserData, loc);
            }

            PrepareDataHeader();
            evaluateEditingControlVisibility();
            postScreenMessage();
            ReloadOpener();
        }
        protected bool postNotice()
        {
            bool result = true;
            bool hasWorlist = hasWorklist(BaseData.AccruedNo);
            clearScreenMessage();

            if (BaseNoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(FormData.UserName);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return false;
                }
            }

            try
            {
                string strNotice = TxtReviseComment.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = BaseScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?accrno={4}&mode=view",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        BaseData.AccruedNo);

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {
                        noticeTo = FormData.UserName;

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(BaseData.ReffNo, "", UserName, noticeTo);
                    }
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(BaseData.ReffNo, "", UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(BaseData.ReffNo.str(),
                                            BaseData.CreatedDt.Year.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    fetchNote();
                    List<FormNote> noticeList = BaseData.Notes;
                    BaseNoteRepeater.DataSource = noticeList;
                    BaseNoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (FormData.UserName != null)
                    {

                        if (mail.ComposeApprovalNotice_EMAIL(BaseData.ReffNo,
                                                             BaseData.CreatedDt.Year.str(),
                                                             strNotice, noticeLink, noticeTo,
                                                             UserData, noticer,
                                                             BaseData.DivisionID,
                                                             BaseScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                            result = false;
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                            result = false;
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
                result = false;
            }
            BaseNoteTextBox.Text = "";

            BaseNoteCommentContainer.Attributes.Remove("style");
            BaseNoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

            postScreenMessage();
            return result;
        }
        protected void btnReviseCancel_Click(object sender, EventArgs e)
        {
            PopUpRevise.Hide();
        }
        protected void evt_btnReviseProceed_Click(object sender, EventArgs e)
        {
            PopUpRevise.Hide();
            string loc = "evt_btnReviseProceed_Click";
            logic.Say(loc, "Revise Accrued {0} by {1} on status {2}", FormData.AccruedNo, UserData.USERNAME, FormData.StatusCode);

            bool success = postNotice();
            if (success)
            {
                doReject(loc);
            }
            ReloadOpener();
        }
        protected void evt_btRevise_onClick(object sender, EventArgs e)
        {
            string accrNo = FormData.AccruedNo;
            if (!string.IsNullOrEmpty(accrNo))
            {
                // do validation here
                bool valid = true;
                if (valid)
                {
                    TxtReviseAccruedNo.Text = accrNo;
                    DdlReviseCategory.Items.Clear();
                    DdlReviseCategory.Items.Add("Reject");
                    
                    if (FormData.StatusCode == 102)
                        DdlReviseCategory.Items.Add("Cancel");
                    
                    DdlReviseCategory.SelectedIndex = 0;
                    TxtReviseComment.Text = "";
                    PopUpRevise.Show();
                }
                else
                {
                    Nag("MSTD00230ERR", accrNo);
                }
            }
        }
        protected void doReject(string loc)
        {
            // do validation here
            bool valid = true;

            if (valid)
            {
                if (DdlReviseCategory.SelectedIndex == 1) // 1 mean "Cancel" selected
                {
                    logic.AccrForm.SetCancelFlag(FormData.AccruedNo, UserData, 1);
                }

                string command = resx.K2ProcID("Form_Registration_Rejected");

                // Call K2
                bool success = logic.PVRVWorkflow.Go(FormData.ReffNo, command, UserData, "N/A", false, FormData.AccruedNo);
                if (success)
                {
                    int newStatus = 0;
                    if (WaitForK2UpdateStatus(out newStatus, 200))
                    {
                        //FormData.StatusCode = newStatus;
                        //SubmStsLabel.Text = logic.AccrForm.GetStatusName(newStatus);
                        reloadFormData();
                        resetWorklist();

                        string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                        if (!Common.AppSetting.NoticeMailUsePort)
                            port = "";
                        string linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?accrno={4}&mode=view",
                                            Request.Url.Scheme,
                                            Request.ServerVariables["SERVER_NAME"],
                                            port,
                                            "/80Accrued/AccrForm.aspx",
                                            FormData.AccruedNo));

                        if(SendEmail(linkEmail))
                        {
                            logic.Say("SendEmail", "SendEmail Accrued {0} finished successfully", FormData.AccruedNo);
                        }
                        else
                        {
                            logic.Say("SendEmail", "SendEmail Accrued {0} finished with failed", FormData.AccruedNo);
                        }

                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Revise");
                    }
                    else
                    {
                        logic.AccrForm.SetCancelFlag(FormData.AccruedNo, UserData, 0);
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00082WRN");
                    }
                }
                else
                {
                    logic.AccrForm.SetCancelFlag(FormData.AccruedNo, UserData, 0);
                    logic.Say(loc, "Process approval to K2 failed");
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Reject");
                }
            }

            PrepareDataHeader();
            evaluateEditingControlVisibility();
            postScreenMessage();
        }

        protected bool SendEmail(string linkEmail)
        {
            String emailTemplate = String.Format("{0}\\AccruedApproval.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));

            EmailFunction mail = new EmailFunction(emailTemplate);

            string[] mailTo = null;
            if (FormData.StatusCode != 105)
            {
                var recipient = logic.Role.getUserRoleHeader(FormData.NextApprover);
                if (recipient == null) 
                    return false;
                
                mailTo = new string[] { recipient.EMAIL };
            }
            else
            {
                var listRecipient = logic.Role.getUserWorklist(FormData.ReffNo);
                if (listRecipient == null || listRecipient.Count == 0)
                    return false;

                mailTo = listRecipient.Select(x => x.EMAIL).ToArray();
            }
            logic.Say("SendEmail", "SendEmail Accrued Approval {0} to : {1}. [link:{2}]"
                , FormData.AccruedNo
                , string.Join(",", mailTo)
                , linkEmail);

            ErrorData err = null;
            if (mail.ComposeAccruedApproval_EMAIL(3, FormData.AccruedNo, mailTo, linkEmail, ref err))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        protected void btEdit_Click(object sender, EventArgs e)
        {
            logic.Say("btEdit_Click", "Edit");

            StartEditing();
        }

        protected bool saveValidation()
        {
            var lstDetail = FormData.Details;

            // Mandatory Validation
            bool isEmptyWBS = lstDetail.Exists(x => x.WbsNumber.isEmpty());
            bool isEmptyDoc = lstDetail.Exists(x => (x.PVTypeCd ?? 0) == 0);
            bool isEmptyAct = lstDetail.Exists(x => x.ActivityDescription.isEmpty());
            bool isEmptyCC = lstDetail.Exists(x => x.CostCenterCode.isEmpty());
            bool isEmptyCurr = lstDetail.Exists(x => x.CurrencyCode.isEmpty());
            bool isEmptyAmt = lstDetail.Exists(x => x.Amount == 0);

            // Business Validation
            bool isBudgetExceed = !CheckWbsNotExceeded();

            bool invalidSus = lstDetail
                                .Exists(x => ((x.PVTypeCd ?? 0) == 2 && x.SuspenseNo.isEmpty()));
            bool invalidNoSus = lstDetail
                                .Exists(x => ((x.PVTypeCd ?? 0) != 2 && !x.SuspenseNo.isEmpty()));
            bool isDuplicate = lstDetail
                                .GroupBy(x => new { x.WbsNumber, x.PVTypeCd, x.ActivityDescription, x.SuspenseNo })
                                .Any(x => x.Count() > 1);

            if (isEmptyWBS)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "WBS No.");

            if (isEmptyDoc)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Doc Type");

            if (isEmptyAct)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Activity");

            if (isEmptyCC)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Cost Center");

            if (isEmptyCurr)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Currency");

            if (isEmptyAmt)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Amount");

            if (invalidSus)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Save", "Suspense type should input Suspense No");

            if (invalidNoSus)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Save", "Suspense No should use Suspense Type");

            if (isDuplicate)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Save", "There is duplicate detail");

            return !isEmptyWBS
                    && !isEmptyDoc
                    && !isEmptyAct
                    && !isEmptyCC
                    && !isEmptyCurr
                    && !isEmptyAmt
                    && !isBudgetExceed
                    && !invalidSus
                    && !invalidNoSus
                    && !isDuplicate;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            logic.Say("btSave_Click", "Save");
            clearScreenMessage();

            UpdateTabel();
            UpdateAmountColumnsAll();
            logic.AccrForm.FillSAPAmount(FormData.Details);

            List<AccrFormDetail> lstDetail = Tabel.getBlankRowCleanedDetails();
            Tabel.DataList = lstDetail;

            if (lstDetail.Count > 0)
            {
                if (saveValidation())
                {
                    SaveFormData();
                    if (FormData.Saved)
                    {
                        CancelEdit();

                        PostLaterOn(ScreenMessage.STATUS_INFO, "MSTD00055INF");

                        DownloadEntertainmentTemplateLink.Visible = false;

                    }
                    else
                    {
                        PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Save");
                    }

                    reloadFormData();
                    FormData.Submitted = false;
                    PrepareDataHeader();
                    evaluateEditingControlVisibility();
                }
                else
                {
                    Mode = EDIT_MODE;
                    FormData.Editing = true;
                    ReloadGrid();
                    DetailGrid.StartEdit(Tabel.EditIndex);
                    loadForm();
                }

                postScreenMessage();
            }
        }
        protected void evt_btCheckBudget_onClick(object sender, EventArgs e)
        {
            logic.Say("evt_btCheckBudget_onClick", "CheckBudget");

            if (FormData.Details.Exists(x => x.CurrencyCode.isEmpty()))
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Check Budget", "Currency could not be empty.");
                postScreenMessage();
                return;
            }

            DetailGrid.UpdateEdit();

            UpdateAmountColumnsAll();
            logic.AccrForm.FillSAPAmount(FormData.Details);
            CheckWbsNotExceeded();

            ReloadGrid();

            if (Mode != VIEW_MODE)
                DetailGrid.StartEdit(Tabel.EditIndex);
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            logic.Say("btCancel_Click", "Cancel");
            reloadFormData();
            CancelEdit();
        }

        protected void btClose_Click(object sender, EventArgs args)
        {
            AttachID a = GetAttachID();
            if (!FormData.Saved && !FormData.OpenExisting)
            {
                string folderPath = a.Year + "/" + ScreenType + "/" + a.ReffNo;
                foreach (FormAttachment att in FormData.AttachmentTable.Attachments)
                {
                    if (!att.Blank)
                    {
                        ftp.Delete(folderPath, att.FileName);
                    }
                    deleteAttachmentInfo(att);
                }

                try
                {
                    ftp.RemoveDir(folderPath);
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("550"))
                    {
                        LoggingLogic.err(ex);
                        throw;
                    }
                }
            }
        }

        protected void evt_btWBSUpdate_onClick(object sender, EventArgs args)
        {
            string _accrNo = FormData.AccruedNo;

            ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "ELVIS_AccrWBS",
                        "openWin('../80Accrued/AccrWBS.aspx?accr_no=" + _accrNo + "','ELVIS_AccrWBS');",
                        true);
        }

        protected void evt_btSAPDocPopupClose_clicked(object sender, EventArgs args)
        {
            SAPpop.Hide();
        }
        private List<CodeConstant> _docType = null;
        protected List<CodeConstant> DocType
        {
            get
            {
                if (_docType == null)
                    _docType = formPersistence.getDocTypes();

                return _docType;

            }
        }
        public List<CodeConstant> CbDocType
        {
            get
            {
                if (ViewState["cbDocType"] != null)
                    return (List<CodeConstant>)ViewState["cbDocType"];
                else
                    return null;
            }

            set
            {
                ViewState["cbDocType"] = value;
            }
        }
        protected void ddlDocType_Load(object sender, EventArgs arg)
        {
            //ASPxComboBox combo = (ASPxComboBox)sender;
            ASPxGridLookup combo = (ASPxGridLookup)sender;
            if (Mode == VIEW_MODE) return;
            //List<CodeConstant> dt = formPersistence.getDocTypes();
            //if (CbDocType == null)
            //{
            //    CbDocType = formPersistence.getDocTypes();
            //}
            combo.DataSource = CbDocType;
            combo.DataBind();
            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccrFormDetail detail = Tabel.DataList[idxSelected];
                    combo.Value = detail.PVTypeCd;
                    combo.Enabled = isEnabledSuspense();
                }
            }
        }

        public string SelectedWBS
        {
            get
            {
                if (Session["selectedWBS"] != null)
                    return Convert.ToString(Session["selectedWBS"]);
                else
                    return "";
            }

            set
            {
                Session["selectedWBS"] = value;
            }
        }
        public List<SuspenseStructure> CbDataSuspense
        {
            get
            {
                if (Session["cbDataSuspense"] != null)
                    return (List<SuspenseStructure>)Session["cbDataSuspense"];
                else
                    return null;
            }

            set
            {
                Session["cbDataSuspense"] = value;
            }
        }
        protected void lkpgridSuspenseNo_Load(object sender, EventArgs args)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            string wbsNo = "";
            int idx = Tabel.EditIndex;
            if (Tabel.rows[idx] != null)
            {
                wbsNo = Tabel.rows[idx].WbsNumber;
            }

            if (wbsNo.isEmpty())
            {
                CbDataSuspense = null;
            }
            else
            {
                if (SelectedWBS != wbsNo)
                {
                    SelectedWBS = wbsNo;
                    CbDataSuspense = logic.Settle.SearchUnclosed(Convert.ToString(UserData.DIV_CD), wbsNo);
                }
                else
                {
                    if (CbDataSuspense == null)
                    {
                        CbDataSuspense = logic.Settle.SearchUnclosed(Convert.ToString(UserData.DIV_CD), wbsNo);
                    }
                }
                
            }
            lookup.DataSource = CbDataSuspense;
            lookup.DataBind();
        }
        protected void lkpgridSuspenseNo_ValueChanged(object sender, EventArgs arg)
        {
            int row = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();

            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            if (!lookup.Text.ToString().isEmpty())
            {
                string susNoYr = lookup.GridView.GetRowValues(lookup.GridView.FocusedRowIndex, "ReffNo").ToString();
                string susNo = lookup.GridView.GetRowValues(lookup.GridView.FocusedRowIndex, "PVNo").ToString();

                SuspenseStructure selectedSuspense = CbDataSuspense.Where(x => x.ReffNo.Value.ToString().Equals(susNoYr)).FirstOrDefault();

                Tabel.rows[row].PVTypeCd = 2;
                Tabel.rows[row].SuspenseNoYear = susNoYr;
                Tabel.rows[row].SuspenseNo = susNo;
                Tabel.rows[row].Amount = selectedSuspense.TotalAmount ?? 0;
                Tabel.rows[row].AmountIdr = selectedSuspense.TotalAmount ?? 0;
                Tabel.rows[row].CurrencyCode = "IDR";
                Tabel.rows[row].ActivityDescription = selectedSuspense.ActivityDesc;
                Tabel.rows[row].CostCenterCode = selectedSuspense.CostCenter;
            }
            else
            {
                Tabel.rows[row].PVTypeCd = null;
                Tabel.rows[row].SuspenseNoYear = null;
                Tabel.rows[row].SuspenseNo = null;
                Tabel.rows[row].Amount = 0;
                Tabel.rows[row].AmountIdr = 0;
                Tabel.rows[row].CurrencyCode = null;
                Tabel.rows[row].ActivityDescription = "";
                Tabel.rows[row].CostCenterCode = null;
            }
            ReloadGrid();
            DetailGrid.StartEdit(Tabel.EditIndex);
        }

        private List<CodeConstant> CbDataCurrency
        {
            set
            {
                ViewState["cbDataCurrency"] = value;
            }
            get
            {
                if (ViewState["cbDataCurrency"] == null)
                {
                    return null;
                }
                else
                {
                    return (List<CodeConstant>)ViewState["cbDataCurrency"];
                }
            }
        }
        protected void coCurrencyCode_Load(object sender, EventArgs e)
        {
            ASPxComboBox co = (ASPxComboBox)sender;

            //if (CbDataCurrency == null)
            //{
            //    CbDataCurrency = formPersistence.getCurrencyCodeList();
            //}

            co.DataSource = CbDataCurrency;
            co.DataBind();

            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccrFormDetail detail = Tabel.DataList[idxSelected];
                    co.Value = detail.CurrencyCode;
                    co.Enabled = isEnabledSuspense();
                }
            }

        }

        public List<CodeConstant> CbDataCostCenter
        {
            get
            {
                if (ViewState["cbDataCostCenter"] != null)
                    return (List<CodeConstant>)ViewState["cbDataCostCenter"];
                else
                    return null;
            }

            set
            {
                ViewState["cbDataCostCenter"] = value;
            }
        }
        protected void lkpgridCostCenterCode_Load(object sender, EventArgs args)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            //List<CodeConstant> cc = formPersistence.getCostCenterCodes();
            //if (CbDataCostCenter == null)
            //{
            //    CbDataCostCenter = formPersistence.getCostCenterCodes();
            //}
            lookup.DataSource = CbDataCostCenter;
            lookup.DataBind();
            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccrFormDetail detail = Tabel.DataList[idxSelected];
                    lookup.Value = detail.CostCenterCode;
                    lookup.Enabled = isEnabledSuspense();
                }
            }
        }
        protected void lkpgridCostCenterCode_ValueChanged(object sender, EventArgs arg)
        {
            int row = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();

            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            Tabel.rows[row].CostCenterCode = lookup.Value as string;
            ReloadGrid();
        }

        public List<WBSStructure> CbDataWBS
        {
            get
            {
                if (ViewState["cbDataWBS"] != null)
                    return (List<WBSStructure>)ViewState["cbDataWBS"];
                else
                    return null;
            }

            set
            {
                ViewState["cbDataWBS"] = value;
            }
        }
        protected void LookupBudgetNo_Load(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE) return;

            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            //List<WBSStructure> lstWbsNumber = formPersistence.getWbsNumbersAccrued(UserData);
            //if (CbDataWBS == null)
            //{
            //    CbDataWBS = formPersistence.getWbsNumbersAccrued(UserData);
            //}
            lookup.DataSource = CbDataWBS;
            lookup.DataBind();
        }
        protected void LookupBudgetNo_ValueChanged(object sender, EventArgs arg)
        {
            int row = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();

            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            string wbsNumber = lookup.Value != null ? lookup.Value.ToString() : "";

            Tabel.rows[row].WbsNumber = wbsNumber;
            Tabel.rows[row].WbsDesc = (string)lookup.GridView.GetRowValues(lookup.GridView.FocusedRowIndex, "Description");

            var sc = new AccrBalanceSearchCriteria() { wbsNoOld = wbsNumber, ignorewbsNoOld = false };
            var bal = logic.AccrBalance.Search(sc);

            if (bal.Any())
            {
                string existedBookingNo = bal.Select(x => x.BOOKING_NO).FirstOrDefault();
                Tabel.rows[row].BookingNo = existedBookingNo;
            }

            ReloadGrid();
            DetailGrid.StartEdit(row);
        }

        #region Initial Form Edit
        
        protected void LookupBudgetNo_Init(object sender, EventArgs arg)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccrFormDetail detail = Tabel.DataList[idxSelected];
                    lookup.Value = detail.WbsNumber;
                    lookup.Text = detail.WbsNumber;
                }
            }
        }

        protected void lkpgridSuspenseNo_Init(object sender, EventArgs arg)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            
            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccrFormDetail detail = Tabel.DataList[idxSelected];
                    lookup.Value = detail.SuspenseNoYear;
                    lookup.Text = detail.SuspenseNo;
                }
            }
        }

        protected void ddlDocType_Init(object sender, EventArgs arg)
        {
            ASPxComboBox combo = (ASPxComboBox)sender;

            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccrFormDetail detail = Tabel.DataList[idxSelected];
                    combo.Value = detail.PVType;
                }
            }
        }
        #endregion

        #endregion

        #region  Move to BaseAccrFormBehind
        /*
         * 
         * 
        #region Attachment
        protected void updateAttachmentList()
        {
            AttachmentTable attachmentTable = FormData.AttachmentTable;
            GridViewColumn column = AttachmentGrid.Columns["#"];
            column.Visible = attachmentTable.Editing;
            AttachmentGrid.DataSource = attachmentTable.Attachments;
            AttachmentGrid.DataBind();
        }

        public struct AttachID
        {
            public string ReffNo;
            public string Year;
            public int Seq;
        }

        protected AttachID GetAttachID()
        {
            AttachID a = new AttachID()
            {
                ReffNo = _accruedNo,
                Year = (FormData.CreatedDt.Year).str(),
                Seq = 0
            };

            return a;
        }

        protected void evt_btSendUploadAttachment_clicked(object sender, EventArgs arg)
        {
            if (AttachmentUpload.HasFile)
            {
                logic.Say("btSendUploadAttachment_Click", "FileName = {0}", AttachmentUpload.FileName);
                string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                int maxSize = int.Parse(strMaxSize);
                maxSize = maxSize * 1024;
                if (AttachmentUpload.FileBytes.Length > maxSize)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                    AttachmentPopup.Hide();
                    return;
                }

                bool uploadSucceded = true;
                string errorMessage = null;

                AttachID a = GetAttachID();
                string filename = CommonFunction.CleanFilename(AttachmentUpload.FileName);
                ftp.ftpUploadBytes(a.Year, ScreenType,
                                a.ReffNo,
                                filename,
                                AttachmentUpload.FileBytes,
                                ref uploadSucceded,
                                ref errorMessage);

                ListEditItem item = AttachmentCategoryDropDown.SelectedItem;
                if (item.Value == null)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                }
                AttachmentTable attachmentTable = FormData.AttachmentTable;
                FormAttachment attachment = attachmentTable.getBlankAttachment();
                attachment.ReferenceNumber = a.ReffNo;
                if (item != null)
                {
                    attachment.CategoryCode = item.Value.ToString();
                    attachment.CategoryName = item.Text;
                }
                attachment.PATH = a.Year + "/" + ScreenType + "/" + a.ReffNo;
                attachment.FileName = filename;
                attachment.Blank = false;
                formPersistence.saveAttachmentInfo(attachment, UserData);

                formPersistence.fetchAttachment(a.ReffNo, attachmentTable, ProcessID);
                updateAttachmentList();
            }
            AttachmentPopup.Hide();
        }
        protected void evt_imgAddAttachment_onClick(object sender, EventArgs arg)
        {
            AttachmentPopup.Show();
        }

        protected void evt_gridAttachment_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {
            AttachmentGrid = (ASPxGridView)sender;

            if (arg.RowType != GridViewRowType.Data)
            {
                return;
            }

            decimal key = (decimal)arg.KeyValue;
            if (key > 0)
            {
                AttachmentTable attachmentTable = FormData.AttachmentTable;
                FormAttachment attachment = attachmentTable.get((int)key);
                if (attachment != null)
                {
                    KeyImageButton imgAdd = (KeyImageButton)AttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgAddAttachment");
                    if (imgAdd != null)
                    {
                        imgAdd.Visible = attachment.Blank;
                    }
                    KeyImageButton imgDelete = (KeyImageButton)AttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgDeleteAttachment");
                    if (imgDelete != null)
                    {
                        int statusCd = FormData.StatusCode ?? 0;
                        bool canDelAttach = statusCd == 0 || statusCd == 112;

                        if (attachment.Blank || !canDelAttach)
                            imgDelete.CssClass = "hidden";
                        else
                            imgDelete.CssClass = "poited";
                    }
                }
            }
        }
        protected void evt_imgDeleteAttachment_onClick(object sender, EventArgs arg)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            int keyNum = int.Parse(key);
            logic.Say("evt_imgDeleteAttachment_onClick", "Key= {0}", keyNum);
            AttachmentTable attachmentTable = FormData.AttachmentTable;
            FormAttachment attachment = attachmentTable.get(keyNum);
            if ((attachment != null) && (!attachment.Blank))
            {
                AttachID a = GetAttachID();

                string folderPath = a.Year + "/" + ScreenType + "/" + a.ReffNo;
                ftp.Delete(folderPath, attachment.FileName);
                formPersistence.deleteAttachmentInfo(attachment);

                formPersistence.fetchAttachment(a.ReffNo, attachmentTable, ProcessID);
                updateAttachmentList();
            }
        }
        protected void evt_cboxAttachmentCategory_onLoad(object sender, EventArgs args)
        {
            List<CodeConstant> a = formPersistence.getAttachmentCategories();
            AttachmentCategoryDropDown.DataSource = a;
            AttachmentCategoryDropDown.DataBind();
        }
        protected void evt_btCloseUploadAttachment_clicked(object sender, EventArgs arg)
        {
            AttachmentPopup.Hide();
        }

        protected void ReArrangeAttachments(string pre, string post, string yy)
        {
            string olde = pre + yy;
            string neo = post + yy;
            if (!pre.StartsWith("T")) return;
            List<FormAttachment> atts = formPersistence.getAttachments(olde);
            string opath = "";
            foreach (FormAttachment a in atts)
            {
                string oldFile = a.PATH + "/" + a.FileName;
                opath = a.PATH;
                string newFile = "";
                string[] p = a.PATH.Split('/');

                if (p.Length > 0)
                {
                    p[p.Length - 1] = post;
                    newFile = string.Join("/", p, 0, p.Length) + "/" + a.FileName;
                }
                ftp.Move(oldFile, newFile);
                formPersistence.MoveAttachment(pre, a.SequenceNumber, post, yy);
            }
            ftp.RemoveDir(opath);
        }

        #endregion Attachment

        #region Notes
        protected void evt_ddlSendTo_onLoad(object sender, EventArgs arg)
        {
            NoteRecipientDropDown = (ASPxComboBox)sender;

            List<UserRoleHeader> u;
            if((FormData.StatusCode ?? 0) > 0)
                u = logic.Notice.GetNoticeUser(FormData.ReffNo, "");
            else
                u = logic.Notice.GetNoticeUser("", "");

            NoteRecipientDropDown.DataSource = u;
            NoteRecipientDropDown.DataBind();
        }
        protected void evt_btSendNotice_onClick(object sender, EventArgs e)
        {
            bool hasWorlist = hasWorklist(FormData.AccruedNo);
            clearScreenMessage();

            if (NoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(NoteRecipientDropDown.Value);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return;
                }
            }

            try
            {
                string strNotice = NoteTextBox.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = ScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?accrno={4}&mode=view",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        FormData.AccruedNo);

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (NoteRecipientDropDown.Value != null)
                    {
                        noticeTo = NoteRecipientDropDown.Value.ToString();

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(FormData.ReffNo, "", UserName, noticeTo);
                    }
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(FormData.ReffNo, "", UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(FormData.ReffNo.str(),
                                            FormData.CreatedDt.Year.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    formPersistence.fetchNote(FormData);
                    List<FormNote> noticeList = FormData.Notes;
                    NoteRepeater.DataSource = noticeList;
                    NoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (NoteRecipientDropDown.Value != null)
                    {

                        if (mail.ComposeApprovalNotice_EMAIL(FormData.ReffNo,
                                                             FormData.CreatedDt.Year.str(),
                                                             strNotice, noticeLink, noticeTo,
                                                             UserData, noticer,
                                                             FormData.DivisionID,
                                                             ScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (cntSentMail > 0)
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF", err.ErrMsg);
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                Handle(ex);
            }
            NoteTextBox.Text = "";

            NoteCommentContainer.Attributes.Remove("style");
            NoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

            postScreenMessage();
        }

        protected void evt_rptrNotice_onItemDataBound(object sender, RepeaterItemEventArgs arg)
        {
            RepeaterItem item = arg.Item;
            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                Literal litOpenDivNotice = (Literal)item.FindControl("litOpenDivNotice");
                Literal litRptrNoticeComment = (Literal)item.FindControl("litRptrNoticeComment");
                Literal litCloseDivNotice = (Literal)item.FindControl("litCloseDivNotice");
                String dateHeader;

                FormNote note = (FormNote)item.DataItem;

                if (note.Date.HasValue)
                {
                    litOpenDivNotice.Text = "<div class='noticeLeft'>";
                    dateHeader = ((DateTime)note.Date.Value).ToString("d MMMM yyyy hh:mm:ss");
                }
                else
                {
                    litOpenDivNotice.Text = "<div class='noticeRight'>";
                    dateHeader = ((DateTime)note.ReplyDate.Value).ToString("d MMMM yyyy hh:mm:ss");
                }

                litRptrNoticeComment.Text = String.Format("<span class='noteSender'>{0}</span> {1}<br>To: <span class='noteSender'>{2}</span><br>{3}",
                    note.SenderName, dateHeader, note.ReceiverName, note.Message);


                litCloseDivNotice.Text = "</div>";
            }
        }
        #endregion Notes

        #region Screen Message
        protected List<ScreenMessage> ScreenMessageList
        {
            get
            {
                List<ScreenMessage> lstMessages = Session[ScreenType + "MsgList"] as List<ScreenMessage>;
                if (lstMessages == null)
                {
                    lstMessages = new List<ScreenMessage>();
                    Session[ScreenType + "MsgList"] = lstMessages;
                    HiddenMessageExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }
        private void PostMsg(bool instant, bool clearfirst, byte sts, string id, params object[] x)
        {
            string m = "";
            StringBuilder b = new StringBuilder("");
            if (x != null && x.Length > 0)
            {
                for (int k = 0; k < x.Length; k++)
                    b.Append(x[k].str() + ", ");
            }

            if (!id.isEmpty())
            {
                m = _m.Message(id);
            }
            if (x != null && x.Length > 0)
            {
                if (!m.isEmpty())
                    m = string.Format(m, x);
                else
                {
                    b.Clear();
                    for (int i = 0; i < x.Length; i++)
                    {
                        b.Append(x[i].str());
                        b.Append(" ");
                    }
                    m = b.ToString().Trim();
                }
            }
            postScreenMessage(
                new ScreenMessage[]
                {
                    new ScreenMessage(sts, m)
                }
                , clearfirst
                , instant);
        }

        private void PostLaterOn(byte sts, string id, params object[] x)
        {
            PostMsg(false, true, sts, id, x);
        }

        private void PostNowPush(byte sts, string id, params object[] x)
        {
            PostMsg(true, false, sts, id, x);
        }

        private void PostNow(byte sts, string id, params object[] x)
        {
            PostMsg(true, true, sts, id, x);
        }

        private void PostLater(byte ScreenMessageStatus, string id, params object[] x)
        {
            PostMsg(false, false, ScreenMessageStatus, id, x);
        }

        protected void postScreenMessage()
        {
            postScreenMessage(null, false, true);
        }

        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = ScreenMessageList;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    lstMessage.Insert(0, messages[i]);
                }
            }
            updateScreenMessages();
        }
        protected void updateScreenMessages()
        {
            List<ScreenMessage> lstMessages = ScreenMessageList;
            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                ScreenMessage msg = lstMessages[0];
                StringBuilder stringBuilder = new StringBuilder();
                string expandFlag = HiddenMessageExpandFlag.Value;

                string messageBoxClass = "message-info";
                string messageBoxSpanClass = "message-span-info";
                if (msg.Status == ScreenMessage.STATUS_WARNING)
                {
                    messageBoxClass = "message-warning";
                    messageBoxSpanClass = "message-span-warning";
                }
                else if (msg.Status == ScreenMessage.STATUS_ERROR)
                {
                    messageBoxClass = "message-error";
                    messageBoxSpanClass = "message-span-error";
                }

                if (expandFlag.Equals("true"))
                {
                    messageBoxClass += " expanded-message-box";
                }
                stringBuilder.AppendLine(string.Format("<div id='messageBox' class='{0}'>", messageBoxClass));
                stringBuilder.AppendLine(string.Format("      <span class='{0}'>{1}</span>", messageBoxSpanClass, msg.Message));

                if (cntMessages > 1)
                {
                    string txtMore = "more";
                    if (expandFlag.Equals("true"))
                    {
                        txtMore = "close";
                    }
                    stringBuilder.AppendLine("      <span class='message-span-more'>");
                    stringBuilder.AppendLine(string.Format("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore));
                    stringBuilder.AppendLine("      </span>");
                }

                stringBuilder.AppendLine("</div>");
                if (expandFlag.Equals("false"))
                {
                    stringBuilder.AppendLine("<div id='messageBox-all' style='display: none'>");
                }
                else
                {
                    stringBuilder.AppendLine("<div id='messageBox-all'>");
                }
                stringBuilder.AppendLine("  <ul>");
                string listClass;
                for (int i = 1; i < cntMessages; i++)
                {
                    msg = lstMessages[i];
                    listClass = "message-detail-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        listClass = "message-detail-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        listClass = "message-detail-error";
                    }

                    stringBuilder.AppendLine(string.Format("<li class='{0}'>", listClass));
                    stringBuilder.AppendLine("      <span>" + msg.Message + "</span>");
                    stringBuilder.AppendLine("</li>");
                }
                stringBuilder.AppendLine("  </ul>");
                stringBuilder.AppendLine("</div>");

                MessageControlLiteral.Text = stringBuilder.ToString();
                MessageControlLiteral.Visible = true;
            }
            else
            {
                MessageControlLiteral.Text = "";
                MessageControlLiteral.Visible = false;
            }
        }
        protected void clearScreenMessage()
        {
            ScreenMessageList.Clear();
            updateScreenMessages();
        }
        protected void evt_messageControl_onLoad(object sender, EventArgs args)
        {
            updateScreenMessages();
        }

        #endregion Screen Message
        */
        #endregion
        
        #region Simulation

        protected void evt_btSimulation_onClick(object sender, EventArgs args)
        {
            logic.Say("evt_btSimulation_onClick", "Simulate {0}", ScreenType);

            if (FormData.AccruedNo.isEmpty())
            {
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00021WRN", "Accrued No");
                return;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ELVIS_AccrSimul",
                "openWin('AccrSimul.aspx?accrno=" + FormData.AccruedNo + "','ELVIS_AccrSimul');",
                true);
        }


        #endregion

        #region Utils
        private bool isNull(string s)
        {
            return s.isEmpty() || s.ToLower().Equals("null");
        }

        protected string normalizeDivisionName(string divName)
        {
            if (divName != null)
            {
                if (divName.ToLower().EndsWith("head"))
                {
                    divName = divName.Substring(0, divName.IndexOf('-'));
                }
            }
            return divName;
        }
        public void dump(string act = "")
        {
            string fn = ((FormData.PVNumber ?? 0) == 0 || (FormData.PVYear ?? 0) == 0) ?
                   "_%TIME%_%MS%" + "act"
                   : string.Format("{0}{1}", FormData.PVNumber, FormData.PVYear);
            File.WriteAllText(
              Util.GetTmp(
                  LoggingLogic.GetPath(), fn, ".txt")
              , FormData.dump());
        }
        #endregion

        /* button upload
         * 
         * 
         */
        protected void btnUploadTemplate_Click(object sender, EventArgs e)
        {
            logic.Say("btUploadTemplate_Click", "Upload : {0}", DataUpload.FileName);
            clearScreenMessage();
            try
            {

                if (!DataUpload.HasFile)
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00034ERR");
                    return;
                }

                int _processId = 0;
                CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                string _strFileType = Path.GetExtension(DataUpload.FileName).ToString().ToLower();
                string _strFileName = DataUpload.FileName;

                if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                    return;
                }

                int TemplateCd = 12;

                string _strMetaData = Server.MapPath(
                            string.Format(
                                logic.Sys.GetText("UPLOAD_META_FMT", "0"), logic.Sys.GetText("TEMPLATE_CD", TemplateCd.str())
                            )
                        );

                string _strFunctionId = "";
                _strFunctionId = resx.FunctionId("FCN_" + ScreenType + "_FORM_UPLOAD");
                _processId = DoStartLog(_strFunctionId, "");

                string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                DataUpload.SaveAs(xlsFile);

                List<AccrFormDetail> dd = new List<AccrFormDetail>();
                string[][] listOfErrors = new string[1][];
                int tt = logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219);

                logic.AccrForm.DeleteTempAccrued(_processId);

                bool Ok = logic.AccrForm.UploadAccr(xlsFile, _strMetaData, ref dd,
                                ref listOfErrors, tt, UserData, _processId);

                if (!Ok)
                {

                    logic.AccrForm.GenerateLogAccr(_processId, UserData);

                    string sourceFile = Server.MapPath(
                                            String.Format(
                                                    logic.Sys.GetText("TEMPLATE.FMT", "ERR"), logic.Sys.GetText("TEMPLATE_CD", TemplateCd.str())
                                                )
                                            );
                    string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                    string fileName = formPersistence.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                    String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                            Request.Url.Scheme,
                                                            Request.ServerVariables["SERVER_NAME"],
                                                            Request.ServerVariables["SERVER_PORT"],
                                                            Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                            fileName,
                                                            fileName);

                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                    return;
                }


                DetailGrid.UpdateEdit();

                //List<AccrFormDetail> newRs = logic.AccrForm.getDescription(dd);

                Tabel.DataList.Clear();
                //Tabel.DataList = newRs;
                Tabel.DataList = dd;
                Tabel.CleanRows();
                //Tabel.DataList = Tabel.sanityCheck();
                var x = Tabel.rows;
                Tabel.resetDisplaySequenceNumber();
                StartEditingTabel();
                PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
            }
            catch (Exception ex)
            {
                PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        protected void btExport_onClick(object sender, EventArgs e)
        {
            
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedForm.xls");
                string excelName = "List_Accrued_Report_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                string pdfName = "";
                if (!(userdata.Roles.Contains("ELVIS_BUDGET") || userdata.Roles.Contains("ELVIS_ACC")))
                {
                    pdfName = "List_Accrued_Report_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
                }

                // TODO: Delete this line and implement proper excel to pdf convert | Issue PDF. Countermeasure : switch to download excel file
                pdfName = "";
                //Xmit.Send(logic.AccrList.GetExcelDownload(
                //            UserName,
                //            templatePath,
                //            SearchCriteria()
                //        ), HttpContext.Current, "AccruedList" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls");
                // pdfName = "";
                

                string fullPath = logic.AccrList.GetLinkExcelDownload(
                            UserName,
                            templatePath,
                            excelName,
                            new AccruedSearchCriteria(FormData.AccruedNo),
                            FormData.StatusCode ?? 0,
                            userdata,
                            pdfName
                        );
                //Response.Write(fullPath);
                //Response.End();
                 Xmit.Transmit(HttpContext.Current, fullPath);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    string xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                    Nag("MSTD00042ERR", ex.Message);
                }

                Response.Write(ex.Message);
                Response.End();
            }
        }

        protected void ddlDocType_ValueChanged(object sender, EventArgs e)
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;

            ASPxGridLookup PvType = (ASPxGridLookup)sender;

            Tabel.rows[idxEditing].PVTypeCd = Convert.ToInt32(PvType.Value);
            Tabel.rows[idxEditing].PVType = PvType.Text;
        }
        private bool WaitForK2UpdateStatus(out int newStatus, int maxLoop = 1200)
        {
            Ticker.In("WaitForK2UpdateStatus");
            int oldStatus = FormData.StatusCode ?? 0;
            int i = 0;

            do
            {
                System.Threading.Thread.Sleep(100 + ((i * 20) % 250));
                newStatus = logic.AccrForm.GetLatestStatus(FormData.AccruedNo);
                i++;
            } while (oldStatus == newStatus && i < maxLoop);

            if (i >= maxLoop)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00082WRN");
                return false;
            }
            else
                return true;

        }

        private bool CheckWbsNotExceeded()
        {
            List<string> wbsExc = FormData
                                    .Details
                                    .Where(x => (x.SapAmtRemaining ?? -1) < 0)
                                    .Select(x => x.WbsNumber).Distinct()
                                    .ToList();

            bool err = true;
            if (wbsExc != null && wbsExc.Count > 0)
            {
                err = false;
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00231ERR", "Budget ", string.Join(", ", wbsExc));
            }
            return err;
        }
    }
}