﻿<%@ Page Title="Cashier Policy Mock" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true" CodeBehind="CashierPolicyMock.aspx.cs" Inherits="ELVISDashBoard._80Accrued.CashierPolicyMock" %>


<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="pre" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Cashier Policy Mock" />
  <dx:ASPxGridView ID="grid" runat="server" ClientIDMode="Static" SettingsEditing-Mode="Inline" KeyFieldName="SequenceNumber">
  <Columns>
    <dx:GridViewCommandColumn ButtonType="Link" >
        <NewButton Text="Add" Visible="true"/>
        <EditButton Text="Edit" Visible="true"/>
        <DeleteButton Text="Delete" Visible="true"/>
    </dx:GridViewCommandColumn>
    <dx:GridViewDataTextColumn FieldName="SequenceNumber">
    <EditItemTemplate>
        <asp:TextBox ID="gSeq" runat="server" />
    </EditItemTemplate>
    </dx:GridViewDataTextColumn>
    <dx:GridViewDataTextColumn FieldName="Description" Width="200px">
        <EditItemTemplate>
            <asp:TextBox ID="gDesc" runat="server" />
        </EditItemTemplate>
    </dx:GridViewDataTextColumn>
    <dx:GridViewDataTextColumn FieldName="CurrencyCode" Width="100px">
        <EditItemTemplate>
            <asp:TextBox ID="gCurr" runat="server" />
        </EditItemTemplate>
    </dx:GridViewDataTextColumn>
    <dx:GridViewDataTextColumn FieldName="Amount" Width="150px">
        <EditItemTemplate>
            <asp:TextBox ID="gAmt" runat="server" />
        </EditItemTemplate>
    </dx:GridViewDataTextColumn>
  </Columns>
  </dx:ASPxGridView>
  <asp:LinqDataSource ID="dsData" runat="server" 
  
  EnableInsert="true" EnableDelete="true" EnableUpdate="true" 
  ContextTypeName="ELVISDashBoard._80Accrued.CashierPolicyMock"
  TableName="Rows"
  
  />
  <asp:ObjectDataSource ID="dsO" runat="server" InsertMethod=""
  />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
