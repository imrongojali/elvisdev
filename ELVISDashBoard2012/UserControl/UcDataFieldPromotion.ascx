﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDataFieldPromotion.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcDataFieldPromotion" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<script type="text/javascript">
    function OnNewClick(s, e) {
        ASPxGridViewPromotion.AddNewRow();
    }

    function OnNewDetailClick(s, e) {
       
        gvDetail.AddNewRow();
    }
    function BeforeUploadPromotion_Click(s, e) {
        var fn = $("#fuDataFieldPromotionList").val();
        if (!fn || fn.length() < 1) {
            alert("Select file");
            return false;
        }
        return true;
    }
   
    
    //function OnEditClick(s, e) {
    //    var index = grid.GetFocusedRowIndex();
    //    grid.StartEditRow(index);
    //}

    //function OnSaveClick(s, e) {
    //    grid.UpdateEdit();
    //}

    //function OnCancelClick(s, e) {
    //    grid.CancelEdit();
    //}

    //function OnDeleteClick(s, e) {
    //    var index = grid.GetFocusedRowIndex();
    //    grid.DeleteRow(index);
    //}  
</script>
<style type="text/css">
    div.inline {
        float: left;
    }

    .clearBoth {
        clear: both;
    }

    /*  #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 150px;
   }
   #divVendorName
   {
       width: 120px;
   }*/
</style>

<asp:UpdatePanel runat="server" ID="UpdatePanelPromotion">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSave" />
        <asp:PostBackTrigger ControlID="btnClose" />
        
         <asp:PostBackTrigger ControlID="btnUploadTemplatePromotion" />
    </Triggers>
    <ContentTemplate>


        <%--    <asp:HiddenField runat="server" ID="hidCategoryCodeProm" />
        <asp:HiddenField runat="server" ID="hidTransactionCodeProm" />
        <asp:HiddenField runat="server" ID="hidReferenceNoProm" />
        <asp:HiddenField runat="server" ID="hidUserNameProm" />--%>


        <%--  FixedStyle="Left" CellStyle-BackColor="#ffffd6--%>
        <%--OnInitNewRow="ASPxGridViewPromotion_InitNewRow"--%>
        <%--  <a onclick="aspxGVScheduleCommand('ASPxGridViewPromotion',['AddNew'],1)" href="javascript:;">New</a>--%>
             <dx:ASPxHyperLink runat="server" ID="lnkDownloadTemplate"  Text="Download Template"
                                            NavigateUrl="~/Template/Excel/TemplateDataFieldPromotion.xls" />
                    <br />
                     <asp:FileUpload ID="fuDataFieldPromotionList" runat="server" ClientIDMode="Static" />
                                        &nbsp;<asp:Button runat="server" ID="btnUploadTemplatePromotion" Text="Upload" OnClick="btnUploadTemplatePromotion_Click"
                                            OnClientClick="return BeforeUploadPromotion_Click()" />
        <br />
      <div>&nbsp;&nbsp;</div>
        <dx:ASPxPanel ID="FormPanelPromotion" runat="server" Width="100%" style="border: 1px solid #000;">
        
            <PanelCollection>
                
                <dx:PanelContent runat="server">
                      
                    <div>&nbsp;&nbsp;</div>
                    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                        <tr>
                            <td valign="baseline" style="width: 170px">Tahun Pajak
                            </td>
                            <td valign="top" style="width: 100px">
                                <dx:ASPxSpinEdit ID="spnTahunPajak" runat="server" Number="0" Width="70" MaxLength="4" ClientInstanceName="spnTahunPajak" OnValidation="TahunPajak_Validation" NumberType="Integer">
                                  <ValidationSettings ValidationGroup="GroupPromotion" SetFocusOnError="True" ErrorTextPosition="Bottom" ErrorText="Name must be at least two characters long">
                            <RequiredField IsRequired="True" ErrorText="Name is required" />
                        </ValidationSettings>
                      
                                   
                                </dx:ASPxSpinEdit>
                            </td>
                            <td valign="baseline" style="width: 170px">Tmp. &  Tgl. Penandatangan
                            </td>
                            <td valign="top" style="width: 260px">
                                <div class="inline">
                                    <dx:ASPxTextBox runat="server" ID="txtTempatPenandatangan" Width="120px">

                                         <ValidationSettings ValidationGroup="GroupPromotion" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True" ErrorText="Name is required" />
                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>

                                <div class="inline" style="width: 10px">&nbsp;&nbsp;</div>

                                <div class="inline">
                                    <dx:ASPxDateEdit ID="dtPenandaTangan"  ClientInstanceName="dtPenandaTangan"
                                        ClientIDMode="static" runat="server" Width="120px" DisplayFormatString="dd MMM yyyy"
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table">

                                      
                                         <CalendarProperties>
                                             <FastNavProperties Enabled="False" />
                                         </CalendarProperties>

                                      
                                         <ValidationSettings ValidationGroup="GroupPromotion" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True"  ErrorText="Name is required" />
                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline" style="width: 200px">Nama Penandatangan
                            </td>
                            <td valign="top" style="width: 200px">
                                <dx:ASPxTextBox runat="server" ID="txtNamaPenandatangan" Width="300px">

                                   
                                         <ValidationSettings ValidationGroup="GroupPromotion" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True" ErrorText="Name is required" />
                        </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>

                            <td valign="baseline">Jabatan Penandatangan
                            </td>
                            <td valign="top">
                                <div>
                                    <dx:ASPxTextBox runat="server" ID="txtJabatanpenandatanagn" Width="250px">

                                         <ValidationSettings ValidationGroup="GroupPromotion" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True" ErrorText="Name is required" />
                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>

                            </td>
                        </tr>

                    </table>
                    </dx:PanelContent>
                            </PanelCollection>
        </dx:ASPxPanel>
        <div>&nbsp;&nbsp;</div>
                    <dx:ASPxButton ID="btnNew" runat="server" Text="ADD" AutoPostBack="false">
                        <ClientSideEvents Click="function (s, e) { OnNewClick(s, e); }" />
                    </dx:ASPxButton>
                    <br />
                    <dx:ASPxGridView ID="ASPxGridViewPromotion" runat="server"
                        ClientInstanceName="ASPxGridViewPromotion"
                        AutoGenerateColumns="False"
                        OnRowInserting="ASPxGridViewPromotion_RowInserting"
                        OnRowUpdating="ASPxGridViewPromotion_RowUpdating"
                        OnRowDeleting="ASPxGridViewPromotion_RowDeleting"
                        OnRowValidating="ASPxGridViewPromotion_RowValidating"
                        OnInit="ASPxGridViewPromotion_Init"
                        OnInitNewRow="ASPxGridViewPromotion_InitNewRow"
                        OnParseValue="ASPxGridViewPromotion_ParseValue"
                        ClientIDMode="AutoID" KeyFieldName="SEQ_NO" Width="100%"
                        Styles-AlternatingRow-CssClass="even">
                        <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <SettingsEditing Mode="Inline" />


                        <Columns>
                            <dx:GridViewCommandColumn VisibleIndex="0" FixedStyle="Left" Width="63px" ButtonType="Image">

                                <EditButton Visible="True">
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/damage.png"></Image>
                                </EditButton>

                                <DeleteButton Visible="True">
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/svn_deleted.png"></Image>
                                </DeleteButton>
                                <CancelButton>
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/svn_modified.png">
                                    </Image>
                                </CancelButton>
                                <UpdateButton>
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/svn_normal.png">
                                    </Image>
                                </UpdateButton>
                               
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>

                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="NO" Caption="No" Width="40px" VisibleIndex="1">
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblSeqNo" Text='<%# Bind("NO")%>'></dx:ASPxLabel>
                                    <asp:HiddenField runat="server" ID="hidSeqNo" Value='<%# Bind("SEQ_NO") %>' />
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNo" Text='<%# Bind("NO") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="NAMA" Caption="Nama" Width="400px" VisibleIndex="2">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNama" Width="392px" Value='<%# Bind("NAMA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">

                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>


                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNama" Text='<%# Bind("NAMA") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="NPWP" Caption="NPWP" Width="150px" VisibleIndex="3">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox ID="txtNPWP" runat="server" Width="142" Value='<%#Bind("NPWP") %>' ClientInstanceName="txtNPWP" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <MaskSettings Mask="99.999.999.9-999.999" IncludeLiterals="None" />
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>

                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                   <%-- <asp:HiddenField runat="server" ID="hidNPWPFORMAT" Value='<%# Bind("NPWPFORMAT") %>' />--%>
                                    <dx:ASPxLabel runat="server" ID="lblNPWP" Text='<%# Bind("NPWP") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="ALAMAT" Caption="Alamat" Width="310px" VisibleIndex="4">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtAlamat" Width="302px" Value='<%# Bind("ALAMAT") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblAlamat" Text='<%# Bind("ALAMAT") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                        

                          

                        </Columns>
                         <SettingsDetail ShowDetailRow="True"  AllowOnlyOneMasterRowExpanded="true"/>
                        <Settings ShowVerticalScrollBar="false"
                           
                            ShowHorizontalScrollBar="True" />
                        <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />
                        <Styles>
                            <AlternatingRow CssClass="even">
                            </AlternatingRow>
                        </Styles>
                       
     
                                <Templates>
            <DetailRow>
                 
                 
                <dx:ASPxGridView ID="gvDetail" ClientInstanceName="gvDetail" runat="server" Width="1000" 
                   
                     OnInit="gvDetail_Init"
                     AutoGenerateColumns="False"
                        OnRowInserting="gvDetail_RowInserting"
                        OnRowUpdating="gvDetail_RowUpdating"
                        OnRowDeleting="gvDetail_RowDeleting"
                        OnRowValidating="gvDetail_RowValidating"
                      
                        OnInitNewRow="gvDetail_InitNewRow"
                    Styles-AlternatingRow-CssClass="even"
                    KeyFieldName="SEQ_NO_DETAIL">
                       <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <SettingsEditing   Mode="Inline" />
                    <Columns>
                         <dx:GridViewCommandColumn VisibleIndex="0" FixedStyle="Left" Width="63px" ButtonType="Image">

                                <EditButton Visible="True">
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/damage.png"></Image>
                                </EditButton>

                                <DeleteButton Visible="True">
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/svn_deleted.png"></Image>
                                </DeleteButton>
                                <CancelButton>
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/svn_modified.png">
                                    </Image>
                                </CancelButton>
                                <UpdateButton>
                                    <Image ToolTip="Editar" Url="~/App_Themes/BMS_Theme/Images/svn_normal.png">
                                    </Image>
                                </UpdateButton>
                              <HeaderTemplate><dx:ASPxButton ID="btnNewDetail" runat="server" Text="ADD" AutoPostBack="false">
                        <ClientSideEvents Click="function (s, e) { OnNewDetailClick(s, e ); }" />
                    </dx:ASPxButton></HeaderTemplate>
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>

                            </dx:GridViewCommandColumn>
                       <%-- <dx:GridViewDataTextColumn FieldName="SEQ_NO" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>--%>
                            <dx:GridViewDataTextColumn FieldName="TANGGAL" Caption="Tanggal" Width="120px" VisibleIndex="1">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxDateEdit ID="dtTanggal" Value='<%# Bind("TANGGAL") %>' ClientInstanceName="dtTanggal"
                                        ClientIDMode="static" runat="server" Width="112px" DisplayFormatString="dd MMM yyyy"
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">

                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblTanggal" Text='<%# Bind("TANGGAL","{0:dd MMM yyyy}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="BENTUK_DAN_JENIS_BIAYA" Caption="Bentuk Dan Jenis Biaya" Width="230px" VisibleIndex="2">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtBentukDanJenisBiaya" Width="222px" Value='<%# Bind("BENTUK_DAN_JENIS_BIAYA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>

                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblBentukDanJenisBiaya" Text='<%# Bind("BENTUK_DAN_JENIS_BIAYA") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="JUMLAH" Caption="Jumlah" Width="180px" VisibleIndex="3">
                                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Right" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxSpinEdit ID="spnJumlah" runat="server" DisplayFormatString="C2" Number="0" Width="172" Value='<%# Bind("JUMLAH") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>

                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblJumlah" Text='<%# Bind("JUMLAH","{0:C2}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn FieldName="JUMLAH_GROSS_UP" Caption="Jumlah Gross Up" Width="180px" VisibleIndex="4">
                                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Right" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxSpinEdit ID="spnJumlahGrossUp" runat="server" DisplayFormatString="C2" Number="0" Width="172" Value='<%# Bind("JUMLAH_GROSS_UP") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblJumlahGrossUp" Text='<%# Bind("JUMLAH_GROSS_UP","{0:C2}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>


                            <dx:GridViewDataTextColumn FieldName="KETERANGAN" Caption="Keterangan" Width="500px" VisibleIndex="5">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtKeterangan" Width="492" Value='<%# Bind("KETERANGAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblKeterangan" Text='<%# Bind("KETERANGAN") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="JUMLAH_PPH" Caption="Jumlah Pph" Width="180px" VisibleIndex="6">
                                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Right" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxSpinEdit ID="spnJumlahPph" runat="server" DisplayFormatString="C2" Number="0" Width="172" Value='<%# Bind("JUMLAH_PPH") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblJumlahPph" Text='<%# Bind("JUMLAH_PPH","{0:C2}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="JENIS_PPH" Caption="Jenis Pph" Width="200px" VisibleIndex="7">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtJenisPph" Width="192" Value='<%# Bind("JENIS_PPH","{0:C2}") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblJenisPph" Text='<%# Bind("JENIS_PPH") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="JUMLAH_NET" Caption="Jumlah Net" Width="180px" VisibleIndex="8">
                                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Right" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxSpinEdit ID="spnJumlahNet" runat="server" DisplayFormatString="C2" Number="0" Width="172" Value='<%# Bind("JUMLAH_NET") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblJumlahNet" Text='<%# Bind("JUMLAH_NET","{0:C2}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="NOMOR_REKENING" Caption="No. Rekening" Width="150px" VisibleIndex="9">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNoRekening" Width="142" Value='<%# Bind("NOMOR_REKENING") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNoRekening" Text='<%# Bind("NOMOR_REKENING") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="NAMA_REKENING_PENERIMA" Caption="Nama Rekening Penerima" Width="300px" VisibleIndex="10">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNamaRekeningPenreima" Width="292" Value='<%# Bind("NAMA_REKENING_PENERIMA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNamaRekeningPenreima" Text='<%# Bind("NAMA_REKENING_PENERIMA") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="NAMA_BANK" Caption="Nama Bank" Width="300px" VisibleIndex="11">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNamaBank" Width="292" Value='<%# Bind("NAMA_BANK") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNamaBank" Text='<%# Bind("NAMA_BANK") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>


                            <dx:GridViewDataTextColumn FieldName="NO_KTP" Caption="NO KTP" Width="150px" VisibleIndex="12">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNoKtp" Width="142" Value='<%# Bind("NO_KTP") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNoKtp" Text='<%# Bind("NO_KTP") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                       
                    </Columns>
                    <Settings ShowVerticalScrollBar="false"
                            VerticalScrollableHeight="250"
                            ShowHorizontalScrollBar="True" />
                        <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />
                        <Styles>
                            <AlternatingRow CssClass="even">
                            </AlternatingRow>
                        </Styles>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
                    </dx:ASPxGridView>
                    <br />
                    <br />
                    <%--<SettingsLoadingPanel ImagePosition="left" Mode="ShowAsPopup" />
            <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />--%>
                    <div class="inline">

                  <dx:ASPxButton ID="btnSave" runat="server" Text="Save" OnClick="evt_Save" ValidationGroup="GroupPromotion" ></dx:ASPxButton>
                         
                        <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="evt_Cose" />
                      
                    </div>

                   <%-- <dx:ASPxPopupControl ID="popaddProm" ClientInstanceName="popaddProm" runat="server"
                        Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                        Width="300px" EnableAnimation="false" HeaderText="Promotion" AllowDragging="false"
                        AllowResize="false">
                        <ContentCollection>
                            <dx:PopupControlContentControl ID="popAddPromotionContent" runat="server">
                                <table>
                                    <tr>
                                        <td style="margin-right: 10px;">Pertayaan Berikutnya
                                        </td>

                                    </tr>

                                </table>
                                <br />
                                <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnsaveprom"
                                    Text="Ok">
                                    <ClientSideEvents Click="function(s,e) { popaddProm.Hide(); loading(); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btCancelprom"
                                    Text="Cancel">
                                    <ClientSideEvents Click="function(s,e) { popaddProm.Hide(); }" />
                                </dx:ASPxButton>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>--%>
                
     </ContentTemplate>

</asp:UpdatePanel>
           
