<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calendar.ascx.cs" Inherits="ELVISDashboard.UserControl.Calendar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:TextBox ID="txtCalendar" runat="server" style="width:80px;"
    ontextchanged="txtCalendar_TextChanged" />
<asp:ImageButton runat="server" ID="imgCalendar" SkinID="ImageCalendar" AlternateText="Click to show calendar" ValidationGroup="MKE" />
<cc1:CalendarExtender ID="CalendarExtender" runat="server" PopupPosition="Right"
    TargetControlID="txtCalendar" PopupButtonID="imgCalendar" Format="dd/MM/yyyy"/>
<cc1:MaskedEditExtender ID="MaskedEditExtender" runat="server"
    TargetControlID="txtCalendar"
    Mask="99/99/9999"
    AutoComplete="true"
    MessageValidatorTip="true"
    OnFocusCssClass="MaskedEditFocus"
    OnInvalidCssClass="MaskedEditError"
    MaskType="Date"
    DisplayMoney="Left"
    AcceptNegative="Left"
    CultureName="id-ID"
    ErrorTooltipEnabled="True" />
<cc1:MaskedEditValidator ID="MaskedEditValidator" runat="server"
    ControlExtender="MaskedEditExtender"
    ControlToValidate="txtCalendar"
    EmptyValueMessage="Date is required"
    InvalidValueMessage="Date is invalid"
    TooltipMessage="Input a date"
    EmptyValueBlurredText="*"
    InvalidValueBlurredMessage="*"
    Display="Static"
    ValidationGroup="MKE" />
<asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="txtCalendar" Display="Static"
    ErrorMessage="Date must be filled" Text="*" ValidationGroup="MKE" />
