﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NumberedTextbox.ascx.cs" Inherits="ELVISDashboard.UserControl.NumberedTextbox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:TextBox runat="server" ID="TextBox" MaxLength="17" Width="140px"></asp:TextBox>
<cc1:MaskedEditExtender ID="MaskedEditExtender" runat="server"
    TargetControlID="TextBox"
    Mask="999,999,999,999"
    AutoComplete="false"
    MessageValidatorTip="true"
    OnFocusCssClass="MaskedEditFocus"
    OnInvalidCssClass="MaskedEditError"
    InputDirection="RightToLeft"
    MaskType="Number"
    AcceptNegative="Left"
    ErrorTooltipEnabled="True" />
<cc1:MaskedEditValidator ID="MaskedEditValidator" runat="server"
    ControlExtender="MaskedEditExtender"
    ControlToValidate="TextBox"
    Display="Dynamic"
    EmptyValueBlurredText="*"
    InvalidValueBlurredMessage="*"
    ValidationGroup="submit" />