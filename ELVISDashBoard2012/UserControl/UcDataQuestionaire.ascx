﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDataQuestionaire.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcDataQuestionaire" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>

    <script type="text/javascript">
        function Validate(s, e) {
            if (ASPxClientEdit.ValidateGroup('testGroup'))
                ClientCallbackPanelDemo.PerformCallback('');
        }
    </script>
    <div>
        <dx:ASPxButton ID="ASPxButtonSave" runat="server" Text="Validate" AutoPostBack="False">
            <ClientSideEvents Click="Validate" />
        </dx:ASPxButton>
         <asp:Button ID="Button2" runat="server" Text="Close" OnClick="evt_DataQuestionaire" />
        <br />
        <dx:ASPxCallbackPanel ID="ASPxCallbackPanelDemo" runat="server"  HideContentOnCallback="False"
            ClientInstanceName="ClientCallbackPanelDemo" OnCallback="ASPxCallbackPanelDemo_Callback">
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server">
                    <table cellspacing="0" cellpadding="4" runat="server" id="serverContainer">
                        <tr>
                            <td style="width: 60px;">
                                <dx:ASPxLabel runat="server" ID="NameLabel" AssociatedControlID="txtNum1" Text="Number 1:" />
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtNum1" runat="server" Width="170px" OnValidation="ASPxTextBoxTest_Validation">
                                    <ValidationSettings ValidationGroup="testGroup">
                                        <RequiredField IsRequired="True" ErrorText="Number 1 is required" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 60px;">
                                <dx:ASPxLabel runat="server" ID="ASPxLabel1" AssociatedControlID="txtNum2" Text="Number 2:" />
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="txtNum2" runat="server" Width="170px" OnValidation="ASPxTextBoxTest_ValidationTes2">
                                    <ValidationSettings ValidationGroup="testGroup">
                                        <RequiredField IsRequired="True" ErrorText="Number 2 is required"/>
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
<%--<asp:UpdatePanel runat="server" ID="pnupdateLitMessage">--%>
    
     <%-- <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" />--%>
    <%--    <ContentTemplate>
             <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" />
            <asp:Literal runat="server" ID="messageControl" Visible="false" EnableViewState="false"
                OnLoad="evt_messageControl_onLoad" />
        </ContentTemplate>--%>
<%--</asp:UpdatePanel>--%>