﻿
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;

using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using Microsoft.Reporting.WebForms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


using QRCoder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace ELVISDashBoard.UserControl
{

    public partial class UcDataFieldPromotion : System.Web.UI.UserControl
    {
        protected readonly GlobalResourceData resx = new GlobalResourceData();
        public static string _CategoryCodeProm;
        public static string _TransactionCodeProm;
        public static string _ReferenceNoProm;
        public static string _UserNameProm;
        public static string _VendorCode;
        public static string _VendorGroup;


        protected FtpLogic ftp = new FtpLogic();

        //public string CategoryCodeProm
        //{
        //    get;
        //    set;
        //}
        //public string TransactionCodeProm
        //{
        //    get;
        //    set;
        //}
        //public string ReferenceNoProm
        //{
        //    get;
        //    set;
        //}
        //public string UserNameProm
        //{
        //    get;
        //    set;
        //}

        //public string VendorCode
        //{
        //    get;
        //    set;
        //}

        //public string VendorGroup
        //{
        //    get;
        //    set;
        //}



        private object _VoucherFormPage()
        {
            return this.Page as _30PvFormList.VoucherFormPage;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            this.InitializeCulture();

           
            spnTahunPajak.Number = DateTime.Now.Year;
            dtPenandaTangan.Date =Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));

        }

        protected void InitializeCulture()
        {
            //CultureInfo Culture = new System.Globalization.CultureInfo("id-ID");
            //Thread.CurrentThread.CurrentCulture = Culture;
            //Thread.CurrentThread.CurrentUICulture = Culture;
            //CultureInfo newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();

            CultureInfo newCulture = new System.Globalization.CultureInfo("id-ID");
            newCulture.NumberFormat.CurrencySymbol = "Rp. ";
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
        }

        //public static string FormatCurrency(decimal value)
        //{
        //    CultureInfo cultureInfo = Thread.CurrentThread.CurrentUICulture;
        //    RegionInfo regionInfo = new RegionInfo(cultureInfo.LCID);
        //    string formattedCurrency = String.Format("{0} {1:C}", regionInfo.ISOCurrencySymbol, value);
        //    return formattedCurrency.Replace(cultureInfo.NumberFormat.CurrencySymbol, String.Empty).Trim();

        //}
        public void PromotionList(string CategoryCodeProm, string TransactionCodeProm, string ReferenceNoProm, string UserNameProm, string VendorCode, string VendorGroup)
        {

            _CategoryCodeProm = CategoryCodeProm;
            _TransactionCodeProm = TransactionCodeProm;
            _ReferenceNoProm = ReferenceNoProm;
            _UserNameProm = UserNameProm;
            _VendorCode = VendorCode;
            _VendorGroup = VendorGroup;
           
            // dtPenandaTangan.Value = date.Year;
           
            

            //this.Load += Page_Load;

            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<Promotion> _listData = new List<Promotion>();
            _listData = def.formPersistence.GetPromotion(CategoryCodeProm, Convert.ToInt32(TransactionCodeProm), ReferenceNoProm);
            ASPxGridViewPromotion.DataSource = _listData;
            ASPxGridViewPromotion.DataBind();

            def.FormData.CategoryCode = CategoryCodeProm;
            def.FormData.TransactionCodeDataField = TransactionCodeProm;
            def.FormData.ReferenceNoDataField = ReferenceNoProm;




            string templatePathSource = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldPromotionTemp.xls");
            string templatePath = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldPromotion.xls");
            //string excelName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
            //string pdfName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
            FileStream file = new FileStream(templatePathSource, FileMode.OpenOrCreate, FileAccess.Read);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
            // sheet.GetRow(3).GetCell(0).StringCellValue.ToString().Trim();

            int i = 0;
            List<DataFieldTemplate> dataField = new List<DataFieldTemplate>();
            dataField = def.formPersistence.getDataFieldNameTemplate(_CategoryCodeProm);
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRow = sheet.GetRow(3);
                    ICell cell = dataRow.GetCell(i);
                    cell.SetCellValue(item.FieldNameByExcel);

                    IRow dataRowValue = sheet.GetRow(4);
                    ICell cellValue = dataRowValue.GetCell(i);

                    EnomVendor _VendorFirst = new EnomVendor();
                    _VendorFirst = def.formPersistence.GetEnomVendor(_VendorCode).FirstOrDefault();
                    if (i == 0)
                    {
                        cellValue.SetCellValue(1);
                    }
                    else if (i == 1)
                    {
                        cellValue.SetCellValue(_VendorFirst.VENDOR_NAME);
                    }
                    else if (i == 2)
                    {
                        cellValue.SetCellValue(RemoveSpecialCharacters(_VendorFirst.NPWP));
                    }
                    else if (i == 4)
                    {
                        cellValue.SetCellValue(DateTime.Now.ToString("dd.MM.yyyy"));
                    }
                    else if (i == 6 || i == 7)
                    {
                        cellValue.SetCellValue(0);
                    }
                    else if (i == 6 || i == 7 || i == 9 || i == 11)
                    {
                        cellValue.SetCellValue(0);
                    }
                    else
                    {
                        cellValue.SetCellValue("");
                    }
                    bool isValidation = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, item.FieldNameByTable, Convert.ToInt32(_VendorGroup));
                    if (isValidation == true)
                    {
                        stylexls(hssfwb, cell);
                    }
                    i++;
                }
            }
            using (FileStream fs = new FileStream(templatePath, FileMode.Create, FileAccess.Write))
            {

                hssfwb.Write(fs);
                fs.Close();
                file.Close();
            }

            int r = 0;
            string templatePathSourceXls = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotionTemp.xls");
            string templatePathXls = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotion.xls");
            FileStream filexls = new FileStream(templatePathSourceXls, FileMode.OpenOrCreate, FileAccess.Read);
            HSSFWorkbook hssfwbxls = new HSSFWorkbook(filexls);
            ISheet sheetxls = hssfwbxls.GetSheet("Default");
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRowXls = sheetxls.GetRow(0);
                    ICell cellXls = dataRowXls.GetCell(r);
                    cellXls.SetCellValue(item.FieldNameByExcel);
                    stylexls(hssfwbxls, cellXls);
                    r++;

                }
            }
            using (FileStream fsXls = new FileStream(templatePathXls, FileMode.Create, FileAccess.Write))
            {

                hssfwbxls.Write(fsXls);
                fsXls.Close();
                filexls.Close();
            }


        }

        public void stylexls(HSSFWorkbook hssfwb, ICell cell)
        {
            ICellStyle style = hssfwb.CreateCellStyle();

            //style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
            style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.PALE_BLUE.index;
            style.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            IFont font = hssfwb.CreateFont();
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            cell.CellStyle = style;
        }

        protected void evt_Cose(object sender, EventArgs arg)
        {

        }


        protected void evt_Save(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            if (ASPxEdit.AreEditorsValid(FormPanelPromotion))
            {



                try
                {

               
              
                
                DataFieldHeader dataFieldHeader = new DataFieldHeader();
                dataFieldHeader.CATEGORY_CODE = IsNullString(_CategoryCodeProm);
                dataFieldHeader.TRANSACTION_CD = IsNullInt(_TransactionCodeProm);
                dataFieldHeader.REFERENCE_NO = IsNullString(_ReferenceNoProm);
                dataFieldHeader.THN_PAJAK = IsNullInt(spnTahunPajak.Value);
                dataFieldHeader.TMP_PENANDATANGAN = IsNullString(txtTempatPenandatangan.Value);
                dataFieldHeader.TGL_PENANDATANGAN = IsNullDateTime(dtPenandaTangan.Value);
                dataFieldHeader.NAMA_PENANDATANGAN = IsNullString(txtNamaPenandatangan.Value);
                dataFieldHeader.JABATAN_PENANDATANGAN = IsNullString(txtJabatanpenandatanagn.Value);
                bool IsValid = def.formPersistence.UpdateHeaderPromotion(dataFieldHeader);
                if (IsValid == false)
                {
                    def.clearScreenMessage();
                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Is Validation");
                }
                else
                {
                    
                  

                        string templatePathSource = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotion.xls");
                    string templatePath = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotionTest.xls");
                    string reportPath = Server.MapPath("~/Report/RDLC/DataFieldPromotion.rdlc");
                     byte[] filexlspromotion= def.formPersistence.ExportXlsPromotion(_CategoryCodeProm, _TransactionCodeProm, _ReferenceNoProm, templatePathSource, templatePath);
                    string pdfName = "Data_Promotion_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
                    string pdfNamexls = "Data_Promotion_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                    byte[] filepdfpromotion = def.formPersistence.ExportPdfPromotion(_CategoryCodeProm, _TransactionCodeProm, _ReferenceNoProm, reportPath);
                    //using (FileStream fs = File.Create(Server.MapPath("~/Report/RDLC/") + pdfName))
                    //{
                    //    fs.Write(filepdfpromotion, 0, filepdfpromotion.Length);
                    //}

                    //using (FileStream fsxls = File.Create(Server.MapPath("~/Report/RDLC/") + pdfNamexls))
                    //{
                    //    fsxls.Write(filexlspromotion, 0, filexlspromotion.Length);
                    //}

                    bool uploadSucceded = true;
                    string errorMessage = null;

                   
                    ftp.ftpUploadBytes(spnTahunPajak.Value.ToString(), def.ScreenType,
                                    IsNullString(_ReferenceNoProm),
                                    pdfName,
                                    filepdfpromotion,
                                    ref uploadSucceded,
                                    ref errorMessage);

                    ftp.ftpUploadBytes(spnTahunPajak.Value.ToString(), def.ScreenType,
                                  IsNullString(_ReferenceNoProm),
                                  pdfNamexls,
                                  filexlspromotion,
                                  ref uploadSucceded,
                                  ref errorMessage);

                        def.FormData.CategoryCode = _CategoryCodeProm;
                        def.FormData.TransactionCodeDataField = _TransactionCodeProm;
                        def.FormData.ReferenceNoDataField = _ReferenceNoProm;

                    }

                    def.clearScreenMessage();
                    // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                    def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                }
                catch (Exception ex)
                {

                    def.clearScreenMessage();
                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                }

            }
        }

        //protected void evt_Save(object sender, EventArgs arg)
        //{

        //    if (ASPxEdit.AreEditorsValid(FormPanelPromotion))
        //    {


        //        var def = this.Page as _30PvFormList.VoucherFormPage;
        //        DataFieldHeader dataFieldHeader = new DataFieldHeader();
        //        dataFieldHeader.CATEGORY_CODE = IsNullString(_CategoryCodeProm);
        //        dataFieldHeader.TRANSACTION_CD = IsNullInt(_TransactionCodeProm);
        //        dataFieldHeader.REFERENCE_NO = IsNullString(_ReferenceNoProm);
        //        dataFieldHeader.THN_PAJAK = IsNullInt(spnTahunPajak.Value);
        //        dataFieldHeader.TMP_PENANDATANGAN = IsNullString(txtTempatPenandatangan.Value);
        //        dataFieldHeader.TGL_PENANDATANGAN = IsNullDateTime(dtPenandaTangan.Value);
        //        dataFieldHeader.NAMA_PENANDATANGAN = IsNullString(txtNamaPenandatangan.Value);
        //        dataFieldHeader.JABATAN_PENANDATANGAN = IsNullString(txtJabatanpenandatanagn.Value);
        //        bool IsValid = def.formPersistence.UpdateHeaderPromotion(dataFieldHeader);
        //        if (IsValid == false)
        //        {
        //            def.clearScreenMessage();
        //            def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Data Field Harus di isi");
        //        }
        //        else
        //        {
        //            def.clearScreenMessage();
        //            def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");

        //            List<PromotionReport> _listData = new List<PromotionReport>();
        //            _listData = def.formPersistence.GetPromotionReport(IsNullString(_CategoryCodeProm), IsNullInt(_TransactionCodeProm), IsNullString(_ReferenceNoProm));

        //            List<PromotionHeaderTemplateReport> dataField = new List<PromotionHeaderTemplateReport>();
        //            dataField.Add(new PromotionHeaderTemplateReport
        //            {
        //                NO = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NO").FirstOrDefault(),
        //                NAMA = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA").FirstOrDefault(),
        //                NPWP = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NPWP").FirstOrDefault(),
        //                ALAMAT = def.formPersistence.getDataFieldName(_CategoryCodeProm, "ALAMAT").FirstOrDefault(),
        //                TANGGAL = def.formPersistence.getDataFieldName(_CategoryCodeProm, "TANGGAL").FirstOrDefault(),
        //                BENTUK_DAN_JENIS_BIAYA = def.formPersistence.getDataFieldName(_CategoryCodeProm, "BENTUK_DAN_JENIS_BIAYA").FirstOrDefault(),
        //                JUMLAH = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH").FirstOrDefault(),
        //                JUMLAH_GROSS_UP = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_GROSS_UP").FirstOrDefault(),
        //                KETERANGAN = def.formPersistence.getDataFieldName(_CategoryCodeProm, "KETERANGAN").FirstOrDefault(),
        //                JUMLAH_PPH = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_PPH").FirstOrDefault(),
        //                JENIS_PPH = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JENIS_PPH").FirstOrDefault(),
        //                JUMLAH_NET = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_NET").FirstOrDefault(),
        //                NOMOR_REKENING = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NOMOR_REKENING").FirstOrDefault(),
        //                NAMA_REKENING_PENERIMA = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA_REKENING_PENERIMA").FirstOrDefault(),
        //                NAMA_BANK = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA_BANK").FirstOrDefault(),
        //                NO_KTP = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NO_KTP").FirstOrDefault()
        //            });


        //            List<ImageData> imagedata = new List<ImageData>();
        //            string data = IsNullString(_CategoryCodeProm) + "-" + IsNullInt(_TransactionCodeProm) + "-" + IsNullString(_ReferenceNoProm);
        //            data = XmlDataPromotion().Replace(@"""", @"\""");

        //            imagedata.Add(new ImageData { QrCode = GenerateQrCode(data) });
        //            string pdfName = "Data_Promotion_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
        //            string pdfName = "File_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".pdf";
        //            string extension;
        //            string encoding;
        //            string mimeType;
        //            string[] streams;
        //            Warning[] warnings;

        //            LocalReport report = new LocalReport();



        //            report.ReportPath = Server.MapPath("~/Report/RDLC/DataFieldPromotion.rdlc");

        //            ReportDataSource rds1 = new ReportDataSource();
        //            rds1.Name = "DataSetPromotionHeader";
        //            rds1.Value = dataField;
        //            report.DataSources.Add(rds1);
        //            ReportDataSource rds2 = new ReportDataSource();
        //            rds2.Name = "DataSetPromotionDetail";
        //            rds2.Value = _listData;
        //            report.DataSources.Add(rds2);

        //            ReportDataSource rds3 = new ReportDataSource();
        //            rds3.Name = "DataSetQrCode";
        //            rds3.Value = imagedata;
        //            report.DataSources.Add(rds3);

        //            Byte[] mybytes = report.Render("PDF", null,
        //                            out extension, out encoding,
        //                            out mimeType, out streams, out warnings); //for exporting to PDF  
        //            using (FileStream fs = File.Create(Server.MapPath("~/Report/RDLC/") + pdfName))
        //            {
        //                fs.Write(mybytes, 0, mybytes.Length);
        //            }

        //            Response.ClearHeaders();
        //            Response.ClearContent();
        //            Response.Buffer = true;
        //            Response.Clear();
        //            Response.ContentType = "application/pdf";
        //            Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfName);
        //            Response.WriteFile(Server.MapPath("~/Report/RDLC/" + pdfName));
        //            Response.Flush();
        //            Response.Close();
        //            Response.End();
        //        }




        //    }
        //}
        
        //public string XmlDataPromotion()
        //{
        //    var def = this.Page as _30PvFormList.VoucherFormPage;
        //    List<Promotion> _listData = new List<Promotion>();
        //    _listData = def.formPersistence.GetPromotion(IsNullString(_CategoryCodeProm), IsNullInt(_TransactionCodeProm), IsNullString(_ReferenceNoProm));



        //    XmlSerializer serializer = new XmlSerializer(typeof(List<Promotion>));

        //    var stringwriter = new System.IO.StringWriter();
        //    serializer.Serialize(stringwriter, _listData);

        //    return stringwriter.ToString();

        //}

        protected void evt_add(object sender, EventArgs arg)
        {
            //ASPxGridViewPromotion.RowInserted += new ASPxDataInsertedEventHandler(grid_add);
            ASPxGridViewPromotion.AddNewRow();

        }

        protected void evt_Save2(object sender, EventArgs arg)
        {

            ASPxGridViewPromotion.RowInserting += new ASPxDataInsertingEventHandler(grid_add);
            var pageControl = ASPxGridViewPromotion.FindEditFormTemplateControl("TextBox1") as TextBox;



        }


        void grid_add(object sender, ASPxDataInsertingEventArgs e)
        {
            var grid = sender as ASPxGridView;
            var pageControl = grid.FindEditFormTemplateControl("TextBox1") as TextBox;
        }


        protected void ASPxGridViewPromotion_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {


            ASPxTextBox txtNama = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA"] as GridViewDataColumn, "txtNama") as ASPxTextBox;
            ASPxTextBox txtNPWP = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NPWP"] as GridViewDataColumn, "txtNPWP") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;
            //ASPxDateEdit dtTanggal = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            //ASPxTextBox txtBentukDanJenisBiaya = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            //ASPxSpinEdit spnJumlah = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            //ASPxSpinEdit spnJumlahGrossUp = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            //ASPxTextBox txtKeterangan = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            //ASPxSpinEdit spnJumlahPph = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            //ASPxTextBox txtJenisPph = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            //ASPxSpinEdit spnJumlahNet = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            //ASPxTextBox txtNoRekening = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            //ASPxTextBox txtNamaRekeningPenreima = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            //ASPxTextBox txtNamaBank = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            //ASPxTextBox txtNoKtp = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            Promotion promotion = new Promotion();
            promotion.CATEGORY_CODE = _CategoryCodeProm;
            promotion.TRANSACTION_CD = Convert.ToInt32(_TransactionCodeProm);
            promotion.REFERENCE_NO = _ReferenceNoProm;
            promotion.NO = 1;

            promotion.NAMA = IsNullString(txtNama.Value);
            promotion.NPWP = FormatNPWP(IsNullString(txtNPWP.Value));
            promotion.ALAMAT = IsNullString(txtAlamat.Value);
            //promotion.TANGGAL = IsNullDateTime(dtTanggal.Value);
            //promotion.BENTUK_DAN_JENIS_BIAYA = IsNullString(txtBentukDanJenisBiaya.Value);
            //promotion.JUMLAH = IsNullDecimal(spnJumlah.Value);
            //promotion.JUMLAH_GROSS_UP = IsNullDecimal(spnJumlahGrossUp.Value);
            //promotion.KETERANGAN = IsNullString(txtKeterangan.Value);
            //promotion.JUMLAH_PPH = IsNullDecimal(spnJumlahPph.Value);
            //promotion.JENIS_PPH = IsNullString(txtJenisPph.Value);
            //promotion.JUMLAH_NET = IsNullDecimal(spnJumlahNet.Value);
            //promotion.NOMOR_REKENING = IsNullString(txtNoRekening.Value);
            //promotion.NAMA_REKENING_PENERIMA = IsNullString(txtNamaRekeningPenreima.Value);
            //promotion.NAMA_BANK = IsNullString(txtNamaBank.Value);
            //promotion.NO_KTP = IsNullString(txtNoKtp.Value);
            promotion.CREATED_DT = DateTime.Now;
            promotion.CREATED_BY = IsNullString(_UserNameProm);
            //  e.Cancel = true;
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.formPersistence.AddPromotion(promotion);
            e.Cancel = true;
            ASPxGridViewPromotion.CancelEdit();

            List<Promotion> _listSave = new List<Promotion>();

            _listSave = def.formPersistence.GetPromotion(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);
            ASPxGridViewPromotion.DataSource = _listSave;
            ASPxGridViewPromotion.DataBind();

            //save selected languages to your data source

            //e.Cancel = true;
            //ASPxGridViewPromotion.CancelEdit();

            //PromotionList();
        }

        protected void ASPxGridViewPromotion_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            HiddenField hidSeqNo = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NO"] as GridViewDataColumn, "hidSeqNo") as HiddenField;
            ASPxTextBox txtNama = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA"] as GridViewDataColumn, "txtNama") as ASPxTextBox;
            ASPxTextBox txtNPWP = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NPWP"] as GridViewDataColumn, "txtNPWP") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;
           // ASPxDateEdit dtTanggal = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            //ASPxTextBox txtBentukDanJenisBiaya = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            //ASPxSpinEdit spnJumlah = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            //ASPxSpinEdit spnJumlahGrossUp = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            //ASPxTextBox txtKeterangan = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            //ASPxSpinEdit spnJumlahPph = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            //ASPxTextBox txtJenisPph = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            //ASPxSpinEdit spnJumlahNet = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            //ASPxTextBox txtNoRekening = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            //ASPxTextBox txtNamaRekeningPenreima = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            //ASPxTextBox txtNamaBank = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            //ASPxTextBox txtNoKtp = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            Promotion promotion = new Promotion();
            promotion.SEQ_NO = IsNullInt64(hidSeqNo.Value);



            promotion.NAMA = IsNullString(txtNama.Value);
            promotion.NPWP = FormatNPWP(IsNullString(txtNPWP.Value));
            promotion.ALAMAT = IsNullString(txtAlamat.Value);
            //promotion.TANGGAL = IsNullDateTime(dtTanggal.Value);
            //promotion.BENTUK_DAN_JENIS_BIAYA = IsNullString(txtBentukDanJenisBiaya.Value);
            //promotion.JUMLAH = IsNullDecimal(spnJumlah.Value);
            //promotion.JUMLAH_GROSS_UP = IsNullDecimal(spnJumlahGrossUp.Value);
            //promotion.KETERANGAN = IsNullString(txtKeterangan.Value);
            //promotion.JUMLAH_PPH = IsNullDecimal(spnJumlahPph.Value);
            //promotion.JENIS_PPH = IsNullString(txtJenisPph.Value);
            //promotion.JUMLAH_NET = IsNullDecimal(spnJumlahNet.Value);
            //promotion.NOMOR_REKENING = IsNullString(txtNoRekening.Value);
            //promotion.NAMA_REKENING_PENERIMA = IsNullString(txtNamaRekeningPenreima.Value);
            //promotion.NAMA_BANK = IsNullString(txtNamaBank.Value);
            //promotion.NO_KTP = IsNullString(txtNoKtp.Value);
            promotion.CREATED_DT = DateTime.Now;
            promotion.CREATED_BY = IsNullString(_UserNameProm);
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.formPersistence.EditPromotion(promotion);
            e.Cancel = true;
            ASPxGridViewPromotion.CancelEdit();

            List<Promotion> _listEdit = new List<Promotion>();

            _listEdit = def.formPersistence.GetPromotion(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);
            ASPxGridViewPromotion.DataSource = _listEdit;
            ASPxGridViewPromotion.DataBind();

        }

        protected void ASPxGridViewPromotion_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var keydata = IsNullInt64(e.Keys["SEQ_NO"]);
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.formPersistence.DeletePromotion(keydata);
            e.Cancel = true;


            List<Promotion> _listDelete = new List<Promotion>();

            _listDelete = def.formPersistence.GetPromotion(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);
            ASPxGridViewPromotion.DataSource = _listDelete;
            ASPxGridViewPromotion.DataBind();
        }

        protected void ASPxGridViewPromotion_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
           
            string msgValidation = "";
            int countValidation = 0;
            var def = this.Page as _30PvFormList.VoucherFormPage;
            ASPxTextBox txtNama = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA"] as GridViewDataColumn, "txtNama") as ASPxTextBox;
            ASPxTextBox txtNPWP = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NPWP"] as GridViewDataColumn, "txtNPWP") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;
            //ASPxDateEdit dtTanggal = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            //ASPxTextBox txtBentukDanJenisBiaya = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            //ASPxSpinEdit spnJumlah = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            //ASPxSpinEdit spnJumlahGrossUp = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            //ASPxTextBox txtKeterangan = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            //ASPxSpinEdit spnJumlahPph = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            //ASPxTextBox txtJenisPph = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            //ASPxSpinEdit spnJumlahNet = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            //ASPxTextBox txtNoRekening = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            //ASPxTextBox txtNamaRekeningPenreima = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            //ASPxTextBox txtNamaBank = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            //ASPxTextBox txtNoKtp = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            bool IS_NAMA = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NAMA", Convert.ToInt32(_VendorGroup));
            bool IS_NPWP = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NPWP", Convert.ToInt32(_VendorGroup));
            bool IS_ALAMAT = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "ALAMAT", Convert.ToInt32(_VendorGroup));
            //bool IS_TANGGAL = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "TANGGAL", Convert.ToInt32(_VendorGroup));
            //bool IS_BENTUK_DAN_JENIS_BIAYA = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "BENTUK_DAN_JENIS_BIAYA", Convert.ToInt32(_VendorGroup));
            //bool IS_JUMLAH = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH", Convert.ToInt32(_VendorGroup));
            //bool IS_JUMLAH_GROSS_UP = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH_GROSS_UP", Convert.ToInt32(_VendorGroup));
            //bool IS_KETERANGAN = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "KETERANGAN", Convert.ToInt32(_VendorGroup));
            //bool IS_JUMLAH_PPH = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH_PPH", Convert.ToInt32(_VendorGroup));
            //bool IS_JENIS_PPH = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JENIS_PPH", Convert.ToInt32(_VendorGroup));
            //bool IS_JUMLAH_NET = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH_NET", Convert.ToInt32(_VendorGroup));
            //bool IS_NOMOR_REKENING = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NOMOR_REKENING", Convert.ToInt32(_VendorGroup));
            //bool IS_NAMA_REKENING_PENERIMA = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NAMA_REKENING_PENERIMA", Convert.ToInt32(_VendorGroup));
            //bool IS_NAMA_BANK = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NAMA_BANK", Convert.ToInt32(_VendorGroup));
            //bool IS_NO_KTP = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NO_KTP", Convert.ToInt32(_VendorGroup));
            if ((IsNullString(txtNama.Value) == "") && (IS_NAMA == true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA").FirstOrDefault();
                txtNama.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if ((IsNullString(txtNPWP.Value) == "") && (IS_NPWP = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NPWP").FirstOrDefault();
                txtNPWP.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtAlamat.Value) == "") && (IS_ALAMAT = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "ALAMAT").FirstOrDefault();
                txtAlamat.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            //if ((IsNullDateTime(dtTanggal.Value) == Convert.ToDateTime("1900-01-01")) && (IS_TANGGAL = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "TANGGAL").FirstOrDefault();
            //    dtTanggal.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtBentukDanJenisBiaya.Value) == "") && (IS_BENTUK_DAN_JENIS_BIAYA = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "BENTUK_DAN_JENIS_BIAYA").FirstOrDefault();
            //    txtBentukDanJenisBiaya.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullDecimal(spnJumlah.Value) == 0) && (IS_JUMLAH = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH").FirstOrDefault();
            //    spnJumlah.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullDecimal(spnJumlahGrossUp.Value) == 0) && (IS_JUMLAH_GROSS_UP = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_GROSS_UP").FirstOrDefault();
            //    spnJumlahGrossUp.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtKeterangan.Value) == "") && (IS_KETERANGAN = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "KETERANGAN").FirstOrDefault();
            //    txtKeterangan.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullDecimal(spnJumlahPph.Value) == 0) && (IS_JUMLAH_PPH = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_PPH").FirstOrDefault();
            //    spnJumlahPph.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtJenisPph.Value) == "") && (IS_JENIS_PPH = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JENIS_PPH").FirstOrDefault();
            //    txtJenisPph.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullDecimal(spnJumlahNet.Value) == 0) && (IS_JUMLAH_NET = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_NET").FirstOrDefault();
            //    spnJumlahNet.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtNoRekening.Value) == "") && (IS_NOMOR_REKENING = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NOMOR_REKENING").FirstOrDefault();
            //    txtNoRekening.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtNamaRekeningPenreima.Value) == "") && (IS_NAMA_REKENING_PENERIMA = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA_REKENING_PENERIMA").FirstOrDefault();
            //    txtNamaRekeningPenreima.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtNamaBank.Value) == "") && (IS_NAMA_BANK = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA_BANK").FirstOrDefault();
            //    txtNamaBank.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}
            //if ((IsNullString(txtNoKtp.Value) == "") && (IS_NO_KTP = true))
            //{
            //    string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NO_KTP").FirstOrDefault();
            //    txtNoKtp.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}

            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }


        protected void ASPxGridViewPromotion_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<Promotion> _listSeqNo = new List<Promotion>();
            _listSeqNo = def.formPersistence.GetPromotion(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);

            e.NewValues["NO"] = _listSeqNo.Count() + 1;
            e.NewValues["SEQ_NO"] = _listSeqNo.Count() + 1;

            EnomVendor _Vendor = new EnomVendor();
            _Vendor = def.formPersistence.GetEnomVendor(_VendorCode).FirstOrDefault();
            e.NewValues["NAMA"] = _Vendor.VENDOR_NAME;
            e.NewValues["NPWP"] = _Vendor.NPWP;
            //e.NewValues["TANGGAL"] = DateTime.Now;
            //e.NewValues["JUMLAH"] = 0;
            //e.NewValues["JUMLAH_GROSS_UP"] = 0;
            //e.NewValues["JUMLAH_PPH"] = 0;
            //e.NewValues["JUMLAH_NET"] = 0;

        }

        protected void ASPxGridViewPromotion_ParseValue(object sender, DevExpress.Web.Data.ASPxParseValueEventArgs ev)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<Promotion> _listSeqNo = new List<Promotion>();

            _listSeqNo = def.formPersistence.GetPromotion(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);

            if (ev.FieldName == "NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("NO number");
                }
            if (ev.FieldName == "NAMA")
                try
                {
                    ev.Value = Convert.ToString(ev.Value);
                }
                catch (Exception)
                {

                    throw new Exception("NO number");
                }
            if (ev.FieldName == "SEQ_NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("SEQ_NO number");
                }
           
        }



        protected void ASPxGridViewPromotion_Init(object sender, EventArgs e)
        {
            if (IsNullString(_CategoryCodeProm) != "")
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;


                foreach (GridViewColumn row in ASPxGridViewPromotion.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            ASPxGridViewPromotion.Columns[namecolums].Caption = _caption;
                        }


                    }


                }
            }

        }

        public string IsNullString(object data)
        {
            string returndata = "";
            if (data == null)
            {
                returndata = "";
            }
            else
            {
                returndata = data.ToString();
            }
            return returndata;
        }

        public decimal IsNullDecimal(object data)
        {
            decimal returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToDecimal(data);
            }
            return returndata;
        }

        public Int32 IsNullInt(object data)
        {
            Int32 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt32(data);
            }
            return returndata;
        }

        public Int64 IsNullInt64(object data)
        {
            Int64 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt64(data);
            }
            return returndata;
        }

        public DateTime IsNullDateTime(object data)
        {
            DateTime returndata = Convert.ToDateTime("1900-01-01");
            if (data == null)
            {
                returndata = Convert.ToDateTime("1900-01-01");
            }
            else
            {
                returndata = Convert.ToDateTime(data);
            }
            return returndata;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string FormatNPWP(string value)
        {

            string a = value.Substring(0, 2);
            string b = value.Substring(2, 3);
            string c = value.Substring(5, 3);
            string d = value.Substring(8, 1);
            string e = value.Substring(9, 3);
            string f = value.Substring(12, 3);

            return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
        }

        protected void TahunPajak_Validation(object sender, ValidationEventArgs e)
        {

            if (e.Value.ToString().Length != 4)

                e.IsValid = false;
        }


        #region Upload
        protected void btnUploadTemplatePromotion_Click(object sender, EventArgs e)
        {
            
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.logic.Say("BeforeUploadPromotion_Click", "Upload : {0}", fuDataFieldPromotionList.FileName);
                def.clearScreenMessage();
                try
                {


                    int _processId = 0;
                    CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                    string _strFileType = Path.GetExtension(fuDataFieldPromotionList.FileName).ToString().ToLower();
                    string _strFileName = fuDataFieldPromotionList.FileName;

                    if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                    {
                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;
                    }





                    string _strFunctionId = "";
                    _strFunctionId = resx.FunctionId("FCN_" + def.ScreenType + "_FORM_UPLOAD");
                    _processId = def.DoStartLog(_strFunctionId, "");

                    string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                    string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                    LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                    fuDataFieldPromotionList.SaveAs(xlsFile);
               
                string[][] listOfErrors = new string[1][];
               
                bool Ok = def.formPersistence.UploadFileTemplatePromotion(
                        _CategoryCodeProm,
                        Convert.ToInt32(_TransactionCodeProm),
                        _ReferenceNoProm,
                        _VendorGroup,
                        DateTime.Now,
                        IsNullString(_UserNameProm),
                        xlsFile);
                    if (!Ok)
                    {
                        string sourceFile = Server.MapPath(
                                                String.Format(
                                                        def.logic.Sys.GetText("TEMPLATE.FMT", "ERR"), "TemplateDataFieldPromotion.xls"
                                                    )
                                                );
                        string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                        string fileName = def.formPersistence.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                        String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                                Request.Url.Scheme,
                                                                Request.ServerVariables["SERVER_NAME"],
                                                                Request.ServerVariables["SERVER_PORT"],
                                                                Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                                fileName,
                                                                fileName);

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else
                    {
                        List<Promotion> _listUpload = new List<Promotion>();

                        _listUpload = def.formPersistence.GetPromotion(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);
                        ASPxGridViewPromotion.DataSource = _listUpload;
                        ASPxGridViewPromotion.DataBind();
                        def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                        
                }






                }
                catch (Exception ex)
                {
                    def.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    LoggingLogic.err(ex);
                }
            fuDataFieldPromotionList.Dispose();

        }

        #endregion Upload

        //protected void gvMaster_DetailRowGetButtonVisibility(object sender, ASPxGridViewDetailRowButtonEventArgs e)
        //{
        //    User currentUser = Users.Find(u => u.ID == (int)gvMaster.GetRowValues(e.VisibleIndex, "ID"));

        //    if (!currentUser.HasProjects())
        //        e.ButtonState = GridViewDetailRowButtonState.Hidden;
        //}
        protected void gvDetail_Init(object sender, EventArgs e)
        {   
            ASPxGridView grid = sender as ASPxGridView;
            //var container = grid.NamingContainer as GridViewDataItemTemplateContainer;
            //var index = container.VisibleIndex;
           
            var def = this.Page as _30PvFormList.VoucherFormPage;
            if (IsNullString(_CategoryCodeProm) != "")
            {

              


                foreach (GridViewColumn row in grid.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            grid.Columns[namecolums].Caption = _caption;
                        }


                    }


                }
            }

                Int64 seqNo = (Int64)grid.GetMasterRowKeyValue();
          
            List<PromotionDetail> _listDetail = new List<PromotionDetail>();
           
            _listDetail = def.formPersistence.GetPromotionDetail(seqNo);

            grid.DataSource = _listDetail.ToList();
         
        }

        protected void gvDetail_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            ASPxGridView gridDetail = sender as ASPxGridView;

            ASPxDateEdit dtTanggal = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            ASPxTextBox txtBentukDanJenisBiaya = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            ASPxSpinEdit spnJumlah = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            ASPxSpinEdit spnJumlahGrossUp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            ASPxTextBox txtKeterangan = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            ASPxSpinEdit spnJumlahPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            ASPxTextBox txtJenisPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            ASPxSpinEdit spnJumlahNet = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            ASPxTextBox txtNoRekening = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            ASPxTextBox txtNamaRekeningPenreima = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            ASPxTextBox txtNamaBank = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            ASPxTextBox txtNoKtp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            PromotionDetail promotionDetail = new PromotionDetail();
            Int64 seqNo = (Int64)gridDetail.GetMasterRowKeyValue();

            promotionDetail.SEQ_NO = seqNo;
            promotionDetail.TANGGAL = IsNullDateTime(dtTanggal.Value);
            promotionDetail.BENTUK_DAN_JENIS_BIAYA = IsNullString(txtBentukDanJenisBiaya.Value);
            promotionDetail.JUMLAH = IsNullDecimal(spnJumlah.Value);
            promotionDetail.JUMLAH_GROSS_UP = IsNullDecimal(spnJumlahGrossUp.Value);
            promotionDetail.KETERANGAN = IsNullString(txtKeterangan.Value);
            promotionDetail.JUMLAH_PPH = IsNullDecimal(spnJumlahPph.Value);
            promotionDetail.JENIS_PPH = IsNullString(txtJenisPph.Value);
            promotionDetail.JUMLAH_NET = IsNullDecimal(spnJumlahNet.Value);
            promotionDetail.NOMOR_REKENING = IsNullString(txtNoRekening.Value);
            promotionDetail.NAMA_REKENING_PENERIMA = IsNullString(txtNamaRekeningPenreima.Value);
            promotionDetail.NAMA_BANK = IsNullString(txtNamaBank.Value);
            promotionDetail.NO_KTP = IsNullString(txtNoKtp.Value);
            promotionDetail.CREATED_DT = DateTime.Now;
            promotionDetail.CREATED_BY = IsNullString(_UserNameProm);
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.formPersistence.AddPromotionDetail(promotionDetail);
            e.Cancel = true;
            gridDetail.CancelEdit();

            List<PromotionDetail> _listSaveDetail = new List<PromotionDetail>();

            _listSaveDetail = def.formPersistence.GetPromotionDetail(seqNo);
            gridDetail.DataSource = _listSaveDetail;
            gridDetail.DataBind();
        }

        protected void gvDetail_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridDetail = sender as ASPxGridView;
           
            ASPxDateEdit dtTanggal = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            ASPxTextBox txtBentukDanJenisBiaya = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            ASPxSpinEdit spnJumlah = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            ASPxSpinEdit spnJumlahGrossUp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            ASPxTextBox txtKeterangan = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            ASPxSpinEdit spnJumlahPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            ASPxTextBox txtJenisPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            ASPxSpinEdit spnJumlahNet = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            ASPxTextBox txtNoRekening = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            ASPxTextBox txtNamaRekeningPenreima = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            ASPxTextBox txtNamaBank = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            ASPxTextBox txtNoKtp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            PromotionDetail promotionDetail = new PromotionDetail();
            promotionDetail.TANGGAL = IsNullDateTime(dtTanggal.Value);
            promotionDetail.BENTUK_DAN_JENIS_BIAYA = IsNullString(txtBentukDanJenisBiaya.Value);
            promotionDetail.JUMLAH = IsNullDecimal(spnJumlah.Value);
            promotionDetail.JUMLAH_GROSS_UP = IsNullDecimal(spnJumlahGrossUp.Value);
            promotionDetail.KETERANGAN = IsNullString(txtKeterangan.Value);
            promotionDetail.JUMLAH_PPH = IsNullDecimal(spnJumlahPph.Value);
            promotionDetail.JENIS_PPH = IsNullString(txtJenisPph.Value);
            promotionDetail.JUMLAH_NET = IsNullDecimal(spnJumlahNet.Value);
            promotionDetail.NOMOR_REKENING = IsNullString(txtNoRekening.Value);
            promotionDetail.NAMA_REKENING_PENERIMA = IsNullString(txtNamaRekeningPenreima.Value);
            promotionDetail.NAMA_BANK = IsNullString(txtNamaBank.Value);
            promotionDetail.NO_KTP = IsNullString(txtNoKtp.Value);
            promotionDetail.CREATED_DT = DateTime.Now;
            promotionDetail.CREATED_BY = IsNullString(_UserNameProm);
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.formPersistence.EditPromotionDetail(promotionDetail);
            e.Cancel = true;
            gridDetail.CancelEdit();
            Int64 seqNo = (Int64)gridDetail.GetMasterRowKeyValue();
            List<PromotionDetail> _listDetailEdit = new List<PromotionDetail>();

            _listDetailEdit = def.formPersistence.GetPromotionDetail(seqNo);
            gridDetail.DataSource = _listDetailEdit;
            gridDetail.DataBind();

        }

        protected void gvDetail_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gridDetail = sender as ASPxGridView;
            Int64 seqNo = (Int64)gridDetail.GetMasterRowKeyValue();
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.formPersistence.DeletePromotionDetail(seqNo);
            e.Cancel = true;
            List<PromotionDetail> _listDetailDelete = new List<PromotionDetail>();
            _listDetailDelete = def.formPersistence.GetPromotionDetail(seqNo);
            gridDetail.DataSource = _listDetailDelete;
            gridDetail.DataBind();
        }

        protected void gvDetail_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridDetail = sender as ASPxGridView;
            string msgValidation = "";
            int countValidation = 0;
            var def = this.Page as _30PvFormList.VoucherFormPage;

            ASPxDateEdit dtTanggal = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            ASPxTextBox txtBentukDanJenisBiaya = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            ASPxSpinEdit spnJumlah = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            ASPxSpinEdit spnJumlahGrossUp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            ASPxTextBox txtKeterangan = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            ASPxSpinEdit spnJumlahPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            ASPxTextBox txtJenisPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            ASPxSpinEdit spnJumlahNet = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            ASPxTextBox txtNoRekening = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            ASPxTextBox txtNamaRekeningPenreima = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            ASPxTextBox txtNamaBank = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            ASPxTextBox txtNoKtp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;

            bool IS_TANGGAL = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "TANGGAL", Convert.ToInt32(_VendorGroup));
            bool IS_BENTUK_DAN_JENIS_BIAYA = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "BENTUK_DAN_JENIS_BIAYA", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH_GROSS_UP = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH_GROSS_UP", Convert.ToInt32(_VendorGroup));
            bool IS_KETERANGAN = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "KETERANGAN", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH_PPH = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH_PPH", Convert.ToInt32(_VendorGroup));
            bool IS_JENIS_PPH = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JENIS_PPH", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH_NET = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "JUMLAH_NET", Convert.ToInt32(_VendorGroup));
            bool IS_NOMOR_REKENING = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NOMOR_REKENING", Convert.ToInt32(_VendorGroup));
            bool IS_NAMA_REKENING_PENERIMA = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NAMA_REKENING_PENERIMA", Convert.ToInt32(_VendorGroup));
            bool IS_NAMA_BANK = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NAMA_BANK", Convert.ToInt32(_VendorGroup));
            bool IS_NO_KTP = def.formPersistence.IsValidationVendorGroup(_CategoryCodeProm, "NO_KTP", Convert.ToInt32(_VendorGroup));

            if ((IsNullDateTime(dtTanggal.Value) == Convert.ToDateTime("1900-01-01")) && (IS_TANGGAL = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "TANGGAL").FirstOrDefault();
                dtTanggal.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtBentukDanJenisBiaya.Value) == "") && (IS_BENTUK_DAN_JENIS_BIAYA = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "BENTUK_DAN_JENIS_BIAYA").FirstOrDefault();
                txtBentukDanJenisBiaya.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullDecimal(spnJumlah.Value) == 0) && (IS_JUMLAH = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH").FirstOrDefault();
                spnJumlah.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullDecimal(spnJumlahGrossUp.Value) == 0) && (IS_JUMLAH_GROSS_UP = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_GROSS_UP").FirstOrDefault();
                spnJumlahGrossUp.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtKeterangan.Value) == "") && (IS_KETERANGAN = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "KETERANGAN").FirstOrDefault();
                txtKeterangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullDecimal(spnJumlahPph.Value) == 0) && (IS_JUMLAH_PPH = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_PPH").FirstOrDefault();
                spnJumlahPph.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtJenisPph.Value) == "") && (IS_JENIS_PPH = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JENIS_PPH").FirstOrDefault();
                txtJenisPph.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullDecimal(spnJumlahNet.Value) == 0) && (IS_JUMLAH_NET = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "JUMLAH_NET").FirstOrDefault();
                spnJumlahNet.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNoRekening.Value) == "") && (IS_NOMOR_REKENING = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NOMOR_REKENING").FirstOrDefault();
                txtNoRekening.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNamaRekeningPenreima.Value) == "") && (IS_NAMA_REKENING_PENERIMA = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA_REKENING_PENERIMA").FirstOrDefault();
                txtNamaRekeningPenreima.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNamaBank.Value) == "") && (IS_NAMA_BANK = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NAMA_BANK").FirstOrDefault();
                txtNamaBank.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNoKtp.Value) == "") && (IS_NO_KTP = true))
            {
                string _caption = def.formPersistence.getDataFieldName(_CategoryCodeProm, "NO_KTP").FirstOrDefault();
                txtNoKtp.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }

        protected void gvDetail_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {

            e.NewValues["TANGGAL"] = DateTime.Now;
            e.NewValues["JUMLAH"] = 0;
            e.NewValues["JUMLAH_GROSS_UP"] = 0;
            e.NewValues["JUMLAH_PPH"] = 0;
            e.NewValues["JUMLAH_NET"] = 0;

        }

      

            

        }
}
