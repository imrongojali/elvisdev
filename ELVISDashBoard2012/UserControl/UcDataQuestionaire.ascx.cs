﻿using BusinessLogic.CommonLogic;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxEditors;
using ELVISDashBoard.presentationHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashBoard.UserControl
{
    public partial class UcDataQuestionaire :System.Web.UI.UserControl
    {

        protected readonly MessageLogic _m = new MessageLogic();
        protected string ScreenType { set; get; }
        public string msgtype { get; set; }
        
       
        protected void Page_Load(object sender, EventArgs e)
        {
            msgtype = "0";
            //if (!IsPostBack)
            //{

                clearScreenMessage();
            HiddenField hdExpandFlag = this.Parent.FindControl("hdExpandFlag") as HiddenField;
            hdExpandFlag.Value = "true";
                
            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron test");
            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "imron gojali");
             //   if (msgtype=="1")
             //   {
             //       PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron sukses");
             //   }
             //}
            //PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron test");
        }

        protected void ASPxCallbackPanelDemo_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            ASPxCallbackPanel callbackPanel = (ASPxCallbackPanel)sender;
            bool isValid = ASPxEdit.ValidateEditorsInContainer(callbackPanel);
            if (isValid)
            {
                msgtype = "1";
               
                //Load += new EventHandler(Page_Load);
                
            }
            else
            {
                Literal messageControl = this.Parent.FindControl("messageControl") as Literal;
                messageControl.Visible = false;
               // Button2.Click += new System.EventHandler(this.evt_DataQuestionaire); 
               // Button2.Click += new EventHandler(Button2);
            }
        }

       

        protected void evt_DataQuestionaire(object sender, EventArgs arg)
        {
            var x1 = txtNum1.Value;
            var x2 = txtNum1.Value;
            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron sukses");
        }
        protected void ASPxTextBoxTest_Validation(object sender, ValidationEventArgs e)
        {
            ASPxTextBox txt = sender as ASPxTextBox;
            int val;
            bool result = Int32.TryParse(txt.Text, out val);
            e.IsValid = result;
            e.ErrorText = "An input value should be a number";
            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron sukses");
        }

        protected void ASPxTextBoxTest_ValidationTes2(object sender, ValidationEventArgs e)
        {
            ASPxTextBox txt = sender as ASPxTextBox;
            int val;
            bool result = Int32.TryParse(txt.Text, out val);
            e.IsValid = result;
            e.ErrorText = "An input value should be a number 2";
            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron sukses");
        }

        public void updateScreenMessages()
        {
            HiddenField hdExpandFlag = this.Parent.FindControl("hdExpandFlag") as HiddenField;
            Literal messageControl = this.Parent.FindControl("messageControl") as Literal;
           
            
            List<ScreenMessage> lstMessages = ScreenMessageList;
            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                ScreenMessage msg = lstMessages[0];
                StringBuilder stringBuilder = new StringBuilder();
                string expandFlag = hdExpandFlag.Value;

                string messageBoxClass = "message-info";
                string messageBoxSpanClass = "message-span-info";
                if (msg.Status == ScreenMessage.STATUS_WARNING)
                {
                    messageBoxClass = "message-warning";
                    messageBoxSpanClass = "message-span-warning";
                }
                else if (msg.Status == ScreenMessage.STATUS_ERROR)
                {
                    messageBoxClass = "message-error";
                    messageBoxSpanClass = "message-span-error";
                }

                if (expandFlag.Equals("true"))
                {
                    messageBoxClass += " expanded-message-box";
                }
                stringBuilder.AppendLine(string.Format("<div id='messageBox' class='{0}'>", messageBoxClass));
                stringBuilder.AppendLine(string.Format("      <span class='{0}'>{1}</span>", messageBoxSpanClass, msg.Message));

                if (cntMessages > 1)
                {
                    string txtMore = "more";
                    if (expandFlag.Equals("true"))
                    {
                        txtMore = "close";
                    }
                    stringBuilder.AppendLine("      <span class='message-span-more'>");
                    stringBuilder.AppendLine(string.Format("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore));
                    stringBuilder.AppendLine("      </span>");
                }

                stringBuilder.AppendLine("</div>");
                if (expandFlag.Equals("false"))
                {
                    stringBuilder.AppendLine("<div id='messageBox-all' style='display: none'>");
                }
                else
                {
                    stringBuilder.AppendLine("<div id='messageBox-all'>");
                }
                stringBuilder.AppendLine("  <ul>");
                string listClass;
                for (int i = 1; i < cntMessages; i++)
                {
                    msg = lstMessages[i];
                    listClass = "message-detail-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        listClass = "message-detail-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        listClass = "message-detail-error";
                    }

                    stringBuilder.AppendLine(string.Format("<li class='{0}'>", listClass));
                    stringBuilder.AppendLine("      <span>" + msg.Message + "</span>");
                    stringBuilder.AppendLine("</li>");
                }
                stringBuilder.AppendLine("  </ul>");
                stringBuilder.AppendLine("</div>");

                messageControl.Text = stringBuilder.ToString();
                messageControl.Visible = true;
            }
            else
            {
                messageControl.Text = "";
                messageControl.Visible = false;
            }
        }

        protected List<ScreenMessage> ScreenMessageList
        {
           
            get
            {
                List<ScreenMessage> lstMessages = Session[ScreenType + "MsgList"] as List<ScreenMessage>;
                if (lstMessages == null)
                {
                    HiddenField hdExpandFlag = this.Parent.FindControl("hdExpandFlag") as HiddenField;
                    lstMessages = new List<ScreenMessage>();
                    Session[ScreenType + "MsgList"] = lstMessages;
                    hdExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }
        private void PostLater(byte ScreenMessageStatus, string id, params object[] x)
        {
            PostMsg(false, false, ScreenMessageStatus, id, x);
        }
        private void PostMsg(bool instant, bool clearfirst, byte sts, string id, params object[] x)
        {
            string m = "";
            StringBuilder b = new StringBuilder("");
            if (x != null && x.Length > 0)
            {
                for (int k = 0; k < x.Length; k++)
                    b.Append(x[k].str() + ", ");
            }

            if (!id.isEmpty())
            {
                m = _m.Message(id);
            }
            if (x != null && x.Length > 0)
            {
                if (!m.isEmpty())
                    m = string.Format(m, x);
                else
                {
                    b.Clear();
                    for (int i = 0; i < x.Length; i++)
                    {
                        b.Append(x[i].str());
                        b.Append(" ");
                    }
                    m = b.ToString().Trim();
                }
            }
            postScreenMessage(
                new ScreenMessage[]
                {
                    new ScreenMessage(sts, m)
                }
                , clearfirst
                , instant);
        }

        protected void postScreenMessage()
        {
            postScreenMessage(null, false, true);
        }

        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = ScreenMessageList;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    lstMessage.Insert(0, messages[i]);
                }
            }
            updateScreenMessages();
        }
        protected void clearScreenMessage()
        {
            ScreenMessageList.Clear();
            updateScreenMessages();
        }

    }
}