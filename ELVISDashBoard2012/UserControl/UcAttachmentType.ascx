﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAttachmentType.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcAttachmentType" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>

<asp:UpdatePanel runat="server" ID="pnupdateAttachmentGridEnom">
    <ContentTemplate>
        <dx:ASPxGridView ID="gridAttachmentEnom" KeyFieldName="RowNumber"  runat="server"  ClientInstanceName="gridAttachmentEnom" Width="300px" AutoGenerateColumns="False">
            <Columns>
              <%--<dx:GridViewCommandColumn FixedStyle="Left" VisibleIndex="1">
                                <HeaderTemplate>
                                    <dx:ASPxButton ID="AddButton" runat="server" Text="Add"  RenderMode="Link" AutoPostBack="false" />
                                </HeaderTemplate>
                            </dx:GridViewCommandColumn>--%>
                <dx:GridViewDataColumn FieldName="#" Width="30" FixedStyle="Left" VisibleIndex="1">
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <DataItemTemplate>
                        
                        <common:KeyImageButton runat="server" ID="imgAddAttachment" Key='<%# Bind("RowNumber") %>'
                            CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png" ></common:KeyImageButton>
                        <common:KeyImageButton runat="server" ID="imgDeleteAttachment" Key='<%# Bind("RowNumber") %>'
                            CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/red-cross.png"  />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="RowNumber" Width="30" FixedStyle="Left" Caption="No" VisibleIndex="2" />
                <dx:GridViewDataColumn FieldName="Description" Width="200" VisibleIndex="3" />
                <dx:GridViewDataColumn FieldName="FileName" Width="210" VisibleIndex="4" />
                <dx:GridViewDataColumn FieldName="FileType" Width="30" VisibleIndex="5" />

            </Columns>
            <SettingsPager Visible="False">
            </SettingsPager>
            <Settings HorizontalScrollBarMode="Visible" />
            <SettingsBehavior AutoExpandAllGroups="true" />
            <Templates>
                <EditForm>
                     <div style="padding: 4px 3px 4px">
                    <dx:ASPxPageControl runat="server" ID="pageControl" Width="100%">
                        <TabPages>
                            <dx:TabPage Text="Info" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl runat="server">
                                        <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                            runat="server">
                                        </dx:ASPxGridViewTemplateReplacement>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Text="Notes" Visible="true">
                                <ContentCollection>
                                    <dx:ContentControl runat="server">
                                        <dx:ASPxMemo runat="server" ID="notesEditor" Text='<%# Eval("Notes")%>' Width="100%" Height="93px" />
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                        </TabPages>
                    </dx:ASPxPageControl>
                </div>
                <div style="text-align: right; padding: 2px">
                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                        runat="server">
                    </dx:ASPxGridViewTemplateReplacement>
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                        runat="server">
                    </dx:ASPxGridViewTemplateReplacement>
                </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
      
    
    </ContentTemplate>
</asp:UpdatePanel>
