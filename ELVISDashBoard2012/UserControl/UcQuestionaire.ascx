﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcQuestionaire.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcQuestionaire" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
   <script type="text/javascript">
 function OnCheckedYesChanged(s, e, index) {


            var nextIndex = index + 1;
            if (s.GetChecked() == true) {

                ASPxClientControl.GetControlCollection().GetByName("boxNo" + index).SetChecked(false);
                try {
                    ASPxClientControl.GetControlCollection().GetByName("boxNo" + nextIndex).SetEnabled(true);
                    ASPxClientControl.GetControlCollection().GetByName("boxYes" + nextIndex).SetEnabled(true);
                }
                catch {

                }
            }
            else {
                ASPxClientControl.GetControlCollection().GetByName("boxYes" + index).SetChecked(true);
            }



            // PerformCallback('next');
            //var maxIndex = ASPxGridViewQuestion.GetTopVisibleIndex() + ASPxGridViewQuestion.GetVisibleRowsOnPage();
            //hiddenField.Set(nextIndex, [s.GetChecked(), false]);
            //hiddenField.Set(index, [true, s.GetChecked()]);
            //if (nextIndex <= maxIndex)
            //ASPxClientControl.GetControlCollection().GetByName("boxNo" + index).SetEnabled(s.GetChecked());
            //    ASPxClientControl.GetControlCollection().GetByName("boxYes0").SetChecked(false);
            //    ASPxClientControl.GetControlCollection().GetByName("boxYes0").SetEnabled(false);
        }

        function OnCheckedNoChanged(s, e, index) {

            var nextIndex = index + 1;
            if (s.GetChecked() == true) {
                ASPxClientControl.GetControlCollection().GetByName("boxYes" + index).SetChecked(false);
                try {
                    ASPxClientControl.GetControlCollection().GetByName("boxNo" + nextIndex).SetEnabled(true);
                    ASPxClientControl.GetControlCollection().GetByName("boxYes" + nextIndex).SetEnabled(true);
                }
                catch {

                }
            }
            else {
                ASPxClientControl.GetControlCollection().GetByName("boxNo" + index).SetChecked(true);
            }
            // PerformCallback('next');
            //var maxIndex = ASPxGridViewQuestion.GetTopVisibleIndex() + ASPxGridViewQuestion.GetVisibleRowsOnPage();
            //hiddenField.Set(nextIndex, [s.GetChecked(), false]);
            //hiddenField.Set(index, [true, s.GetChecked()]);
            //if (nextIndex <= maxIndex)
            //ASPxClientControl.GetControlCollection().GetByName("boxNo" + index).SetEnabled(s.GetChecked());
            //ASPxClientControl.GetControlCollection().GetByName("boxYes0").SetChecked(false);
            //ASPxClientControl.GetControlCollection().GetByName("boxYes0").SetEnabled(false);
       }
   </script>
 <asp:UpdatePanel runat="server" ID="UpdatePanelQuestion">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnClose" />
            
            <asp:PostBackTrigger ControlID="btnBack" />
        </Triggers>
        <ContentTemplate>
            <br />

             <asp:HiddenField runat="server" ID="hidCategoryCode" />
             <asp:HiddenField runat="server" ID="hidTransactionCode" />
            <asp:HiddenField runat="server" ID="hidReferenceNo" />
             <asp:HiddenField runat="server" ID="hidUserName" />
            <asp:HiddenField runat="server" ID="hidDIV_CD" />
             <asp:HiddenField runat="server" ID="hidPageSelectedCategory" />
    <asp:HiddenField runat="server" ID="hidCountSelectedCategory"  />
            <asp:HiddenField runat="server" ID="hidVendorCode"  />
            <asp:HiddenField runat="server" ID="hidVendorGroup"  />
           
            <dx:ASPxGridView runat="server" ID="ASPxGridViewQuestion" Width="100%" ClientInstanceName="ASPxGridViewQuestion"
                ClientIDMode="Static" KeyFieldName="SequenceNumber" EnableCallBacks="false" AutoGenerateColumns="false"
                SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false"
                SettingsBehavior-AllowSelectSingleRowOnly="false"  SettingsBehavior-AllowFocusedRow="false"
                SettingsBehavior-AllowDragDrop="False">
                <%-- <ClientSideEvents EndCallback="OnEndCallback"/>--%>
                <Styles>
                    <AlternatingRow Enabled="True" />
                </Styles>
                <Columns>
                    <dx:GridViewDataTextColumn FieldName="RowNumber"  Caption="No" Width="20px">
                        <CellStyle HorizontalAlign="Center" />
                        <DataItemTemplate>
                                   <asp:HiddenField runat="server" ID="hidQuestionId" Value='<%# Bind("QuestionId") %>' />
                            <dx:ASPxLabel runat="server" ID="lblRowNumber" Text='<%# Bind("RowNumber") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DescriptionQuestion" Caption="Question" Width="500px">
                        <CellStyle HorizontalAlign="Left" />
                        <DataItemTemplate>

                            <dx:ASPxLabel runat="server" ID="lblDescriptionQuestion" Text='<%# Bind("DescriptionQuestion") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ValueCheckedYes" Caption="Yes" Width="30px">
                        <CellStyle HorizontalAlign="Center" />
                        <DataItemTemplate>
                            <dx:ASPxCheckBox runat="server" ID="chkYes" Checked='<%# Bind("ValueCheckedYes") %>' OnInit="CheckBoxYes_Init" ClientEnabled='<%# Bind("EnableChk") %>'>
                            </dx:ASPxCheckBox>
                            <asp:HiddenField runat="server" ID="hidchkYes" Value='<%# Bind("IsYes") %>' />
                             <asp:HiddenField runat="server" ID="hidWarningStatusYes" Value='<%# Bind("WarningStatusYes") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ValueCheckedNo" Caption="No" Width="30px">
                        <CellStyle HorizontalAlign="Center" />
                        <DataItemTemplate>


                            <dx:ASPxCheckBox runat="server" ID="chkNo" Checked='<%# Bind("ValueCheckedNo") %>' OnInit="CheckBoxNo_Init" ClientEnabled='<%# Bind("EnableChk") %>'>
                            </dx:ASPxCheckBox>
                            <asp:HiddenField runat="server" ID="hidchkNo" Value='<%# Bind("IsNo") %>' />
                            <asp:HiddenField runat="server" ID="hidWarningStatusNo" Value='<%# Bind("WarningStatusNo") %>'/>
                        </DataItemTemplate>


                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager PageSize="50" Mode="ShowAllRecords" AlwaysShowPager="false" />
            </dx:ASPxGridView>
            <br />
            <br />
            <div class="display-inline-table">
                 
                 <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="evt_Back" Visible="false"/>
                 <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="evt_Save" />
                <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="evt_Cose" />
                 <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="evt_Clear_Click" />
               
                </div>
           
                 <dx:ASPxPopupControl ID="popmsgnext" ClientInstanceName="popmsgnext" runat="server"
                    Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                    Width="300px" EnableAnimation="false" HeaderText="Add New Vendor" AllowDragging="false"
                    AllowResize="false">
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="popAddVendorContent" runat="server">
                            <table>
                                <tr>
                                    <td style="margin-right: 10px;">Pertayaan Berikutnya
                                    </td>
                                    
                                </tr>
                                
                            </table>
                            <br />
                            <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnextpopup"
                                Text="Ok" OnClick="evt_btnext_Click">
                                <ClientSideEvents Click="function(s,e) { popmsgnext.Hide(); loading(); }" />
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btCancel"
                                Text="Cancel" OnClick="evt_cancel_Click">
                                <ClientSideEvents Click="function(s,e) { popmsgnext.Hide(); }" />
                            </dx:ASPxButton>

                            
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>

        </ContentTemplate>
    </asp:UpdatePanel>
