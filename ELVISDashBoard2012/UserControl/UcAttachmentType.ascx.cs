﻿using BusinessLogic.VoucherForm;
using Common.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    
    public partial class UcAttachmentType : System.Web.UI.UserControl
    {
        public int TRANSACTION_CD { get; set; }
        public string DIVISIONData { get; set; }
        public string groupSeq { get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(TRANSACTION_CD) != "")
            {
              
               
            }
        }

        public void ShowLoadtest()
        {
             List<AttachmentType> _list = new List<AttachmentType>();
        var x = TRANSACTION_CD;
            var x2 = DIVISIONData;
            var x3 = groupSeq;
            var def = this.Page as _30PvFormList.VoucherFormPage;
            //def.clearScreenMessage();
            AttachID a = def.GetAttachID();
           
        _list = def.formPersistence.GetAttachmentType(x, x2, Convert.ToInt32(x3));
           
            gridAttachmentEnom.DataSource = _list;

            gridAttachmentEnom.DataBind();
           
            //def.PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "imron test");
        }
    }
}