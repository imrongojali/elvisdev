﻿using BusinessLogic.VoucherForm;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcQuestionaire : System.Web.UI.UserControl
    {
        public string CategoryCode
        {
            get { return hidCategoryCode.Value; }
            set { hidCategoryCode.Value = value; }
        }
        public string TransactionCode
        {
            get { return hidTransactionCode.Value; }
            set { hidTransactionCode.Value = value; }
        }
        public string ReferenceNo
        {
            get { return hidReferenceNo.Value; }
            set { hidReferenceNo.Value = value; }
        }

        public string UserName
        {
            get { return hidUserName.Value; }
            set { hidUserName.Value = value; }
        }
        public string DIV_CD
        {
            get { return hidDIV_CD.Value; }
            set { hidDIV_CD.Value = value; }
        }

       

        public string PageSelectedCategory
        {
            get { return hidPageSelectedCategory.Value; }
            set { hidPageSelectedCategory.Value = value; }
        }

        public string CountSelectedCategory
        {
            get { return hidCountSelectedCategory.Value; }
            set { hidCountSelectedCategory.Value = value; }
        }

        public string VendorCode
        {
            get { return hidVendorCode.Value; }
            set { hidVendorCode.Value = value; }
        }

        public string VendorGroup
        {
            get { return hidVendorGroup.Value; }
            set { hidVendorGroup.Value = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           

        }

        //public void QuestionList(PVFormData formData, string CategoryCode, Int32 TransactionCode, int pid = 0)
        public void QuestionList()
        {
           
             

                //var categoryCode = CategoryCode;
                //var transactionCode = TransactionCode;
                //var referenceNo = ReferenceNo;

                List<Question> _list = new List<Question>();
                var def = this.Page as _30PvFormList.VoucherFormPage;
                //def.clearScreenMessage();
               // AttachID a = def.GetAttachID();

                _list = def.formPersistence.GetQuestion(hidCategoryCode.Value, Convert.ToInt32(hidTransactionCode.Value), hidReferenceNo.Value);
                ASPxGridViewQuestion.DataSource = _list;
                ASPxGridViewQuestion.DataBind();

                //HiddenField hidPageSelectedCategory = (HiddenField)this.FindSiblingControl("hidPageSelectedCategory");
                //HiddenField hidCountSelectedCategory = (HiddenField)this.FindSiblingControl("hidCountSelectedCategory");
                int PageSelectedCategory = Convert.ToInt32(hidPageSelectedCategory.Value);
                int CountSelected = Convert.ToInt32(hidCountSelectedCategory.Value);
                if ((PageSelectedCategory == CountSelected) && (PageSelectedCategory != 1))
                {

                    btnBack.Visible = true;
                }
                else
                {
                    btnBack.Visible = false;
                }
            
        }

        void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {


            ASPxCheckBox boxYes = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "boxYes" + e.VisibleIndex) as ASPxCheckBox;
            HiddenField hidchkYes = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidchkYes") as HiddenField;
            HiddenField hidWarningStatusYes = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidWarningStatusYes") as HiddenField;
            bool _hidchkYes = Convert.ToBoolean(hidchkYes.Value);
            if (boxYes.Checked == true && hidWarningStatusYes.Value == "OQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedYes")
                {
                    e.Cell.BackColor = System.Drawing.Color.Yellow;
                }
            }
            else if (boxYes.Checked == true && _hidchkYes == false)
            {
                if (e.DataColumn.FieldName == "ValueCheckedYes")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                }
            }
            if (boxYes.Checked == true && hidWarningStatusYes.Value == "BQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedYes")
                {
                    e.Cell.BackColor = System.Drawing.Color.LightSeaGreen;
                }
            }
            ASPxCheckBox boxNo = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "boxNo" + e.VisibleIndex) as ASPxCheckBox;
            HiddenField hidchkNo = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidchkNo") as HiddenField;
            HiddenField hidWarningStatusNo = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidWarningStatusNo") as HiddenField;
            bool _hidchkNo = Convert.ToBoolean(hidchkNo.Value);

            if (boxNo.Checked == true && hidWarningStatusNo.Value == "OQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedNo")
                {
                    e.Cell.BackColor = System.Drawing.Color.Yellow; // or the color you want
                }
            }
            else if (boxNo.Checked == true && _hidchkNo == false)
            {
                if (e.DataColumn.FieldName == "ValueCheckedNo")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red; // or the color you want
                }
            }
            if (boxNo.Checked == true && hidWarningStatusNo.Value == "BQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedNo")
                {
                    e.Cell.BackColor = System.Drawing.Color.LightSeaGreen; // or the color you want
                }
            }



            if (boxYes.Checked == false && boxNo.Checked == false)
            {
                e.Cell.BackColor = System.Drawing.Color.Red;
            }

        }

        protected void evt_Save(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.clearScreenMessage();
            ASPxPanel PanelQuestion = (ASPxPanel)this.FindSiblingControl("PanelQuestion");
            ASPxPanel PanelMasterHeader = (ASPxPanel)this.FindSiblingControl("MasterHeader");
            ASPxPanel PanelEntertainment = (ASPxPanel)this.FindSiblingControl("PanelEntertainment");
            ASPxPanel PanelPromotion = (ASPxPanel)this.FindSiblingControl("PanelPromotion");
           

            //HiddenField hidPageSelectedCategory = (HiddenField)this.FindSiblingControl("hidPageSelectedCategory");
            //HiddenField hidCountSelectedCategory = (HiddenField)this.FindSiblingControl("hidCountSelectedCategory");


            ASPxGridViewQuestion.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
            IList<QuestionDetail> questionDetail = new List<QuestionDetail>();

            int IsValidateCheck = 0;
            int IsValidateData = 0;
            int countdata = 0;

            for (int i = 0; i < ASPxGridViewQuestion.VisibleRowCount; i++)
            {
                countdata++;
                ASPxCheckBox boxYes = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "boxYes" + i) as ASPxCheckBox;
                ASPxCheckBox boxYesNext = ASPxGridViewQuestion.FindRowCellTemplateControl((i + 1), ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "boxYes" + (i + 1)) as ASPxCheckBox;
                HiddenField hidchkYes = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidchkYes") as HiddenField;
                HiddenField hidWarningStatusYes = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidWarningStatusYes") as HiddenField;
                bool _hidchkYes = Convert.ToBoolean(hidchkYes.Value);

                ASPxCheckBox boxNo = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "boxNo" + i) as ASPxCheckBox;
                ASPxCheckBox boxNoNext = ASPxGridViewQuestion.FindRowCellTemplateControl((i + 1), ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "boxNo" + (i + 1)) as ASPxCheckBox;
                HiddenField hidchkNo = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidchkNo") as HiddenField;
                HiddenField hidWarningStatusNo = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidWarningStatusNo") as HiddenField;
                bool _hidchkNo = Convert.ToBoolean(hidchkNo.Value);

                if (boxYes.Checked == false && boxNo.Checked == false)
                {
                    IsValidateData += 1;
                }

                if ((boxNo.Checked == true && _hidchkNo == false) || (boxYes.Checked == true && _hidchkYes == false))
                {
                    IsValidateCheck += 1;
                }




                bool IsCheck = false;
                if (boxYes.Checked == true && boxNo.Checked == false)
                {
                    IsCheck = true;
                }
                else if (boxYes.Checked == false && boxNo.Checked == true)
                {
                    IsCheck = false;
                }




                HiddenField hidQuestionId = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["RowNumber"] as GridViewDataColumn, "hidQuestionId") as HiddenField;
                questionDetail.Add(new QuestionDetail { QuestionId = Convert.ToInt64(hidQuestionId.Value), IsYes = IsCheck });


                if (boxYes.Checked == true || boxNo.Checked == true)
                {
                    boxYes.ClientEnabled = true;
                    boxNo.ClientEnabled = true;
                    if (countdata < ASPxGridViewQuestion.VisibleRowCount)
                    {
                        boxYesNext.ClientEnabled = true;
                        boxNoNext.ClientEnabled = true;
                    }
                   

                }


            }
            if (IsValidateData > 0)
            {
                def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Data Harus di sis");
                PanelQuestion.ClientVisible = true;
                PanelMasterHeader.ClientVisible = false;

                return;
            }
            else if (IsValidateCheck > 0)
            {
                int PageSelectedCategory = Convert.ToInt32(hidPageSelectedCategory.Value);
                int CountSelected = Convert.ToInt32(hidCountSelectedCategory.Value);
                if (PageSelectedCategory < CountSelected)
                {
                    popmsgnext.ShowOnPageLoad = true;
                   // btnNext.Visible = false;
                    btnBack.Visible = true;
                }
                def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Data tidak sesuai");

                PanelQuestion.ClientVisible = true;
                PanelMasterHeader.ClientVisible = false;
                return;
            }
            else if (IsValidateData == 0 && IsValidateCheck == 0)
            {
                def.formPersistence.AddQuestion(hidCategoryCode.Value, Convert.ToInt32(hidTransactionCode.Value), hidReferenceNo.Value, questionDetail, DateTime.Now, hidUserName.Value);
                PanelQuestion.ClientVisible = false;
                PanelMasterHeader.ClientVisible = false;
                PanelPromotion.ClientVisible = true;
                def.ucDataFieldPromotion.PromotionList(hidCategoryCode.Value, hidTransactionCode.Value, hidReferenceNo.Value, hidUserName.Value, hidVendorCode.Value, hidVendorGroup.Value);

                //UcDataFieldPromotion DataFieldPromotion = (UcDataFieldPromotion)LoadControl("UcDataFieldPromotion.ascx");
                ////DataFieldPromotion.CategoryCodeProm = hidCategoryCode.Value;
                ////DataFieldPromotion.TransactionCodeProm = hidTransactionCode.Value;
                ////DataFieldPromotion.ReferenceNoProm = hidReferenceNo.Value;
                ////DataFieldPromotion.UserNameProm = hidUserName.Value;
                ////DataFieldPromotion.UserNameProm = hidVendorCode.Value;
                ////DataFieldPromotion.UserNameProm = hidVendorGroup.Value;

                //DataFieldPromotion.PromotionList(hidCategoryCode.Value, hidTransactionCode.Value, hidReferenceNo.Value, hidUserName.Value, hidVendorCode.Value, hidVendorGroup.Value);
                //ASPxGridView grid = DataFieldPromotion.FindControl("ASPxGridViewPromotion") as ASPxGridView;
                //grid.Init += new EventHandler(DataFieldPromotion.ASPxGridViewPromotion_Init);
            }

            // var def = this.Page as _30PvFormList.VoucherFormPage;
            // def.formPersistence.AddQuestion(hidCategoryCode.Value, Convert.ToInt32(hidTransactionCode.Value), hidReferenceNo.Value, questionDetail, DateTime.Now, hidUserName.Value);




        }



        protected void CheckBoxYes_Init(object sender, EventArgs e)
        {
            var box = sender as ASPxCheckBox;
            var container = box.NamingContainer as GridViewDataItemTemplateContainer;
            var index = container.VisibleIndex;
            box.ID = "boxYes" + index;
            box.ClientInstanceName = "boxYes" + index;

            box.ClientSideEvents.CheckedChanged = "function(s,e){OnCheckedYesChanged(s,e," + index + ") }";
        }

        protected void CheckBoxNo_Init(object sender, EventArgs e)
        {
            var box = sender as ASPxCheckBox;
            var container = box.NamingContainer as GridViewDataItemTemplateContainer;
            var index = container.VisibleIndex;
            box.ID = "boxNo" + index;
            box.ClientInstanceName = "boxNo" + index;

            box.ClientSideEvents.CheckedChanged = "function(s,e){OnCheckedNoChanged(s,e," + index + ") }";
        }
        protected void evt_Cose(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.clearScreenMessage();
            ASPxPanel PanelQuestion = (ASPxPanel)this.FindSiblingControl("PanelQuestion");
            ASPxPanel PanelMasterHeader = (ASPxPanel)this.FindSiblingControl("MasterHeader");
            PanelQuestion.ClientVisible = false;
            PanelMasterHeader.ClientVisible = true;
        }

        

        protected void evt_Back(object sender, EventArgs arg)
        {
            ASPxPanel PanelQuestion = (ASPxPanel)this.FindSiblingControl("PanelQuestion");
            ASPxPanel PanelMasterHeader = (ASPxPanel)this.FindSiblingControl("MasterHeader");
            PanelQuestion.ClientVisible = true;
            PanelMasterHeader.ClientVisible = false;
            //HiddenField hidPageSelectedCategory = (HiddenField)this.FindSiblingControl("hidPageSelectedCategory");
            //HiddenField hidCountSelectedCategory = (HiddenField)this.FindSiblingControl("hidCountSelectedCategory");
            int PageSelectedCategory = Convert.ToInt32(hidPageSelectedCategory.Value);
            int CountSelected = Convert.ToInt32(hidCountSelectedCategory.Value);
            if (PageSelectedCategory <= CountSelected)
            {
                int nexpagedata = PageSelectedCategory - 1;

                hidPageSelectedCategory.Value = Convert.ToString(nexpagedata);
                string groupid = getCategoryCode(nexpagedata);
                QuestionListByCategoryCode(groupid);

                if ((nexpagedata == CountSelected) && (nexpagedata != 1))
                {

                    btnBack.Visible = true;
                }
                else
                {
                    btnBack.Visible = false;
                }
            }
            
        }

        protected void evt_btnext_Click(object sender, EventArgs arg)
        {
            ASPxPanel PanelQuestion = (ASPxPanel)this.FindSiblingControl("PanelQuestion");
            ASPxPanel PanelMasterHeader = (ASPxPanel)this.FindSiblingControl("MasterHeader");
            PanelQuestion.ClientVisible = false;
            PanelMasterHeader.ClientVisible = true;

            //HiddenField hidPageSelectedCategory = (HiddenField)this.FindSiblingControl("hidPageSelectedCategory");
            //HiddenField hidCountSelectedCategory = (HiddenField)this.FindSiblingControl("hidCountSelectedCategory");
            //TextBox txttest = (TextBox)this.FindSiblingControl("txttest");
            int PageSelectedCategory = Convert.ToInt32(hidPageSelectedCategory.Value);
            int CountSelected = Convert.ToInt32(hidCountSelectedCategory.Value);
            if (PageSelectedCategory < CountSelected)
            {

                int nexpagedata = PageSelectedCategory + 1;
                //txttest.Text = "5";
                hidPageSelectedCategory.Value = Convert.ToString(nexpagedata);
                string groupid = getCategoryCode(nexpagedata);
                QuestionListByCategoryCode(groupid);
               
               if ((nexpagedata == CountSelected) && (nexpagedata != 1))
                {
                    
                    btnBack.Visible = true;
                }
                else
                {
                    btnBack.Visible = false;
                }
            }
          

        }
        protected void evt_cancel_Click(object sender, EventArgs arg)
        {
            PagingButton();
            ASPxGridViewQuestion.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
        }

        protected void evt_Clear_Click(object sender, EventArgs arg)
        {
            //this.Controls.Clear();
            //ASPxGridViewQuestion.DataSource = null;
            //ASPxGridViewQuestion.Columns.Clear();

            List<Question> _list = new List<Question>();
            var def = this.Page as _30PvFormList.VoucherFormPage;

            def.clearScreenMessage();
            _list = def.formPersistence.GetQuestion(hidCategoryCode.Value, Convert.ToInt32(hidTransactionCode.Value), hidReferenceNo.Value);
            ASPxGridViewQuestion.DataSource = _list;
            ASPxGridViewQuestion.DataBind();


        }
        


        public void QuestionListByCategoryCode(string _CategoryCode)
        {
           
                hidCategoryCode.Value = _CategoryCode;
               


                List<Question> _list = new List<Question>();
                var def = this.Page as _30PvFormList.VoucherFormPage;
              

                _list = def.formPersistence.GetQuestion(_CategoryCode, Convert.ToInt32(hidTransactionCode.Value), hidReferenceNo.Value);
                ASPxGridViewQuestion.DataSource = _list;
                ASPxGridViewQuestion.DataBind();


               
            
        }

        public string getCategoryCode(int seqNumber)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.clearScreenMessage();
            List<SelectedCategory> _list = new List<SelectedCategory>();
            _list = def.formPersistence.GetSelectedCategory(Convert.ToInt32(hidDIV_CD.Value), Convert.ToInt32(hidTransactionCode.Value));
            var datacat = _list.Where(x => x.GroupSeq == seqNumber).FirstOrDefault();
           // hidCategoryCode.Value = datacat.CategoryCode;
            return datacat.CategoryCode;
        }

        public void PagingButton()
        {
            //HiddenField hidPageSelectedCategory = (HiddenField)this.FindSiblingControl("hidPageSelectedCategory");
            //HiddenField hidCountSelectedCategory = (HiddenField)this.FindSiblingControl("hidCountSelectedCategory");
            int PageSelectedCategory = Convert.ToInt32(hidPageSelectedCategory.Value);
            int CountSelected = Convert.ToInt32(hidCountSelectedCategory.Value);
            if ((PageSelectedCategory == CountSelected) && (PageSelectedCategory != 1))
            {

                btnBack.Visible = true;
            }
            else
            {
                btnBack.Visible = false;
            }


        }
    }
}