Dear Mr./Mrs. Division Administrator,<br />
<br />
you have Invoice ready to be Converted to PV from <br /><br />

Vendor Code         : [VENDOR_CD] <br />
Vendor Name 		: [VENDOR_NAME] <br />
Invoice Date		: [INVOICE_DATE] <br />
Invoice No          : [INVOICE_NO] <br />
Transaction Type	: [TRANSACTION_TYPE_NM] <br />

<br />
<br />
Please click this following <a href="[DOC_LINK]">link</a>:	<br />
[DOC_LINK]
 <br />
<br />
Thank you.<br />
<br />
ELVIS Administrator