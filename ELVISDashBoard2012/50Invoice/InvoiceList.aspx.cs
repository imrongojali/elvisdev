﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._50Invoice;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._50Invoice;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;
namespace ELVISDashboard.Invoice
{
    public partial class InvoiceList : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID;

        public InvoiceList()
        {

            _ScreenID = resx.Screen("ELVIS_Screen_5001");
        }

        bool IsEditing = false;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }


        private bool _CanAttach = false;
        private readonly string _CAN_ATTACH_ = "invoice.canAttach";
        private bool CanAttach
        {
            get
            {
                if (Session[_CAN_ATTACH_] == null)
                {
                    Session[_CAN_ATTACH_] = false;
                    return false;
                }
                else
                    _CanAttach = (bool)Session[_CAN_ATTACH_];
                return _CanAttach;
            }

            set
            {
                Session[_CAN_ATTACH_] = value;
            }
        }
        #endregion

        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            lit = litMessage;

            hidScreenID.Value = _ScreenID;

            ASPxListBox statusList = (ASPxListBox)ddlLastStatus.FindControl("LastStatusListBox");
            statusList.DataSource = logic.Invoice.getInvoiceStatus();
            statusList.DataBind();

            bool isVendor = UserData.Roles.Contains("ELVIS_EXT_VENDOR");
            if (isVendor)
            {
                txtVendorCode.Value = Convert.ToString(UserData.REG_NO).PadLeft(10, '0'); ;
            }
            txtVendorCode.ReadOnly = isVendor;
            txtVendorCode.DropDownButton.Visible = !isVendor;

            if (!IsPostBack)
            {
                PrepareLogout();

                gridInvoice.DataSourceID = null;
                SetFirstLoad();
                AddEnterComponent(calInvoiceDateFrom);
                AddEnterComponent(calInvoiceDateTo);
                AddEnterComponent(txtInvoiceNo);
                AddEnterComponent(txtVendorCode);
                AddEnterComponent(ddlLastStatus);
                AddEnterComponent(calUploadDateFrom);
                AddEnterComponent(calUploadDateTo);
                AddEnterComponent(calSubmitDateFrom);
                AddEnterComponent(calSubmitDateTo);
                AddEnterComponent(calReceivedDateFrom);
                AddEnterComponent(calReceivedDateTo);
                AddEnterComponent(calConvertDateFrom);
                AddEnterComponent(calConvertDateTo);

                AddEnterAsSearch(btnSearch, _ScreenID);

                bool fixedDivision = !(isVendor || logic.User.IsAuthorizedFinance || logic.User.IsDirectorUp);
                lblDivision.Text = Convert.ToString(UserData.DIV_NAME);
                if (fixedDivision)
                    DivisionLookup.Value = UserData.DIV_CD;
                lblDivision.Visible = fixedDivision;
                DivisionLookup.ReadOnly = fixedDivision;
                DivisionLookup.Visible = !fixedDivision;

            }
            else
            {
                if (Session["REFF_NO"] != null
                    && Session["REFF_DATE"] != null
                    && gridInvoice != null
                    && gridInvoice.Visible
                    && gridInvoice.VisibleRowCount > 0)
                {
                    ModalPopupAttachment.Show();
                    if (Session["ATTACHMENT_VALID"] != null && Convert.ToBoolean(Session["ATTACHMENT_VALID"]))
                    {
                        LitMsgAttachment.Text = "";
                    }
                }
            }
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);

            btnSearch.Visible = _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;
            downloadDiv.Visible = false;
            btnDelete.Visible = false;

            btnExportExcel.Visible = false;
            btnConvertPV.Visible = false;
            btnClose.Visible = true;
            btnSubmit.Visible = false;
            btnReceived.Visible = false;
            btnInvoiceReceipt.Visible = false;

            bool canUpload = _RoleLogic.isAllowedAccess("btnUpload");
            CanAttach = _RoleLogic.isAllowedAccess("btnAttach");
            DownloadTemplateLinkDiv.Visible = canUpload;
            UploadDiv.Visible = canUpload;
            bool hasRequest = false;
            DateTime dReq = DateTime.Now;
            string vendorCd = "";
            if (Request.QueryString["date"] != null)
            {
                if (Request.QueryString["date"].ToString().DateOk("yyyy-MM-dd", ref dReq))
                {
                    calInvoiceDateFrom.Date = dReq;
                    calInvoiceDateTo.Date = dReq;
                    hasRequest = true;
                }
                else
                {
                    calInvoiceDateFrom.Date = DateTime.Now;
                    calInvoiceDateTo.Date = DateTime.Now;
                }
            }

            if (Request.QueryString["vendor"] != null)
            {
                vendorCd = Convert.ToString(Request.QueryString["vendor"]);
                txtVendorCode.Value = vendorCd;
                hasRequest = true;
            }
            if (hasRequest)
            {
                btnSearch_Click(null, null);
            }
        }
        #endregion

        #region Grid
        protected void gridInvoice_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoInvoice = gridInvoice.FindRowCellTemplateControl(e.VisibleIndex, gridInvoice.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                Literal litInvoiceDetail = gridInvoice.FindRowCellTemplateControl(e.VisibleIndex, gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "litInvoiceNo") as Literal;
                HyperLink linkPV = gridInvoice.FindRowCellTemplateControl(e.VisibleIndex, gridInvoice.Columns["PV_NO"] as GridViewDataColumn, "linkPV") as HyperLink;
                string pvNo = Convert.ToString(e.GetValue("PV_NO")) ?? "";
                string pvYear = Convert.ToString(e.GetValue("PV_YEAR")) ?? "";
                string detailURL = ResolveClientUrl(string.Format("~/30PvFormList/PVFormList.aspx?mode=view&pv_no={0}&pv_year={1}", pvNo, pvYear));
                detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3001"));
                linkPV.NavigateUrl = detailURL;
                linkPV.Text = pvNo;
                if (litGridNoInvoice != null)
                {
                    litGridNoInvoice.Text = Convert.ToString(e.VisibleIndex + 1);
                }

                if (litInvoiceDetail != null)
                {
                    litInvoiceDetail.Text = "<a href='" + Server.MapPath("~/50Invoice") + "/InvoiceListDetail.aspx?InvoiceNo="
                                                        + litInvoiceDetail.Text + "&invoiceDate=" + e.Row.Cells[2].Text
                                                        + "&vendorCode=" + e.Row.Cells[4].Text + "&vendorName= " + e.Row.Cells[5].Text
                                                        + "&division=" + e.Row.Cells[8].Text + "&invoiceStatus= " + e.Row.Cells[11].Text + "'/>";
                }
                RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);
                btnDelete.Visible = true && _RoleLogic.isAllowedAccess("btnDelete");
                btnExportExcel.Visible = true && _RoleLogic.isAllowedAccess("btnExportExcel");
                btnConvertPV.Visible = true && _RoleLogic.isAllowedAccess("btnConvertPV");

            }
            #endregion
            #region rowtype edit
            if (IsEditing)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    #region init
                    ASPxTextBox txtGridInvoiceNo = gridInvoice.FindEditRowCellTemplateControl(gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "txtGridInvoiceNo") as ASPxTextBox;
                    #endregion
                    #region add
                    if (PageMode == Common.Enum.PageMode.Add)
                    {
                        txtGridInvoiceNo.Focus();
                    }
                    #endregion
                    #region edit
                    else if (PageMode == Common.Enum.PageMode.Edit)
                    {
                        int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                        if (_VisibleIndex == e.VisibleIndex)
                        {
                            #region fill data edit
                            string _invoiceNo = Convert.ToString(gridInvoice.GetRowValues(_VisibleIndex, "INVOICE_NO"));

                            txtInvoiceNo.Focus();
                            txtInvoiceNo.Text = _invoiceNo;
                            txtInvoiceNo.ReadOnly = true;
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            #endregion

        }

        protected void gridInvoice_PageIndexChanged(object sender, EventArgs e)
        {
            gridInvoice.DataBind();
        }


        protected InvoiceSearchCriteria Criteria()
        {
            return new InvoiceSearchCriteria
                        (
                        calInvoiceDateFrom.Text, calInvoiceDateTo.Text,
                        txtInvoiceNo.Text,
                        Convert.ToString(txtVendorCode.Value),
                        getSelectedStatus(),
                        Convert.ToString(DivisionLookup.Value),
                        calUploadDateFrom.Text, calUploadDateTo.Text,
                        calSubmitDateFrom.Text, calSubmitDateTo.Text,
                        calReceivedDateFrom.Text, calReceivedDateTo.Text,
                        calConvertDateFrom.Text, calConvertDateTo.Text
                        );
        }

        protected void ldsLog_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            try
            {
                e.KeyExpression = "INVOICE_NO";
                e.QueryableSource = logic.Invoice.Search(Criteria());
            }
            catch (Exception ex)
            {
                Handle(ex);
                Nag("MSTD00018ERR");
            }
        }

        protected void lkpgridVendorCode_Load(object sender, EventArgs arg)
        {
            List<VendorCdData> _list = logic.Look.GetVendorCodeData("", "");
            txtVendorCode.DataSource = _list;
            txtVendorCode.DataBind();
        }

        protected void evt_cboxAttachmentCategory_onLoad(object sender, EventArgs args)
        {
            cboxAttachmentCategory.DataSource = logic.PVForm.getAttachmentCategories();
            cboxAttachmentCategory.DataBind();
        }

        protected void evt_btSendUploadAttachment_clicked(object sender, EventArgs e)
        {
            logic.Say("evt_btSendUploadAttachment_clicked", "Invoice No : {0} File: {1}", Session["INVOICE_NO"], uploadAttachment.FileName);
            bool valid = true;
            if (!uploadAttachment.HasFile)
            {
                LitMsgAttachment.Text = _m.Message("MSTD00034ERR", "attachment");
                valid = false;
                Session["ATTACHMENT_VALID"] = false;
            }
            if (cboxAttachmentCategory.SelectedItem == null)
            {
                LitMsgAttachment.Text = _m.Message("MSTD00017WRN", "Category");
                valid = false;
                Session["ATTACHMENT_VALID"] = false;
            }
            if (String.IsNullOrEmpty(tboxAttachmentDescription.Text))
            {
                LitMsgAttachment.Text = _m.Message("MSTD00017WRN", "Description");
                valid = false;
                Session["ATTACHMENT_VALID"] = false;
            }
            if (valid && Session["REFF_NO"] != null && Session["REFF_DATE"] != null)
            {
                bool uploadSucceded = true;
                string errorMessage = null;
                string pvNumber = Session["REFF_NO"].ToString().Replace("/", "_").Replace(" ", "_");
                string pvYear = Session["REFF_DATE"].ToString();
                string invoiceNo = Convert.ToString(Session["INVOICE_NO"]);

                string fileName = invoiceNo + "_" + CommonFunction.CleanFilename(uploadAttachment.FileName);
                ftp.ftpUploadBytes(pvYear, "Invoice",
                                pvNumber,
                                fileName,
                                uploadAttachment.FileBytes,
                                ref uploadSucceded,
                                ref errorMessage);
                if (uploadSucceded)
                {
                    ListEditItem selectedItem = cboxAttachmentCategory.SelectedItem;
                    FormAttachment attachment = new FormAttachment()
                    {
                        ReferenceNumber = Session["REFF_NO"].ToString(),
                        CategoryCode = selectedItem.Value.ToString(),


                        FileName = fileName,
                        DisplayFilename = invoiceNo,
                        Description = tboxAttachmentDescription.Text,
                        PATH = pvYear + "/Invoice/" + pvNumber
                    };
                    logic.PVForm.saveAttachmentInfo(attachment, UserData);
                    addNewRowToAttachment(invoiceNo, attachment.ReferenceNumber);
                    popdlgUploadAttachment.Hide();
                    Session["ATTACHMENT_VALID"] = true;
                }
                else
                {
                    LitMsgAttachment.Text = _m.SetMessage(errorMessage, "ERR");
                    Session["ATTACHMENT_VALID"] = false;
                }

            }
        }



        private void addNewRowToAttachment(string invoiceNo, string reffNumber)
        {
            List<FormAttachment> attcList = logic.Invoice.SearchAttachment(reffNumber, AppSetting.FTP_DOWNLOADHTTP);
            if (Session["CONVERT_FLAG"] == null || !Session["CONVERT_FLAG"].ToString().Equals("1"))
            {
                if (attcList == null || attcList.Count == 0)
                {
                    attcList = new List<FormAttachment>();
                }

                attcList.Add(new FormAttachment() { ReferenceNumber = reffNumber });
            }

            foreach (FormAttachment attachment in attcList)
            {
                attachment.DisplayFilename = string.IsNullOrEmpty(invoiceNo) ? attachment.FileName : invoiceNo;
            }

            gridAttachment.DataSource = attcList;
            gridAttachment.DataBind();
        }

        #endregion

        protected void VendorCode_TextChanged(object sender, EventArgs e)
        {
            logic.Say("VendorCode_TextChanged", "Vendor Code = {0}", txtVendorCode.Text);
            if (!String.IsNullOrEmpty(txtVendorCode.Text))
            {
                lblVendorName.Text = "";
                string[] vendorCds = txtVendorCode.Text.Split(';');
                foreach (string vendorCd in vendorCds)
                {
                    if (!String.IsNullOrWhiteSpace(vendorCd))
                    {
                        List<VendorCdData> _list = logic.Look.GetVendorCodeData(vendorCd.Trim(), "");
                        if (_list.Count > 0)
                        {
                            VendorCdData firstVendorCode = _list[0];
                            lblVendorName.Text += firstVendorCode.VENDOR_NAME + ";";
                        }
                        else
                        {
                            litMessage.Text += _m.Message("MSTD00021WRN",
                                            "Vendor " + vendorCd.Trim());
                            lblVendorName.Text = "";
                        }
                    }
                }
            }
            else
            {
                lblVendorName.Text = "";
            }
        }
        #endregion

        #region Button

        protected void btnClear_Click(object sender, EventArgs e)
        {
            logic.Say("btnClear_Click", "Clear");

            gridInvoice.DataSourceID = null;
            gridInvoice.DataBind();
            btnExportExcel.Visible = false;
            btnConvertPV.Visible = false;

            btnDelete.Visible = false;

            txtInvoiceNo.Text = "";
            if (!txtVendorCode.ReadOnly)
            {
                txtVendorCode.Text = "";
                lblVendorName.Text = "";
            }
            calInvoiceDateFrom.Text = "";
            calInvoiceDateTo.Text = "";
            LitMsgAttachment.Text = "";
            clearSelectedStatus();

            calUploadDateFrom.Text = "";
            calUploadDateTo.Text = "";
            calSubmitDateFrom.Text = "";
            calSubmitDateTo.Text = "";
            calReceivedDateFrom.Text = "";
            calReceivedDateTo.Text = "";
            calConvertDateFrom.Text = "";
            calConvertDateTo.Text = "";
            downloadDiv.Visible = false;

            litMessage.Visible = false;
        }

        protected List<int> getSelectedStatus()
        {
            ASPxListBox statusList = (ASPxListBox)ddlLastStatus.FindControl("LastStatusListBox");
            SelectedItemCollection selectedItems = statusList.SelectedItems;
            List<int> lstSelectedStatus = new List<int>();
            if ((selectedItems != null) && (selectedItems.Count > 0))
            {
                ListEditItem item;
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    item = selectedItems[i];
                    lstSelectedStatus.Add(Convert.ToInt32(item.Value));
                }
            }

            return lstSelectedStatus;
        }
        protected void clearSelectedStatus()
        {
            ASPxListBox statusList = (ASPxListBox)ddlLastStatus.FindControl("LastStatusListBox");
            statusList.UnselectAll();
            ddlLastStatus.Text = String.Empty;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            logic.Say("btnSearch_Click", "Search");
            ButtonClosePopUpAttachment_Click(sender, e);
            if (ValidateInputSearch())
            {
                gridInvoice.Selection.UnselectAll();
                gridInvoice.DataSourceID = ldsLog.ID;
                gridInvoice.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            logic.Say("btnUpload_Click", "Upload");
            downloadDiv.Visible = false;
            try
            {

                if (!fuInvoiceList.HasFile)
                {
                    Nag("MSTD00034ERR", "excel");
                    return;
                }

                int _processId = 0;

                string _strFileType = System.IO.Path.GetExtension(fuInvoiceList.FileName).ToString().ToLower();
                string _strFileName = fuInvoiceList.FileName;
                string _strTempDir = Common.AppSetting.TempFileUpload;
                string _strTempFileName = "INVOICE_LIST" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                string _strMetaData = Server.MapPath("./INVOICE_META.txt");

                string _strTempPath = Server.MapPath(_strTempDir + _strTempFileName + _strFileType);
                fuInvoiceList.SaveAs(_strTempPath);
                if (!CommonExcelUpload.IsExcelFile(fuInvoiceList.FileName))
                {
                    Nag("MSTD00033ERR", fuInvoiceList.FileName);
                    return;
                }

                string _strFunctionId = "";

                _strFunctionId = resx.FunctionId("FCN_INVOICE_LIST_UPLOAD");
                DoStartLog(_strFunctionId, "");
                _processId = ProcessID;
                List<InvoiceData> l = new List<InvoiceData>();

                string[][] listOfErrors = new string[1][];
                List<string> invoices = new List<string>();
                bool Ok;
                Ok = logic.Invoice.Upload(_strTempPath, _strMetaData, l, ref listOfErrors, UserData, Convert.ToString(UserData.REG_NO), ProcessID);
                if (!Ok)
                {
                    string fileName = DownloadInvoiceErrorTemplate(listOfErrors);
                    downloadDiv.Visible = true;
                    linkDownloadError.NavigateUrl = Request.Url.Scheme + @"://"
                        + Request.Url.Authority + @"/"
                        + AppSetting.ErrorUploadFilePath.Substring(2) + fileName;

                    litMessage2.Text = "Upload failed. Download ";

                    Nag("MSTD00019ERR");
                    return;
                }

                Ok = logic.Invoice.SaveUpload(ProcessID, UserData.USERNAME, UserData.DIV_CD, l, ref invoices);
                if (!Ok)
                {
                    Nag("MSTD00019ERR");
                    return;
                }
                gridInvoice.DataSourceID = null;
                gridInvoice.DataSource = logic.Invoice.SearchInquryAfterUpload(invoices);
                gridInvoice.DataBind();


                ErrorData emailErr = new ErrorData();

                Nag("MSTD00020INF");

                SetCancelEditDetail();


            }
            catch (Exception ex)
            {
                Handle(ex);
                if (ex.InnerException != null)
                    litMessage.Text = _m.SetMessage(ex.InnerException.Message.ToString(), "ERR");
                else
                    litMessage.Text = _m.SetMessage(ex.Message.ToString(), "ERR");
            }
        }

        private bool SendEmail(List<InvoiceData> l, ErrorData _Err)
        {
            int sent = 0;

            var qDiv = (from div in l select div.DIVISION_ID);

            string[] divs = qDiv.Distinct().ToArray();
            if (divs.Length < 1) return false;

            List<UserData> uda;
            String emailTemplate = String.Format("{0}\\InvoiceUploadConvertNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
            EmailFunction mail = new EmailFunction(emailTemplate);

            for (int i = 0; i < divs.Length; i++)
            {
                uda = logic.Invoice.GetAdminUserByDivision(divs[i]);
                if (uda == null || uda.Count < 1) continue;

                var mailToList = from ux in uda select ux.EMAIL;

                string[] mailtos = mailToList.ToArray();

                if (mailtos != null && mailtos.Length > 0)
                {
                    var q = (from x in l
                             where x.DIVISION_ID == divs[i]
                             select new
                             {
                                 x.VENDOR_CD,
                                 x.VENDOR_NAME,
                                 x.INVOICE_NO,
                                 x.INVOICE_DATE,
                                 x.TRANSACTION_NAME
                             })
                    .Distinct()
                    .OrderBy(inv => inv.INVOICE_NO);
                    string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                    foreach (var li in q)
                    {
                        String docLink = String.Format("{0}://{1}{2}{3}?date={4}&vendor={5}",
                            Request.Url.Scheme,
                            Request.ServerVariables["SERVER_NAME"],
                            port,
                            Request.ServerVariables["URL"],
                            li.INVOICE_DATE.ToString("yyyy-MM-dd"),
                            li.VENDOR_CD);

                        if (mail.ComposeUploadInvoice_EMAIL(
                            UserData,
                            mailtos,
                            _Err,
                                li.VENDOR_CD,
                                li.VENDOR_NAME,
                                li.INVOICE_DATE.ToString(InvoiceListLogic.XLS_DATE),
                                li.INVOICE_NO,
                                li.TRANSACTION_NAME,
                                docLink))
                            sent++;
                    }

                }
            }


            return (sent > 0);
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            logic.Say("btnEdit_Click", "Edit");
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridInvoice.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i <= gridInvoice.VisibleRowCount; i++)
                    {
                        if (gridInvoice.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridInvoice.SettingsBehavior.AllowSort = false;
                            IsEditing = true;
                            Session["visibleIndex"] = i;
                            gridInvoice.StartEdit(i);
                            gridInvoice.Selection.UnselectAll();
                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridInvoice.Selection.Count == 0)
                {
                    Nag("MSTD00009WRN");
                }
                else if (gridInvoice.Selection.Count > 0)
                {
                    Nag("MSTD00016WRN");
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logic.Say("btnDelete_Click", "Delete");
            if (gridInvoice.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = string.Format(resx.Message("MSTD00008CON"), "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridInvoice.Selection.Count == 0)
            {
                #region Error
                Nag("MSTD00009WRN");

                #endregion
            }

        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ButtonClosePopUpAttachment_Click(sender, e);
            logic.Say("btnExportExcel_Click", "Download");
            if (gridInvoice.VisibleRowCount > 0)
            {
                try
                {
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    UserData userdata = (UserData)Session["UserData"];
                    string _FileName = "INVOICE_LIST" + DateTime.Now.ToString("ddMMyyyy_HHmmss");

                    this.Response.Clear();
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.BinaryWrite(logic.Invoice.Get(UserName, imagePath, Criteria()));

                    this.Response.End();
                }
                catch (Exception Ex)
                {
                    litMessage.Text = MessageLogic.Wrap("Cannot get file: " + Ex.Message, "ERR");
                    Handle(Ex);
                    throw;
                }
            }
            else Nag("MSTD00018INF");
        }

        private bool GetListed(ref List<InvoiceData> listedInvoices)
        {
            if (gridInvoice.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return false;
            }

            if (gridInvoice.VisibleRowCount > InvoiceListLogic.MAX_INVOICE_ITEM)
            {
                Nag("MSTD00003WRN", "Listed data exceed allowed limit ("
                        + InvoiceListLogic.MAX_INVOICE_ITEM
                        + " line). Enter more criteria.");
                return false;
            }

            listedInvoices = ListedInvoice();
            return true;
        }

        private bool DoOp(InvoiceStatus s, ref List<InvoiceData> listedInvoices, bool justValidate = false)
        {
            bool Ok = GetListed(ref listedInvoices);
            if (!Ok) return false;

            List<InvoiceData> idata = GenerateInvoiceDataList();
            string opName = logic.Invoice.statusName[(int)s];


            if (idata == null)
            {
                Nag("MSTD00002ERR", "Get selected item failed, please Search and pick item again");
                return false;
            }

            Ok = logic.Invoice.PreCheckSetStatus(s, idata);

            if (!Ok)
            {
                Nag("MSTD00057ERR", opName, Common.Function.CommonFunction.CommaJoin(logic.Invoice.Errors, " "));
                return false;
            }
            if (justValidate)
            {
                listedInvoices = idata;
            }
            Ok = justValidate || logic.Invoice.SetStatus(s, idata);
            if (Ok)
            {
                gridInvoice.DataSourceID = null;
                gridInvoice.DataSource = logic.Invoice.Search(listedInvoices);
                gridInvoice.DataBind();
            }

            return Ok;

        }

        private void ReSelect(List<InvoiceData> ida)
        {
            for (int i = 0; i < gridInvoice.VisibleRowCount; i++)
            {
                string ino = gridInvoice.GetRowValues(i, "INVOICE_NO").str();
                string ive = gridInvoice.GetRowValues(i, "VENDOR_CD").str();
                object iid = gridInvoice.GetRowValues(i, "INVOICE_DATE");
                DateTime invDate = DateTime.Now;
                if (iid != null)
                {
                    invDate = Convert.ToDateTime(iid);
                }
                if (ida.Where(a => a.INVOICE_NO == ino && a.VENDOR_CD == ive && a.INVOICE_DATE.Equals(invDate)).Count() > 0)
                {
                    gridInvoice.Selection.SetSelection(i, true);
                }

            }
        }

        protected void InvoiceReceipt(List<InvoiceData> li)
        {
            /// Invoice Receipt 
            string imgPath = System.IO.Path.GetDirectoryName(Server.MapPath(AppSetting.CompanyLogo));
            string tPath = Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE","DIR") + "/InvoiceReceiptTemplate.htm");
            string oPath = Server.MapPath(AppSetting.TempFileUpload);

            string InvoiceReceiptHtml = logic.Invoice.GetReceipt(UserData.DIV_NAME + " Staff"
                , imgPath, tPath, logic.Invoice.SearchInvoicesByListed(li));

            string oFile = logic.Invoice.writeReceipt(imgPath, oPath, UserData, InvoiceReceiptHtml);

            string outLink = Request.Url.Scheme + @"://"
                + Request.Url.Authority + @"/"
                + AppSetting.TempFileUpload.Substring(2) + oFile;

            ScriptManager.RegisterStartupScript(this,
                                   this.GetType(), "openWinLogin",
                                   "openWin('" + outLink + "','pdfDownload');",
                                   true);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            logic.Say("btnSubmit_Click", "Submit");
            string opName = logic.Invoice.statusName[(int)InvoiceStatus.IS_SUBMITTED];
            List<InvoiceData> li = new List<InvoiceData>();
            if (DoOp(InvoiceStatus.IS_SUBMITTED, ref li))
            {
                InvoiceReceipt(li);

                Nag("MSTD00060INF", opName);
            }
        }

        protected void btnReceived_Click(object sender, EventArgs e)
        {
            logic.Say("btnReceived_Click", "Receive");
            string opName = logic.Invoice.statusName[(int)InvoiceStatus.IS_RECEIVED];

            List<InvoiceData> li = new List<InvoiceData>();

            if (DoOp(InvoiceStatus.IS_RECEIVED, ref li))
            {
                Nag("MSTD00060INF", opName);
                gridInvoice.Selection.UnselectAll();
            }
        }

        protected void btnInvoiceReceipt_Click(object sender, EventArgs e)
        {
            List<InvoiceData> li = new List<InvoiceData>();
            bool Ok = GetListed(ref li);
            if (!Ok) return;

            InvoiceReceipt(li);
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            logic.Say("btnOkConfirmationDelete_Click", "Confirm Delete");
            bool result = false;
            if (gridInvoice.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }

            List<InvoiceData> listInvoice = new List<InvoiceData>();
            bool isOkay = true;
            for (int i = 0; i <= gridInvoice.VisibleRowCount; i++)
            {
                if (gridInvoice.Selection.IsRowSelected(i))
                {
                    Literal litInvoiceNo = gridInvoice.FindRowCellTemplateControl(i, gridInvoice.Columns["INVOICE_NO"] as GridViewDataTextColumn, "litGridPVNo") as Literal;
                    Literal litVendorCode = gridInvoice.FindRowCellTemplateControl(i, gridInvoice.Columns["VENDOR_CD"] as GridViewDataTextColumn, "litVendorCd") as Literal;
                    int convertFlag = Convert.ToInt16(gridInvoice.GetRowValues(i, "CONVERT_FLAG"));
                    DateTime invoiceDate = (DateTime)(gridInvoice.GetRowValues(i, "INVOICE_DATE"));
                    if (convertFlag == 0)
                    {
                        var data = new InvoiceData();
                        data.INVOICE_NO = litInvoiceNo.Text;
                        data.VENDOR_CD = litVendorCode.Text;
                        data.INVOICE_DATE = invoiceDate;
                        listInvoice.Add(data);
                    }
                    else
                    {
                        litMessage.Text += _m.Message("MSTD00090ERR", litInvoiceNo.Text);
                        isOkay = false;
                        break;
                    }
                }
            }

            if (listInvoice.Count > 0 && isOkay)
            {
                PageMode = Common.Enum.PageMode.View;
                result = logic.Invoice.Delete(listInvoice);
                if (!result)
                {

                    Nag("MSTD00014ERR");
                    gridInvoice.Selection.UnselectAll();
                }
                else
                {
                    Nag("MSTD00015INF");

                    gridInvoice.Selection.UnselectAll();

                }

                gridInvoice.DataSourceID = ldsLog.ID;
                gridInvoice.DataBind();
            }

        }

        protected void BtnCancelConfirmationDelete_Click(object sender, EventArgs e)
        {

        }

        protected void BtnCloseOtherAmount_Click(object sender, EventArgs e)
        {
            ClearModal();
            gridOtherAmount.Selection.UnselectAll();
            gridOtherAmount.Visible = false;

            ModalPopupOtherAmount.Hide();
        }

        protected void SetCancelEditDetail()
        {
            bool hasRows = gridInvoice.PageCount > 0;
            if (hasRows)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData Err = null;
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref Err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
                btnConvertPV.Visible = role.isAllowedAccess("btnConvertPV");
                btnSubmit.Visible = role.isAllowedAccess("btnSubmit");
                btnReceived.Visible = role.isAllowedAccess("btnReceived");
                btnInvoiceReceipt.Visible = role.isAllowedAccess("btnInvoiceReceipt");
            }
        }

        protected void SetReadOnlyDetail()
        {
            if (gridInvoice.PageCount > 0)
            {
                btnConvertPV.Visible = false;
            }
        }

        private void SetSavedMode(bool convertPv)
        {
            gridInvoice.CancelEdit();
            gridInvoice.DataBind();
            SetFirstLoad();
            PageMode = Common.Enum.PageMode.View;
            gridInvoice.SettingsBehavior.AllowSort = true;
            if (!convertPv)
                SetCancelEditDetail();
            else
                SetReadOnlyDetail();
        }

        private bool isInvoiceDataValid(List<InvoiceData> _invoiceList)
        {
            bool valid = true;

            if (_invoiceList != null && _invoiceList.Count > 0)
            {
                int? transType = null;
                for (int a = 0; a < _invoiceList.Count; a++)
                {
                    InvoiceData i = _invoiceList.ElementAt(a);
                    if (transType == null)
                    {
                        transType = i.TRANSACTION_CD;
                    }
                    else
                    {
                        if (i.TRANSACTION_CD != transType)
                        {
                            Nag("MSTD00077ERR", "Transaction Type");
                            valid = false;
                            break;
                        }
                    }

                    for (int b = a + 1; b < _invoiceList.Count; b++)
                    {
                        InvoiceData j = _invoiceList.ElementAt(b);
                        if (i.INVOICE_NO.Equals(j.INVOICE_NO) && !i.VENDOR_CD.Equals(j.VENDOR_CD))
                        {

                            Nag("MSTD00087ERR", j.INVOICE_NO);
                            valid = false;
                            break;
                        }
                    }

                    if (!valid)
                        break;
                }
            }
            else
            {
                valid = false;
            }

            return valid;
        }

        protected void btnConvertPV_Click(object sender, EventArgs e)
        {
            ButtonClosePopUpAttachment_Click(sender, e);
            bool convertPV = true;

            List<InvoiceData> _invoiceList = new List<InvoiceData>();
            logic.Say("btnConvertPV_Click", "Convert to PV");
            if (DoOp(InvoiceStatus.IS_CONVERTED, ref _invoiceList, true))
            {
                if (isInvoiceDataValid(_invoiceList))
                {
                    List<String> result = logic.Invoice.ConvertPV(UserData, convertPV, _invoiceList);
                    int pvNo = _invoiceList[0].PV_NO ?? 0;
                    int pvYear = _invoiceList[0].PV_YEAR ?? 0;
                    if (result[0].ToString().Equals("2"))
                    {
                        string okInvoice = result[2].ToString();
                        okInvoice = okInvoice.Substring(0, (okInvoice.Length - 2));
                        String generatedTxt =
                            "PV No. " +
                            "<a id=\"docLink\" href=\"#\" onclick=\"javascript:openWin('../30PvFormList/PVFormList.aspx?mode=edit&pv_no=" +
                            pvNo +
                            "&pv_year=" + pvYear +
                            "','ELVIS_PVFormList')\">" +
                            pvNo +
                            "</a> ";
                        LblConvertPV.Text = "<b>Congratulations!</b> <br />Invoice No. " + okInvoice + " is successfully <br />converted with " + generatedTxt;
                        AlertConvertPVPopUp.Show();
                        gridInvoice.DataBind();
                        ScriptManager.RegisterStartupScript(this,
                           this.GetType(), "",
                           "assignPopClick();",
                           true);
                    }
                    else
                    {
                        //Result == 0 --> Error in SQL, or Logic execution
                        if (result[0].ToString().Equals("0"))
                        {
                            Nag("MSTD00028ERR");
                        }
                        //Result == 1 --> Error Already converted data exists
                        else if (result[0].ToString().Equals("1"))
                        {
                            string convInvoice = result[1].ToString();
                            convInvoice = convInvoice.Substring(0, (convInvoice.Length - 2));

                            litMessage.Text = MessageLogic.Wrap("Invoice No. " + convInvoice + " has been converted", "ERR");
                        }
                    }
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void LoadGridAttachment(object sender, EventArgs e)
        {
            gridAttachment.Visible = true;
            LitMsgAttachment.Text = "";
            panelAttachment.GroupingText = "Attachment";
            Button btnIndex = (Button)sender;

            GridViewDataRowTemplateContainer container = btnIndex.NamingContainer as GridViewDataRowTemplateContainer;
            HiddenField hidInvoiceNo = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "hidInvoiceNo") as HiddenField;
            HiddenField hidVendorCd = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["VENDOR_CD"] as GridViewDataColumn, "hidVendorCd") as HiddenField;
            Literal litInvoiceDate = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["INVOICE_DATE"] as GridViewDataColumn, "litInvoiceDate") as Literal;
            HiddenField hidConvertFlag = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "hidConvertFlag") as HiddenField;
            DateTime invoiceDate = DateTime.ParseExact(litInvoiceDate.Text, "dd MMM yyyy", null);
            String reffNumber = String.Concat(hidInvoiceNo.Value, hidVendorCd.Value, invoiceDate.ToString("yyyyMMdd"));
            Session["CONVERT_FLAG"] = hidConvertFlag.Value;
            Session["INVOICE_NO"] = hidInvoiceNo.Value;
            addNewRowToAttachment(hidInvoiceNo.Value, reffNumber);
            Session["REFF_NO"] = reffNumber;
            Session["REFF_DATE"] = invoiceDate.ToString("yyyyMMdd");
            ModalPopupAttachment.Show();
        }

        protected void ButtonClosePopUpAttachment_Click(object sender, EventArgs e)
        {
            gridAttachment.Visible = false;
            Session["REFF_NO"] = null;
            Session["REFF_DATE"] = null;
            Session["ATTACHMENT_VALID"] = null;
            Session["CONVERT_FLAG"] = null;
            popdlgUploadAttachment.Hide();
            ModalPopupAttachment.Hide();
        }
        #endregion

        #region Function
        #region convert pv

        private List<InvoiceData> ListedInvoice(bool selected = false)
        {
            List<InvoiceData> r = new List<InvoiceData>();

            if (gridInvoice.VisibleRowCount > 0 && gridInvoice.VisibleRowCount <= InvoiceListLogic.MAX_INVOICE_ITEM)
            {
                DateTime invoiceDate = new DateTime(StrTo.NULL_YEAR, 1, 1);
                string invoiceNo = "";
                string vendorCode = "";
                for (int i = 0; i < gridInvoice.VisibleRowCount; i++)
                {
                    invoiceNo = Convert.ToString(gridInvoice.GetRowValues(i, "INVOICE_NO"));
                    vendorCode = Convert.ToString(gridInvoice.GetRowValues(i, "VENDOR_CD"));
                    invoiceDate = Convert.ToDateTime(gridInvoice.GetRowValues(i, "INVOICE_DATE"));

                    r.Add(new InvoiceData()
                    {
                        INVOICE_NO = invoiceNo,
                        INVOICE_DATE = invoiceDate,
                        VENDOR_CD = vendorCode
                    });
                }

            }

            return r;
        }


        private List<InvoiceData> GenerateInvoiceDataList(

            )
        {
            List<InvoiceData> _listInvoiceData = null;
            if (gridInvoice.VisibleRowCount >= 1)
            {
                _listInvoiceData = new List<InvoiceData>();
                for (int i = 0; i < gridInvoice.VisibleRowCount; i++)
                {
                    if (gridInvoice.Selection.IsRowSelected(i))
                    {
                        InvoiceData invoiceData = new InvoiceData();
                        Literal litInvoiceNo = gridInvoice.FindRowCellTemplateControl(i,
                                                            gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn,
                                                            "litGridPVNo") as Literal;
                        Literal litVendorCode = gridInvoice.FindRowCellTemplateControl(i,
                                                            gridInvoice.Columns["VENDOR_CD"] as GridViewDataColumn,
                                                            "litVendorCd") as Literal;
                        HiddenField hidTransactionCd = gridInvoice.FindRowCellTemplateControl(i,
                                                            gridInvoice.Columns["TRANSACTION_CD"] as GridViewDataColumn,
                                                            "hidTransactionCd") as HiddenField;
                        Literal litInvoiceDate = gridInvoice.FindRowCellTemplateControl(i,
                            gridInvoice.Columns["INVOICE_DATE"] as GridViewDataColumn, "litInvoiceDate") as Literal;
                        DateTime invoiceDate = DateTime.ParseExact(litInvoiceDate.Text, "dd MMM yyyy", null);

                        if (!string.IsNullOrEmpty(litInvoiceNo.Text))
                        {
                            invoiceData.INVOICE_NO = litInvoiceNo.Text;
                            invoiceData.VENDOR_CD = litVendorCode.Text;
                            invoiceData.INVOICE_DATE = invoiceDate;
                            if (!String.IsNullOrEmpty(hidTransactionCd.Value))
                                invoiceData.TRANSACTION_CD = Convert.ToInt32(hidTransactionCd.Value);
                        }

                        _listInvoiceData.Add(invoiceData);
                    }
                }
            }

            return _listInvoiceData;
        }

        #endregion

        private bool ValidateInputSave()
        {
            bool code = true;

            //REGION FOR VALIDATING INPUT SAVE
            ASPxTextBox txtGridInvoiceNo = gridInvoice.FindEditRowCellTemplateControl(gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "txtGridInvoiceNo") as ASPxTextBox;
            if (txtGridInvoiceNo.Text.Equals(string.Empty))
            {
                Nag("MSTD00017WRN", "Invoice No");
                code = false;
            }

            return code;
        }

        private bool ValidateInputSearch()
        {
            if (calInvoiceDateTo.Date.CompareTo(calInvoiceDateFrom.Date) < 0)
            {
                Nag("MSTD00029ERR", "Invoice Date To", "Invoice Date From");

                SetFocus(calInvoiceDateTo);
                return false;
            }
            return true;
        }

        protected void SetEditDetail()
        {
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnConvertPV.Visible = false;
            btnClose.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridInvoice.SettingsBehavior.AllowSort = false;
        }
        #endregion

        #region paging
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridInvoice.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }
        protected void grid_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridInvoice.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            if (gridInvoice.VisibleRowCount > 0)
                gridInvoice.DataSourceID = ldsLog.ID;
            gridInvoice.DataBind();
        }
        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridInvoice.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        #endregion

        #region upload

        public bool isDateFormat(string StrInput)
        {
            try
            {
                DateTime resDate = DateTime.ParseExact(StrInput, InvoiceListLogic.XLS_DATE, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private const string _p_ = "|";
        private void Adds(string a, ref string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                s = s + _p_ + a;
            }
            else
            {
                s = a;
            }
        }

        #region Download Invoice Error Template

        public string DownloadInvoiceErrorTemplate(string[][] le)
        {


            string fileName = ("INVOICE_UPLOAD_FAILED_" + DateTime.Now.ToString("HHmmss") + ".xls").ToString();
            try
            {
                string templatePath = System.IO.Path.Combine( Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE","DIR")), "Invoice Upload Error-Template.xls");
                ExcelWriter x = new ExcelWriter(templatePath);
                string _sheetName = "Template";

                x.MarkPage(_sheetName);
                x.MarkRow("[INVOICE_NO],[INVOICE_DATE],[DIVISION_ID],[DIVISION_NAME],"
                    + "[TAX_NO],[INVOICE_TAX_DATE],[TMMIN_INVOICE],[REMARK],[C20],[C40],"
                    + "[ITEM_TRANSACTION],[ITEM_DESCRIPTION],"
                    + "[CURRENCY_CD],[AMOUNT],[TAX_CD],[ERROR_REASON]");

                int j = 0;
                if (le.Length > 0)
                {
                    for (int i = 0; i < le.Length; i++)
                    {
                        x.PutRow(le[i][0], le[i][1], le[i][2], le[i][3], le[i][4],
                                 le[i][5], le[i][6], le[i][7], le[i][8], le[i][9],
                                 le[i][10], le[i][11], le[i][12], le[i][13], le[i][14],
                                 le[i][15]);
                    }
                }

                if (j < 1)
                {
                    x.PutRow("", "", "", "", "",
                             "", "", "", "", "",
                             "", "", "", "", "",
                             "");
                }

                string writePath = Server.MapPath(AppSetting.ErrorUploadFilePath + fileName);
                x.Write(writePath);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    string xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                    Nag("MSTD00042ERR", ex.Message);
                }
        #endregion
            }

            return fileName;
        }

        #endregion

        protected void CalInvoiceDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, calInvoiceDateTo, true);
        }

        protected void CalInvoiceDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, calInvoiceDateFrom, false);
        }

        #region Popup

        #region Vendor
        private void LoadGridPopUpVendorCd()
        {
            List<VendorCdData> _list = logic.Look.GetVendorCodeData(txtSearchCd.Text, txtSearchDesc.Text);
            gridPopUpVendorCd.DataSource = _list;
            gridPopUpVendorCd.DataBind();
        }

        protected void gridPopUpVendorCd_PageIndexChanged(object sender, EventArgs e)
        {
            LoadGridPopUpVendorCd();
        }

        protected void gridPopUpVendorCd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoVendorCd = gridPopUpVendorCd.FindRowCellTemplateControl(e.VisibleIndex, gridPopUpVendorCd.Columns["NO"] as GridViewDataColumn, "litGridNoVendorCd") as Literal;
                litGridNoVendorCd.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void lvVendorCode_Click(object sender, ImageClickEventArgs e)
        {
            ModalButton = Common.Enum.ModalButton.VendorCd;

            panelModal.GroupingText = "List Vendor";
            lblPopUpSearchCd.Text = "Vendor Code";
            lblPopUpSearchDesc.Text = "Vendor Name";

            gridPopUpVendorCd.Visible = true;

            popUpList.Show();
        }
        #endregion

        #region Invoice Other Amount
        private void LoadGridPopUpOtherInvoiceAmount(string invoiceNo, string vendorCd, string invoiceDate)
        {
            List<OtherAmountData> _list = logic.Look.GetOtherInvoiceAmountData(invoiceNo, vendorCd, invoiceDate);
            gridOtherAmount.DataSource = _list;
            gridOtherAmount.DataBind();
        }

        protected void gridOtherAmount_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoOtherAmount = gridOtherAmount.FindRowCellTemplateControl(e.VisibleIndex, gridOtherAmount.Columns["NO"] as GridViewDataColumn, "litGridNoOtherAmount") as Literal;
                litGridNoOtherAmount.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void LoadGridPopUpOtherAmount(object sender, EventArgs e)
        {
            gridOtherAmount.Visible = true;
            LitMsgOtherAmount.Text = "";
            panelOtherAmount.GroupingText = "List Other Amount";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            Literal litGridInvoiceNo = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "litGridInvoiceNo") as Literal;
            Literal litVendorCd = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["VENDOR_CD"] as GridViewDataColumn, "litVendorCd") as Literal;
            Literal litInvoiceDate = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["INVOICE_DATE"] as GridViewDataColumn, "litInvoiceDate") as Literal;
            LoadGridPopUpOtherInvoiceAmount(litGridInvoiceNo.Text, litVendorCd.Text, litInvoiceDate.Text);
            ModalPopupOtherAmount.Show();
        }
        #endregion

        protected void gridAttachment_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Button btnGridAttachmentAdd = gridAttachment.FindRowCellTemplateControl(e.VisibleIndex, gridAttachment.Columns["Action"] as GridViewDataColumn, "btnGridAttachmentAdd") as Button;
                Button btnGridAttachmentDel = gridAttachment.FindRowCellTemplateControl(e.VisibleIndex, gridAttachment.Columns["Action"] as GridViewDataColumn, "btnGridAttachmentDel") as Button;

                if (btnGridAttachmentAdd != null && btnGridAttachmentDel != null)
                {
                    btnGridAttachmentAdd.Enabled = CanAttach;
                    btnGridAttachmentDel.Enabled = CanAttach;

                    if (Session["CONVERT_FLAG"] != null && Session["CONVERT_FLAG"].ToString().Equals("1"))
                    {
                        btnGridAttachmentAdd.Visible = false;
                        btnGridAttachmentDel.Visible = false;
                    }
                    else
                    {
                        if (e.GetValue("SequenceNumber") == null || e.GetValue("SequenceNumber").ToString().Equals("0"))
                        {
                            btnGridAttachmentAdd.Visible = true;
                            btnGridAttachmentDel.Visible = false;
                        }
                        else
                        {
                            btnGridAttachmentAdd.Visible = false;
                            btnGridAttachmentDel.Visible = true;
                        }
                    }
                }
            }
        }

        private void ClearModal()
        {
            txtSearchCd.Text = "";
            txtSearchDesc.Text = "";
        }

        protected void btnClearPopUp_Click(object sender, EventArgs e)
        {
            ClearModal();
        }

        protected void btnSearchPopUp_Click(object sender, EventArgs e)
        {
            LoadGridPopUpVendorCd();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (gridPopUpVendorCd.Selection.Count >= 1)
            {
                txtVendorCode.Text = "";
                lblVendorName.Text = "";
                for (int i = 0; i <= gridPopUpVendorCd.VisibleRowCount; i++)
                {
                    if (gridPopUpVendorCd.Selection.IsRowSelected(i))
                    {
                        if (!txtVendorCode.Text.Contains(gridPopUpVendorCd.GetRowValues(i, "VENDOR_CD").ToString()))
                        {
                            txtVendorCode.Text += gridPopUpVendorCd.GetRowValues(i, "VENDOR_CD").ToString() + ";";
                            lblVendorName.Text += gridPopUpVendorCd.GetRowValues(i, "VENDOR_NAME").ToString().Trim() + ";";
                        }
                    }
                }
            }
            else
            {
                LitPopUpMessage.Text = _m.Message("MSTD00009WRN");
                return;
            }
            popUpList.Hide();
        }

        protected void btnClosePopUp_Click(object sender, EventArgs e)
        {
            popUpList.Hide();
        }

        protected void btnAddAttch_Click(object sender, EventArgs args)
        {
            cboxAttachmentCategory.SelectedItem = null;
            tboxAttachmentDescription.Text = "";
            LitMsgAttachment.Text = "";
            popdlgUploadAttachment.Show();
        }

        protected void btnDelAttch_Click(object sender, EventArgs args)
        {
            LitMsgAttachment.Text = "";
            Button btnDel = (Button)sender;
            GridViewDataRowTemplateContainer container = btnDel.NamingContainer as GridViewDataRowTemplateContainer;
            ASPxHyperLink lblGridFilename = gridAttachment.FindRowCellTemplateControl(container.VisibleIndex,
                            gridAttachment.Columns["FileName"] as GridViewDataColumn, "lblGridFilename") as ASPxHyperLink;
            Literal litGridSequenceNumber = gridAttachment.FindRowCellTemplateControl(container.VisibleIndex,
                            gridAttachment.Columns["SequenceNumber"] as GridViewDataColumn, "litGridSequenceNumber") as Literal;
            HiddenField hidGridPath = gridAttachment.FindRowCellTemplateControl(container.VisibleIndex,
                            gridAttachment.Columns["FileName"] as GridViewDataColumn, "hidGridPath") as HiddenField;
            HiddenField hidInvoiceNo = gridInvoice.FindRowCellTemplateControl(container.VisibleIndex,
                            gridInvoice.Columns["INVOICE_NO"] as GridViewDataColumn, "hidInvoiceNo") as HiddenField;
            try
            {
                if (ftp.Delete(hidGridPath.Value, lblGridFilename.Text))
                {
                    decimal seqNo = 0;
                    Decimal.TryParse(litGridSequenceNumber.Text, out seqNo);
                    if (logic.Invoice.DeleteAttachment(Session["REFF_NO"].ToString(), seqNo))
                    {
                        addNewRowToAttachment(hidInvoiceNo.Value, Session["REFF_NO"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LitMsgAttachment.Text = _m.SetMessage(ex.Message, "ERR");
            }
        }

        private DateTime? stringToDateTime(string strDate)
        {
            if (strDate != null)
            {
                string[] strFraction = strDate.Split('/');
                if ((strFraction != null) && (strFraction.Length == 3))
                {
                    int day = int.Parse(strFraction[0]);
                    int month = int.Parse(strFraction[1]);
                    int year = int.Parse(strFraction[2]);

                    DateTime date = new DateTime(year, month, day);
                    return date;
                }
            }
            return null;
        }
        #endregion
    }
}