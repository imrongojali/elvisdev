﻿using System;
using Common;
using Common.Function;

namespace ELVISDashBoard
{
    public partial class Default : System.Web.UI.Page
    {
        GlobalResourceData _globalResxData = new GlobalResourceData();
        private static string _ScreenID = "ELVIS_Default";
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect(AppSetting.LOGIN_PAGE);
            base.OnLoad(e);                       
        }
        public string LoginPage()
        {
            return AppSetting.LOGIN_PAGE;
        }
    }
}
