﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Function;
using Common.Data;
using System.Web.Security;

namespace ELVISDashboard
{
    public partial class TokenGenerator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FormsAuthentication.SetAuthCookie("First", false);
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    string Key = Request.QueryString[0];
                    if (Key.Trim() != "")
                    {
                        string TOKEN = new BusinessLogic.TokenLogic().GetToken(Key);
                        Response.Write(TOKEN);
                    }
                }
            }
        }
    }
}