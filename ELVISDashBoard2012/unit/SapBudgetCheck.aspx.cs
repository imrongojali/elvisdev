﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Data.SAPData;
using Common.Function;
using Common.Data._30PVFormList;
using Common;

namespace ELVISDashBoard.presentationHelper
{
    public partial class SapBudgetCheck : BaseCodeBehind
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RunButton_Click(object sender, EventArgs e)
        {
            if (UserData == null)
            {
               Response.Redirect("~/Default.aspx");

                OutputLabel.Text = "Login as Admin to access this page";
                return;
            }
            if (!UserData.Roles.Contains("ELVIS_ADMIN"))
            {
                OutputLabel.Text = "Non Admin user cannot budget check";
                return;
            }
            string r = "";
            try
            {
                PVApprovalData pv = logic.PVApproval.Search(DocNoText.Text, DocYearText.Text);
                if (pv == null)
                {
                    OutputLabel.Text = "enter existing PV";
                    return;
                }

                BudgetCheckInput b = new BudgetCheckInput()
                {
                    DOC_NO = pv.PV_NO,
                    DOC_YEAR = pv.PV_YEAR,
                    DOC_STATUS = OperationBox.SelectedValue.Int(0),
                    AMOUNT = pv.TOTAL_AMOUNT ?? 0,
                    WBS_NO = pv.BUDGET_NO
                };

                BudgetCheckResult o = logic.SAP.BudgetCheck(b, UserData.USERNAME);

                r = r + string.Format("  %/ [{0}][{1}] /% ", "DOC_NO", o.DOC_NO)
                       + string.Format("  %/ [{0}][{1}] /% ", "DOC_YEAR", o.DOC_YEAR)
                       + string.Format("  %/ [{0}][{1}] /% ", "STATUS", o.STATUS)
                       + string.Format("  %/ [{0}][{1}] /% ", "MESSAGE", o.MESSAGE)
                       + string.Format("  %/ [{0}][{1}] /% ", "ORIGINAL", o.ORIGINAL)
                       + string.Format("  %/ [{0}][{1}] /% ", "AVAILABLE", o.AVAILABLE)
                       + string.Format("  %/ [{0}][{1}] /% ", "AMOUNT", b.AMOUNT)
                       + string.Format("  %/ [{0}][{1}] /% ", "WBS_NO", b.WBS_NO)
                       ;


                r = r.Replace("[", "<td>").Replace("]", "</td>").Replace("%/", "\r\n<tr>").Replace("/%", "</tr>\r\n");
                r = "<table id=\"BudgetCheckResult\">\r\n" + r + "\r\n</table>";
            }
            catch (Exception ex)
            {
                r = ex.Message + "\r\n" +
                    LoggingLogic.Trace(ex);
                r = r.Replace("\r\n", "<br/>\r\n");
            }
            OutputLabel.Text = r;
        }
    }
}