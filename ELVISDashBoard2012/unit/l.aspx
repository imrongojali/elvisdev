﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="System.Web.UI.Page" Theme="" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Session Login Information</title>
    <script src="../App_Themes/BMS_Theme/Script/jquery.min.js" type="text/javascript"></script>
    
    <style type="text/css">
    .tab 
    {
        padding-left: 20px;
        display: block; 
    }
    td 
    {
        font-family: Consolas, Courier, Monospace;
        font-size: 12pt;        
    }
    td.val 
    {
        padding-left: 10px;
    }
    
    #SessionUserData 
    {
        width: 500px;
        display: block;
        clear: both;
        margin: 10px;        
    }
    
    .hide 
    {
        display: hidden;
    }
    
    </style>	
    
</head>
<body>
    <div>
        Page.Session <br />
		<div class="tab">
        <table cellpadding="2" cellspacing="0" border="0">
        <tr><td>SessionID </td><td>=</td><td class="val"> <%=Session.SessionID %></td></tr>		
        <tr><td>Timeout </td><td>=</td> <td class="val"><%=Session.Timeout %> minute(s)</td></tr>
        <tr><td>Mode </td><td>=</td><td class="val"> <%=Session.Mode %></td></tr>
        <tr><td>CookieMode </td><td>=</td><td class="val"> <%=Session.CookieMode %></td></tr>
        <tr><td>IsCookieLess </td><td>=</td><td class="val"> <%=Session.IsCookieless %></td></tr>        
        </table>
		</div>
        <hr />
        Session 
        <div class="tab">
        <table cellpadding="2" cellspacing="0" border="0" id="SessionVars">
            <tbody>
            <tr id="template0" class="hide">
            <td class="colnm"></td>
            <td class="colval"></td>
            </tr>
            </tbody>
       
        </table>
        </div>
    </div>

    <div id="SessionUserData">
        <input type="button" id="Ticking" value="Tick" />
        <input type="button" id="UserDataButton" value="UserData" />
        <input type="text" id="VarText" value="">
        <input type="button" id="GetSessionButton" value="Session" />
        <input type="button" id="GetHeapButton" value="Heap" />
        <div id="UserDataDiv">
            &nbsp;
        </div>
    </div>

    <div id="ErrDiv">&nbsp;
    </div>

    <script src="../src/l.js" type="text/javascript"></script>
</body>
</html>
