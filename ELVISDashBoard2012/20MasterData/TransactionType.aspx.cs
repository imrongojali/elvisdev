﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._20MasterData;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._20MasterData
{
    public partial class TransactionType : BaseCodeBehind
    {
        #region Init
        readonly TransactionTypeLogic myLogic = new TransactionTypeLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2001");
        
        
        private const string mySelf = "Transaction Type";

        bool IsEditing;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

        #region Getter Setter for Data List
        private List<TransactionTypeData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<TransactionTypeData>();
                }
                else
                {
                    return (List<TransactionTypeData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            lit = litMessage;
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            
            hidScreenID.Value = _ScreenID;
            
            if (!IsPostBack)
            {
                PrepareLogout();
                GenerateComboData(ddlTransaction, "TRANSACTION_TYPE", true);
                GenerateComboData(ddlTransactionLevel, "TRANSACTION_LEVEL", true);
                GenerateComboData(ddlGroup, "GROUP_CODE_TRANSACTION", true);
                
                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();
                AddEnterComponent(ddlTransaction);
                AddEnterComponent(txtStdWording);
                AddEnterAsSearch(btnSearch, _ScreenID);

             }
        }

        protected void RecordPerPageSelect_Init(object sender, EventArgs e)
        {
            RecordPerPage_Init(sender, e, gridGeneralInfo);
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);
            

            btnSearch.Visible = true && _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;

            btnAdd.Visible = true && _RoleLogic.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;
            
            btnClose.Visible = true;            
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data          
            if (e.RowType == GridViewRowType.Data)
            {

                string _tcode = Convert.ToString(gridGeneralInfo.GetRowValues(e.VisibleIndex, "TEMPLATE_CD"));
                string _gcode = Convert.ToString(gridGeneralInfo.GetRowValues(e.VisibleIndex, "GROUP_CD"));
                string _lcode = Convert.ToString(gridGeneralInfo.GetRowValues(e.VisibleIndex, "TRANSACTION_LEVEL_CD"));


                Literal litTemplate = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex, 
                    gridGeneralInfo.Columns["TEMPLATE_CD"] as GridViewDataColumn, "TemplateNameLiteral")
                    as Literal;
                
                Literal litGroup = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                    gridGeneralInfo.Columns["GROUP_CD"] as GridViewDataColumn, "GroupLiteral")
                    as Literal;
                Literal litTransactionLevel = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                    gridGeneralInfo.Columns["TRANSACTION_LEVEL_CD"] as GridViewDataColumn, "TransactionLevelLiteral")
                    as Literal;

                litTemplate.Text = myLogic.TemplateName(_tcode);
                litGroup.Text = myLogic.GroupName(_gcode);
                litTransactionLevel.Text = myLogic.TransactionLevelName(_lcode);
            }
            #endregion
            #region rowtype edit
            if (IsEditing)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    #region init
                    TextBox txtGridTransactionCode = 
                            gridGeneralInfo.FindEditRowCellTemplateControl(
                            gridGeneralInfo.Columns["TRANSACTION_CD"] as GridViewDataColumn
                            , "txtGridTransactionCode") as TextBox;
                    TextBox txtGridTransactionName = 
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["TRANSACTION_NAME"] as GridViewDataColumn, 
                        "txtGridTransactionName") as TextBox;
                    TextBox txtGridStdWording = 
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["STD_WORDING"] as GridViewDataColumn, 
                        "txtGridStdWording") as TextBox;
                    ASPxComboBox ddlTransactionLevel = 
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["TRANSACTION_LEVEL_CD"] as GridViewDataColumn, 
                        "ddlGridTransactionLevel") as ASPxComboBox;
                    ASPxComboBox ddlUploadTemplate = 
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["TEMPLATE_CD"] as GridViewDataColumn, 
                        "ddlUploadTemplate") as ASPxComboBox;
                    ASPxComboBox ddlGridGroup =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["GROUP_CD"] as GridViewDataColumn,
                        "ddlGridGroup") as ASPxComboBox;
                    ASPxCheckBox CloseCheck =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["CLOSE_FLAG"] as GridViewDataColumn,
                        "CloseCheck") as ASPxCheckBox;
                    ASPxCheckBox ServiceCheck =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["SERVICE_FLAG"] as GridViewDataColumn,
                        "ServiceCheck") as ASPxCheckBox;
                    ASPxCheckBox BudgetCheck =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["BUDGET_FLAG"] as GridViewDataColumn,
                        "BudgetCheck") as ASPxCheckBox;
                    ASPxCheckBox AttachCheck =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["ATTACHMENT_FLAG"] as GridViewDataColumn,
                        "AttachCheck") as ASPxCheckBox;
                    ASPxCheckBox ProdCheck =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["PRODUCTION_FLAG"] as GridViewDataColumn,
                        "ProdCheck") as ASPxCheckBox;
                    ASPxCheckBox CostCenterCheck =
                        gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["COST_CENTER_FLAG"] as GridViewDataColumn,
                        "CostCenterCheck") as ASPxCheckBox;

                    GenerateComboData(ddlTransactionLevel, "TRANSACTION_LEVEL", false);
                    GenerateComboData(ddlUploadTemplate, "TEMPLATE_CD");
                    GenerateComboData(ddlGridGroup, "GROUP_CODE_TRANSACTION");

                    #endregion
                    #region add
                    if (PageMode == Common.Enum.PageMode.Add)
                    {
                        txtGridTransactionCode.Focus();
                    }
                    #endregion
                    #region edit
                    else if (PageMode == Common.Enum.PageMode.Edit)
                    {
                        int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                        if (_VisibleIndex == e.VisibleIndex)
                        {
                            #region fill data edit
                            string _TransactionCode = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "TRANSACTION_CD"));
                            string _TransactionName = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "TRANSACTION_NAME"));
                            string _StdWording = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "STD_WORDING"));
                            string _Group = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "GROUP_CD"));
                            string _FTCode = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex,"FT_CD"));
                            string _TemplateCode = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "TEMPLATE_CD"));
                            string _TransactionLevel = gridGeneralInfo.GetRowValues(_VisibleIndex, "TRANSACTION_LEVEL_CD").str();
                            bool CLOSE_FLAG = gridGeneralInfo.GetRowValues(_VisibleIndex, "CLOSE_FLAG").str() == "1";
                            bool SERVICE_FLAG = gridGeneralInfo.GetRowValues(_VisibleIndex, "SERVICE_FLAG").str() == "1";
                            bool BUDGET_FLAG = gridGeneralInfo.GetRowValues(_VisibleIndex, "BUDGET_FLAG").str() == "1";
                            bool ATTACH_FLAG = gridGeneralInfo.GetRowValues(_VisibleIndex, "ATTACHMENT_FLAG").str() == "1";
                            string PRODUCTION_FLAG = gridGeneralInfo.GetRowValues(_VisibleIndex, "PRODUCTION_FLAG").str();
                            bool COST_CENTER_FLAG = gridGeneralInfo.GetRowValues(_VisibleIndex, "COST_CENTER_FLAG").str() == "1";
                            txtGridTransactionName.Focus();
                            txtGridTransactionCode.Text = _TransactionCode; txtGridTransactionCode.ReadOnly = true;
                            txtGridTransactionName.Text = _TransactionName;
                            txtGridStdWording.Text = _StdWording;

                            ddlGridGroup.Value = _Group;
                            ddlTransactionLevel.Value = _TransactionLevel;
                            ddlUploadTemplate.Value = _TemplateCode;

                            CloseCheck.Checked = CLOSE_FLAG;
                            ServiceCheck.Checked = SERVICE_FLAG;
                            BudgetCheck.Checked = BUDGET_FLAG;
                            AttachCheck.Checked = ATTACH_FLAG;
                            ProdCheck.Value = PRODUCTION_FLAG;
                            CostCenterCheck.Value = COST_CENTER_FLAG;                            
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            gridGeneralInfo.DataBind();
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = myLogic.Search(ddlTransaction.SelectedValue, txtStdWording.Text, ddlTransactionLevel.SelectedValue, ddlGroup.SelectedValue);
            
            e.Result = ListInquiry;
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            ddlTransaction.SelectedIndex = 0;
            txtStdWording.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = 
                    
                Nagging("MSTD00008CON", "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridGeneralInfo.Selection.Count == 0)
            {
                #region Error
                
                Nag("MSTD00009WRN");
                #endregion
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                List<string> listTransactionCode = new List<string>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        string TRANSACTION_CD = "";
                        TRANSACTION_CD = gridGeneralInfo.GetRowValues(i, "TRANSACTION_CD").ToString();
                        listTransactionCode.Add(TRANSACTION_CD);
                    }
                }

                if (listTransactionCode.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = myLogic.Delete(listTransactionCode);
                    if (!result)
                    {
                        
                        Nag("MSTD00014ERR");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        
                        Nag("MSTD00049INF",mySelf);
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
                GenerateComboData(ddlTransaction, "TRANSACTION_TYPE", true);
            }
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {            
            TextBox txtGridTransactionCode = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TRANSACTION_CD"] as GridViewDataColumn, 
                "txtGridTransactionCode") as TextBox;
            TextBox txtGridTransactionName = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TRANSACTION_NAME"] as GridViewDataColumn, 
                "txtGridTransactionName") as TextBox;
            TextBox txtGridStdWording = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["STD_WORDING"] as GridViewDataColumn, 
                "txtGridStdWording") as TextBox;
            ASPxCheckBox CloseCheck = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["CLOSE_FLAG"] as GridViewDataColumn, 
                "CloseCheck") as ASPxCheckBox;
            ASPxCheckBox ServiceCheck = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["SERVICE_FLAG"] as GridViewDataColumn, 
                "ServiceCheck") as ASPxCheckBox;
            ASPxCheckBox BudgetCheck = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["BUDGET_FLAG"] as GridViewDataColumn, 
                "BudgetCheck") as ASPxCheckBox;
            ASPxCheckBox AttachCheck = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["ATTACHMENT_FLAG"] as GridViewDataColumn, 
                "AttachCheck") as ASPxCheckBox;
            ASPxCheckBox ProdCheck = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["PRODUCTION_FLAG"] as GridViewDataColumn, 
                "ProdCheck") as ASPxCheckBox;
            ASPxCheckBox CostCenterCheck =
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["COST_CENTER_FLAG"] as GridViewDataColumn,
                "CostCenterCheck") as ASPxCheckBox;
            ASPxComboBox ddlTransactionLevel = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TRANSACTION_LEVEL_CD"] as GridViewDataColumn, 
                "ddlGridTransactionLevel") as ASPxComboBox;
            ASPxComboBox ddlUploadTemplate = 
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TEMPLATE_CD"] as GridViewDataColumn, 
                "ddlUploadTemplate") as ASPxComboBox;
            ASPxComboBox ddlGridGroup =
                gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["GROUP_CD"] as GridViewDataColumn,
                "ddlGridGroup") as ASPxComboBox;

            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    ErrorData Err = new ErrorData();
                    DoStartLog(resx.FunctionId("FCN_TRANSACTION_TYPE_MASTER"), "");
                    TransactionTypeData t = new TransactionTypeData()
                    {
                        TRANSACTION_CD = txtGridTransactionCode.Text.Int(),
                        TRANSACTION_NAME = txtGridTransactionName.Text,
                        STD_WORDING = txtGridStdWording.Text,
                        TRANSACTION_LEVEL_CD = ddlTransactionLevel.Value.Int(),
                        CLOSE_FLAG = GetFlagValue(CloseCheck),
                        SERVICE_FLAG = GetFlagValue(ServiceCheck),
                        BUDGET_FLAG = GetFlagValue(BudgetCheck),
                        ATTACHMENT_FLAG = GetFlagValue(AttachCheck),
                        TEMPLATE_CD  = ddlUploadTemplate.Value.Int(),
                        GROUP_CD = ddlGridGroup.Value.Int(),
                        PRODUCTION_FLAG = GetProdValue(ProdCheck),
                        COST_CENTER_FLAG = GetFlagValue(CostCenterCheck)
                        
                    };
                    bool result = myLogic.InsertTransactionType(t, UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        
                        Nag("MSTD00011INF", mySelf);
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Log("MSTD00011INF", "INF", "btnSaveDetail_Click", string.Format(resx.Message( "MSTD00011INF"), mySelf));
                    }
                    else
                    {
                        string msg="", msgCode="";
                        GetLastErrMessages(myLogic.LastErr, mySelf, txtGridTransactionCode.Text, "Add", ref msg, ref msgCode);
                        litMessage.Text = _m.SetMessage(msg, msgCode);
                        
                        Log(msgCode, "ERR", "btnSaveDetail_Click", msg);
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    DoStartLog(resx.FunctionId("FCN_TRANSACTION_TYPE_MASTER"), "");
                    TransactionTypeData t = new TransactionTypeData()
                    {
                        TRANSACTION_CD = txtGridTransactionCode.Text.Int(),
                        TRANSACTION_NAME = txtGridTransactionName.Text,
                        STD_WORDING = txtGridStdWording.Text,
                        TRANSACTION_LEVEL_CD = Convert.ToInt32(ddlTransactionLevel.Value),
                        CLOSE_FLAG = GetFlagValue(CloseCheck),  
                        SERVICE_FLAG = GetFlagValue(ServiceCheck), 
                        BUDGET_FLAG = GetFlagValue(BudgetCheck),
                        ATTACHMENT_FLAG = GetFlagValue(AttachCheck), 
                        PRODUCTION_FLAG = GetProdValue(ProdCheck), 
                        COST_CENTER_FLAG = GetFlagValue(CostCenterCheck),
                        TEMPLATE_CD = (Convert.ToInt32(ddlUploadTemplate.Value)),
                        GROUP_CD = ddlGridGroup.Value.Int()
                    };
                    bool result = myLogic.EditTransactionType(t, UserData);

                    if (result == true)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        Nag("MSTD00013INF", mySelf);
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Log("MSTD00013INF", "INF", "btnSaveDetail_Click", string.Format(resx.Message( "MSTD00013INF"), mySelf));
                    }
                    else
                    {
                        Nag("MSTD00012ERR", mySelf);
                        Log("MSTD00012ERR", "INF", "btnSaveDetail_Click", string.Format(resx.Message( "MSTD00012ERR"), mySelf));
                    }
                }
                #endregion
            }
            GenerateComboData(ddlTransaction, "TRANSACTION_TYPE", true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                
                gridGeneralInfo.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {                
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                            IsEditing = true;
                            Session["visibleIndex"] = i;
                            gridGeneralInfo.StartEdit(i);
                            gridGeneralInfo.Selection.UnselectAll();
                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    
                    Nag("MSTD00009WRN");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    
                    Nag("MSTD00016WRN");
                }
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        #region export to excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
                string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                UserData userdata = (UserData)Session["UserData"];
                string _FileName = "TRANSACTION_TYPE" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                this.Response.Clear();
                this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                this.Response.Charset = "";
                this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                this.Response.ContentType = "application/vnd.ms-excel";
                try
                {
                    this.Response.BinaryWrite(myLogic.Get(UserName, imagePath, ListInquiry));
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                        
                    litMessage.Text = Nagging("MSTD00002ERR", ex.Message);
                }
                this.Response.End();
               
            }
            else
                Nag("MSTD00018INF");                
        }
        #endregion

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;
            
            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;
            
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;

            setSearchEnabled(false);
            
            btnClose.Enabled = false;
            
        }
        
        protected void SetCancelEditDetail()
        {
            btnClose.Enabled = true;
            
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData Err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref Err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref Err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;                
                btnExportExcel.Visible = false;
            }
            setSearchEnabled();
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        } 

        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            StringBuilder s = new StringBuilder("");

            //REGION FOR VALIDATING INPUT SAVE
            TextBox txtGridTransactionCode = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TRANSACTION_CD"] as GridViewDataColumn, "txtGridTransactionCode") as TextBox;
            TextBox txtGridTransactionName = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TRANSACTION_NAME"] as GridViewDataColumn, "txtGridTransactionName") as TextBox;
            TextBox txtGridStdWording = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["STD_WORDING"] as GridViewDataColumn, "txtGridStdWording") as TextBox;

            if (txtGridTransactionCode.Text.Equals(string.Empty))
            {
                s.AppendLine(Nagging("MSTD00017WRN", "Transaction Code"));
            }
            else {
                int tt = -1;
                if (!Int32.TryParse(txtGridTransactionCode.Text, out tt)) {
                    s.AppendLine(Nagging("MSTD00032WRN", "Transaction Code", "between 1 to 2147483647"));
                }                
            }

            if (txtGridTransactionName.Text.Equals(string.Empty))
            {
                s.AppendLine(Nagging("MSTD00017WRN", "Transaction Name"));
            }
            if (txtGridStdWording.Text.Equals(string.Empty))
            {
                s.AppendLine(Nagging("MSTD00017WRN", "Standard Wording"));
            }
            

            if (s.Length > 0)
            {
                litMessage.Text = s.ToString();
            }
            return (s.Length<1);            
        }

        
        private void setSearchEnabled(bool can=true)
        {
            ddlTransaction.Enabled = can;
            txtStdWording.Enabled = can;
        }

        private bool ValidateInputSearch()
        {
            return true;
        }
        #endregion

        #region paging
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }

        }

        protected override void Rebind()
        {
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
            
        }
        protected void RecordPerPageSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecordPerPage_Selected(sender, e, gridGeneralInfo);
        }

        protected int GetProdValue(ASPxCheckBox c)
        {
            int v = 0;
            switch(c.CheckState)
            {
                case DevExpress.Web.ASPxClasses.CheckState.Checked: v = 1; break;
                case DevExpress.Web.ASPxClasses.CheckState.Unchecked: v = 2; break;
                default: v = 0; break;
            }
            return v;
        }

        protected int? GetFlagValue(ASPxCheckBox c)
        {
            int? v = null;
            switch (c.CheckState)
            {
                case DevExpress.Web.ASPxClasses.CheckState.Checked: v = 1; break;
                case DevExpress.Web.ASPxClasses.CheckState.Unchecked: v = 0; break;
                default: v =null; break;
            }
            return v;

        }

        #endregion

    }
}
