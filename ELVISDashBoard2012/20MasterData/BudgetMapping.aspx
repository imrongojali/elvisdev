﻿<%@ Page Title="Budget Mapping" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="BudgetMapping.aspx.cs" Inherits="ELVISDashBoard._20MasterData.BudgetMapping" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xPre" ContentPlaceHolderID="pre" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Budget Mapping Master Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="row">
                    <label>
                        Transaction Code</label>
                    <div class="rowwarphalfLeft">
                        <asp:DropDownList runat="server" ID="ddlTransaction" AutoPostBack="false" />
                    </div>
                    <label>
                        WBS Code</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxGridLookup runat="server" ID="lookWbs" ClientInstanceName="lookWbs" AutoPostBack="false"
                            SelectionMode="Single" Width="250px" KeyFieldName="WBS_CODE" TextFormatString="{0} [{1}]"
                            OnTextChanged="lookWbs_TextChanged" DataSourceID="vwWBSCodeDs" IncrementalFilteringDelay="150"
                            IncrementalFilteringMode="Contains">
                            <GridViewProperties EnableCallBacks="false">
                                <SettingsBehavior AllowSelectByRowClick="true" AllowSelectSingleRowOnly="true" EnableRowHotTrack="true"
                                    AllowSort="true" />
                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" />
                                <SettingsPager PageSize="10" />
                            </GridViewProperties>
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                <dx:GridViewDataTextColumn Caption="WBS Code" FieldName="WBS_CODE" />
                                <dx:GridViewDataTextColumn Caption="WBS Name" FieldName="WBS_CODE_NAME" />
                            </Columns>
                        </dx:ASPxGridLookup>
                    </div>
                </div>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" OnClientClick="loading()" />
                    </div>
                </div>
            </div>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click"
                        OnClientClick="loading()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                KeyFieldName="BUDGET_ITEM" OnCustomCallback="grid_CustomCallback" EnableCallBacks="False"
                OnCustomUnboundColumnData="grid_CustomUnbound" OnPageIndexChanged="gridGeneralInfo_PageIndexChanged"
                Styles-AlternatingRow-CssClass="even">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="BUDGET_ITEM" Visible="false" UnboundType="String" />
                    <dx:GridViewDataColumn FieldName="TRANSACTION_CD" Visible="false" />
                    <dx:GridViewDataColumn FieldName="WBS_CODE" Visible="false">                    
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" Width="30px">
                        <DataItemTemplate>
                            <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                        </DataItemTemplate>
                        <EditItemTemplate />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Transaction Name" FieldName="TRANSACTION_NAME"
                        VisibleIndex="2" Width="160px">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridTransactionCode" Width="154px">
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="WBS Code Name" FieldName="WBS_CODE_NAME" Width="200px"
                        VisibleIndex="3" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>                            
                            <dx:ASPxGridLookup runat="server" ID="lookGridWbs" ClientInstanceName="lookGridWbs"
                                AutoPostBack="false" SelectionMode="Single" Width="194px" KeyFieldName="WBS_CODE"
                                TextFormatString="{0} [{1}]" 
                                OnTextChanged="lookGridWbs_TextChanged"
                                IncrementalFilteringDelay="150" IncrementalFilteringMode="Contains"
                                DataSourceID="vwWBSCodeDs">
                                <GridViewProperties EnableCallBacks="false">
                                    <SettingsBehavior AllowSelectByRowClick="true" AllowSelectSingleRowOnly="true" EnableRowHotTrack="true"
                                        AllowSort="true" />
                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" />
                                    <SettingsPager PageSize="10" />
                                </GridViewProperties>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                    <dx:GridViewDataTextColumn Caption="WBS Code" FieldName="WBS_CODE" />
                                    <dx:GridViewDataTextColumn Caption="WBS Name" FieldName="WBS_CODE_NAME" />
                                </Columns>
                            </dx:ASPxGridLookup>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Valid From" FieldName="VALID_FROM" Width="110px"
                        VisibleIndex="4">
                        <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}" />
                        <EditItemTemplate>
                            <dx:ASPxDateEdit ID="dtGridValidFrom" runat="server" AllowUserInput="true" Width="104px"
                                EditFormat="Date" EditFormatString="dd/MM/yyyy" />
                        </EditItemTemplate>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn Caption="Valid To" FieldName="VALID_TO" Width="110px"
                        VisibleIndex="5">
                        <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}" />
                        <EditItemTemplate>
                            <dx:ASPxDateEdit ID="dtGridValidTo" runat="server" AllowUserInput="true" Width="104px"
                                EditFormat="Date" EditFormatString="dd/MM/yyyy" />
                        </EditItemTemplate>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewBandColumn Caption="Created" VisibleIndex="6">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0"
                                Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1" Width="100px"
                                ReadOnly="true">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Changed" VisibleIndex="7">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CHANGED_DT" VisibleIndex="0"
                                Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1" Width="100px"
                                ReadOnly="true">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <AlternatingRow CssClass="even">
                    </AlternatingRow>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible" />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                        <div style="text-align: left;">
                            Records per page:
                            <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                <option value="5" <%# WriteSelectedIndex(5) %>>5</option>
                                <option value="10" <%# WriteSelectedIndex(10) %>>10</option>
                                <option value="15" <%# WriteSelectedIndex(15) %>>15</option>
                                <option value="20" <%# WriteSelectedIndex(20) %>>20</option>
                                <option value="25" <%# WriteSelectedIndex(25) %>>25</option>
                            </select>&nbsp;
                             <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">
                                &lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a>
                            &nbsp; Page
                            <input type="text" onchange="gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                style="width: 20px" />
                            of
                            <%# gridGeneralInfo.PageCount%>&nbsp;
                            <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%> 
                             <a title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp;
                            <a title="Last" href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">
                                &gt;&gt;</a> &nbsp;
                        </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new (TRANSACTION_CD, TRANSACTION_NAME, WBS_CODE, WBS_CODE_NAME, VALID_FROM, VALID_TO, CREATED_DT, CREATED_BY, CHANGED_DT, CHANGED_BY)"
                OnSelecting="LinqDataSource1_Selecting" TableName="vw_BudgetMapping">
            </asp:LinqDataSource>
            <asp:LinqDataSource ID="vwWBSCodeDs" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                TableName="vw_WBS_Code" Select="new(WBS_CODE, WBS_CODE_NAME)" />
            <div class="rowbtn">
                <div class="btnleft">
                    <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                        SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>
                <div class="btnright">
                    <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click"
                        OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click"
                        OnClientClick="loading()" />&nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
</asp:Content>
