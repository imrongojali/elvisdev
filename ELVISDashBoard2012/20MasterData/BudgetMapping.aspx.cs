﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._20MasterData;
using BusinessLogic.CommonLogic;
using Common.Data;
using Common.Data._20MasterData;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using ELVISDashBoard.presentationHelper;


namespace ELVISDashBoard._20MasterData
{
    public partial class BudgetMapping : BaseCodeBehind
    {
        #region Init
        private const string mySelf = "Budget Mapping";
        private const string logID = "FCN_BUDGET_MAPPING";
        readonly BudgetMappingLogic myLogic = new BudgetMappingLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2006");

        readonly MessageLogic _message = new MessageLogic();
        
        bool IsEditing;

        private static readonly string[] aFields = { "TRANSACTION_NAME", "WBS_CODE_NAME", "VALID_FROM", "VALID_TO"};
        private static readonly string[] aColumns = { "ddlGridTransactionCode", "lookGridWbs", "dtGridValidFrom", "dtGridValidTo"};
        private object[] aGrid = new object[aFields.Length];

        private enum me : int
        {
            TRANSACTION_CD = 0,
            WBS_CODE_NAME = 1,
            VALID_FROM = 2,
            VALID_TO = 3            
        };

      

        private string wbsCode
        {
            set
            {
                ViewState["WBS_CODE"] = value;
            }
            get
            {
                if (!(ViewState["WBS_CODE"] == null))
                {
                    return Convert.ToString(ViewState["WBS_CODE"]);
                }
                else return null;

            }
        }

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

        #region Getter Setter for Data List
        private List<BudgetMappingData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<BudgetMappingData>();
                }
                else
                {
                    return (List<BudgetMappingData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;

            if (!IsPostBack)
            {
                PrepareLogout();
                GenerateComboData(ddlTransaction, "TRANSACTION_CD", true);
                
                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();
                AddEnterComponent(ddlTransaction);
                AddEnterComponent(lookWbs);
                
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);

            btnSearch.Visible = true && _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;

            btnAdd.Visible = true && _RoleLogic.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;
            btnClose.Visible = true;
        }

    
        #endregion

        #region Grid

        private bool GetIntFromObject(ref int ifield, object o)
        {
            int v = 0;
            if (o != null && Int32.TryParse(o.ToString(), out v))
            {
                ifield = v;
                return true;
            }
            else
                return false;
        }

        private BudgetMappingData Gather(int row)
        {
            BudgetMappingData r = new BudgetMappingData();
            int iCode = 0;            
            object _wbsNo = "";
            object _nf = "";
            if (row >= 0)
            {
                if (GetIntFromObject(ref iCode, gridGeneralInfo.GetRowValues(row, aFields[(int)me.TRANSACTION_CD])))
                    r.TRANSACTION_CD = iCode;
                else if (GetIntFromObject(ref iCode, gridGeneralInfo.GetRowValues(row, "TRANSACTION_CD")))
                    r.TRANSACTION_CD = iCode;
                r.WBS_CODE = Convert.ToString( gridGeneralInfo.GetRowValues(row, "WBS_CODE"));
                wbsCode = r.WBS_CODE;               
                r.VALID_FROM = Convert.ToDateTime(gridGeneralInfo.GetRowValues(row, aFields[(int)me.VALID_FROM]));
                r.VALID_TO= Convert.ToDateTime(gridGeneralInfo.GetRowValues(row, aFields[(int)me.VALID_TO]));                        
            }

            else
            {
                if (GetIntFromObject(ref iCode, (aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox).Value))
                    r.TRANSACTION_CD = iCode;
                
                r.WBS_CODE = Convert.ToString((aGrid[(int)me.WBS_CODE_NAME] as ASPxGridLookup).Value);
                if (String.IsNullOrEmpty(r.WBS_CODE))
                {
                    r.WBS_CODE = wbsCode;
                }
                else {
                    wbsCode = r.WBS_CODE;
                }
                r.VALID_FROM = Convert.ToDateTime((aGrid[(int)me.VALID_FROM] as ASPxDateEdit).Date);
                r.VALID_TO = Convert.ToDateTime((aGrid[(int)me.VALID_TO] as ASPxDateEdit).Date);
            }
            return r;
        }

        private void Scatter(BudgetMappingData x)
        {
            (aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox).Value = Convert.ToString(x.TRANSACTION_CD);
            wbsCode = x.WBS_CODE;
            (aGrid[(int)me.WBS_CODE_NAME] as ASPxGridLookup).Value = x.WBS_CODE;
            
            if (x.VALID_FROM != null)
                (aGrid[(int)me.VALID_FROM] as ASPxDateEdit).Date = (DateTime)x.VALID_FROM;
            if (x.VALID_TO != null)
                (aGrid[(int)me.VALID_TO] as ASPxDateEdit).Date = (DateTime) x.VALID_TO;
        }

        private void Mark()
        {
            for (int i = 0; i < (aFields.Length); i++)
            {
                Object o = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns[aFields[i]] as GridViewDataColumn, aColumns[i]);

                if (o == null)
                    logic.Say("Mark", "ERR:{2} cannot find field {0} in grid of column {1}", aFields[i], aColumns[i], i);
                else
                {
                    aGrid[i] = o;
                }
            }
        }

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data
            
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoGeneralInfo = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex, gridGeneralInfo.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
            #endregion
            #region rowtype edit
            if (!IsEditing || e.RowType != GridViewRowType.InlineEdit) return;

            #region init
            Mark();

            ASPxComboBox key1 = aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox;
            ASPxGridLookup key2 = aGrid[(int)me.WBS_CODE_NAME] as ASPxGridLookup;            
            
            GenerateComboData(key1, "TRANSACTION_CD", false);
            

            #endregion
            #region add
            if (PageMode == Common.Enum.PageMode.Add)
            {
                key1.Focus();
            }
            #endregion
            #region edit
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                if (_VisibleIndex == e.VisibleIndex)
                {
                    #region fill data edit
                    BudgetMappingData g = Gather(_VisibleIndex);

                    Scatter(g);

                    key1.ReadOnly = true;
                    key2.ReadOnly = true;
                    key1.DropDownButton.Visible = false;
                    key1.Enabled = false;

                    key2.DropDownButton.Enabled = false;
                    key2.DropDownButton.Visible = false;
                    key2.Enabled = false;
                    #endregion
                }
            }
            #endregion


            #endregion
        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            gridGeneralInfo.DataBind();
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = myLogic.SearchInqury(ddlTransaction.SelectedValue, Convert.ToString(lookWbs.Value));
            e.Result = ListInquiry;
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            ddlTransaction.SelectedIndex = 0;
            lookWbs.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = string.Format(resx.Message( "MSTD00008CON"), "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridGeneralInfo.Selection.Count == 0)
            {
                #region Error
                Err("MSTD00009WRN", "WRN", "delete");
                #endregion
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                List<BudgetMappingData> l = new List<BudgetMappingData>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        BudgetMappingData g = Gather(i);
                        l.Add(g);
                    }
                }

                if (l.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = myLogic.Delete(l);
                    if (!result)
                    {
                        Err("MSTD00014ERR", "ERR", "ConfirmDelete");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        Err("MSTD00049INF", "INF", "ConfirmDelete", mySelf);
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
                GenerateComboData(ddlTransaction, "TRANSACTION_CD", true);                
            }
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            Mark();
            
            BudgetMappingData g = Gather(-1);

            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    ErrorData err = new ErrorData();
                    DoStartLog(resx.FunctionId(logID), "");
                    bool result = myLogic.InsertRow(g, UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Err("MSTD00011INF", "INF", "btnSaveDetail_Click", mySelf);
                    }
                    else
                    {
                        string msg = "", msgCode = "";
                        GetLastErrMessages(myLogic.LastErr, mySelf,
                           String.Format("{0} {1}", new object[] { g.TRANSACTION_NAME, g.WBS_CODE }),
                           "Add", ref msg, ref msgCode);
                        litMessage.Text = _message.SetMessage(msg, msgCode);
                        Log(msgCode, "ERR", "btnSaveDetail_Click", msg);
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    DoStartLog(resx.FunctionId(logID), "");
                    bool result = myLogic.EditRow(g, UserData);

                    if (result == true)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Err("MSTD00013INF", "INF", "btnSaveDetail_Click", mySelf);
                    }
                    else
                    {
                        Err("MSTD00012ERR", "INF", "btnSaveDetail_Click", mySelf);
                    }
                }
                #endregion
            }
            GenerateComboData(ddlTransaction, "TRANSACTION_CD", true);        
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;

                gridGeneralInfo.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }

        }

        protected void setSearchEnabled(bool can = true)
        {
            ddlTransaction.Enabled = can;
            lookWbs.Enabled = can;             
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                            IsEditing = true;
                            Session["visibleIndex"] = i;
                            gridGeneralInfo.StartEdit(i);
                            gridGeneralInfo.Selection.UnselectAll();

                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    Err("MSTD00009WRN", "WRN", "");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    Err("MSTD00016WRN", "WRN", "");
                }
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        #region export to excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
                
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    
                    string _FileName = "BUDGET_MAPPING" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    this.Response.Clear();
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    try
                    {
                        this.Response.BinaryWrite(myLogic.Get(UserName, imagePath, ListInquiry));
                    }
                    catch (Exception ex)
                    {
                        log.Log("MSTD00018INF", ex.Message, "btnExportExcel_Click");                        
                    }
                    this.Response.End();
                
            }
            else
                Err("MSTD00018INF", "ERR", "ExportExcel");
        }
        #endregion

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;

            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
            setSearchEnabled(false);
        }

        protected void SetCancelEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
                btnExportExcel.Visible = false;
            }
            setSearchEnabled();
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        }

        #endregion


        #region Lookup
        protected void lookWbs_TextChanged(object sender, EventArgs arg)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            string code = Convert.ToString(lookup.Value);
        }

        protected void lookGridWbs_TextChanged(object sender, EventArgs arg)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            string code = Convert.ToString(lookup.Value);           
        }      
        #endregion

        #region Function


        private bool ValidateInputSave()
        {
            bool code = true;
            Mark();

            //REGION FOR VALIDATING INPUT SAVE

            ASPxComboBox key1 = (aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox);
            ASPxGridLookup key2 = (aGrid[(int)me.WBS_CODE_NAME] as ASPxGridLookup);
            ASPxDateEdit validFrom = (aGrid[(int)me.VALID_FROM] as ASPxDateEdit);
            ASPxDateEdit validTo = (aGrid[(int)me.VALID_TO] as ASPxDateEdit);

            if (key1.Value == null || ((key1.Value != null) && String.IsNullOrEmpty(key1.Value.ToString())))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "Transaction Code");
                code = false;
            }

            if (String.IsNullOrEmpty(wbsCode))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "WBS Code");
                code = false;
            }

            if (validFrom.Value == null) { 
                Err("MSTD00017WRN", "WRN", "Validate", "Valid From");
                code = false;            
            }

            if (validTo.Value == null)
            {
                Err("MSTD00017WRN", "WRN", "Validate", "Valid To");
                code = false;
            }

            if ( Convert.ToDateTime(validFrom.Value) >  Convert.ToDateTime(validTo.Value))
            {
                DateTime t = Convert.ToDateTime(validTo.Value);
                validTo.Value = validFrom.Value;
                validFrom.Value = t;

                Err("MSTD00029ERR", "ERR", "Validate", "Valid From", "Valid To");
                code = false;                
            }
            
            return code;
        }

        private bool ValidateInputSearch()
        {
            return true;
        }

        private void Err(string code, string msgType, string Where, string Why = null, string Why2 = null)
        {
            if (String.IsNullOrEmpty(Why))
                litMessage.Text += _message.SetMessage(resx.Message( code), code) + Environment.NewLine;
            else
                litMessage.Text += _message.SetMessage(string.Format(resx.Message( code), Why, Why2), code) + Environment.NewLine;

            if (!msgType.Equals("WRN"))
                Log(code, msgType, Where, string.Format(resx.Message( code), Why, Why2));
        }

        #endregion

        #region paging
        // const string GridCustomPageSizeName = "gridCustomPageSize";
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }
        protected void grid_CustomUnbound(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "BUDGET_ITEM")
            {
                e.Value = Convert.ToString(e.GetListSourceFieldValue("TRANSACTION_CD")) + "_" + Convert.ToString(e.GetListSourceFieldValue("WBS_CODE"));
            }            
        }

        protected void grid_CustomCallback(object sender,
    DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridGeneralInfo.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
        }
        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        //protected string GetShowingOnPage()
        //{
        //    int pageSize = gridGeneralInfo.SettingsPager.PageSize;
        //    if (gridGeneralInfo.PageIndex < 0)
        //        gridGeneralInfo.PageIndex = 0;
        //    int startIndex = gridGeneralInfo.PageIndex * pageSize + 1;
        //    int endIndex = (gridGeneralInfo.PageIndex + 1) * pageSize;
        //    if (endIndex > gridGeneralInfo.VisibleRowCount)
        //    {
        //        endIndex = gridGeneralInfo.VisibleRowCount;
        //    }
        //    return string.Format("Showing {0}-{1} of {2}", startIndex, endIndex, gridGeneralInfo.VisibleRowCount);
        //}
        #endregion
    }
}