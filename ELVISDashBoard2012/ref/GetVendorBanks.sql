-- GetVendorBanks

---- DECLARE @VendorCode varchar(10) = '0000250041' -- 0000200011' --0000250041
----	 , @Key varchar(15) = 'BCA278'

DECLARE 
    @@VENDOR_CD VARCHAR(10) = NULLIF(RTRIM(LTRIM(@VendorCode)), '')
  , @@KEY VARCHAR(15) = NULLIF(@Key,'') 

SELECT [VENDOR_CD]              [VendorCode]
      ,[CTRY]                   [Country] 
      ,[BANK_KEY]               [Key] 
      ,[BANK_ACCOUNT]           [Account]
      ,LTRIM(RTRIM([BANK_TYPE]))[Type]
      ,NULLIF(ACCT_HOLDER,'')   [AccountHolder]
      ,NULLIF(NAME_OF_BANK, '') [Name]
      ,[CURRENCY]               [Currency] 
  FROM SAP_DB.dbo.TB_M_VENDOR_BANK
 WHERE (@@VENDOR_CD IS NOT NULL AND  LTRIM(RTRIM(VENDOR_CD)) = @@VENDOR_CD)
   AND (@@KEY IS NULL OR BANK_KEY = @@KEY) 