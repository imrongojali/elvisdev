-- getExchangeRates
select LTRIM(RTRIM(v.CURRENCY_CD)) [CurrencyCode],
	v.EXCHANGE_RATE [Rate], 
	v.VALID_FROM [ValidFrom] 
from vw_ExchangeRate v