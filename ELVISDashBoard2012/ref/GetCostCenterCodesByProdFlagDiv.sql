-- GetCostCenterCodesByProdFlagDiv

SELECT LTRIM(RTRIM(COST_CENTER)) AS [Code], [DESCRIPTION] as [Description] FROM dbo.vw_CostCenter
WHERE ((NULLIF(@div, '') IS NULL) OR DIVISION = @div)
AND   ((@prodFlag IS NULL) OR PRODUCTION_FLAG = @prodFlag);