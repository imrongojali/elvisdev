-- GetListNoticeByDocNoYear 

SELECT
	NOTICE_DT AS [Date]
	, NOTICE_COMMENT AS [Message]
	, REPLY_DT AS [ReplyDate]
	, NOTICE_FROM_NAME AS [SenderName]
	, USER_ROLE_FROM AS [SenderRole]
	, NOTICE_TO_NAME AS [ReceiverName]
	, USER_ROLE_TO AS [ReceiverRole]
	, NOTICE_NO AS [SeqNumber]
	, CASE WHEN ISNULL(REPLY_FLAG, 0) = 1 THEN 1 ELSE 0 END AS [ToBeReplied]
FROM dbo.vw_Notice v
WHERE v.DOC_NO = @p1
AND v.DOC_YEAR = @p2
AND (
	((v.NOTICE_DT IS NOT NULL) AND (v.REPLY_DT IS NULL))
	 OR
	((v.REPLY_DT IS NOT NULL) AND (v.USER_ROLE_TO IS NOT NULL))
	)
ORDER BY CREATED_DT DESC