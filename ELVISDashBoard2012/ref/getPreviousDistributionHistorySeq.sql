-- getPreviousDistributionHistorySeq

     SELECT h.max_seq_no
       FROM (SELECT 1 AS y) x
  LEFT JOIN (SELECT reff_no
                    , MAX(seq_no) max_seq_no
             FROM tb_h_distribution_status
             WHERE reff_no = @p1
             GROUP BY reff_no) h ON 1 = 1