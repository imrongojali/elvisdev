-- GetSapDocNoByBookNo
SELECT DISTINCT
	trsdn.CURRENCY_CD,
	trsdn.INVOICE_NO ,
	trsdn.SAP_DOC_NO ,
	trsdn.SAP_CLEARING_DOC_NO ,
	trsdn.PAYPROP_DOC_NO ,
	trsdn.PAYPROP_ID
FROM
	TB_R_ACCR_DOC_NO tradn
JOIN TB_R_ACCR_BALANCE trab ON tradn.ACCR_NO = trab.BOOKING_NO 
JOIN TB_R_SAP_DOC_NO trsdn ON (@accrNo != '' and tradn.ACCR_DOC_NO = trsdn.DOC_NO) 
							or (@accrNo = '' and tradn.ACCR_DOC_NO = trsdn.DOC_NO AND trab.SAP_DOC_NO_CLOSE = trsdn.SAP_DOC_NO)
LEFT JOIN TB_R_ACCR_LIST_D trald ON trald.PV_TYPE_CD = 2 OR (trald.PV_TYPE_CD != 2 AND trsdn.ITEM_NO = trald.SAP_ITEM_NO)
WHERE
	tradn.ACCR_NO = @bookNo
	AND (@accrNo = '' OR trald.ACCRUED_NO = @accrNo)