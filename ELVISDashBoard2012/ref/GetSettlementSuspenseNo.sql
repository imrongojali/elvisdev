-- GetSettlementSuspenseNo
SELECT TOP 1 SUSPENSE_NO 
FROM TB_R_PV_H 
WHERE PV_YEAR = @pv_year 
AND PV_NO = @pv_no
AND PV_TYPE_CD = 3 