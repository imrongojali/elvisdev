-- GetIsGLAccountProject
SELECT    
	 COUNT(GLAP.GL_ACC) IS_GL_ACCOUNT_PROJECT
FROM TB_M_GL_ACCOUNT_MAPPING  M
LEFT JOIN (SELECT LTRIM(RTRIM(SYSTEM_VALUE_TXT)) GL_ACC FROM TB_M_SYSTEM 
WHERE SYSTEM_TYPE = 'GL_ACCOUNT_PROJECT') GLAP ON GLAP.GL_ACC= M.GL_ACCOUNT
WHERE M.ITEM_TRANSACTION_CD = 1  AND M.TRANSACTION_CD = @p1
GROUP BY M.TRANSACTION_CD, M.GL_ACCOUNT