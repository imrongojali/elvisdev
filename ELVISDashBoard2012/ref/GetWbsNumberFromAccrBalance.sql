-- GetWbsNumberFromAccrBalance
SELECT distinct w.[WbsNumber], w.[Description] 
FROM dbo.[TB_R_ACCR_BALANCE] a 
inner join dbo.[vw_WBS] w on a.[WBS_NO_OLD] = w.[WbsNumber] 
where a.[booking_no] = @bookingID