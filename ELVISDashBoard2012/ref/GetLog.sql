-- GetLog

/*
use elvis_db
go

declare
    @id int = 0
  , @limit int = -1
  , @start int = 1
  , @filter varchar(800) = null--'msgtype=ERR' -- null -- 'Who=sulihati;remarks=;what=test;Where=this'
*/

DECLARE
    @@id INT = NULLIF(CASE WHEN ISNUMERIC(@id)=1 then CONVERT(INT, @id) else 0 end, 0)
  , @@limit INT = ISNULL(case when isnumeric(@limit)=1 then convert(int, @limit) else 0 end,0)
  , @@start int = ISNULL(case when isnumeric(@start)=1 then convert(int, @start) else 0 end,0)
  , @@filter VARCHAR(800) =NULLIF( @filter,'')
  , @@datum DATETIME = NULL
  , @@counts INT = 0

SELECT
  @@datum=CASE WHEN ID.IS_DATUM=1 THEN CONVERT(DATETIME, @@filter) ELSe NULL END
, @@filter = CASE WHEN ID.IS_DATUM=1 THEN NULL ELSE @@filter END
, @@limit = id.lim
, @@start = id.ofs
FROM
    (
        SELECT ISDATE(@@filter) IS_DATUM
            ,   CASE WHEN @@limit = 0   then 50
                     WHEN @@limit > 65535 then 65535
                ELSE @@limit end [LIM]
            ,   CASE WHEN @@start <= 0  then 1
                ELSE @@start END [OFS]
    ) ID;

IF @@datum is not null
begin
    select @@datum = dateadd(day, 0, datediff(day, 0, @@datum));
end

declare
    @@Where Varchar(50) = null, @@WhereLike int = 0,
    @@Who VARCHAR(50) = NULL, @@WhoLike int = 0,
    @@Whats VARCHAR(200) = NULL, @@WhatsLike int = 0,
    @@REMARKS VARCHAR(50)= NULL, @@RemarksLike int = 0,
    @@MsgId VARCHAR(50) = NULL, @@MsgIdLike int = 0,
    @@MsgType VARCHAR(10) = NULL, @@msgTypeLike int = 0,
    @@WhenDt DATETIME = null, @@filterLike int = 0

IF (CHARINDEX('=', @@filter) >1)
begin
    declare @@ct TABLE(KY VARCHAR(15), VAL VARCHAR(200));
    INSERT INTO @@CT(KY,VAL)
    select
        substring(items, 1, charindex('=', items)-1) ky,
        substring(items, charindex('=', items)+1, datalength(items) - charindex('=', items) ) val
     from dbo.fn_explode(';', @@filter) c

     select TOP 1 @@Where = c.val
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'Where';

     select TOP 1 @@Who = c.val
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'Who';

     select TOP 1 @@Whats = c.val
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'What';

     select TOP 1 @@Remarks = c.val
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'Remarks';

     select TOP 1 @@MsgId = c.val
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'MsgId';

     select TOP 1 @@MsgType = c.val
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'MsgType';

     select TOP 1 @@WhenDt = case when isdate(c.val)=1 then convert(datetime, c.val) else null end
     from (SELECT 1 x) l LEFT JOIN @@CT C ON C.KY = 'When';

     select
        @@WhatsLike = case when (charindex('%', @@Whats) > 0) then 1 else 0 end
     ,  @@WhereLike = case when (charindex('%', @@Where) > 0) then 1 else 0 end
     ,  @@WhoLike = case when (charindex('%', @@Who) > 0) then 1 else 0 end
     ,  @@RemarksLike = case when (charindex('%', @@Remarks) > 0) then 1 else 0 end
     ,  @@MsgIdLike = case when (charindex('%', @@MsgId) > 0) then 1 else 0 end
     ,  @@MsgTypeLike = case when (charindex('%', @@MsgType) > 0) then 1 else 0 end
     ,  @@filterLike = case when (charindex('%', @@filter) > 0) then 1 else 0 end

     SET @@filter = NULL;
end;

IF @@limit < 0
BEGIN
SELECT @@start = (
    SELECT COUNT(1)
       FROM tb_r_log_d D
       JOIN TB_R_LOG_H H
         ON H.process_iD =D.process_id
       WHERE  (@@id IS NULL OR (h.process_id = @@id))
         AND (@@datum IS NULL OR d.err_dt >= @@datum )
         AND (@@filter IS NULL
             OR (@@filterLike=0 AND d.err_message  =@@filter) OR (@@filterLike=1 AND d.err_message  LIKE @@filter)
             OR (@@filterLike=0 AND h.remarks      =@@filter) OR (@@filterLike=1 AND h.remarks      LIKE @@filter)
             OR (@@filterLike=0 AND d.err_Location =@@filter) OR (@@filterLike=1 AND d.err_Location LIKE @@filter)
             OR (@@filterLike=0 AND h.[USER_ID]    =@@filter) OR (@@filterLike=1 AND h.[USER_ID]    LIKE @@filter)
             )
         AND (@@Where IS NULL    OR (@@WhereLike=0 and d.err_location=@@Where) OR (@@WhereLike=1 and d.err_location like  @@Where ))
         AND (@@remarks IS NULL  OR (@@RemarksLike=0 and h.remarks=@@Remarks)  OR (@@RemarksLike=1 and h.remarks like  @@remarks ))
         AND (@@who IS NULL      OR (@@Wholike =0 and h.[user_id] = @@Who)     OR (@@WhoLike=1 and h.[USER_ID] like  @@WHO ))
         AND (@@Whats IS NULL    OR (@@WhatsLike=0 and d.err_message=@@Whats)  OR (@@WhatsLike=1 and d.err_message like  @@Whats ))
         AND (@@msgid IS NULL    OR (@@MsgIdLike=0 and d.msg_id=@@msgid)       OR (@@MsgIdLike=1 and d.msg_id like  @@msgid ))
         AND (@@msgtype IS NULL  OR (@@msgtypelike=0 and d.msg_type=@@msgtype) OR (@@msgTypeLike=1 and d.msg_type like  @@msgtype ))
    )
    , @@limit =1
    , @@counts = 1;
END;
/*
 SELECT @@filter[filter], @@start [start], @@limit [limit]
   ,  @@id [id], @@datum [datum], @@counts [Counts]
   , @@WhenDt [When], @@Where [Where], @@Who [Who], @@Remarks [Remarks], @@MsgId [MsgId], @@MsgType [MsgType]
*/
SELECT
    x.[RowNum], x.[Id], x.[Seq], x.[When], x.[Where], x.[Who], x.[What]
    , x.[Remarks], x.msg_id [MsgId], x.msg_type [MsgTpe]
    , x.process_sts [Sts]
FROM
  (
    SELECT TOP (@@LIMIT + (@@counts * (@@start+1)))
          ROW_NUMBER() OVER (ORDER BY d.process_id, d.seq_no) AS [RowNum]
        , d.process_id as [Id]
        , d.seq_no as [Seq]
        , d.err_dt as [When]
        , d.err_message  AS [What]
        , d.err_location As [Where]
        , h.user_id as [Who]
        , h.remarks [Remarks]
        , D.MSG_ID
        , D.MSG_TYPE
        , H.PROCESS_STS
     FROM dbo.tb_r_log_d d
     JOIN dbo.TB_R_LOG_H h
       ON h.process_id = d.process_id
    WHERE 1=1
      AND (@@id IS NULL OR (h.process_id = @@id))
      AND (@@datum IS NULL OR d.err_dt >= @@datum )
      AND (@@filter IS NULL
          OR (@@filterLike=0 AND d.err_message  =@@filter) OR (@@filterLike=1 AND d.err_message  LIKE @@filter)
          OR (@@filterLike=0 AND h.remarks      =@@filter) OR (@@filterLike=1 AND h.remarks      LIKE @@filter)
          OR (@@filterLike=0 AND d.err_Location =@@filter) OR (@@filterLike=1 AND d.err_Location LIKE @@filter)
          OR (@@filterLike=0 AND h.[USER_ID]    =@@filter) OR (@@filterLike=1 AND h.[USER_ID]    LIKE @@filter)
          )
      AND (@@Where IS NULL    OR (@@WhereLike=0 and d.err_location=@@Where) OR (@@WhereLike=1 and d.err_location like  @@Where ))
      AND (@@remarks IS NULL  OR (@@RemarksLike=0 and h.remarks=@@Remarks)  OR (@@RemarksLike=1 and h.remarks like  @@remarks ))
      AND (@@who IS NULL      OR (@@Wholike =0 and h.[user_id] = @@Who)     OR (@@WhoLike=1 and h.[USER_ID] like  @@WHO ))
      AND (@@Whats IS NULL    OR (@@WhatsLike=0 and d.err_message=@@Whats)  OR (@@WhatsLike=1 and d.err_message like  @@Whats ))
      AND (@@msgid IS NULL    OR (@@MsgIdLike=0 and d.msg_id=@@msgid)       OR (@@MsgIdLike=1 and d.msg_id like  @@msgid ))
      AND (@@msgtype IS NULL  OR (@@msgtypelike=0 and d.msg_type=@@msgtype) OR (@@msgTypeLike=1 and d.msg_type like  @@msgtype ))
  ) x
   WHERE (x.[RowNum] >=  @@start AND x.[RowNum] <= (@@limit+@@start-1))
