-- GetVendorBank
SELECT TOP 1
	AccountHolder = v.ACCT_HOLDER,
	Account = v.BANK_ACCOUNT,
	[Key] = v.BANK_KEY,
	Country = v.CTRY,
	Name = ISNULL(v.NAME_OF_BANK,''),
	[Type] = LTRIM(RTRIM(v.BANK_TYPE)),
	VendorCode = LTRIM(RTRIM(v.VENDOR_CD)),
	Currency = v.CURRENCY
FROM 
	vw_VendorBank v 
WHERE (v.VENDOR_CD = @vendorCode )
  AND ((NULLIF(@key, '') IS NULL) OR  v.BANK_TYPE = @key )