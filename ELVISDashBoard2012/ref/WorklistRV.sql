-- WorklistRV

SELECT
	[Folio]= CONVERT(VARCHAR(15), dbo.fn_DocNoYearToReff(h.RV_NO, h.RV_YEAR)) , 
    [DocNumber] = h.RV_NO ,
    [DocYear] = h.RV_YEAR ,
    [DocDate]= h.RV_DATE ,
	[HypValue]=  CONVERT(VARCHAR(10), h.RV_NO)
     + (CASE WHEN (ISNULL (nc.NO_OF_NOTICE,0)> 0) THEN '(' + CONVERT(VARCHAR(10), ISNULL (nc.NO_OF_NOTICE,0)) + ')' ELSE '' END) 
     + (CASE WHEN ISNULL (nc.REPLY_FLAG,0) > 0 THEN '*' ELSE '' END) , 
    [Description] = t.TRANSACTION_NAME + '-' + v.VENDOR_NAME, 
	[TotalAmount] =aidr.TOTAL_AMOUNT_IDR , 
    [Notices] = ISNULL(nc.NO_OF_NOTICE,0) , 
    [PaidDate] = h.RECEIVED_DATE , 
    [HoldFlag] =h.HOLD_FLAG , 
    [NeedReply] = isnull(nc.REPLY_FLAG,0) , 
    [DivName] = CASE
        WHEN CHARINDEX ('-',REVERSE (m.DIVISION_NAME)) > 0
        THEN RTRIM (SUBSTRING(m.DIVISION_NAME, 0, LEN (m.DIVISION_NAME) -
                CHARINDEX ('-',REVERSE (m.DIVISION_NAME)) +1))
        ELSE m.DIVISION_NAME
    END, 
    [Rejected] = CASE WHEN h.STATUS_CD IN (SELECT STATUS_CD FROM dbo.fn_GetRejectedStatus(2) RS) 
        THEN 1
        ELSE 0
    END, 
    h.STATUS_CD, 
    [StatusName] = st.status_name , 
    [PvTypeName] = rt.rv_type_name , 

	[ActivityDateFrom] = CONVERT(VARCHAR(20), h.ACTIVITY_DATE_FROM, 106) , 
    [ActivityDateTo] = CONVERT(VARCHAR(20), h.ACTIVITY_DATE_TO, 106) , 
    [PayMethodName] = pm.PAY_METHOD_NAME , 
    [BudgetNo] = h.BUDGET_NO , 
    [WorklistNotice] = CONVERT(BIT, CASE WHEN ISNULL (r.NO_NOTICE_WORKLIST,0) > 0 THEN 1 ELSE 0 END) ,
	[TransCd] = h.TRANSACTION_CD /* FID.Ridwan 11072018 */ 
	
FROM dbo.TB_R_RV_H [h]
JOIN (

	{0}

) g ON g.DOC_NO = h.RV_NO
JOIN TB_M_STATUS st ON st.STATUS_CD = h.STATUS_CD
JOIN TB_M_RV_TYPE rt ON rt.RV_TYPE_CD = H.RV_TYPE_CD
JOIN TB_M_PAYMENT_METHOD pm ON pm.PAY_METHOD_CD = H.PAY_METHOD_CD
LEFT JOIN TMMIN_ROLE.dbo.TB_M_DIVISION m ON m.DIVISION_ID = h.DIVISION_ID
LEFT JOIN
        dbo.TB_M_TRANSACTION_TYPE [t]
    ON h.TRANSACTION_CD = t.TRANSACTION_CD
LEFT JOIN SAP_DB.dbo.TB_M_VENDOR v ON V.VENDOR_CD = h.VENDOR_CD
    
LEFT JOIN (
		select 
		  n.DOC_NO,
		  n.DOC_YEAR, 
		  COUNT(1) [NO_NOTICE_WORKLIST]
		from TB_R_NOTICE n 
		LEFT JOIN 
		(
		SELECT X.REFF_NO, MAX(ACTUAL_DT) ACTUAL_DT
		 FROM TB_R_DISTRIBUTION_STATUS X 
		 WHERE X.ACTUAL_DT IS NOT NULL
		 GROUP BY X.REFF_NO 
		 ) a ON a.REFF_NO = dbo.fn_DocNoYearToReff(n.DOC_NO, n.doc_year) 
		where 
		a.ACTUAL_DT < n.NOTICE_DT 
	group by n.DOC_YEAR, n.DOC_NO 
) R on r.DOC_NO = h.RV_NO and r.DOC_YEAR = h.RV_YEAR     
    
LEFT JOIN
    (SELECT
        n.DOC_NO,
        n.DOC_YEAR,
        COUNT(n.NOTICE_NO) [NO_OF_NOTICE],
        SUM(n.REPLY_FLAG) [REPLY_FLAG]
        FROM dbo.TB_R_NOTICE [n]
        WHERE (((n.USER_ROLE_TO IS NOT NULL) 
		    AND (len(n.USER_ROLE_TO)>0)) 
			OR (n.NOTICE_DT IS NOT NULL))
        GROUP BY n.DOC_YEAR, n.DOC_NO) [nc]
ON nc.DOC_NO = h.RV_NO AND nc.DOC_YEAR = h.RV_YEAR    
LEFT JOIN
    (SELECT a.DOC_YEAR,
                a.DOC_NO,
                ROUND(SUM(a.EXCHANGE_RATE*a.TOTAL_AMOUNT),2) [TOTAL_AMOUNT_IDR]
        FROM dbo.TB_R_PVRV_AMOUNT [a]
            LEFT JOIN dbo.vw_ExchangeRate [x]
			ON x.CURRENCY_CD = a.CURRENCY_CD
        GROUP BY a.DOC_YEAR, a.DOC_NO) [aidr]
ON aidr.DOC_YEAR = h.RV_YEAR AND aidr.DOC_NO = h.RV_NO