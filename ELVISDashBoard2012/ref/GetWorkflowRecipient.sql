-- GetWorkflowRecipient

SELECT d.PROCCED_BY
FROM TB_R_DISTRIBUTION_STATUS d
WHERE REFF_NO = dbo.fn_DocNoYearToReff(@no, @year)
AND STATUS_CD <= @status
AND d.STATUS_CD not in (select STATUS_CD from dbo.vw_ExcludedStatusNotice)
order by d.STATUS_CD