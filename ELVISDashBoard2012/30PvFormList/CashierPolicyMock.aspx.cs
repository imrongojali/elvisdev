﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic.VoucherForm;

namespace ELVISDashBoard._30PvFormList
{
    public partial class CashierPolicyMock : System.Web.UI.Page
    {
        readonly string dataID = "CashierPolicyMockData";
        public List<PVFormDetail> Rows
        {
            get
            {
                object o = Session[dataID];
                if (o == null)
                {
                    
                    List<PVFormDetail> l = new List<PVFormDetail>();
                    l.Add(new PVFormDetail() {
                        SequenceNumber = 1,
                        Description="one",
                        CurrencyCode = "IDR",
                        Amount = 100                      
                    });
                    Session[dataID] = l;
                    return l;
                }
                else
                {
                    return o as List<PVFormDetail>;
                }
            }

            set
            {
                Session[dataID] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                grid.DataSource = new List<PVFormDetail>();
                grid.DataBind();
            }
        }

       
    }
}