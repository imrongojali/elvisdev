﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.VoucherForm;
using BusinessLogic.SyncLogic;
using BusinessLogic.CommonLogic;
using ELVISJob.ELVIS.JOBCommon;
using Common.Data;
using System.IO;
using BusinessLogic._30PVList;
using Common;

namespace ELVISJob.ELVIS.JOB0004
{
    class JOB0004 : BaseJob
    {
        public override void Run()
        {
            JOB0004BO bo = new JOB0004BO();
            bo.SetFuncId("JOB0004");

            try
            {
                Say(bo.GetFuncId(), "Start");
                bo.SyncBudget();
                Say(bo.GetFuncId(), "Finish");
            }
            catch (Exception e)
            {
                Handle(e);
            }
        }
    }
}
