﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELVISJob.ELVIS.JOBCommon;
using BusinessLogic.VoucherForm;
using Common.Data;
using DataLayer.Model;
using Dapper;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Data.Common;
using BusinessLogic;
using BusinessLogic.SyncLogic;
using System.IO;
using BusinessLogic.CommonLogic;
using Common.Data._30PVList;
using Common.Function;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Common;

namespace ELVISJob.ELVIS.JOB0001
{
    class JOB0001BO : BaseBO
    {
        DateTime today = DateTime.Now;

        public List<PVSyncH> ClearUnsyncPVDraft(List<PVSyncH> unsyncPV)
        {
            // find Unsync PV TOWASS Draft that already requested to be cancelled
            var canceledPVT = (from a in unsyncPV
                               join b in unsyncPV on a.PVTowass equals b.PVTowass
                               where a.SyncAction == 1
                                    && b.SyncAction == 2
                                    && b.CreatedDt > a.CreatedDt
                               select a.PVTowass).ToList();

            foreach (var c in canceledPVT)
            {
                unsyncPV.ForEach(x => {
                    if (x.PVTowass.Equals(c))
                    {
                        x.SyncStatus = 1;
                    }
                });
            }

            return unsyncPV
                    //.Where(x => x.SyncAction == SYNC_PV_DRAFT)
                    .OrderByDescending(x => x.CreatedDt)
                    .ToList();
        }

        public PVFormData ParsingUnsycPVFormData(PVSyncH data, ref List<string> errMsg)
        {
            var pv = new PVFormData();

            // Process PV Detail first
            #region PV Detail
            int newPid = Log(MSG_INF, string.Format("PV Detail of : {0}", data.PVTowass), "Process Detail", FuncId);

            DynamicParameters d = new DynamicParameters();
            d.Add("@pid", newPid);
            d.Add("@transCd", data.TransCode);
            d.Add("@vendorCd", data.VendorCode);
            d.Add("@pvNoWOS", data.PVTowass);
            d.Add("@uid", data.CreatedBy);
            d.Add("@syncId", data.SyncId);

            Exec("sp_wos_receive_pvd", d);

            var errUpl = GetErrorUploadData(newPid);
            if (errUpl != null && errUpl.Count > 0)
            {
                errMsg = errUpl.Select(x => x.ERRMSG).ToList();

                return null;
            }

            pv.Details = GetLastUpload(newPid);
            #endregion

            #region PV Header
            pv.PVNumber = data.PVNo ?? 0;
            pv.PVTowass = data.PVTowass;
            pv.PVYear = data.PVYear;
            pv.SyncId = data.SyncId;
            pv.PaymentMethodCode = data.PayMethodCode;
            pv.VendorCode = data.VendorCode;
            pv.PVTypeCode = data.PVType;
            pv.TransactionCode = data.TransCode;
            pv.DivisionID = data.DivCd;
            pv.PVDate = data.PVDate;
            pv.UserName = data.CreatedBy;

            var v = jo.getVendor(pv.VendorCode);
            if (v != null)
            {
                pv.VendorGroupCode = v.VENDOR_GROUP_CD;
                pv.BookingNo = v.VENDOR_NAME; //numpang property. minimalisir tambah property ditempat lain aja
            }

            pv._TransactionType = jo.GetTransType(data.TransCode ?? 0);
            if (pv.Budgeted)
            {
                pv.BudgetNumber = data.BudgetNo;
            }

            pv.isSubmit = false;
            pv.FinanceAccessEnabled = false;
            pv.StatusCode = 0;
            #endregion

            #region Commented. using SP to process PV detail
            //if (data.details.Count > 0)
            //{
            //    List<PVFormDetail> listDet = new List<PVFormDetail>();

            //    foreach (var det in data.details)
            //    {
            //        var pvDet = new PVFormDetail();

            //        pvDet.SequenceNumber = det.SeqNo;
            //        pvDet.DisplaySequenceNumber = det.SeqNo + 1;
            //        pvDet.CurrencyCode = det.CurrCode;
            //        pvDet.Description = det.Description;
            //        pvDet.Amount = det.Amount ?? 0;
            //        pvDet.DppAmount = det.DPPAmount ?? 0;
            //        pvDet.InvoiceNumber = det.InvoiceNo;
            //        pvDet.InvoiceDate = det.InvoiceDate;
            //        pvDet.TaxNumber = det.TaxNo;
            //        pvDet.TaxCode = det.TaxCd;
            //        pvDet.TaxAssignment = det.TaxAssignment;
                        
            //        if(pv.EntryCostCenter)
            //            pvDet.CostCenterCode = det.CostCenter;

            //        int? gl = jo.GLAccount(pvDet.CostCenterCode, pv.TransactionCode ?? 0, 1);
            //        pvDet.GlAccount = gl != null ? gl.ToString() : null;

            //        listDet.Add(pvDet);
            //    }

            //    pv.Details = listDet;
            //}
            #endregion

            #region PV Attachments
            if (data.attachments.Count > 0)
            {
                List<FormAttachment> lstAttachment = pv.AttachmentTable.Attachments;
                FormAttachment attachment;

                foreach (var att in data.attachments)
                {
                    string reffNo = "T" + ProcessId.ToString();
                    string dir = att.PVYear + "/PV/" + reffNo;

                    attachment = new FormAttachment()
                    {
                        Blank = false,
                        CategoryCode = "4",                 // GENERAL ATTACHMENT
                        Description = "",
                        FileName = att.FileName,
                        PATH = dir,
                        ReferenceNumber = reffNo,
                        SequenceNumber = att.SeqNo,
                    };

                    lstAttachment.Add(attachment);
                }
            }
            #endregion

            return pv;
        }

        public long GetDocNo(byte DocCd, string username)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@doc_cd", DocCd);
            d.Add("@user", username);
            d.Add("@NO", null, DbType.Int32, ParameterDirection.Output);

            Exec("sp_GetDocNumber", d);

            long ID = d.Get<int>("@NO");
            return ID;
        }

        public bool SaveData(
            PVFormData formData, UserData userData, List<string> msgErr, int? needCompare)
        {
            bool Ok = true;
            today = DateTime.Now;

            DbTransaction TX = null;
            using (ContextWrap eco = new ContextWrap())
            {
                ELVIS_DBEntities db = eco.db;
                try
                {
                    TX = db.Connection.BeginTransaction();

                    int version = 1;

                    Ok = saveHeader(formData, userData, out version, db);

                    saveDetail(formData, userData, version, db);

                    saveAmounts(formData, userData, db);

                    saveAttachment(formData, userData, db);

                    db.SaveChanges();

                    int reCompare, issue, passed;

                    if((needCompare ?? 0) == 1)
                    {
                        reCompare = 1;
                        issue = 0;
                        passed = 0;
                    }
                    else
                    {
                        reCompare = 0;
                        issue = 0;
                        passed = 0;
                    }

                    var pvListLo = new BusinessLogic._30PVList.PVListLogic();
                    pvListLo.UpdateStatusCompare(formData.PVNumber ?? 0, formData.PVYear ?? 0, passed, issue, reCompare, db);

                    if (Ok)
                    {
                        TX.Commit();

                        SavePVFP(formData, userData);
                        pvListLo.UpdateAfterCompare(formData.PVNumber ?? 0);
                        pvListLo.UpdateEfakturAfterSubmit((formData.PVNumber ?? 0).ToString(), formData._TransactionType.TemplateCd.ToString());
                    }
                }
                catch (Exception e)
                {
                    TX.Rollback();
                    Handle(e);
                    Ok = false;
                    string imsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    msgErr.Add(imsg);
                }
            }


            return Ok;
        }

        public bool saveHeader(PVFormData formData, UserData userData,
            out int version, ELVIS_DBEntities DB)
        {
            bool headerSaved = false;
            TB_R_PV_H h = null;
            version = 1;
            try
            {
                bool added = false;
                int _no, _yy;
                _no = formData.PVNumber ?? 0;
                _yy = formData.PVYear ?? 0;
                var existingHeaders = (from t in DB.TB_R_PV_H
                                       where (t.PV_NO == _no)
                                          && (t.PV_YEAR == _yy)
                                          && (_no != 0)
                                       select t).ToList();


                if (existingHeaders != null && existingHeaders.Any())
                {
                    h = existingHeaders[0];
                }
                else
                {
                    added = true;
                    h = new TB_R_PV_H();
                    _no = (int)GetDocNo(1, userData.USERNAME);
                    formData.PVNumber = _no;
                    DB.TB_R_PV_H.AddObject(h);
                }

                h.PV_NO = formData.PVNumber.Value;
                h.PV_YEAR = formData.PVYear.Value;
                h.PAY_METHOD_CD = formData.PaymentMethodCode;
                h.VENDOR_GROUP_CD = formData.VendorGroupCode;
                h.VENDOR_CD = formData.VendorCode;
                h.PV_TYPE_CD = formData.PVTypeCode;
                h.TRANSACTION_CD = formData.TransactionCode;
                h.BUDGET_NO = formData.BudgetNumber;
                //h.BUDGET_NO = formData.BookingNo;
                h.ACTIVITY_DATE_FROM = formData.ActivityDate;
                h.ACTIVITY_DATE_TO = formData.ActivityDateTo;
                h.DIVISION_ID = formData.DivisionID;
                h.PV_DATE = formData.PVDate.Value;
                //h.REFFERENCE_NO = formData.ReferenceNo;

                if (added)
                {
                    h.CREATED_DATE = today;
                    h.CREATED_BY = userData.USERNAME;
                }
                else
                {
                    h.CHANGED_DATE = today;
                    h.CHANGED_BY = userData.USERNAME;
                }
                //h.TOTAL_AMOUNT = formData.GetTotalAmount();
                h.TOTAL_AMOUNT = formData.Details.Sum(x => x.Amount);

                if (formData.SuspenseNumber != null && formData.SuspenseNumber != int.MinValue)
                {
                    h.SUSPENSE_NO = formData.SuspenseNumber;
                    h.SUSPENSE_YEAR = formData.SuspenseYear;
                }

                h.STATUS_CD = formData.StatusCode ?? 0;

                DB.SaveChanges();

                headerSaved = true;
            }
            catch (Exception ex)
            {
                headerSaved = false;
                Handle(ex);
                throw;
            }
            return headerSaved;
        }

        public void saveDetail(PVFormData formData, UserData userData, int version,
            ELVIS_DBEntities DB)
        {
            foreach (PVFormDetail d in formData.Details)
            {
                int? glacc = null;

                if (string.IsNullOrEmpty(d.GlAccount))
                {
                    glacc = jo.GLAccount(d.CostCenterCode, formData.TransactionCode ?? 0, d.ItemTransaction);
                }
                else 
                {
                    int t;
                    if (Int32.TryParse(d.GlAccount.Trim(), out t))
                    {
                        glacc = t;
                    }
                }

                TB_R_PV_D x = new TB_R_PV_D()
                {
                    SEQ_NO = d.SequenceNumber,
                    PV_NO = formData.PVNumber.Value,
                    PV_YEAR = formData.PVYear.Value,
                    AMOUNT = (d.CurrencyCode == "IDR" ? Math.Round(d.Amount) : d.Amount),
                    COST_CENTER_CD = d.CostCenterCode,
                    CURRENCY_CD = d.CurrencyCode,
                    TAX_CD = d.TaxCode,
                    TAX_NO = d.TaxNumber,
                    TAX_DT = d.TaxDate,
                    ITEM_TRANSACTION_CD = d.ItemTransaction,
                    GL_ACCOUNT = glacc,
                    DESCRIPTION = d.Description,
                    FROM_CURR = d.FromCurr,
                    FROM_SEQ = d.FromSeq,
                    ITEM_NO = d.ItemNo,
                    INVOICE_DATE = d.InvoiceDate,
                    INVOICE_NO = d.InvoiceNumber,
                    TAX_ASSIGNMENT = d.TaxAssignment,
                    DPP_AMOUNT = d.DppAmount,
                    ACC_INF_ASSIGNMENT = d.AccInfAssignment,
                    CREATED_BY = userData.USERNAME,
                    CREATED_DT = today,
                    #region EFB
                    WHT_TAX_CODE = d.WHT_TAX_CODE,
                    WHT_TAX_TARIFF = d.WHT_TAX_TARIFF,
                    WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO,
                    WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT
                    #endregion
                };
                DB.TB_R_PV_D.AddObject(x);

                DB.SaveChanges();
                d.Persisted = true;
            }
        }

        public void saveAmounts(PVFormData formData, UserData userData, ELVIS_DBEntities DB)
        {
            int _no, _yy;
            _no = formData.PVNumber ?? 0;
            _yy = formData.PVYear ?? 0;

            OrderedDictionary mapTotalAmount = formData.FormTable.TotalAmountMap();
            List<ExchangeRate> exchangeRates = jo.getExchangeRates();
            
            ICollection lstKey = mapTotalAmount.Keys;
            foreach (string key in lstKey)
            {
                decimal xr = (from er in exchangeRates
                              where er.CurrencyCode.Equals(key)
                              select er.Rate).FirstOrDefault();
                decimal exchangeRate = xr;

                TB_R_PVRV_AMOUNT tblAmount = new TB_R_PVRV_AMOUNT()
                {
                    CREATED_BY = userData.USERNAME,
                    CREATED_DATE = today,
                    CURRENCY_CD = key,
                    EXCHANGE_RATE = exchangeRate,
                    DOC_NO = _no,
                    DOC_YEAR = _yy,
                    TOTAL_AMOUNT = Convert.ToDecimal(mapTotalAmount[key])
                };

                DB.TB_R_PVRV_AMOUNT.AddObject(tblAmount);
                DB.SaveChanges();
            }
        }

        public void saveAttachment(PVFormData formData, UserData userData, ELVIS_DBEntities DB)
        {
            var ftpLo = new FtpLogic();
            bool uploadSucceded;
            string errMsgUpload;

            string docNo = (formData.PVNumber ?? 0).ToString();
            string docYear = (formData.PVYear ?? 0).ToString();
            string reffNo = string.Format("{0}{1}", docNo, docYear);
            string dir = string.Format("{0}/PV/{1}", docYear, docNo);

            var lstAtt = formData.AttachmentTable.Attachments;

            for(int i = 1; i <= lstAtt.Count(); i++)
            {
                uploadSucceded = true;
                errMsgUpload = null;

                var att = lstAtt[i - 1];

                string fileName = Path.GetFileName(att.FileName);
                string wosDir = jo.GetSysText("TOWASS_INTEGRATION", "WCAR_ATT_DIR");

                var fileBytes = ftpLo.DownloadFromWOSinByte(wosDir, fileName);
                if (fileBytes != null && fileBytes.Length > 0)
                {
                    ftpLo.ftpUploadBytes(docYear, "PV", docNo, fileName, fileBytes, ref uploadSucceded, ref errMsgUpload);
                }
                else
                {
                    uploadSucceded = false;
                    PutLog(MSG_ERR, string.Format("Failed download from WOS => PV {0} attachment {1}", docNo, fileName), "Upload Attachment");
                }

                if (uploadSucceded)
                {
                    var dataAtt = new TB_R_ATTACHMENT()
                    {
                        REFERENCE_NO = reffNo,
                        REF_SEQ_NO = i,
                        ATTACH_CD = att.CategoryCode,
                        DESCRIPTION = att.Description,
                        DIRECTORY = dir,
                        FILE_NAME = fileName,
                        CREATED_BY = userData.USERNAME,
                        CREATED_DT = today,
                        CHANGED_BY = userData.USERNAME,
                        CHANGED_DT = today
                    };

                    DB.TB_R_ATTACHMENT.AddObject(dataAtt);
                    DB.SaveChanges();
                }
                else
                {
                    PutLog(MSG_ERR, string.Format("Failed upload => PV {0} attachment : {1}", docNo, fileName), "Upload Attachment");
                }
            }
        }

        public void returnPVDraft(List<PVSyncH> allUnsyncPVH)
        {
            if (allUnsyncPVH == null)
                return;

            var ret = allUnsyncPVH
                        .Where(x => x.ResultSts != null)
                        .ToList();

            if (ret.Count == 0)
                return;

            try
            {
                foreach (var up in ret)
                {
                    DynamicParameters d = new DynamicParameters();
                    d.Add("@sync_id", up.SyncId);
                    d.Add("@sync_action", up.SyncAction);
                    d.Add("@pv_no_towass", up.PVTowass);
                    d.Add("@pv_no_elvis", up.PVNo);
                    d.Add("@pv_year", up.PVYear);
                    d.Add("@pv_amount", up.TotalAmount);
                    d.Add("@vendor_cd", up.VendorCode);
                    d.Add("@vendor_nm", up.VendorNm);
                    d.Add("@pv_cover_file", up.PVCover);
                    d.Add("@submit_date", null);
                    d.Add("@cancel_date", null);
                    d.Add("@result_sts", up.ResultSts);
                    d.Add("@message", up.Message);

                    Exec("sp_wos_send_result", d);
                }
            }
            catch (Exception e)
            {
                Handle(e);
            }
        }

        public void returnPVCover(List<PVSyncH> allUnsyncPVH)
        {
            if (allUnsyncPVH == null)
                return;

            var ret = allUnsyncPVH
                        .Where(x => x.PVCover != null)
                        .ToList();

            if (ret.Count == 0)
                return;

            try
            {
                foreach (var up in ret)
                {
                    DynamicParameters d = new DynamicParameters();
                    d.Add("@pv_no_elvis", up.PVNo);
                    d.Add("@pv_cover_file", up.PVCover);

                    Exec("sp_wossync_receive_pv_cover", d);
                }
            }
            catch (Exception e)
            {
                Handle(e);
            }
        }

        private PVListData getPVCoverData(string pvNo, string pvYear)
        {
            PVListData d = jo.getPVListData(pvNo, pvYear);

            if (d != null)
            {
                string rc = jo.GetSysText("INVOICE_UPLOAD", "NO_FRACTION_CURRENCY");
                if (string.IsNullOrEmpty(rc))
                {
                    AppSetting.RoundCurrency = new string[] { "IDR", "JPY" };
                }
                else
                {
                    AppSetting.RoundCurrency = rc.Split(',');
                }

                var oad = jo.getAmountsData(pvNo, pvYear);

                AmountData oa = null;

                oa = oad.Where(c => c.CURRENCY_CD == "IDR").FirstOrDefault();
                d.AMOUNT_IDR = (oa != null) ? CommonFunction.Eval_Curr("IDR", oa.TOTAL_AMOUNT) : "";
                oa = oad.Where(c => c.CURRENCY_CD == "JPY").FirstOrDefault();
                d.AMOUNT_JPY = (oa != null) ? CommonFunction.Eval_Curr("JPY", oa.TOTAL_AMOUNT) : "";
                oa = oad.Where(c => c.CURRENCY_CD == "USD").FirstOrDefault();
                d.AMOUNT_USD = (oa != null) ? CommonFunction.Eval_Curr("USD", oa.TOTAL_AMOUNT) : "";
            }

            return d;
        }

        public string printCover(string pvNo, string pvYear, UserData _UserData)
        {
            string fileName = null;
            try
            {
                string basePath = AppDomain.CurrentDomain.BaseDirectory;

                string imagePath = Path.GetDirectoryName(basePath + Common.AppSetting.CompanyLogo);
                string templatePath = basePath + Common.AppSetting.PdfTemplPath;

                PVListData data = getPVCoverData(pvNo, pvYear);

                Ini pvcover = new Ini(templatePath + "\\PVCoverTemplate.ini");
                string tempelCover = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateNew.html"));
                string tempelInvoice = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateInvoice.html"));
                string tempelTTD = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateTTD.html"));
                string tempelUnsettled = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateUnsettled.html"));
                Templet tCover = new Templet(tempelCover);
                Templet tInvoice = new Templet(tempelInvoice);
                Templet tUnsettled = new Templet(tempelUnsettled);

                Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
                fileName = "PV_Cover_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                String contentTemp;
                HTMLWorker hw = new HTMLWorker(doc);


                MemoryStream ms = new MemoryStream();
                PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);

                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                doc.Open();

                StringBuilder amounts = new StringBuilder("");

                tCover.Init(tempelCover);
                tInvoice.Init(tempelInvoice);
                tUnsettled.Init(tempelUnsettled);

                Filler fCover = tCover.Mark("COMPANY_NAME,ISSUING_DIVISION,PV_DATE,PV_NO,"
                    + "VENDOR_NAME,TRANSACTION_TYPE,PAYMENT_METHOD,"
                    + "BUDGET_NO_DESC,DESC,PV_TYPE_NAME,AMOUNT,INVOICE,UNSETTLED,TTD,USERNAME,PRINT_DATE"); // Rinda Rahayu 20160328

                Filler fInvoice = tInvoice.Mark("rows", "INVOICE_NO,CURRENCY,"
                    + "val,TAX_NO,INVOICE_DATE,valTax");

                Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(Common.AppSetting.CompanyLogo)));
                img.ScaleToFit(pvcover["logo.width"].Int(), pvcover["logo.height"].Int());
                System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
                username = myTI.ToTitleCase(username.ToLower());

                amounts.Clear();
                List<AmountData> la = jo.getAmountsData(pvNo, pvYear);
                if (la != null)
                    foreach (AmountData amt in la)
                    {
                        if (amt.TOTAL_AMOUNT != null && (amt.TOTAL_AMOUNT > 0))
                            amounts.AppendFormat(
                                "<tr><td>{0}</td><td style='font-size: 16pt;' align='right'>{1}</td></tr>"
                                , amt.CURRENCY_CD
                                , CommonFunction.Eval_Curr(amt.CURRENCY_CD, amt.TOTAL_AMOUNT));
                    }

                List<PVCoverData> listInvoiceData = null;

                listInvoiceData = jo.getInvoiceValue(data.PV_NO, data.PV_YEAR);
                StringBuilder amtStr = new StringBuilder("");

                int seq = 0;
                if (listInvoiceData != null)
                    foreach (var invoice in listInvoiceData)
                    {
                        string val = "";
                        string valTax = "";

                        if (invoice.AMOUNT != null)
                        {
                            if (!AppSetting.RoundCurrency.Contains(invoice.CURRENCY))
                            {
                                val = String.Format("{0:N2}", invoice.AMOUNT.Value);
                                valTax = String.Format("{0:N2}", invoice.AMOUNT * invoice.TAX_AMOUNT);
                            }
                            else
                            {
                                val = String.Format("{0:N0}", invoice.AMOUNT.Value);
                                valTax = String.Format("{0:N0}", invoice.AMOUNT * invoice.TAX_AMOUNT);
                            }
                        }
                        else
                        {
                            val = " ";
                            valTax = " ";
                        }
                        fInvoice.Add(
                            (invoice.INVOICE_NO == "" ? " " : invoice.INVOICE_NO),
                            (invoice.CURRENCY == "" ? " " : invoice.CURRENCY),
                            val,
                            (invoice.TAX_NO == "" ? " " : invoice.TAX_NO),
                            (invoice.INVOICE_DATE == null ? " " : invoice.INVOICE_DATE.Value.ToString("dd/MM/yyyy")),
                            valTax);
                    }

                if (listInvoiceData != null && listInvoiceData.Count > 0)
                {
                    amtStr.Append(tInvoice.Get());
                }
                string barcodeFormat = "{0}\t{1}\r";
                if (!pvcover["barcode.format"].isEmpty())
                {
                    barcodeFormat = pvcover["barcode.format"];
                    barcodeFormat = barcodeFormat.Trim().Replace("\\t", "\t").Replace("\\r", "\r").Replace("\\n", "\n");
                }

                Barcode128 bar = new Barcode128();

                bar.CodeType = Barcode.CODE128;
                bar.ChecksumText = false;
                bar.GenerateChecksum = false;
                bar.StartStopText = false;
                bar.Code = string.Format(barcodeFormat, data.PV_NO, data.PV_YEAR);

                Image imgBarCode = null;

                imgBarCode = bar.CreateImageWithBarcode(PDFWriter.DirectContent, null, null);

                StyleSheet sh = new StyleSheet();
                string budgetNoDesc = "";

                if (!data.BUDGET_NO.isEmpty())
                {
                    budgetNoDesc = data.BUDGET_NO + "-" + data.BUDGET_DESCRIPTION;
                }


                /* Start Rinda Rahayu 20160330*/
                String ttd = "";

                string codeSkipApp = jo.GetSysText("TRANSACTION_TYPE", "SKIP_USER_APPROVAL");
                if (!("," + codeSkipApp + ",").Contains("," + data.TRANSACTION_CD + ","))
                {
                    ttd = tempelTTD;
                }

                string pvTypeName = "";
                if (data.PV_TYPE_NAME != null && !data.PV_TYPE_NAME.Equals(""))
                {
                    pvTypeName = data.PV_TYPE_NAME;
                }
                else
                {
                    List<CodeConstant> pvTypeList = jo.GetPVTypes(0);
                    for (int i = 0; i < pvTypeList.Count; i++)
                    {
                        CodeConstant codeCs = pvTypeList[i];
                        if (codeCs.Code.Equals(data.PV_TYPE_CD))
                        {
                            pvTypeName = codeCs.Description;
                            break;
                        }
                    }
                }
                /* End Rinda Rahayu 20160330*/

                List<OutstandingSuspense> los = new List<OutstandingSuspense>();
                if (string.Compare(data.PV_TYPE_CD.Trim(), "2") == 0)
                    los = jo.GetUnsettledByDivision(data.DIVISION_ID);
                string loses = "";
                if (los.Any())
                {
                    Filler fSUSPENSE_COUNT = tUnsettled.Mark("TITLE", "SUS");
                    fSUSPENSE_COUNT.Add(los.Count.str());

                    Filler fLINE = tUnsettled.Mark("LINE", "ROW");
                    if (los.Count <= 5)
                    {
                        foreach (var osu in los)
                        {
                            fLINE.Add(string.Format("{0} Rp. {1} - {2} - {3}{4}"
                                    , osu.DOC_NO
                                    , osu.TOTAL_AMOUNT.fmt()
                                    , osu.VENDOR_NAME
                                    , ((osu.DAYS_OUTSTANDING > 0) ? osu.DAYS_OUTSTANDING.str() + " day" : "")
                                    , (osu.DAYS_OUTSTANDING > 1) ? "s" : "")
                                    );
                        }

                    }
                    else
                    {
                        fLINE.Add(string.Format("TOTAL Rp. {0}", los.Sum(a => a.TOTAL_AMOUNT).fmt()));

                        /// mapped out to array 5x5 
                        string[][] lono = new string[][] 
                            { 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" } 
                            };
                        int losj = (los.Count > 25) ? 25 : los.Count;
                        for (int losi = 0; losi < losj; losi++)
                        {
                            lono[losi / 5][losi % 5] = los[losi].DOC_NO.str();
                        }
                        if (los.Count > 24)
                        {
                            lono[4][4] = "...NOT SHOWN";
                        }
                        for (int mx = 0; mx < 5; mx++)
                        {
                            fLINE.Add(string.Format("{0} &nbsp; {1} &nbsp; {2} &nbsp; {3} &nbsp; {4} &nbsp;"
                                , lono[mx][0], lono[mx][1], lono[mx][2], lono[mx][3], lono[mx][4]));
                        }

                    }

                    loses = tUnsettled.Get();
                }
                else
                {
                    loses = "";
                }

                fCover.Set(
                        AppSetting.CompanyName,
                        data.DIVISION_NAME,
                        data.PV_DATE.ToString("d/M/yyyy"),
                        data.PV_NO,
                        String.Format("{0}: {1}", data.VENDOR_CD, data.VENDOR_NAME),
                        data.TRANSACTION_NAME,
                        data.PAY_METHOD_NAME,
                        budgetNoDesc,
                        data.DESCRIPTION,
                        pvTypeName, // Rinda Rahayu 20160328
                        amounts.ToString(), amtStr.ToString(),
                        loses,
                        ttd, //Rinda Rahayu 20160330                            
                        username,
                        DateTime.Now.ToString("d MMMM yyyy"));

                contentTemp = tCover.Get();
                // File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), "pvCover_%TIME%", ".txt"), contentTemp);
                List<IElement> elements = HTMLWorker.ParseToList(new StringReader(contentTemp), sh);

                string padding = pvcover["MF.Padding"];
                PrintHelper.padMF = padding.Int(PrintHelper.padMF);
                string mfCols = pvcover["MF.Columns"];
                string mfRows = pvcover["MF.Rows"];
                string mfTable = pvcover["MF.Table"];

                PdfPTable digits = new PdfPTable(10);
                for (int ix = 0; ix < 10; ix++)
                {
                    PdfPCell x10 = new PdfPCell(new Paragraph("  "));
                    x10.BorderWidthTop = 1;
                    x10.BorderWidthRight = 1;
                    x10.BorderWidthBottom = 1;
                    x10.BorderWidthLeft = (ix > 0) ? 0 : 1;
                    x10.PaddingTop = PrintHelper.padMF;
                    x10.PaddingBottom = PrintHelper.padMF;
                    x10.PaddingLeft = PrintHelper.padMF;
                    x10.PaddingRight = PrintHelper.padMF;
                    digits.AddCell(x10);
                }
                digits.WidthPercentage = 100;

                float[] wtDigits = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                PrintHelper.WidthsParse(pvcover, "MF.Digits", ref wtDigits);
                digits.SetWidths(wtDigits);

                float[] wtRow = new float[] { 17, 20 };
                PrintHelper.WidthsParse(pvcover, "MF.Rows", ref wtRow);

                float[] wtTable = new float[] { 5, 6 };
                PrintHelper.WidthsParse(pvcover, "MF.Table", ref wtTable);

                PdfPTable pRow = new PdfPTable(2);

                PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Invoice Document No");
                PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Payment Document No");
                PrintHelper.coverMFRow(pRow, new PdfPCell(new Paragraph("")), "Payment Date");

                pRow.SetWidths(wtRow);

                pRow.WidthPercentage = 100;

                PdfPTable tMF = new PdfPTable(2);
                PdfPCell mf0 = new PdfPCell(imgBarCode);
                mf0.BorderWidth = 0;
                tMF.AddCell(mf0);
                PdfPCell mf1 = new PdfPCell(pRow);
                tMF.AddCell(mf1);

                tMF.SetWidths(wtTable);
                tMF.WidthPercentage = 100;
                tMF.SpacingBefore = 0f;
                tMF.SpacingAfter = 10f;
                doc.Add(tMF);

                int logoTop = pvcover["logo.top"].Int(680);
                int logoLeft = pvcover["logo.left"].Int(40);

                //img.SetAbsolutePosition(logoLeft, logoTop);
                //doc.Add(img);
                int elmnt = 1;

                PdfPTable tableHeader = elements[elmnt++] as PdfPTable;

                PdfPCell[] hCells = tableHeader.Rows[0].GetCells();
                hCells[0].AddElement(img);

                tableHeader.SetWidths(new float[] { 1f, 7f });
                //tableHeader.SpacingBefore = 1f;
                //tableHeader.SpacingAfter = 1f;
                doc.Add(tableHeader);

                Paragraph par = elements[elmnt++] as Paragraph;
                par.SpacingBefore = 10f;
                par.SpacingAfter = 5f;

                doc.Add(par);

                PdfPTable tableDetail = elements[elmnt++] as PdfPTable;

                tableDetail.SetWidths(new float[] { 4f, 7f, 2f, 4f });

                for (int j = 0; j < tableDetail.Rows.Count; j++)
                {
                    PdfPCell[] cells = tableDetail.Rows[j].GetCells();

                    for (int k = 0; k < cells.Count(); k++)
                    {
                        PdfPCell cell = cells[k];
                        if (cell != null)
                        {
                            cell.NoWrap = true;

                            if (k == 1 && j == 1)
                            {
                                // cell.PaddingTop = -17f;
                            }
                            else if (j != 1 && k % 2 != 0)
                            {
                                cell.PaddingTop = -4f;
                                cell.PaddingBottom = 3f;
                            }
                            else if (j != 1 && k % 2 == 0)
                            {
                                cell.PaddingTop = -2f;
                                cell.PaddingBottom = 4f;
                            }
                        }
                    }
                }

                PdfPTable wraper = new PdfPTable(1);
                PdfPCell cellWrap = new PdfPCell(tableDetail);
                cellWrap.BorderWidth = 1f;
                wraper.AddCell(cellWrap);
                wraper.WidthPercentage = 100f;

                doc.Add(wraper);

                doc.Add(new Paragraph("\n"));

                PdfPTable tableAmount = elements[elmnt++] as PdfPTable;
                tableAmount.SetWidths(new float[] { 1f, 4f });
                tableAmount.WidthPercentage = 50f;

                for (int j = 0; j < tableAmount.Rows.Count; j++)
                {
                    PdfPCell[] cells = tableAmount.Rows[j].GetCells();

                    for (int k = 0; k < cells.Count(); k++)
                    {
                        PdfPCell cell = cells[k];
                        if (cell != null)
                        {
                            if (k % 2 != 0)
                            {
                                cell.PaddingTop = -7f;
                            }
                            else
                            {
                                cell.PaddingTop = -4f;
                            }
                        }
                    }
                }
                doc.Add(tableAmount);


                if (listInvoiceData != null && listInvoiceData.Count > 0)
                {
                    doc.Add(elements[elmnt++] as PdfPTable);
                    PdfPTable tableInvoice = elements[elmnt++] as PdfPTable;


                    float[] aic = new float[] { 4f, 2f, 3f, 4f, 3f, 3f };
                    PrintHelper.WidthsParse(pvcover, "Invoice.Columns", ref aic);

                    tableInvoice.SetWidths(aic);

                    for (int j = 0; j < tableInvoice.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableInvoice.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                if (j == 0)
                                    cell.PaddingTop = -3f;
                                else
                                    cell.PaddingTop = -2f;
                            }
                        }
                    }

                    doc.Add(tableInvoice);
                    elmnt++;
                }
                if (los.Any())
                {
                    PdfPTable tableLUS = elements[elmnt++] as PdfPTable;
                    doc.Add(tableLUS);
                }
                // Start Rinda Rahayu 20160330
                if (!ttd.isEmpty())
                {
                    PdfPTable tableTtd = elements[elmnt++] as PdfPTable;
                    tableTtd.TotalWidth = 540f;
                    tableTtd.SetWidths(new float[] { 8f, 98f });
                    tableTtd.CalculateHeights();

                    tableTtd.WriteSelectedRows(pvcover["ttd.rowStart"].Int(0), pvcover["ttd.rowEnd"].Int(-1),
                        (float)pvcover["ttd.xPos"].Num(4d), (float)pvcover["ttd.yPos"].Num(210d), PDFWriter.DirectContent);
                }
                // End Rinda Rahayu 20160330

                PdfPTable tableFooter = elements[elmnt++] as PdfPTable;
                tableFooter.TotalWidth = 300f;
                float[] tfcols = new float[] { 2f, 7f };
                PrintHelper.WidthsParse(pvcover, "tableFooter.Columns", ref tfcols);
                tableFooter.SetWidths(tfcols);
                tableFooter.WriteSelectedRows(pvcover["tableFooter.rowStart"].Int(0), pvcover["tableFooter.rowEnd"].Int(-1),
                    (float)pvcover["tableFooter.xPos"].Num(390), (float)pvcover["tableFooter.yPos"].Num(80), PDFWriter.DirectContent);

                doc.Close();


                string tpath = Common.AppSetting.PVCoverPath;

                if (!Directory.Exists(tpath))
                    Directory.CreateDirectory(tpath);

                File.WriteAllBytes(Path.Combine(tpath, fileName), ms.GetBuffer());

                // fid.Hadid 20190325. Put PV Cover into MFT, make it able to download by TOWASS
                try
                {
                    bool succesUpload = false;
                    var ftpLo = new FtpLogic();
                    ftpLo.ftpUploadWOS(ms.ToArray(), fileName, ref succesUpload);
                }
                catch(Exception ex)
                {
                    Handle(ex);
                }
            }
            catch (Exception e)
            {
                Handle(e);
                fileName = null;
            }

            return fileName;
        }

        public List<PVFormDetail> GetLastUpload(int pid)
        {
            string sql = string.Format(@"
                                        SELECT
                                           PROCESS_ID as [Pid],
                                           SEQ_NO as [SequenceNumber] ,
                                           COST_CENTER_CD as [CostCenterCode],
                                           CURRENCY_CD as [CurrencyCode],
                                           DESCRIPTION as [Description],
                                           AMOUNT as [Amount],
                                           WBS_NO as [WbsNumber],
                                           INVOICE_NO as [InvoiceNumber],
                                           INVOICE_DATE as [InvoiceDate],
                                           ITEM_TRANSACTION_CD 	as [ItemTransaction],
                                           TAX_NO as [TaxNumber],
                                           TAX_CD as [TaxCode],
                                           TAX_DT as [TaxDate], 
                                           GL_ACCOUNT as [GlAccount], 
                                           ITEM_NO as [ItemNo],
                                           DPP_AMOUNT as [DppAmount],
                                           TAX_ASSIGNMENT as [TaxAssignment],
                                           ACC_INF_ASSIGNMENT as [AccInfAssignment],
                                           --EFB
                                           WHT_TAX_CODE,
                                           WHT_TAX_TARIFF,
                                           WHT_TAX_ADDITIONAL_INFO,
                                           WHT_DPP_PPh_AMOUNT    
                                        FROM dbo.TB_T_PV_D
                                        WHERE PROCESS_ID = {0}", pid);

            return DB.Query<PVFormDetail>(sql).ToList();
        }

        public List<ErrorUploadData> GetErrorUploadData(int pid)
        {
            string sql = string.Format(@"
                                    SELECT 
	                                    D.SEQ_NO,
	                                    D.INVOICE_NO + '|' + D.ERRMSG ERRMSG
                                    FROM TB_T_UPLOAD_D D 
                                    WHERE D.PROCESS_ID = {0} 
                                    AND NULLIF(D.ERRMSG,'') IS NOT NULL", pid);

            return DB.Query<ErrorUploadData>(sql).ToList();
        }

        public void SavePVFP(PVFormData formData, UserData userData)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@SyncId", formData.SyncId);
            d.Add("@PVNo", formData.PVNumber ?? 0);
            d.Add("@UserId", userData.USERNAME);

            Exec("sp_MovePVFP", d);
        }
    }
}
