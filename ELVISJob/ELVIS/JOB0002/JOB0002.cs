﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.VoucherForm;
using BusinessLogic.SyncLogic;
using BusinessLogic.CommonLogic;
using ELVISJob.ELVIS.JOBCommon;
using Common.Data;
using System.IO;
using BusinessLogic._30PVList;

namespace ELVISJob.ELVIS.JOB0002
{
    class JOB0002 : BaseJob
    {
        public override void Run()
        {
            JOB0002BO bo = new JOB0002BO();
            bo.SetFuncId("JOB0002");

            List<PVSyncH> allUnsyncPV = null;

            try
            {
                allUnsyncPV = bo.GetUnsyncPVH(SYNC_CANCEL_PV);

                bool dataExists = allUnsyncPV
                                    .Exists(x => x.SyncAction == SYNC_CANCEL_PV
                                                && x.SyncStatus == 0);

                if (dataExists)
                {
                    ProcessId = bo.PutLog(MSG_INF, "Start Process", "Start");
                    bo.SetProcessId(ProcessId);

                    PVListLogic lilo = new PVListLogic();
                    List<PVFormData> tempList = null;
                    bool success;
                    string errMsg;
                    foreach (var up in allUnsyncPV
                                        .Where(x => x.SyncAction == SYNC_CANCEL_PV)
                                        .ToList())
                    {
                        if (up.SyncStatus == 1)
                            continue;

                        success = false;
                        errMsg = "";
                        bo.PutLog(MSG_INF, "Sync Cancel PV TOWASS : " + up.PVNo, "Sync");

                        // do validation
                        int canBeDeleted = bo.canBeDeleted(up, ref errMsg);
                        if (canBeDeleted == 1)
                        {
                            try
                            {
                                var pv = new PVFormData();
                                pv.PVNumber = up.PVNo;
                                pv.PVYear = up.PVYear;

                                tempList = new List<PVFormData>() { pv };

                                // do delete
                                success = lilo.Delete(tempList, up.CreatedBy);
                                lilo.RollbackDataPVEFB((pv.PVNumber ?? 0).ToString(), "delete");
                            }
                            catch (Exception e)
                            {
                                Handle(e);
                                success = false;
                                errMsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                            }
                        }
                        else if (canBeDeleted == -1)
                        {
                            success = true;
                        }

                        // fill property to be returned
                        if (success)
                        {
                            bo.PutLog(MSG_INF, "Sync Cancel PV Successfully", "Sync");

                            up.ResultSts = 1;
                            up.CancelDate = DateTime.Now;
                        }
                        else
                        {
                            bo.PutLog(MSG_ERR, "Sync Cancel PV Failed: " + errMsg, "Sync");

                            up.ResultSts = 0;
                            up.Message = errMsg;
                        }

                        allUnsyncPV.ForEach(x =>
                        {
                            if (x.SyncAction == SYNC_CANCEL_PV && x.PVNo.Equals(up.PVNo))
                            {
                                x.SyncStatus = 1;
                            }
                        });
                    }

                    bo.returnPVCancel(allUnsyncPV);
                    bo.CloseSyncFlag(allUnsyncPV);

                    bo.PutLog(MSG_INF, "Finish Process", "Finish");
                }
            }
            catch (Exception e)
            {
                string exMsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                bo.PutLog(MSG_ERR, "Sync Cancel PV Exception: " + exMsg, "Sync");
                Handle(e);
            }
        }
    }
}
