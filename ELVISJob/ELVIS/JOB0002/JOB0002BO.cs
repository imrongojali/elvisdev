﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELVISJob.ELVIS.JOBCommon;
using BusinessLogic.VoucherForm;
using Common.Data;
using DataLayer.Model;
using Dapper;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Data.Common;
using BusinessLogic;
using BusinessLogic.SyncLogic;
using System.IO;
using BusinessLogic.CommonLogic;

namespace ELVISJob.ELVIS.JOB0002
{
    class JOB0002BO : BaseBO
    {
        DateTime today = DateTime.Now;

        public int canBeDeleted(PVSyncH pv, ref string errMsg)
        {
            var q = (from d in _db.TB_R_PV_H
                    where d.PV_NO == pv.PVNo && d.PV_YEAR == pv.PVYear
                    select d).FirstOrDefault();

            if (q != null)
            {
                if (q.DELETED == 1)
                {
                    errMsg = string.Format("PV {0} already deleted", pv.PVNo);
                    return -1;
                }
                if (!(q.STATUS_CD == 0 
                    || q.STATUS_CD == 31 
                    || q.CANCEL_FLAG == 1))
                {
                    errMsg = string.Format("Status PV {0} should be Draft, or Rejected by SH, or PV already cancelled", pv.PVNo);
                    return 0;
                }
            }
            else
            {
                errMsg = string.Format("PV {0} is not found", pv.PVNo);
                return 0;
            }

            return 1;
        }

        public void returnPVCancel(List<PVSyncH> allUnsyncPVH)
        {
            if (allUnsyncPVH == null)
                return;

            var ret = allUnsyncPVH
                        .Where(x => x.ResultSts != null)
                        .ToList();

            if (ret.Count == 0)
                return;

            try
            {
                foreach (var up in ret)
                {
                    //switch to SSIS method
                    //DynamicParameters d = new DynamicParameters();
                    //d.Add("@pv_no_elvis", up.PVNo);
                    //d.Add("@cancel_date", up.CancelDate);
                    //d.Add("@return_sts", up.ResultSts);
                    //d.Add("@message", up.Message);

                    //Exec("sp_wossync_receive_cancel", d);

                    DynamicParameters d = new DynamicParameters();
                    d.Add("@sync_id", up.SyncId);
                    d.Add("@sync_action", up.SyncAction);
                    d.Add("@pv_no_towass", null);
                    d.Add("@pv_no_elvis", up.PVNo);
                    d.Add("@pv_year", up.PVYear);
                    d.Add("@pv_amount", null);
                    d.Add("@vendor_cd", null);
                    d.Add("@vendor_nm", null);
                    d.Add("@pv_cover_file", null);
                    d.Add("@submit_date", null);
                    d.Add("@cancel_date", up.CancelDate);
                    d.Add("@result_sts", up.ResultSts);
                    d.Add("@message", up.Message);

                    Exec("sp_wos_send_result", d);
                }
            }
            catch (Exception e)
            {
                Handle(e);
            }
        }
    }
}
