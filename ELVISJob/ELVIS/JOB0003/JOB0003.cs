﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.VoucherForm;
using BusinessLogic.SyncLogic;
using BusinessLogic.CommonLogic;
using ELVISJob.ELVIS.JOBCommon;
using Common.Data;
using System.IO;
using BusinessLogic._30PVList;
using ELVISJob.ELVIS.JOB0001;

namespace ELVISJob.ELVIS.JOB0003
{
    class JOB0003 : BaseJob
    {
        public override void Run()
        {
            JobFunctions jo = new JobFunctions();
            JOB0003BO bo = new JOB0003BO();
            bo.SetFuncId("JOB0003");

            List<PVSyncH> allUnsyncPV = null;

            try
            {
                allUnsyncPV = bo.GetUnsyncPVH(SYNC_SUBMIT_PV);

                bool dataExists = allUnsyncPV
                                    .Exists(x => x.SyncAction == SYNC_SUBMIT_PV
                                                && x.SyncStatus == 0);

                if (dataExists)
                {
                    ProcessId = bo.PutLog(MSG_INF, "Start Process", "Start");
                    bo.SetProcessId(ProcessId);

                    bool success;
                    List<string> errMsgs = new List<string>();
                    foreach (var up in allUnsyncPV
                                        .Where(x => x.SyncAction == SYNC_SUBMIT_PV)
                                        .ToList())
                    {
                        if (up.SyncStatus == 1)
                            continue;

                        bo.PutLog(MSG_INF, "Sync Submit PV TOWASS : " + up.PVNo, "Sync");

                        success = false;
                        errMsgs = new List<string>();
                        DateTime? lastSubmitDt = null;

                        PVFormData FormData = bo.searchPV(up.PVNo ?? 0, up.PVYear, up.CreatedBy);
                        if (FormData == null)
                        {
                            success = false;
                            errMsgs.Add(string.Format("PV {0}{1} not found", up.PVNo, up.PVYear));
                        }
                        else if (!FormData.BookingNo.Equals("MATCHED")) // property boleh numpang
                        {
                            success = false;
                            errMsgs.Add(string.Format("PV {0} need to be process Compare in ELVIS first!", up.PVNo));
                        }
                        else if (FormData.SubmitDate != null)
                        {
                            lastSubmitDt = FormData.SubmitDate;
                            success = true;
                        }
                        else if (bo.performNonSubmissionValidation(FormData, errMsgs) && bo.validateGeneralData(FormData, errMsgs))
                        {
                            UserData u = jo.GetUserData(up.CreatedBy);
                            bo.saveSubmit(FormData, u, errMsgs);
                            success = bo.postToK2(FormData, u, errMsgs);
                        }

                        // fill property to be returned
                        if (success)
                        {
                            bo.PutLog(MSG_INF, "Sync Submit PV Successfully", "Sync");

                            up.ResultSts = 1;
                            up.SubmitDate = lastSubmitDt.HasValue ? lastSubmitDt.Value : DateTime.Now;
                        }
                        else
                        {
                            string errMsg = string.Join("|", errMsgs);
                            bo.PutLog(MSG_ERR, "Sync Submit PV Failed: " + errMsg, "Sync");

                            up.ResultSts = 0;
                            up.Message = errMsg;
                        }

                        allUnsyncPV.ForEach(x =>
                        {
                            if (x.SyncAction == SYNC_SUBMIT_PV && x.PVNo.Equals(up.PVNo))
                            {
                                x.SyncStatus = 1;
                            }
                        });
                    }

                    bo.returnPVSubmit(allUnsyncPV);
                    bo.CloseSyncFlag(allUnsyncPV);

                    bo.PutLog(MSG_INF, "Finish Process", "Finish");
                }
            }
            catch (Exception e)
            {
                string exMsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                bo.PutLog(MSG_ERR, "Sync Submit PV Exception: " + exMsg, "Sync");
                Handle(e);
            }
        }
    }
}
