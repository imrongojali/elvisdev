﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace ELVISJob.ELVIS.JOBCommon
{
    public class BaseClass
    {
        public const int SYNC_PV_DRAFT = 1;
        public const int SYNC_CANCEL_PV = 2;
        public const int SYNC_SUBMIT_PV = 3;

        protected const string MSG_INF = "INF";
        protected const string MSG_WRN = "WRN";
        protected const string MSG_ERR = "ERR";
        protected int ProcessId { get; set; }
        protected string FuncId { get; set; }
        protected string UID { get { return "ELVIS.JOB"; } }

        protected void Handle(Exception x)
        {
            LoggingLogic.err(x);
        }

        protected void Say(string name, string msg, params object[] x)
        {
            LoggingLogic.say(name, msg, x);
        }
    }
}
