﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Common.Data;
using Dapper;
using DataLayer.Model;
using Common.Data._30PVList;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;

namespace ELVISJob.ELVIS.JOBCommon
{
    public class JobFunctions : BaseClass
    {
        private IDbConnection _DB = null;
        protected IDbConnection DB
        {
            get
            {
                if (_DB == null)
                {
                    _DB = new SqlConnection(ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString);
                }
                return _DB;
            }
        }

        public string GetSysText(string stype, string scode = "")
        {
            string r = "";
            try
            {
                string sql = string.Format("SELECT TOP 1 SYSTEM_VALUE_TXT "
                                         + " FROM dbo.TB_M_SYSTEM "
                                         + " WHERE SYSTEM_TYPE='{0}' "
                                         + " AND (NULLIF('{1}','') IS NULL OR SYSTEM_CD='{1}')", stype, scode);
                var q = DB.Query<string>(sql).ToList();
                if (q != null && q.Count > 0)
                {
                    r = q[0];
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
                r = null;
            }
            return r;
        }

        public UserData GetUserData(string username)
        {
            string sql = string.Format(@"SELECT 
	                                        REG_NO = x.REG_NO,
	                                        USERNAME = x.USERNAME,
	                                        TITLE = x.TITLE,
	                                        FIRST_NAME = x.FIRST_NAME,
	                                        LAST_NAME = x.LAST_NAME,
	                                        EMAIL = x.EMAIL,
	                                        DIV_CD = CASE WHEN ISNUMERIC( x.DIVISION_ID) = 1 THEN CONVERT(INT, X.DIVISION_ID) ELSE NULL END,
	                                        DIV_NAME = x.DIVISION_NAME,
	                                        DEPARTMENT_ID = x.DEPARTMENT_ID,
	                                        DEPARTMENT_NAME = x.DEPARTMENT_NAME,
	                                        GROUP_CD = x.GROUP_CD
                                        FROM VW_USER_DATA X 
                                        WHERE UPPER(X.USERNAME) = UPPER('{0}')", username);

            return DB.Query<UserData>(sql).FirstOrDefault();
        }

        public TransactionType GetTransType(int transCd)
        {
            string sql = string.Format(@"SELECT 
		                                        Code = CONVERT(VARCHAR, x.TRANSACTION_CD), 
		                                        Name = x.TRANSACTION_NAME,
		                                        StandardWording = x.STD_WORDING,
		                                        AttachmentFlag = ISNULL(x.ATTACHMENT_FLAG, 0),
		                                        BudgetFlag = ISNULL(x.BUDGET_FLAG,0), 
		                                        CostCenterFlag = ISNULL(COST_CENTER_FLAG, 0), 
		                                        ProductionFlag = ISNULL(x.PRODUCTION_FLAG, 0),
		                                        TemplateCd = ISNULL(x.TEMPLATE_CD, 0), 
		                                        TemplateName = m.TEMPLATE_NAME 
                                        FROM  	TB_M_TRANSACTION_TYPE x
                                        LEFT JOIN  
	                                        (SELECT 
		                                        CASE WHEN ISNUMERIC(SYSTEM_CD)=1 
			                                         THEN CONVERT(INT, SYSTEM_CD) 
			                                         ELSE NULL 
			                                         END AS TEMPLATE_CD
		                                        , SYSTEM_VALUE_TXT AS TEMPLATE_NAME 
		                                        FROM TB_M_SYSTEM 
	                                         WHERE SYSTEM_TYPE = 'TEMPLATE_CD'	
	                                        ) m 
	                                        ON (m.TEMPLATE_CD = ISNULL(x.TEMPLATE_CD,0)) 

                                        WHERE x.TRANSACTION_CD = ISNULL({0},0) OR ISNULL({0},0) < 1", transCd);

            var q = DB.Query<TransactionType>(sql).ToList();
            if (q != null && q.Count > 0)
            {
                return q[0];
            }

            return null;
        }

        public int? GLAccount(string CostCenterCd, int transactionCd, int? itemTransactionCd = null)
        {
            string sql = string.Format("SELECT dbo.fn_GlAccount('{0}', {1}, {2})"
                    , CostCenterCd
                    , transactionCd
                    , (itemTransactionCd == null) ? "NULL" : itemTransactionCd.ToString());
            return DB.Query<int?>(sql).FirstOrDefault();
        }

        public List<ExchangeRate> getExchangeRates()
        {
            string sql = string.Format(@"select LTRIM(RTRIM(v.CURRENCY_CD)) [CurrencyCode],
	                                        v.EXCHANGE_RATE [Rate], 
	                                        v.VALID_FROM [ValidFrom] 
                                        from vw_ExchangeRate v");
            return DB.Query<ExchangeRate>(sql).ToList();
        }

        public vw_Vendor getVendor(string vendorCd)
        {
            string sql = string.Format(@"select * from vw_Vendor where VENDOR_CD = '{0}'", vendorCd);
            return DB.Query<vw_Vendor>(sql).FirstOrDefault();
        }

        public string getAttachCatName(string code)
        {
            string sql = string.Format(@"SELECT TOP 1  ATTACH_NAME
                                        FROM TB_M_ATTACH_CATEGORY
                                        WHERE ATTACH_CD= '{0}'", code);

            return DB.Query<string>(sql).FirstOrDefault();
        }

        public int[] getEntertainmentTransactionCodes()
        {
            string sql = string.Format(@"SELECT DISTINCT CASE
                                                 WHEN ISNUMERIC (SYSTEM_VALUE_TXT) = 1 THEN CONVERT (INT,SYSTEM_VALUE_TXT)
                                                 ELSE NULL
                                               END
                                        FROM dbo.TB_M_SYSTEM
                                        WHERE SYSTEM_TYPE = 'K2_POSTING'
                                        AND   SYSTEM_CD = 'ENTERTAINMENT_TRANSACTION'
                                        AND   ISNUMERIC (SYSTEM_VALUE_TXT) = 1 ");

            List<int?> et = DB.Query<int?>(sql).ToList();

            var q = et.Where(c => c != null).Distinct();
            if (q != null)
            {
                q = q.OrderBy(a => a ?? 0);
                return q.Select(b => b ?? 0).ToArray();
            }
            else
            {
                return new int[0];
            }
        }

        public PVListData getPVListData(string no, string year)
        {
            string sql = string.Format(@"
                                SELECT TOP 1 
                                  PV_NO
                                , PV_YEAR
                                , PV_DATE
                                , CONVERT(VARCHAR(10), PV_TYPE_CD) PV_TYPE_CD
                                , DIVISION_NAME
                                , DIVISION_ID
                                , VENDOR_CD
                                , VENDOR_NAME
                                , CONVERT(VARCHAR(10), STATUS_CD) [STATUS_CD] 
                                , CONVERT(VARCHAR(10), TRANSACTION_CD) [TRANSACTION_CD]
                                , TRANSACTION_NAME
                                , PAY_METHOD_CD
                                , PAY_METHOD_NAME
                                , PLANNING_PAYMENT_DATE
                                , PRINT_TICKET_BY
                                , PRINT_TICKET_FLAG
                                , BUDGET_NO
                                , ISNULL((SELECT TOP 1 [Description] FROM vw_WBS WHERE WbsNumber = v.BUDGET_NO),'') [BUDGET_DESCRIPTION]
                                , NEXT_APPROVER
                                , (ISNULL((select top 1 [description] from tb_r_pv_d d where d.pv_no = v.pv_no and d.pv_year = v.PV_YEAR order by seq_no), '')) [DESCRIPTION]
                                , ISNULL(DELETED,0) [DELETED]
                                FROM vw_PV_List v
                                WHERE PV_NO = '{0}' AND PV_YEAR = '{1}'", no, year);

            return DB.Query<PVListData>(sql).FirstOrDefault();
        }

        public List<AmountData> getAmountsData(string no, string year)
        {
            string sql = string.Format(@"
                                SELECT 
	                                a.DOC_NO,
	                                a.DOC_YEAR,
	                                a.CURRENCY_CD,
	                                a.EXCHANGE_RATE,
	                                a.TOTAL_AMOUNT,
	                                a.CREATED_DATE,
	                                a.CREATED_BY,
	                                a.CHANGED_DATE,
	                                a.CHANGED_BY
                                FROM TB_R_PVRV_AMOUNT a
                                WHERE DOC_NO = {0} AND DOC_YEAR = {1}", no, year);

            return DB.Query<AmountData>(sql).ToList();
        }

        public List<PVCoverData> getInvoiceValue(string no, string year)
        {
            string sql = string.Format(@"
                                SELECT
                                    CURRENCY_CD [CURRENCY],
                                    AMOUNT,
                                    INVOICE_DATE,
                                    INVOICE_NO,
                                    TAX_AMOUNT,
                                    TAX_NO
                                FROM vw_PVCoverInvoiceList
                                WHERE PV_NO = '{0}' and PV_YEAR = '{1}'", no, year);

            return DB.Query<PVCoverData>(sql).ToList();
        }

        public List<CodeConstant> GetPVTypes(int exclude)
        {
            string sql = string.Format(@"
                                SELECT CONVERT(VARCHAR(1), PV_TYPE_CD) AS [Code]
                                    , PV_TYPE_NAME AS [Description] 
                                FROM dbo.TB_M_PV_TYPE 
                                WHERE PV_TYPE_CD <> {0}", exclude);

            return DB.Query<CodeConstant>(sql).ToList();
        }

        public List<OutstandingSuspense> GetUnsettledByDivision(string DivisionId)
        {
            string sql = string.Format(@"
                                 SELECT x.DIVISION_ID, x.DOC_NO, x.DOC_YEAR, x.VENDOR_CD, x.VENDOR_NAME, x.DAYS_OUTSTANDING, x.TOTAL_AMOUNT, x.STATUS_CD, x.PV_DATE	
                                 FROM 
                                 (
                                SELECT 
                                  DIV.DIVISION_ID, h.PV_NO DOC_NO, h.PV_YEAR DOC_YEAR 
                                , MAX(h.VENDOR_CD) [VENDOR_CD], MAX(v.VENDOR_NAME)[VENDOR_NAME]
                                , DATEDIFF(day, h.ACTIVITY_DATE_TO, GETDATE()) [DAYS_OUTSTANDING]
                                , SUM(h.TOTAL_AMOUNT) [TOTAL_AMOUNT]
                                , h.status_cd, h.PV_DATE
                                FROM (select NULLIF('{0}',0) DIV_CD) D
                                JOIN TB_R_PV_H h ON (d.DIV_CD IS NULL OR h.DIVISION_ID = d.DIV_CD)
                                JOIN TMMIN_ROLE.dbo.TB_M_DIVISION div ON div.DIVISION_ID = H.DIVISION_ID AND (D.DIV_CD IS NULL OR DIV.DIVISION_ID= D.DIV_CD)
                                LEFT JOIN (select 99 STATUS_CD) exs on exs.STATUS_CD = h.STATUS_CD
                                LEFT JOIN SAP_DB.dbo.TB_M_VENDOR v  ON v.VENDOR_CD = h.VENDOR_CD
                                LEFT JOIN TB_R_PV_H hs on hs.SUSPENSE_NO = h.PV_NO  AND HS.SUSPENSE_YEAR = h.PV_YEAR
                                LEFT JOIN TB_R_RV_H rs on rs.SUSPENSE_NO = h.PV_NO and rs.SUSPENSE_YEAR = h.PV_YEAR
                                WHERE ISNULL(h.CANCEL_FLAG, 0) = 0       -- not canceled
                                  AND ISNULL(h.DELETED, 0) = 0           -- not deleted
                                  AND h.PV_TYPE_CD = 2
                                  AND NOT (
	                                     (hs.SUBMIT_HC_DOC_DATE IS NOT NULL AND hs.COUNTER_FLAG = 1
	                                      AND ISNULL(hs.CANCEL_FLAG,0) = 0 AND ISNULL(hs.DELETED, 0) = 0)
	                                OR   (rs.SUBMIT_HC_DOC_DATE IS NOT NULL AND rs.COUNTER_FLAG = 1
	                                      AND ISNULL(rs.CANCEL_FLAG,0) = 0 AND ISNULL(rs.DELETED, 0) = 0)
                                  ) 
                                  AND (dbo.fn_GetSettlementStatus(dbo.fn_DocNoYearToReff(h.PV_NO, h.PV_YEAR)) < 2) 
                                  AND exs.STATUS_CD IS NULL 
                                  AND (CONVERT(DATE, GETDATE()) >= h.ACTIVITY_DATE_TO)   
                                  AND (SELECT TOP 1 ACTUAL_DT
                                    FROM TB_R_DISTRIBUTION_STATUS
                                    WHERE REFF_NO = dbo.fn_DocNoYearToReff(h.PV_NO, h.PV_YEAR)
                                    ORDER BY STATUS_CD DESC) IS NOT NULL  -- final approved	
                                GROUP BY div.DIVISION_ID, h.PV_NO, h.PV_YEAR, h.ACTIVITY_DATE_TO, h.STATUS_CD, h.PV_DATE
                                ) [x]
                                ", DivisionId);

            return DB.Query<OutstandingSuspense>(sql).ToList();
        }
        public List<PVFormDetail> GetLastUpload(string pid)
        {
            string sql = string.Format(@"
                                    SELECT
                                       PROCESS_ID as [Pid],
                                       SEQ_NO as [SequenceNumber] ,
                                       COST_CENTER_CD as [CostCenterCode],
                                       CURRENCY_CD as [CurrencyCode],
                                       DESCRIPTION as [Description],
                                       AMOUNT as [Amount],
                                       WBS_NO as [WbsNumber],
                                       INVOICE_NO as [InvoiceNumber],
                                       INVOICE_DATE as [InvoiceDate],
                                       ITEM_TRANSACTION_CD 	as [ItemTransaction],
                                       TAX_NO as [TaxNumber],
                                       TAX_CD as [TaxCode],
                                       TAX_DT as [TaxDate], 
                                       GL_ACCOUNT as [GlAccount], 
                                       ITEM_NO as [ItemNo],
                                       DPP_AMOUNT as [DppAmount],
                                       TAX_ASSIGNMENT as [TaxAssignment],
                                       ACC_INF_ASSIGNMENT as [AccInfAssignment],
                                       --EFB
                                       WHT_TAX_CODE,
                                       WHT_TAX_TARIFF,
                                       WHT_TAX_ADDITIONAL_INFO,
                                       WHT_DPP_PPh_AMOUNT    
                                    FROM dbo.TB_T_PV_D
                                    WHERE PROCESS_ID = '{0}'
                                        ", pid);

            return DB.Query<PVFormDetail>(sql).ToList();
        }
    }
}
