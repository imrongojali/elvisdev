﻿using BusinessLogic;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELVISJob.ELVIS.JOBCommon
{
    public abstract class BaseJob : BaseClass
    {
        public abstract void Run();
    }
}
