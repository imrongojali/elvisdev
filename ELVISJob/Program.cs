﻿using ELVISJob.ELVIS.JOBCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ELVISJob
{
    class Program
    {
        static void Main(string[] args)
        {
            String functionId = args[0];

            Console.WriteLine("parameter pass: " + functionId);

            Assembly assembly = typeof(Program).Assembly; // in the same assembly!
            Type type = assembly.GetType("ELVISJob.ELVIS." + functionId + "." + functionId);
            BaseJob job = (BaseJob)Activator.CreateInstance(type);
            job.Run();
        }
    }
}
